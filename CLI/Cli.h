#pragma once

#include "../Utilities/MainIncludes.h"
#include "../Lexer/lexer.h"
#include "../Parser/Parser.h"
#include "../SemanticAnalyzer/SemanticAnalyzer.h"
#include "../CodeGen/CodeGenerator.h"
#include "llvm/Support/Path.h"
#include "CliInfo.h"

#include <array>
#include <unordered_map>

using std::unordered_map;
using std::vector;
using std::pair;

class CLI
{
private:
    unordered_map<string, IDataAccess*> _data_accesses;
    unordered_map<IDataAccess*, vector<Token*>> _lexer_output;
    unordered_map<IDataAccess*, vector<AbstractSyntaxTree::Node*>> _parser_output;
    std::vector<pair<string, Module*>> _semantic_analyzer_output;
    CGOutput _codegen_output;

    CliInfo _cli_info;

    static void printHelpMessage();
    
    void lex();
    void parse();
    void semanticAnalysis();
    void codeGeneration();

    pair<IDataAccess*, vector<Token*>> lexFile(const string& file_path, bool absolute = false);
    string getAbsolutePath(const string& path, const string& relative_to = "");
    bool parseRequestedImport(AbstractSyntaxTree::ImportStatement* import_statement, const string& source, vector<pair<string, AbstractSyntaxTree::ImportStatement*>>& inesrtion_point);

    static constexpr std::array<void (CLI::*)(), 4> _compilation_functions = {
                            &CLI::lex, 
                            &CLI::parse, 
                            &CLI::semanticAnalysis,
                            &CLI::codeGeneration
                        };

    template <class T>
    static void delete_resource(unordered_map<IDataAccess*, vector<T*>>& to_delete)
    {
        for (auto& obj : to_delete)
        {
            for(auto& inner_data : obj.second)
            {
                if (inner_data != nullptr) 
                {
                    delete inner_data;
                    inner_data = nullptr;
                }
            }
        }
        
        to_delete.clear();
    }

public:
    CLI(const CliInfo& cli_info = CliInfo());
    ~CLI();

    void compile();
};