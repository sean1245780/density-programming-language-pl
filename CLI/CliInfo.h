#pragma once

#include "../Parser/Parser.h"
#include <vector>
#include <string>
#include <unordered_set>

using std::vector;
using std::string;
using std::unordered_set;
using std::tuple;

static constexpr const char* DEFAULT_EXEC_NAME = "a";

enum class CompilationStep { lex, parse, semantic_analysis, code_generation };

class CliInfo
{
public:
    // Variables
    vector<string> files_to_compile;
    string name = "";
    string executable_name = DEFAULT_EXEC_NAME + string(EXECUTABLE_FILE_EXT);
    CompilationStep compilation_step = CompilationStep::code_generation;
    CompliationOptm optimization_flags = CompliationOptm::O0;
    bool asking_help = false;
    bool asking_version = false;
    bool raw_data = false;
    bool emit_ir = false;
    bool emit_obj = false;
    bool emit_bc = false;
    bool jit = false;
    bool interpreted = false;
    bool is_verbose = false;

    // CTORS & DTORS
    CliInfo(int argc, const char* argv[]);
    CliInfo() : compilation_step(CompilationStep::code_generation), asking_help(false),
        asking_version(false), raw_data(false), emit_ir(false), emit_obj(false), emit_bc(false) { }
    
    unsigned int getGeneralOptimazationFlag() const noexcept;
    unsigned int getSizeOptimazationFlag() const noexcept;

    // Static Methods
    static int getArgumentIndex(char* arg) noexcept;
    static int getArgumentIndex(const string& arg) noexcept;
    static bool validate(const string& value, bool (*func)(const char&));
};

static inline bool name_check(const char& ch)
{
    return ((ch < '0' || ch > '9') && (ch < 'a' || ch > 'z') && (ch < 'A' || ch > 'Z')
                && ch != '_' && ch != '-' && ch != '.');
}

static inline bool path_check(const char& ch)
{
    return ((ch < '0' || ch > '9' || ch < 'a' || ch > 'z' || ch < 'A' || ch > 'Z')
                && ch != '_' && ch != ' ' && ch != '-' && ch != '/');
}

static bool endsWithExtension(const string& str)
{
    string s1 = extension_1, s2 = extension_2, s3 = extension_h_1, s4 = extension_h_2;
    size_t st1 = s1.size() - 1, st2 = s2.size() - 1, st3 = s3.size() - 1,
            st4 = s4.size() - 1;
	bool b1 = true, b2 = true, b3 = true, b4 = true;

	if(str == s1 || str == s2 || str == s3 || str == s4) return true;
	for(auto it = (--str.end()); it != str.begin(); it--)
    {
		if(!b1 && !b2 && !b3 && !b4) return false;
		if(s1[st1] == *it && b1) st1--; else b1 = false;
		if(s2[st2] == *it && b2) st2--; else b2 = false;
		if(s3[st3] == *it && b3) st3--; else b3 = false;
		if(s4[st4] == *it && b4) st4--; else b4 = false;
		if(!st1 || !st2 || !st3 || !st4) return true;
    }

	return false;
}

static constexpr unsigned int CLI_ARGS_LENGTH = 18;

/*
    -o              -> Set executable name - Has default.
    -lexOnly        -> Only lex the code - No default.
    -parseOnly        -> Only parse the code - No default.
    --help          -> Prints helping data - Information.
    --version       -> Get the compilers version - Information.
*/
static constexpr tuple<pair<const char*, const char *>, const char*, bool (*)(CliInfo&, const char*)> arguments[CLI_ARGS_LENGTH] = 
{
    {{"-o", "--output_file"},              "-o/--output_file <output-filename>\tDefines the name of the output file.",
            [](CliInfo& cli, const char* next) -> bool {
                if(next == nullptr) throw std::invalid_argument("CLI Error: After '-o/--output_file' there needs to be a name!");
                cli.executable_name = next;
                return true;
            }},
    {{"-r", "--raw"},            "Defines terminal as an additional input stream (files are still compiled).",
            [](CliInfo& cli, const char* next) -> bool {cli.raw_data = true; return false;}},
    {{"-l", "--lex_only"},        "Defines compilation end point to be the lexer's tokens.",
            [](CliInfo& cli, const char* next) -> bool {cli.compilation_step = CompilationStep::lex; return false;}},
    {{"-p", "--parse_only"},      "Defines compilation end point to be the parse tree.",
            [](CliInfo& cli, const char* next) -> bool {cli.compilation_step = CompilationStep::parse; return false;}},
    {{"-d", "--decorate_only"},   "Defines compilation end point to be the semantic analysis tree.", 
            [](CliInfo& cli, const char* next) -> bool {cli.compilation_step = CompilationStep::semantic_analysis; return false;}},
    {{"-O0", "--optimization-0"},        "Instruct the compiler to compile without any optimizations.", 
            [](CliInfo& cli, const char* next) -> bool {cli.optimization_flags = CompliationOptm::O0; return false;}},
    {{"-O1", "--optimization-1"},        "Instruct the compiler to compile with O1 optimizations.", 
            [](CliInfo& cli, const char* next) -> bool {cli.optimization_flags = CompliationOptm::O1; return false;}},
    {{"-O2", "--optimization-2"},        "Instruct the compiler to compile with O2 optimizations.", 
            [](CliInfo& cli, const char* next) -> bool {cli.optimization_flags = CompliationOptm::O2; return false;}},
    {{"-O3", "--optimization-3"},        "Instruct the compiler to compile with O3 optimizations.", 
            [](CliInfo& cli, const char* next) -> bool {cli.optimization_flags = CompliationOptm::O3; return false;}},
    {{"-Os", "--optimization-s"},        "Instruct the compiler to compile with O2 optimizations with extra code reduction optimizations.",
            [](CliInfo& cli, const char* next) -> bool {cli.optimization_flags = CompliationOptm::Os; return false;}},
    {{"-Oz", "--optimization-z"},        "Instruct the compiler to compile with Os optimizations with further code reduction optimizations.",
            [](CliInfo& cli, const char* next) -> bool {cli.optimization_flags = CompliationOptm::Oz; return false;}},
    {{"-ir", "--emit_ir"},        "Instruct the compiler to emit an intermidiate representation file (a .ll file).", 
            [](CliInfo& cli, const char* next) -> bool {cli.emit_ir = true; return false;}},
    {{"-obj", "--emit_obj"},        "Instruct the compiler to emit object files (a .o file).", 
            [](CliInfo& cli, const char* next) -> bool {cli.emit_obj = true; return false;}},
    {{"-bc", "--emit_bitcode"},   "Instruct the compiler to emit a bitcode file (a .bc file).", 
            [](CliInfo& cli, const char* next) -> bool {cli.emit_bc = true; return false;}},
    {{"-it", "--interprete"},        "Instruct the compiler to run the code through an interpreter.", 
            [](CliInfo& cli, const char* next) -> bool {cli.interpreted = true; return false;}},
    {{"-vr", "--verbose"},   		"Do not display warnings if exist.", 
            [](CliInfo& cli, const char* next) -> bool {cli.is_verbose = true; return false;}},
    {{"-h", "--help"},            "Displays the descriptions of every argument flag.",
            [](CliInfo& cli, const char* next) -> bool {cli.asking_help = true; return false;}},
    {{"-v", "--version"},         "Display the version of the compiler.",
            [](CliInfo& cli, const char* next) -> bool {cli.asking_version = true; return false;}}
};