#include "CliInfo.h"

bool CliInfo::validate(const string& value, bool (*func)(const char&))
{
    for(const char& ch : value)
    {
        if(func(ch)) return false;
    }

    return true;
}

CliInfo::CliInfo(int argc, const char* argv[]) :
        compilation_step(CompilationStep::code_generation), asking_help(false),
        asking_version(false), raw_data(false)
{
    if(argc <= 1) throw std::invalid_argument("CLI Error: Invalid number of parameters! Use `--help` flag for usage.");
    if(argv == nullptr) throw std::invalid_argument("CLI Error: Invalid given values! -> argv");
    this->name = argv[0];

    int i = 0;
    int index = 0;
    for(i = 1; i < argc; i++)
    { 
        if(argv[i] == nullptr) throw std::invalid_argument("CLI Error: Invalid parameter in argv!");

        if((index = getArgumentIndex(argv[i])) != -1)
        {
            if(std::get<2>(arguments[index])(*this, argv[i + 1])) (++i); // If the next value is part of the argument, then skip.
        }
        else if(endsWithExtension(argv[i]))
        {
            this->files_to_compile.push_back(argv[i]);
        }
        else
        {
            string tmp = "CLI Error: The '";
            tmp += argv[i];
            tmp += "' parameter is wrong! Use '--help' for more info.";
            throw std::invalid_argument(tmp);
        }
    }
}

unsigned int CliInfo::getGeneralOptimazationFlag() const noexcept
{
    switch (this->optimization_flags)
    {
    case CompliationOptm::O0: return 0;
    case CompliationOptm::O1: return 1;
    case CompliationOptm::Os:
    case CompliationOptm::Oz:
    case CompliationOptm::O2: return 2;
    case CompliationOptm::O3: return 3;
    default: return 0;
    }
}

unsigned int CliInfo::getSizeOptimazationFlag() const noexcept
{
    switch (this->optimization_flags)
    {
    case CompliationOptm::Os: return 1;
    case CompliationOptm::Oz: return 2;
    default: return 0;
    }
}


int CliInfo::getArgumentIndex(char* arg) noexcept
{
    if (arg == nullptr) return -1;
    for(unsigned int i = 0; i < CLI_ARGS_LENGTH; i++)
    {
        const auto& name = std::get<0>(arguments[i]);
        if(!strcmp(arg, name.first) || !strcmp(arg, name.second)) return (int)i;
    }

    return -1;
}

int CliInfo::getArgumentIndex(const string& arg) noexcept
{
    for(unsigned int i = 0; i < CLI_ARGS_LENGTH; i++)
    {
        const auto& name = std::get<0>(arguments[i]);
        if(arg == name.first || arg == name.second) return (int)i;
    }

    return -1;
}