#include "Cli.h"

CLI::CLI(const CliInfo& cli_info): _cli_info(cli_info) {}

void CLI::lex()
{
    // initlialize raw data accesses
    if (this->_cli_info.raw_data)
    {
        std::string user_input = "", all_user_input = "";

		std::cout << ">> ";

        while(std::getline(std::cin, user_input) && !user_input.empty())
        {
			std::cout << ">> ";
            all_user_input += user_input;
            all_user_input += "\n";
        }

        if (!user_input.empty()) all_user_input.pop_back();
        
        // create the data access and lex the data
        IDataAccess* str_access = new StringAccess(all_user_input);

        this->_data_accesses.insert({ RAW_DATA_MODULE_NAME, str_access });
        this->_lexer_output.insert({ str_access, Lexer::lex(*str_access) });
    }

    // lex the files given as input
    for (const auto& file : this->_cli_info.files_to_compile)
    {
        this->_lexer_output.insert(lexFile(file));
    }

    // print output if flag is turned on and if there are no errors
    if (!ErrorRegister::hasErrors() && this->_cli_info.compilation_step == CompilationStep::lex)
    {
        for (const auto& curr_out : this->_lexer_output)
        {
            std::cout << "Lexer output for: " << curr_out.first->getFilePath() << cendl;

            for (auto* token : curr_out.second)
            {
                if (token != nullptr) token->print();
            }

            std::cout << cendl;
        }
    }
}

string CLI::getAbsolutePath(const string& path, const string& relative_to)
{
    llvm::SmallString<0> modified_path;

    std::error_code error_code;

    // use the current path as the reference point
    if (relative_to == "" || relative_to == RAW_DATA_MODULE_NAME)
    {
        error_code = llvm::sys::fs::real_path(path, modified_path);
        if (error_code) throw std::runtime_error(error_code.message());
    }
    else
    {
        // save the last path
        llvm::SmallString<0> last_path;
        llvm::SmallString<0> curr_path;
        error_code = llvm::sys::fs::current_path(last_path);
        if (error_code) throw std::runtime_error(error_code.message());

        curr_path += relative_to;

        // if the current path is not a directory, get the parent directory
        if (!llvm::sys::fs::is_directory(curr_path))
            curr_path = llvm::sys::path::parent_path(curr_path);
        
        // temporarily set the current path to be the input path
        error_code = llvm::sys::fs::set_current_path(curr_path);
        if (error_code) throw std::runtime_error(error_code.message());

        // get the real path of the path given
        error_code = llvm::sys::fs::real_path(path, modified_path);
        if (error_code) throw std::runtime_error(error_code.message());

        // set the current path back to its original value
        error_code = llvm::sys::fs::set_current_path(last_path);
        if (error_code) throw std::runtime_error(error_code.message());
    } 

    llvm::sys::fs::make_absolute(modified_path, modified_path);

    return modified_path.str().str();
}

pair<IDataAccess*, vector<Token*>> CLI::lexFile(const string& file_path, bool absolute)
{
    string absolute_path = absolute ? file_path : getAbsolutePath(file_path);
    const auto& found = this->_data_accesses.find(absolute_path);

    // check if the file has already been taken into account
    if (found != this->_data_accesses.end())
        return { nullptr, {} };

    // insert the data and lex the file
    auto* data_access = new FileAccess(absolute_path);
    this->_data_accesses.insert({ absolute_path, data_access });

    return { data_access, Lexer::lex(*data_access) };
}

void CLI::parse()
{
    // parse current lexer output
    for (const auto& lexer_output : this->_lexer_output)
    {
        // insert the parsed data
        auto parser_output = Parser::parse(lexer_output.second);
        this->_parser_output.insert({ lexer_output.first, parser_output.parsed_data });

        vector<pair<string, AbstractSyntaxTree::ImportStatement*>> import_reqs {};

        // put the import requests in the local vector which contains the source file
        for (auto* import_req : parser_output.import_requests)        
            import_reqs.push_back({ lexer_output.first->getFilePath(), import_req });

        // parse the import requests
        for (unsigned int i = 0; i < import_reqs.size(); i++)
            parseRequestedImport(import_reqs[i].second, import_reqs[i].first, import_reqs);
    }

    // print output if flag is turned on and if there are no errors
    if (!ErrorRegister::hasErrors() && this->_cli_info.compilation_step == CompilationStep::parse)
    {
        for (const auto& curr_out : this->_parser_output)
        {
            std::cout << "Parser output for: " << curr_out.first->getFilePath() << cendl;

            for (auto* ast : curr_out.second)
            {
                if (ast != nullptr) ast->print();
            }

            std::cout << cendl;
        }
    }

    delete_resource(this->_lexer_output);
}

bool CLI::parseRequestedImport(AbstractSyntaxTree::ImportStatement* import_statement, const string& source, vector<pair<string, AbstractSyntaxTree::ImportStatement*>>& insertion_point)
{
    if (import_statement == nullptr) return false;

    pair<IDataAccess*, vector<Token*>> lexed_data = { nullptr, {} };

    // lex the content of the file
    try {
        // set the absolute path and lex the file
        import_statement->setFullPath(getAbsolutePath(import_statement->getImportModuleName(), source));
        lexed_data = lexFile(import_statement->getFullPath(), true);
    } catch(std::exception& err) {
        ErrorRegister::reportError(new CompilerErrors::ParserError(
            string("Could not import \"") + import_statement->getImportModuleName() + "\": " + err.what(),
            import_statement->getPosition()
        ));
    }

    // if the file has not been lexed yet, parse it and add it to the output
    if (lexed_data.first != nullptr)
    {
        auto parsed_data = Parser::parse(lexed_data.second);
        this->_parser_output.insert({ lexed_data.first, parsed_data.parsed_data });

        // add the import requests of the new file
        for (const auto& import_req : parsed_data.import_requests)
            insertion_point.push_back({ lexed_data.first->getFilePath(), import_req });
    }

    return true;
}

void CLI::semanticAnalysis()
{
    TypeSystemInfo::init();

    this->_semantic_analyzer_output = SemanticAnalyzer::decorate(this->_parser_output);

    // print output if flag is turned on and if there are no errors
    if (!ErrorRegister::hasErrors() && this->_cli_info.compilation_step == CompilationStep::semantic_analysis)
    {
        for (const auto& curr_out : this->_semantic_analyzer_output)
        {
            std::cout << "Semantic analysis output for: " << curr_out.first << cendl;

            curr_out.second->print();

            std::cout << cendl;
        }
    }
}

void CLI::codeGeneration()
{
    this->_codegen_output = std::move(CodeGenerator::generateCode(this->_semantic_analyzer_output));

    // print output if flag is turned on and if there are no errors
    if (!ErrorRegister::hasErrors())
    {
        // Run optimizations if exists
        CodeGenerator::optimizeModules(this->_codegen_output, this->_cli_info.getGeneralOptimazationFlag(), this->_cli_info.getSizeOptimazationFlag());   
        
        if (this->_cli_info.emit_bc)
            CodeGenerator::compileToBitcode(this->_codegen_output, this->_cli_info.executable_name, this->_cli_info.optimization_flags);
        if (this->_cli_info.emit_ir)
            CodeGenerator::compileToIR(this->_codegen_output, this->_cli_info.executable_name, this->_cli_info.optimization_flags);
        
        if(this->_cli_info.interpreted)
            CodeGenerator::runByJIT(this->_codegen_output, this->_cli_info.executable_name, this->_cli_info.optimization_flags, false);
        else
            CodeGenerator::compileToExecutable(this->_codegen_output, this->_cli_info.executable_name, this->_cli_info.optimization_flags, this->_cli_info.emit_obj);
    }

    TypeSystemInfo::cleanup();
}

void CLI::printHelpMessage()
{
    std::cout << "Usage: density [flags] file..." << cendl << cendl;

    for (unsigned int i = 0; i < CLI_ARGS_LENGTH; i++)
    {
        std::cout << std::get<0>(arguments[i]).first << " | " << std::get<0>(arguments[i]).second << "\n\t" 
            << std::get<1>(arguments[i]) << cendl;
    }
}

CLI::~CLI()
{
    // delete the data accesses
    for (const auto& data_access : this->_data_accesses)
        if (data_access.second != nullptr)
            delete data_access.second;
    this->_data_accesses.clear();

    delete_resource(this->_lexer_output);
    delete_resource(this->_parser_output);
}

void CLI::compile()
{
    if (this->_cli_info.asking_help)
    {
        CLI::printHelpMessage();
        return;
    }

    if (this->_cli_info.asking_version)
    {
        std::cout << "Density (" << density_version << ")" << cendl
            << "Development Version: " << development_version << cendl
            << "CPP Compiling Version: " << version_cpp << cendl;
        return;
    }
    
    int compilation_step = (int)CompilationStep::lex;
    auto curr_action = CLI::_compilation_functions.begin();
    bool has_errors = false;

    ErrorRegister::setIgnoringWarnings(this->_cli_info.is_verbose);

    for (; !has_errors && curr_action != CLI::_compilation_functions.end() && 
            compilation_step <= (int)(this->_cli_info.compilation_step); compilation_step++, curr_action++)
    {
        try {
            std::invoke(*curr_action, this);
        }
        catch (std::exception& err) {
            std::cout << "Encountered an error: " << err.what() << cendl;
            has_errors = true;
        }

        if (ErrorRegister::hasErrors())
        {
            ErrorRegister::printCompilingOutputInfo();
            ErrorRegister::flush();
            has_errors = true;
        }
        else if (ErrorRegister::hasWarnings())
        {
            ErrorRegister::printCompilingOutputInfo();
            ErrorRegister::flush();            
        }
    }

    if (compilation_step < (int)(this->_cli_info.compilation_step))
    {
        std::cout << "Last compiled part: " << compilation_step << cendl;
    }
}
