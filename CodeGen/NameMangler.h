#pragma once

#include "../Utilities/DefinedObjs/Types.h"

class NameMangler
{
private:
    vector<string> mangled_name = {"$Z"};

public:
    string getMangaledName(const string& name = "") noexcept;
    string getGlboalMangledName(const string& name = "") noexcept;
    void enterScopeData(const string& obj_name, SynthesizedType* obj_type) noexcept;
    void exitScopeData() noexcept;
};

typedef struct ManglingObj
{
    NameMangler& _name_mangler;
    ManglingObj(NameMangler& name_mangler, const string& obj_name, SynthesizedType* obj_type = nullptr) 
        : _name_mangler(name_mangler)
        { _name_mangler.enterScopeData(obj_name, obj_type); }
    ~ManglingObj() { _name_mangler.exitScopeData(); }
} ManglingObj;