#pragma once

#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/GlobalAlias.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/Type.h"
#include <llvm/IR/LegacyPassManager.h>

#include "llvm/Support/Host.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/IR/LegacyPassManager.h"

#include "llvm/Bitcode/BitcodeWriter.h"

#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"

#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/Interpreter.h"

#include "llvm/Linker/Linker.h"

#include <llvm/Analysis/TargetLibraryInfo.h>
#include <llvm/Analysis/TargetTransformInfo.h>

#include "llvm/Transforms/Utils/ModuleUtils.h"
#include <llvm/Transforms/IPO.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

#include "../Utilities/AbstractSyntaxTree/Nodes.h"
#include "../Utilities/Errors/CompilerErrors.h"
#include "../Utilities/DefinedObjs/Constants.h"
#include "../Utilities/DefinedObjs/Operators.h"
#include "../Utilities/DefinedObjs/GC.h"
#include "../Utilities/LangDefinedObjs/PreDefinedObjs.h"
#include "../SemanticAnalyzer/ScopeInfo.h"
#include "NameMangler.h"

#include <string>
#include <stack>

using std::string;
using std::stack;
using std::pair;
using AbstractSyntaxTree::Module;
using CompilerErrors::ErrorRegister;
using CompilerErrors::CodeGenError;
using CompilerErrors::CodeGenWarning;

typedef struct CGOutput{
    vector<std::unique_ptr<llvm::Module>> modules;

    CGOutput() = default;
    bool valid() const { return !modules.empty(); }
} CGOutput;

typedef struct DemangledInfo{
    string name;
    unsigned int id = 0;
    AbstractSyntaxTree::Properties properties;
} DemangledInfo;

typedef struct SafeCreationInfo{
    union safe_ptr_type{ 
        StrongPointerType* strong_ptr_type = nullptr;
        WeakPointerType* weak_ptr_type;

        safe_ptr_type() = default;
        safe_ptr_type(StrongPointerType* ptr) : strong_ptr_type(ptr) {}
        safe_ptr_type(WeakPointerType* ptr) : weak_ptr_type(ptr) {}
    } safe_ptr_type;

    llvm::Value* safe_ptr = nullptr;
    bool is_strong = true;
    bool is_tmp = true;
} SafeCreationInfo;

typedef vector<pair<CustomType*, llvm::Value*>> AllocationList;
typedef vector<SafeCreationInfo> SafeCreationList;
typedef struct FunctionMetaData{
    llvm::Function* func_val = nullptr;
    AbstractSyntaxTree::FunctionDefinition* func_def = nullptr;
    llvm::Value* variadic_list = nullptr;
    llvm::BasicBlock* allocation_block = nullptr;
    llvm::AllocaInst* return_allocation = nullptr;
    llvm::BasicBlock* return_block = nullptr;
    unsigned int ret_count = 0;
    list<AllocationList> alloc_objs; // Not the friendliest memory management
    list<SafeCreationList> creation_objs; // Don't do this at home
} FunctionMetaData;

typedef struct CGScopeInfo : public ScopeInformation {
    stack<FunctionMetaData> functions_data;

    CGScopeInfo() : ScopeInformation() {};
    CGScopeInfo(Module* module, const SymbolTable& global_table) 
        : ScopeInformation(module, global_table) {}

    void enterScope(const SymbolTable& table, const AllocationList& list = {}, const SafeCreationList& safe_list = {}) noexcept;
	void exitScope(bool create_alloc_block = true) noexcept;
    void enterFunction(const FunctionMetaData& function) noexcept;
	void exitFunction() noexcept;

} CGScopeInfo;

class CodeGenerator 
{
private:
    static llvm::TargetMachine* _target_machine;
    static string _target_triple;
    static llvm::LLVMContext& _context;

    const std::vector<pair<string, Module*>>& _modules;
    std::unique_ptr<llvm::Module> _curr_module;
    Module* _curr_module_node;
    vector<llvm::Function*> _global_init_var_funcs;
    int _curr_priority;
    llvm::IRBuilder<> _builder;
    NameMangler _name_mangler;
    GarbageCollector _gc;

    static std::string _llvm_err;
    static llvm::raw_string_ostream _llvm_err_register;

    CodeGenerator(const std::vector<pair<string, Module*>>& modules);

    CGOutput codegen();

    std::unique_ptr<llvm::Module> codegen(AbstractSyntaxTree::Module* node, const string& module_name);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::Module* node);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::Node* node);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::Statement* node);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::StatementBlock* node, bool merge_with_before = true);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::ConditionalStatement* node);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::WhileStatement* node);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::ForStatement* node);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::FunctionDefinition* node);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::NamespaceDefinition* node);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::TypeDefinition* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::VariableDeclaration* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::ComplexTypeComponent* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::ReturnStatement* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::Expression* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::UnaryOperatorExpression* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::IdentifierExpression* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::ExpressionList* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::CallExpression* node, llvm::Value* this_ptr = nullptr);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::CastExpression* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::MemberAccessExpression* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::VarArgsExpression* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::TypeInstatiation* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::CreateFunctionExpression* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::NewFunctionExpression* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::DeleteFunctionStatement   * node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::LiteralExpression* node);
    llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::BooleanLiteral* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::CharLiteral* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::IntegerLiteral* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::StringLiteral* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::DoubleLiteral* node);
	llvm::Value* codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::NullLiteral* node);
    
    llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::Node* node);
    llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::Statement* node);
	llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::Expression* node);
	llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node);
	llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::UnaryOperatorExpression* node);
	llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::IdentifierExpression* node);
	llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::CallExpression* node);
	llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::MemberAccessExpression* node);
	llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* node);
	llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::TypeInstatiation* node);
	llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::CreateFunctionExpression* node);
	llvm::Value* codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::NewFunctionExpression* node);

    llvm::Value* createCastIfNeccessary(const position_data& position, llvm::Value* to_cast, SynthesizedType* source_type, SynthesizedType* dest_type);

    llvm::Value* getValue(CGScopeInfo& scope_info, AbstractSyntaxTree::MemberAccessExpression* expr);
    void reportCantCast(SynthesizedType* source, SynthesizedType* dest, const position_data& pos);
    void warningIfUndefOrPoison(const string& warning,llvm::Value* val, const position_data& pos);
    llvm::BasicBlock* allocaLangParams(CGScopeInfo& scope_info, AbstractSyntaxTree::FunctionDefinition* node, llvm::Function* func);
    llvm::BasicBlock* allocaParams(CGScopeInfo& scope_info, AbstractSyntaxTree::FunctionDefinition* node, llvm::Function* func);
    llvm::BasicBlock* insertReturnBlockImpl(CGScopeInfo& scope_info, AbstractSyntaxTree::FunctionDefinition* node, llvm::BasicBlock* return_block);

    llvm::Value* CodegenLogicalBinaryOperation(CGScopeInfo& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node, Operator* op);
    llvm::BasicBlock* createBlockFromStatement(CGScopeInfo& scope_info, AbstractSyntaxTree::Statement* statement, const std::string& name = "statement_block", bool merge = false);
    void generateGlobalInitFuncs(CGScopeInfo& scope_info);
    bool addGlobalVariable(CGScopeInfo& scope_info, AbstractSyntaxTree::VariableDeclarationComponent* declaration);
    void mergeLastBlockInto(CGScopeInfo& scope_info, llvm::BasicBlock* block);
    std::vector<llvm::Value*> toValueVector(CGScopeInfo& scope_info, AbstractSyntaxTree::ExpressionList* node);
    llvm::GlobalValue* getOrCreateVarLinkageIfGlobal(CGScopeInfo& scope_info, const string& name, AbstractSyntaxTree::Node* node);
    void createDestructorCalls(CGScopeInfo& scope_info, const FunctionMetaData& metadata);
    void createDestructorCalls(CGScopeInfo& scope_info, const AllocationList& alloc_list);
    void createDestructorCall(CGScopeInfo& scope_info, CustomType* type, llvm::Value* self_ptr);
    void createReleaseCalls(CGScopeInfo& scope_info, const FunctionMetaData& metadata);
    void createReleaseCalls(CGScopeInfo& scope_info, const SafeCreationList& alloc_list);
    void createReleaseCall(CGScopeInfo& scope_info, const SafeCreationInfo& alloc_info);
    void createConstructorCall(CGScopeInfo& scope_info, CustomType* type, llvm::ArrayRef<llvm::Value*> parameters);
    void insertDefaultConstructorImpl(CGScopeInfo& scope_info, CustomType* type, llvm::Value* self_ptr);
    void insertDefaultDestructorImpl(CGScopeInfo& scope_info, CustomType* type, llvm::Value* self_ptr);

    /*
        The function will initialize the llvm type of the node given and report an
        error if any occurs.
    */
    llvm::Type* initLLVMType(AbstractSyntaxTree::Node* node);
    llvm::Type* initLLVMType(SynthesizedType* type, const position_data& position);

    /*
        The function returns the llvm value that is gurenteed to be defined in the current module.
        This function is used to prevent referencing global objects from other modules.
    */
    llvm::Value* constrainValueToModule(llvm::Module* mod, llvm::Value* value);

    static bool initializeTarget();

    static void linkModules(CGOutput& codegen_output);
    static void addOptPasses(llvm::legacy::PassManagerBase &passes, llvm::legacy::FunctionPassManager &fnPasses, unsigned int opt, unsigned int size_opt);
    static void addLinkPasses(llvm::legacy::PassManagerBase &passes, unsigned int opt, unsigned int size_opt);

public:

    static CGOutput generateCode(const std::vector<pair<string, Module*>>& modules);

    static void optimizeModules(CGOutput& cgoutput, unsigned int opt = 0, unsigned int size_opt = 0);
    static void compileToExecutable(CGOutput& cgoutput, const string& output_filename, const CompliationOptm& opt_flags, bool emit_obj);
    static void compileToObj(CGOutput& cgoutput, const string& output_filename, const CompliationOptm& opt_flags);
    static void runByJIT(CGOutput& cgoutput, const string& output_filename, const CompliationOptm& opt_flags, bool jit_use = true);
    static void compileToIR(CGOutput& cgoutput, const string& output_filename, const CompliationOptm& opt_flags);
    static void compileToBitcode(CGOutput& cgoutput, const string& output_filename, const CompliationOptm& opt_flags);

    // Possible: LLVM JIT Run
    // Possible: Assembly Code
};