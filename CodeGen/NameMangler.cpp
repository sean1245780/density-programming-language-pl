#include "NameMangler.h"

string NameMangler::getMangaledName(const string& name) noexcept 
{
    string ans;

    for(const auto& inner_name : this->mangled_name)
    {
        ans += inner_name;
    }

    return std::move(ans + name);
}

string NameMangler::getGlboalMangledName(const string& name) noexcept
{
    return this->mangled_name.front() + name;
}

void NameMangler::enterScopeData(const string& obj_name, SynthesizedType* obj_type) noexcept
{
    if(obj_type == nullptr) return;

    if(obj_type->getType() == SynthesizedType::type_category::function_type)
        mangled_name.push_back("$F" + obj_name + "." + obj_type->getManglingName());
    else //if(obj_type->getType() == SynthesizedType::type_category::namespace_type)
        mangled_name.push_back("$N" + obj_name + "." + obj_type->getManglingName());

    // The last one means that mangling a class is the same as mangling a namespace
}

void NameMangler::exitScopeData() noexcept
{
    this->mangled_name.pop_back();
}
