#include "CodeGenerator.h"

llvm::TargetMachine* CodeGenerator::_target_machine = nullptr;
string CodeGenerator::_target_triple = llvm::sys::getDefaultTargetTriple();
llvm::LLVMContext& CodeGenerator::_context = TypeSystemInfo::getGlobalLLVMContext();
std::string CodeGenerator::_llvm_err = "";
llvm::raw_string_ostream CodeGenerator::_llvm_err_register(CodeGenerator::_llvm_err);

bool CodeGenerator::initializeTarget()
{
    // initialize native target (compiling for this machine)
    if (llvm::InitializeNativeTarget() || llvm::InitializeNativeTargetAsmPrinter() ||
            llvm::InitializeNativeTargetAsmParser())
    {
        ErrorRegister::reportError(new CodeGenError(
            "Could not initialize target (native target init failed)"
        ));
        return false;
    }

    // get the target
    string err;
    const llvm::Target* target = llvm::TargetRegistry::lookupTarget(_target_triple, err);

    if (target == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            std::string("Could not initialize target (target lookup failed : ") + err + std::string(")")
        ));
        return false;
    }   

    // create the target machine
    llvm::StringRef cpu = llvm::sys::getHostCPUName();
    string features = "";
    llvm::TargetOptions options;
    llvm::Optional<llvm::Reloc::Model> reloc_model = llvm::Optional<llvm::Reloc::Model>(llvm::Reloc::PIC_);

    _target_machine = target->createTargetMachine(_target_triple, cpu, features, options, reloc_model);

    return true;
}

CodeGenerator::CodeGenerator(const std::vector<pair<string, Module*>>& modules) :
    _modules(modules), _builder(_context), _curr_priority(0), _gc(_builder, _context, nullptr, nullptr)
{
    static bool initialized_target = initializeTarget();
    this->_gc.setTarget(CodeGenerator::_target_machine);

    // Setting the name of the type to the right type
    auto* global_container = TypeSystemInfo::getGlobalContainer();
    auto* no =  global_container->getOrCreateStrongPointerType(
            global_container->getType("u8"));
    this->_gc.setSafeI8PtrNameType(no->getGCName());
}

CGOutput CodeGenerator::generateCode(const std::vector<pair<string, Module*>>& modules)
{
    CodeGenerator generator(modules);

    return std::move(generator.codegen());
}

void CodeGenerator::linkModules(CGOutput& codegen_output)
{
    if (codegen_output.modules.empty()) throw std::runtime_error("There are no modules to link!");

    llvm::Linker linker(*(codegen_output.modules.front()));

    // link the modules
    for (unsigned int i = 1; i < codegen_output.modules.size(); i++)
        linker.linkInModule(std::move(codegen_output.modules[i]));

    // erase remains of the used modules
    if (codegen_output.modules.size() > 1)
	    codegen_output.modules.erase(codegen_output.modules.begin() + 1, codegen_output.modules.end());
    
    if (llvm::verifyModule(*(codegen_output.modules.front()), &CodeGenerator::_llvm_err_register))
    {
        ErrorRegister::reportError(new CodeGenError("LLVM error: " + _llvm_err));
    }
}

void CodeGenerator::addOptPasses(llvm::legacy::PassManagerBase &passes, llvm::legacy::FunctionPassManager &fnPasses, unsigned int opt, unsigned int size_opt)
{
    llvm::PassManagerBuilder builder;
    builder.OptLevel = opt;
    builder.SizeLevel = size_opt;
    builder.Inliner = llvm::createFunctionInliningPass(opt, size_opt, false);
    //builder.LoopVectorize = true; -> Probably fits opt3
    //builder.SLPVectorize = true; -> Probably fits opt3
    CodeGenerator::_target_machine->adjustPassManager(builder);

    builder.populateFunctionPassManager(fnPasses);
    builder.populateModulePassManager(passes);
}

void CodeGenerator::addLinkPasses(llvm::legacy::PassManagerBase &passes, unsigned int opt, unsigned int size_opt) 
{
    llvm::PassManagerBuilder builder;
    builder.VerifyInput = true;
    builder.Inliner = llvm::createFunctionInliningPass(opt, size_opt, false);
    builder.populateLTOPassManager(passes);
}

void CodeGenerator::optimizeModules(CGOutput& cgoutput, unsigned int opt, unsigned int size_opt)
{
    // Skip optimazations
    if(opt == 0 && size_opt == 0) return;

	llvm::legacy::PassManager passes;
	passes.add(new llvm::TargetLibraryInfoWrapperPass(CodeGenerator::_target_machine->getTargetTriple()));
	passes.add(llvm::createTargetTransformInfoWrapperPass(CodeGenerator::_target_machine->getTargetIRAnalysis()));

    for (auto& module : cgoutput.modules)
    {
        llvm::legacy::FunctionPassManager fnPasses(module.get());
        fnPasses.add(llvm::createTargetTransformInfoWrapperPass(CodeGenerator::_target_machine->getTargetIRAnalysis()));

        addOptPasses(passes, fnPasses, opt, size_opt);
        addLinkPasses(passes, opt, size_opt);

        fnPasses.doInitialization();
        for (llvm::Function &func : *module) {
            fnPasses.run(func);
        }

        fnPasses.doFinalization();

        passes.add(llvm::createVerifierPass());
        passes.run(*module);
    }
}

void CodeGenerator::compileToExecutable(CGOutput& cgoutput, const string& output_filename, const CompliationOptm& opt_flags, bool emit_obj)
{
    if (!cgoutput.valid()) return;

    compileToObj(cgoutput, output_filename, opt_flags);

    // compile the generated .o file
    string cmd = string("gcc ") + output_filename + OBJ_FILE_EXT + " -o "
                + output_filename;
    if (system(cmd.c_str()) == -1)
        throw std::runtime_error("GCC: failed to link object file");

    // delete the obj file if not requested to keep it
    if (!emit_obj)
        llvm::sys::fs::remove(output_filename + OBJ_FILE_EXT);
}

void CodeGenerator::compileToObj(CGOutput& cgoutput, const string& output_filename,  const CompliationOptm& opt_flags)
{
    if (!cgoutput.valid()) return;

    // link the modules and create the pass manager
    if (cgoutput.modules.size() > 1) linkModules(cgoutput);
    llvm::legacy::PassManager pass_manager;

    pass_manager.add(new llvm::TargetLibraryInfoWrapperPass(CodeGenerator::_target_machine->getTargetTriple()));
	pass_manager.add(llvm::createTargetTransformInfoWrapperPass(CodeGenerator::_target_machine->getTargetIRAnalysis()));

    // create the output file
    std::error_code err_code;
    llvm::raw_fd_ostream output_file(output_filename + OBJ_FILE_EXT, err_code, llvm::sys::fs::OF_None);

    if (err_code)
        throw std::runtime_error(std::string("could not open ") + output_filename + OBJ_FILE_EXT 
                    + string(" - error code: ") + err_code.message());

    // emit the obj file
    if (_target_machine->addPassesToEmitFile(pass_manager, output_file, nullptr, llvm::CGFT_ObjectFile))
        throw std::runtime_error((string("the target machine could not create ") + OBJ_FILE_EXT).c_str());
    
    pass_manager.run(*cgoutput.modules.front());
    output_file.flush();
}

void CodeGenerator::runByJIT(CGOutput& cgoutput, const string& output_filename, const CompliationOptm& opt_flags, bool jit_use)
{
    if (!cgoutput.valid()) return;

    // link the modules and create the pass manager
    if (cgoutput.modules.size() > 1) linkModules(cgoutput);

    // Getting the main function
    auto* main_fun = cgoutput.modules.front().get()->getFunction("main");

    string err_str;
    llvm::EngineBuilder EB(std::move(cgoutput.modules.front()));
    cgoutput.modules.pop_back();

    // Setting the needed use
    if(jit_use)
        EB.setEngineKind(llvm::EngineKind::JIT).setErrorStr(&err_str);
    else
        EB.setEngineKind(llvm::EngineKind::Interpreter).setErrorStr(&err_str);

    llvm::ExecutionEngine *EE = EB.create();

    if (EE == nullptr)
        throw std::runtime_error("could not activate Execution engine: " + err_str);

    // Setting some last things and checking the execution engine
    EE->finalizeObject();

    // Running the function
    EE->runFunction(main_fun, llvm::ArrayRef<llvm::GenericValue>());
    //auto* func_m = EE->getPointerToNamedFunction("main");
    //std::cout << "Result: "<< func_m << "\n";
}

void CodeGenerator::compileToIR(CGOutput& cgoutput, const string& output_filename, const CompliationOptm& opt_flags)
{
    if (!cgoutput.valid()) return;

    // link the modules
    if (cgoutput.modules.size() > 1) linkModules(cgoutput);

    // create the output file
    std::error_code err_code;
    llvm::raw_fd_ostream output_file(output_filename + IR_FILE_EXT, err_code, llvm::sys::fs::OF_None);

    if (err_code)
        throw std::runtime_error(std::string("could not open ") + output_filename + IR_FILE_EXT + 
                string(" - error code: ") + err_code.message());

    // emit the ll file
    cgoutput.modules.front()->print(output_file, nullptr);
}

void CodeGenerator::compileToBitcode(CGOutput& cgoutput, const string& output_filename, const CompliationOptm& opt_flags)
{
    if (!cgoutput.valid()) return;

    // link the modules
    if (cgoutput.modules.size() > 1) linkModules(cgoutput);

    // create the output file
    std::error_code err_code;
    llvm::raw_fd_ostream output_file(output_filename + IR_BITCODE_FILE_EXT, err_code, llvm::sys::fs::OF_None);

    if (err_code)
        throw std::runtime_error(std::string("could not open ") + 
            output_filename + IR_BITCODE_FILE_EXT + string(" - error code: ") + err_code.message());

    // emit the bc file
    llvm::WriteBitcodeToFile(*(cgoutput.modules.front()), output_file);
}

CGOutput CodeGenerator::codegen()
{
    CGOutput output;

    // generate code for all of the modules
    for (const auto& module : this->_modules)
        output.modules.push_back(std::move(codegen(module.second, module.first)));

    return std::move(output);
}

std::unique_ptr<llvm::Module> CodeGenerator::codegen(AbstractSyntaxTree::Module* node, const string& module_name)
{
    if (node == nullptr) throw std::runtime_error("Encountered an empty module node");

    this->_curr_module_node = node;
    this->_curr_module = std::make_unique<llvm::Module>(module_name, _context);
    this->_curr_module->setTargetTriple(this->_target_triple);
    this->_curr_module->setDataLayout(this->_target_machine->createDataLayout());

    auto& symbol_table = node->getSymbolTable();
    auto scope_info = CGScopeInfo(node, symbol_table);

    try {
        scope_info.includeImportedSymbols();
    } catch(SymbolRedefinitionException& err) {
        ErrorRegister::reportError((new CodeGenError(
            "Redefined Symbol",
            err.previous_definition->getPosition()
        ))->withNote(err.redefined_symbol + " is already defined here", err.new_definition->getPosition()));
    }

    // Set the current module for the garbage collector
    this->_gc.setCurrentModule(this->_curr_module.get());
    
    // generate code for all nodes in the module
    for (auto* component : node->getCommands())
        codegen(scope_info, component);

    // generate the initializer functions for the globals if any exist
    /*if (!this->_global_init_var_funcs.empty())
    {
        this->generateGlobalInitFuncs();
        this->_global_init_var_funcs.clear();
    }*/

    for(auto* init_func : this->_global_init_var_funcs)
        llvm::appendToGlobalCtors(*(this->_curr_module), init_func, this->_curr_priority++);

    this->_global_init_var_funcs.clear();

    // this->_curr_module->print(llvm::errs(), nullptr);
    
    // verify the module
    if (llvm::verifyModule(*this->_curr_module, &this->_llvm_err_register))
    {
        ErrorRegister::reportError(new CodeGenError(
            "LLVM error in module " + module_name + ": " + this->_llvm_err,
            node->getPosition()
        ));
    }

    return std::move(this->_curr_module);
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::Node* node)
{
    if (node == nullptr) return nullptr;
    
    switch (node->getNodeType())
    {
        case AbstractSyntaxTree::Node::node_type::module:
            return codegen(scope_info, static_cast<AbstractSyntaxTree::Module*>(node));
        case AbstractSyntaxTree::Node::node_type::function_definition:
            return codegen(scope_info, static_cast<AbstractSyntaxTree::FunctionDefinition*>(node));
        case AbstractSyntaxTree::Node::node_type::namespace_definition:
            return codegen(scope_info, static_cast<AbstractSyntaxTree::NamespaceDefinition*>(node));
        case AbstractSyntaxTree::Node::node_type::type_definition:
            return codegen(scope_info, static_cast<AbstractSyntaxTree::TypeDefinition*>(node));
        case AbstractSyntaxTree::Node::node_type::statement:
            return codegen(scope_info, static_cast<AbstractSyntaxTree::Statement*>(node));
        case AbstractSyntaxTree::Node::node_type::variable_declaration_component:
            return codegen(scope_info, static_cast<AbstractSyntaxTree::VariableDeclarationComponent*>(node)->getIdentifier());
        case AbstractSyntaxTree::Node::node_type::complex_type_component:
            return codegen(scope_info, static_cast<AbstractSyntaxTree::ComplexTypeComponent*>(node)->getIdentifier());
        case AbstractSyntaxTree::Node::node_type::import_statement:
            // import statements do not have any representation in LLVM IR
            return nullptr;
        default:
            ErrorRegister::reportError(new CodeGenError(
                "Encountered an invalid node",
                node->getPosition()
            ));
    }
    
    return nullptr;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::Module* node)
{
    if (node == nullptr) return nullptr;

    for (auto* command : node->getCommands())
        codegen(scope_info, command);
    
    return nullptr;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::Statement* node)
{
    if (node == nullptr) return nullptr;

    switch (node->getStatementType())
    {
    case AbstractSyntaxTree::Statement::statement_type::conditional_statement:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::ConditionalStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::while_statement:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::WhileStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::for_statement:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::ForStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::expression:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::Expression*>(node));
    case AbstractSyntaxTree::Statement::statement_type::return_statement:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::ReturnStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::statement_block:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::StatementBlock*>(node));
    case AbstractSyntaxTree::Statement::statement_type::variable_declaration:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::VariableDeclaration*>(node));
    case AbstractSyntaxTree::Statement::statement_type::delete_function_statement:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::DeleteFunctionStatement*>(node));
    default:
        ErrorRegister::reportError(new CodeGenError(
            "Encountered an invalid node",
            node->getPosition()
        ));
        break;
    }

    return nullptr;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::CastExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getInnerExpression()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();
    
    if(node->getInnerExpression() == nullptr || node->getTargetType() == nullptr) return nullptr;
    
    // codegen the inner expression
    auto* inner_val = codegen(scope_info, node->getInnerExpression());

    if (inner_val == nullptr) return nullptr;

    // when casting to the same type, return the inner value
    if (node->getSynthesizedType()->getLLVMType() == inner_val->getType())
    {
        node->setLLVMValue(inner_val);
        return inner_val;
    } 

    switch (node->getCastingType())
    {
    case AbstractSyntaxTree::CastExpression::cast_type::cast_static:
    {
        // get the matching codegeneration function
        auto func = node->getInnerExpression()->getSynthesizedType()->getStaticCast(
            node->getTargetType()->getSynthesizedType()
        );

        if (func == nullptr)
            ErrorRegister::reportError(new CodeGenError(
                string("Static cast from ") + node->getInnerExpression()->getSynthesizedType()->getName() + 
                " to " + node->getTargetType()->getSynthesizedType()->getName() + 
                " is not defined",
                node->getPosition()
            ));
        else 
            node->setLLVMValue(func(
                inner_val, 
                node->getTargetType()->getSynthesizedType()->getLLVMType(), 
                node->getInnerExpression()->getSynthesizedType(),
                node->getTargetType()->getSynthesizedType(),
                this->_builder,
                this->_gc
            ));
        break;
    }
    case AbstractSyntaxTree::CastExpression::cast_type::cast_bitcast:
        // invoke the bitcast codegeneration function
        node->setLLVMValue(SynthesizedType::def_bitcast(
            inner_val,
            node->getTargetType()->getSynthesizedType()->getLLVMType(),
            node->getInnerExpression()->getSynthesizedType(),
            node->getTargetType()->getSynthesizedType(),
            this->_builder,
            this->_gc
        ));
        break;
    case AbstractSyntaxTree::CastExpression::cast_type::cast_dynamic:
    default:
        ErrorRegister::reportError(new CodeGenError(
            "This cast type is not supported in this version",
            node->getPosition()
        ));
        break;
    }

    if (llvm::isa_and_nonnull<llvm::PoisonValue>(node->getLLVMValue()) ||
        llvm::isa_and_nonnull<llvm::UndefValue>(node->getLLVMValue()))
    {
        ErrorRegister::reportWarning(new CodeGenWarning(
            "The cast creates undefined/poisoned llvm value",
            node->getPosition(),
            "Creates a value with undefined behaviour"
        ));
    }

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::StatementBlock* node, bool merge_with_before)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    // report an error if there is no parent function
    if (scope_info.functions_data.empty())
    {
        ErrorRegister::reportError(new CodeGenError(
            "Encountered statement block without a parent function",
            node->getPosition()
        ));

        return nullptr;
    }

    ScopeObject obj(scope_info, node->getSymbolTable());

    // create the new code block
    llvm::BasicBlock* block = llvm::BasicBlock::Create(
        this->_context,
        "statement_block_entry",
        scope_info.functions_data.top().func_val,
        scope_info.functions_data.top().return_block
    );

    // merge the last block of the current function into this new block
    if (merge_with_before) mergeLastBlockInto(scope_info, block);

    _builder.SetInsertPoint(block);

    bool has_return = false;
    for (auto command = node->getCommands().begin(); command != node->getCommands().end() && !has_return; command++)
    {
        if(*command != nullptr)
        {
            if ((*command)->getHasReturn())
                has_return = true;
            
            // Pushing a tmp list of object creation
            scope_info.functions_data.top().alloc_objs.push_front({});
            scope_info.functions_data.top().creation_objs.push_front({});

            // codegen the instruction
            codegen(scope_info, *command);

            if (!has_return)
            {
                createDestructorCalls(scope_info, scope_info.functions_data.top().alloc_objs.front());
                createReleaseCalls(scope_info, scope_info.functions_data.top().creation_objs.front());
            }

            // Poping a tmp list of object creation
            scope_info.functions_data.top().alloc_objs.pop_front();
            scope_info.functions_data.top().creation_objs.pop_front();
        }
    }

    // call destructors for current scope if there is no return in this scope
    if (!has_return)
    {
        createDestructorCalls(scope_info, scope_info.functions_data.top().alloc_objs.front());
        createReleaseCalls(scope_info, scope_info.functions_data.top().creation_objs.front());
    }
    
    node->setLLVMValue(block);

    return block;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::ConditionalStatement* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    if (_builder.GetInsertBlock() == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            "Encountered if statement outside of a code block",
            node->getPosition()
        ));

        return nullptr;
    }

    /*
    Conditional statements will be codegened in the format:
        ...
        br %cond, true_branch, false_branch
    true_branch:
        <true_commands>
        br after_branch
    false_branch:
        <false_commands>
        br after_branch    
    after_branch:
        ...
    */

    auto* cond = codegen(scope_info, node->getCondition());
    if (cond == nullptr) return nullptr;

    // create the true branch
    auto* true_branch = createBlockFromStatement(scope_info, node->getIfCommands(), "true_branch");
    if (true_branch == nullptr) return nullptr;

    // create the false branch if it exists
    llvm::BasicBlock* false_branch = nullptr;
    if(node->getElseCommands() != nullptr)
    {
        false_branch = createBlockFromStatement(scope_info, node->getElseCommands(), "false_branch");
        if (false_branch == nullptr) return nullptr;
    }

    if (!node->getHasReturn())
    {
        llvm::BasicBlock* after_branch = llvm::BasicBlock::Create(
            this->_context,
            "after_branch",
            scope_info.functions_data.top().func_val,
            scope_info.functions_data.top().return_block
        );

        _builder.CreateCondBr(cond, true_branch, false_branch == nullptr ? after_branch : false_branch);

        // merge the true branch to the after branch if it is not terminated
        if ((false_branch == nullptr ? after_branch : false_branch)->getPrevNode()->getTerminator() == nullptr)
        {
            _builder.SetInsertPoint((false_branch == nullptr ? after_branch : false_branch)->getPrevNode());
            _builder.CreateBr(after_branch);
        }

        // merge the false branch to the after branch if it exists
        if (false_branch != nullptr && after_branch->getPrevNode()->getTerminator() == nullptr)
        {
            _builder.SetInsertPoint(after_branch->getPrevNode());
            _builder.CreateBr(after_branch);
        }

        // continue code generation from the after branch
        _builder.SetInsertPoint(after_branch);
    }
    else if (false_branch != nullptr)
        _builder.CreateCondBr(cond, true_branch, false_branch);
    else 
        ErrorRegister::reportError(new CodeGenError(
            "Return information was not proccessed correctly",
            node->getPosition()
        ));

    node->setLLVMValue(llvm::UndefValue::get(node->getSynthesizedType()->getLLVMType()));

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::WhileStatement* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    if (_builder.GetInsertBlock() == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            "Encountered while statement outside of code block",
            node->getPosition()
        ));

        return nullptr;
    }

    /*
    While statements will be codegened in the format:
        ...
        br evaluate_cond
    evaluate_cond:
        br %cond, while_block, after_branch
    while_block:
        <commands>
        br evaluate_cond
    after_branch:
        ...
    */

   // create the evaluate condition branch
   llvm::BasicBlock* eval_cond = llvm::BasicBlock::Create(
       this->_context,
       "evaluate_cond",
       scope_info.functions_data.top().func_val,
       scope_info.functions_data.top().return_block
   );

    mergeLastBlockInto(scope_info, eval_cond);

    this->_builder.SetInsertPoint(eval_cond);

    auto* cond = codegen(scope_info, node->getCondition());
    if (cond == nullptr) return nullptr;

    // create the commands branch
    auto* while_block = createBlockFromStatement(scope_info, node->getCommands(), "while_block");
    if (while_block == nullptr) return nullptr;

    llvm::BasicBlock* after_branch = llvm::BasicBlock::Create(
        this->_context,
        "after_branch",
        scope_info.functions_data.top().func_val,
        scope_info.functions_data.top().return_block
    );

    // if the last block generated inside the while does not terminate
    if (after_branch->getPrevNode()->getTerminator() == nullptr)
    {
        // create a branch instruction to evaluate the condition again
        this->_builder.SetInsertPoint(after_branch->getPrevNode());
        
        this->_builder.CreateBr(eval_cond);
    }

    // create the conditional branch in the end of the eval_cond block
    this->_builder.SetInsertPoint(eval_cond);
    this->_builder.CreateCondBr(cond, while_block, after_branch);

    // continue code generation from the after branch
    this->_builder.SetInsertPoint(after_branch);

    node->setLLVMValue(llvm::UndefValue::get(node->getSynthesizedType()->getLLVMType()));

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::ForStatement* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getInitialization() != nullptr && initLLVMType(node->getInitialization()) == nullptr) return nullptr;
    if (node->getIncrementation() != nullptr && initLLVMType(node->getIncrementation()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    if (_builder.GetInsertBlock() == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            "Encountered for statement outside of a code block",
            node->getPosition()
        ));

        return nullptr;
    }

    // enter into the scope of the for statement
    ScopeObject obj(scope_info, node->getSymbolTable());

    /*
    For statements will be codegened in the format:
        ...
        <initializations>
        br evaluate_cond
    evaluate_cond:
        br %cond, for_block, after_branch
    for_block:
        <commands>
        <incrementations>
        br evaluate_cond
    after_branch:
        ...
    */

   // codegen the initializations
   codegen(scope_info, node->getInitialization());

   // create the evaluate condition branch
   llvm::BasicBlock* eval_cond = llvm::BasicBlock::Create(
       this->_context,
       "evaluate_cond",
       scope_info.functions_data.top().func_val,
       scope_info.functions_data.top().return_block
   );

    mergeLastBlockInto(scope_info, eval_cond);

    this->_builder.SetInsertPoint(eval_cond);

    auto* cond = codegen(scope_info, node->getCondition());
    if (cond == nullptr) return nullptr;

    // create the commands branch
    auto* for_block = createBlockFromStatement(scope_info, node->getCommands(), "for_block");
    if (for_block == nullptr) return nullptr;

    llvm::BasicBlock* after_branch = llvm::BasicBlock::Create(
        this->_context,
        "after_branch",
        scope_info.functions_data.top().func_val,
        scope_info.functions_data.top().return_block
    );

    // if the last block generated inside the while does not terminate
    if (after_branch->getPrevNode()->getTerminator() == nullptr)
    {
        this->_builder.SetInsertPoint(after_branch->getPrevNode());

        // codegen the incrementation expression
        codegen(scope_info, node->getIncrementation());
        
        // create a branch instruction to evaluate the condition again
        this->_builder.CreateBr(eval_cond);
    }

    // create the conditional branch in the end of the eval_cond block
    this->_builder.SetInsertPoint(eval_cond);

    this->_builder.CreateCondBr(cond, for_block, after_branch);

    // continue code generation from the after branch
    this->_builder.SetInsertPoint(after_branch);

    node->setLLVMValue(llvm::UndefValue::get(node->getSynthesizedType()->getLLVMType()));

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::CodegenLogicalBinaryOperation(CGScopeInfo& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node, Operator* op)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getRightExpression()) == nullptr) return nullptr;
    if (initLLVMType(node->getLeftExpression()) == nullptr) return nullptr;
    if (node == nullptr) return nullptr;
    if (op == nullptr) return nullptr;
    if (op->op != "&&" && op->op != "||") return nullptr;
    
    llvm::Value* left_val = nullptr, * right_val = nullptr;
    auto* bool_type = TypeSystemInfo::getGlobalContainer()->getType("bool");

    // codegen the left value
    left_val = createCastIfNeccessary(
        node->getLeftExpression()->getPosition(),
        ((op->bin_mods.left_input == operator_mod::reference) ?
            codegen_modifiable(scope_info, node->getLeftExpression()) :
            codegen(scope_info, node->getLeftExpression())),
        node->getLeftExpression()->getSynthesizedType(),
        bool_type
    );

    if (left_val == nullptr) return nullptr;

    // Create the right block and insert it
    auto* rhs_check = llvm::BasicBlock::Create(
        this->_context, "right_check",
        scope_info.functions_data.top().func_val,
        scope_info.functions_data.top().return_block
    );

    this->_builder.SetInsertPoint(rhs_check);

    // codegen the right value
    right_val = createCastIfNeccessary(
        node->getRightExpression()->getPosition(),
        ((op->bin_mods.right_input == operator_mod::reference) ?
            codegen_modifiable(scope_info, node->getRightExpression()) :
            codegen(scope_info, node->getRightExpression())),
        node->getRightExpression()->getSynthesizedType(),
        bool_type
    );

    if (right_val == nullptr) return nullptr;

    // Create the after block
    auto* after_check = llvm::BasicBlock::Create(
        this->_context, "after_check",
        scope_info.functions_data.top().func_val,
        scope_info.functions_data.top().return_block
    );

    this->_builder.CreateBr(after_check);
    this->_builder.SetInsertPoint(rhs_check->getPrevNode());

    // Those operators are returning values without calculating the right side
    if (op->op == "&&")
    {
        this->_builder.CreateCondBr(left_val, rhs_check, after_check);
    }
    else if(op->op == "||")
    {
        this->_builder.CreateCondBr(left_val, after_check, rhs_check);
    }

    this->_builder.SetInsertPoint(after_check);
    
    // Create the phi node and insert into it
    auto* bool_llvm_type = bool_type->getLLVMType(this->_context, this->_gc);
    auto* phi_res = this->_builder.CreatePHI(
        bool_llvm_type, 2);

    phi_res->addIncoming(
        llvm::ConstantInt::get(bool_llvm_type,
        llvm::APInt(bool_llvm_type->getIntegerBitWidth(), op->op == "||")),
        rhs_check->getPrevNode());
    phi_res->addIncoming(right_val, after_check->getPrevNode());
    
    return phi_res;
}

llvm::BasicBlock* CodeGenerator::createBlockFromStatement(CGScopeInfo& scope_info, AbstractSyntaxTree::Statement* statement, const std::string& name, bool merge)
{
    if(statement == nullptr) return nullptr;

    // save the last insert point
    llvm::BasicBlock* last_insert_block = _builder.GetInsertBlock();

    if(statement->getStatementType() == AbstractSyntaxTree::Statement::statement_type::statement_block)
    {
        auto* val = codegen(scope_info, static_cast<AbstractSyntaxTree::StatementBlock*>(statement), merge);
        if(val == nullptr) return nullptr;
    
        auto* block = llvm::dyn_cast_or_null<llvm::BasicBlock>(val);
        
        if (block == nullptr) // Technically, never should happen
            ErrorRegister::reportError(new CodeGenError(
                "Expected a code block - generated a non basic block value",
                statement->getPosition()
            ));
        
        block->setName(name);
        
        this->_builder.SetInsertPoint(last_insert_block);

        return block;
    }

    // create the new block
    llvm::BasicBlock* block = llvm::BasicBlock::Create(
        this->_context,
        name,
        scope_info.functions_data.top().func_val,
        scope_info.functions_data.top().return_block
    );

    if(merge)
        mergeLastBlockInto(scope_info, block);

    this->_builder.SetInsertPoint(block);

    ScopeObject obj(scope_info);

    // push a new allocation list
    scope_info.functions_data.top().alloc_objs.push_front({});

    auto* val = codegen(scope_info, statement);
    if(val == nullptr) return nullptr;

    // deinitialize any variables declared in the inner scope
    createDestructorCalls(scope_info, scope_info.functions_data.top().alloc_objs.front());
    createReleaseCalls(scope_info, scope_info.functions_data.top().creation_objs.front());

    // pop the inserted allocation list
    scope_info.functions_data.top().alloc_objs.pop_front();

    // deinitialize any variables declared in this scope
    createDestructorCalls(scope_info, scope_info.functions_data.top().alloc_objs.front());
    createReleaseCalls(scope_info, scope_info.functions_data.top().creation_objs.front());

    this->_builder.SetInsertPoint(last_insert_block);
        
    return block;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::FunctionDefinition* node)
{
    if (initLLVMType(node->getInputType()) == nullptr) return nullptr;
    if (initLLVMType(node->getReturnType()) == nullptr) return nullptr;
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    llvm::FunctionType* fun_type = llvm::dyn_cast_or_null<llvm::FunctionType>(node->getSynthesizedType()->getLLVMType());
    
    if(fun_type == nullptr) return nullptr;

    string func_name = node->getFunctionName()->getIdentifier();
    ManglingObj manglig_obj(_name_mangler, func_name, node->getSynthesizedType());

    // create the function
    llvm::Function* func = llvm::cast_or_null<llvm::Function>(this->_curr_module->getOrInsertFunction(
		func_name,
        fun_type
    ).getCallee());

    // report an error if anything went wrong
    if(func == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            "Invalid creation of function",
            node->getPosition(),
            "LLVM failed to create the function"
        ));
        
        return nullptr;
    }

    // set the function to externally link with its body
    func->setLinkage(llvm::GlobalValue::LinkageTypes::ExternalLinkage);
    
    node->setLLVMValue(func);

	// If the function is externed, then it can't have a body
	if (node->getProperties().is_extern) return func;

    // Create an alias to "main" function otherwise, mangle the name itself
    if (scope_info.isInGlobalScope() && func_name == "main")
        llvm::GlobalAlias::create(
            fun_type, 0, llvm::GlobalValue::LinkageTypes::ExternalLinkage,
            _name_mangler.getMangaledName(), func);
    else
        func->setName(_name_mangler.getMangaledName());        

    // add the current function
    scope_info.functions_data.push({func, node, nullptr, nullptr, nullptr, nullptr, 0});

    llvm::BasicBlock* body = nullptr;
    llvm::BasicBlock* last_insert_point = _builder.GetInsertBlock();
    llvm::BasicBlock* inner_entry = nullptr, *return_block = nullptr;

    // codegen the body of the function
    {
        ScopeObject obj(scope_info, node->getInputType()->getSymbolTable());

        // pass the parameters by value (copy into the stack)
        llvm::BasicBlock* entry = allocaParams(scope_info, node, func);
        _builder.SetInsertPoint(entry);

        // if the function is a constructor, implement default construction
        if (node->getFunctionName()->getIdentifier() == CONSTRUCTOR_KEYWORD)
            insertDefaultConstructorImpl(
                scope_info, 
                static_cast<CustomType*>(node->getParentType()->getSynthesizedType()),
                func->getArg(0)
            );

        llvm::BasicBlock* entry_lang = allocaLangParams(scope_info, node, func);
        _builder.SetInsertPoint(entry_lang);

        // The allocation of the alloca entry block
        inner_entry = llvm::BasicBlock::Create(
            this->_context,
            node->getFunctionName()->getIdentifier() + "_alloca_entry",
            scope_info.functions_data.top().func_val
        );

        if(inner_entry == nullptr)
        {
            ErrorRegister::reportError(new CodeGenError(
                "Invalid function structure",
                node->getPosition(),
                "The allocation block couldn't be created"
            ));
            
            return nullptr;
        }

        scope_info.functions_data.top().allocation_block = inner_entry;

        // Merging blocks
        mergeLastBlockInto(scope_info, inner_entry);
        _builder.SetInsertPoint(inner_entry);

        // The allocation of the return block
        return_block = llvm::BasicBlock::Create(
            this->_context,
            node->getFunctionName()->getIdentifier() + "_return_block",
            scope_info.functions_data.top().func_val
        );

        if(return_block == nullptr)
        {
            ErrorRegister::reportError(new CodeGenError(
                "Invalid function structure",
                node->getPosition(),
                "The return block couldn't be created"
            ));
            
            return nullptr;
        }

        _builder.SetInsertPoint(return_block);

        // if the function is a destructor, implement default destruction
        if (node->getFunctionName()->getIdentifier() == DESTRUCTOR_KEYWORD)
            insertDefaultDestructorImpl(
                scope_info, 
                static_cast<CustomType*>(node->getParentType()->getSynthesizedType()),
                func->getArg(0)
            );

        // codegen the return block
        insertReturnBlockImpl(scope_info, node, return_block);

        // codegen the body of the function
        body = createBlockFromStatement(scope_info, node->getCommands(), "entry_block", true);
        
        if (body == nullptr)
        {
            ErrorRegister::reportError(new CodeGenError(
                "Invalid function structure",
                node->getPosition(),
                "The body of the function did not codegen into a code block"
            ));
            
            return nullptr;
        }

        // check if the last basic block in the function has a terminator
        this->_builder.SetInsertPoint(body = func->getBasicBlockList().back().getPrevNode());

        // If in no point there isn't a return statement
        if(!scope_info.functions_data.top().ret_count)
            this->_builder.CreateBr(scope_info.functions_data.top().return_block);
    }
    
    // automatically generate void returns
    /*if (func->getReturnType()->isVoidTy() && body->getTerminator() == nullptr)
    {
        if(node->getProperties().is_variadic)
            this->_builder.CreateCall(VarArg::createOrGetEndDec(this->_context, this->_curr_module.get()),
                {scope_info.functions_data.top().variadic_list});
        this->_builder.CreateRetVoid();
    }*/

    //verify the function
    if (llvm::verifyFunction(*func, &this->_llvm_err_register))
    {
        ErrorRegister::reportError(new CodeGenError(
            "LLVM error in function " + node->getFunctionName()->getIdentifier() + ": " + this->_llvm_err,
            node->getPosition()
        ));
    }

    // remove the function
    scope_info.functions_data.pop();

    // set the insert point to be the previous insert point
    this->_builder.SetInsertPoint(last_insert_point);

    return func;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::NamespaceDefinition* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    ScopeObject obj(scope_info, node->getSymbolTable());
    ManglingObj manglig_obj(_name_mangler, node->getIdentifier()->getIdentifier(), node->getSynthesizedType());

    for (auto* command : node->getCommands())
        codegen(scope_info, command);

    node->setLLVMValue(llvm::UndefValue::get(llvm::Type::getVoidTy(this->_context)));

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::TypeDefinition* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    ScopeObject obj(scope_info, node->getFunctionsSymbolTable());
    ManglingObj manglig_obj(_name_mangler, node->getTypeName()->getIdentifier(), node->getSynthesizedType());

    node->setLLVMValue(llvm::UndefValue::get(llvm::Type::getVoidTy(this->_context)));

    // codegen the functions
    for (auto* func : node->getTypeFunctions())
        codegen(scope_info, func);

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getLeftExpression()) == nullptr) return nullptr;
    if (initLLVMType(node->getRightExpression()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();
    if (node->getLeftExpression() == nullptr) return nullptr;

    // codegen the referenced node
    return constrainValueToModule(
            this->_curr_module.get(), 
            codegen(scope_info, node->getReferencedNode()
        ));
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::MemberAccessExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getLeftExpression()) == nullptr) return nullptr;
    if (initLLVMType(node->getRightExpression()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    switch (node->getAccessType())
    {
        case AbstractSyntaxTree::MemberAccessExpression::AccessType::index_acs:
        {
            llvm::Value *left_val = nullptr, *index_val = nullptr;

            if ((left_val = codegen(scope_info, node->getLeftExpression())) == nullptr)
                return nullptr;

            if ((index_val = codegen(scope_info, node->getRightExpression())) == nullptr)
                return nullptr;

            // Cast the index if neccessary
            index_val = createCastIfNeccessary(node->getRightExpression()->getPosition(),
                                    index_val, node->getRightExpression()->getSynthesizedType(),
                                    TypeSystemInfo::getGlobalContainer()->getType("u64"));

            // get the pointer to the element at the index specified by the right expression and load it
            node->setLLVMValue(
                _builder.CreateLoad(
                    _builder.CreateGEP(
                        left_val,
                        index_val
                    )
                )
            );
            break;
        }
        case AbstractSyntaxTree::MemberAccessExpression::AccessType::pointer_member_access:
        {
            llvm::Value* left_val = nullptr;

            if (node->getRightExpression()->getExpressionType() == AbstractSyntaxTree::Expression::expression_type::call_expression)
            {
                if((left_val = codegen(scope_info, node->getLeftExpression())) == nullptr) return nullptr;

                auto* call_expr = static_cast<AbstractSyntaxTree::CallExpression*>(node->getRightExpression());

                auto* ty = node->getLeftExpression()->getSynthesizedType();

                if (ty->getType() != SynthesizedType::type_category::pointer_type)
                {
                    ErrorRegister::reportError(new CodeGenError(
                        "Cannot dereference a non pointer type",
                        node->getLeftExpression()->getPosition()
                    ));
                    return nullptr;                    
                }

                ty = static_cast<PointerType*>(ty)->getBaseType();

                if (ty->getType() != SynthesizedType::type_category::custom_type)
                {
                    ErrorRegister::reportError(new CodeGenError(
                        "Cannot call a function of a non type",
                        node->getLeftExpression()->getPosition()
                    ));
                    return nullptr;
                }

                string identifier = static_cast<AbstractSyntaxTree::IdentifierExpression*>(call_expr->getFunctionExpression())->getIdentifier();

                auto* custom_ty = static_cast<CustomType*>(ty);

                auto* func = custom_ty->getDefinition()->getFunctionsSymbolTable().at(identifier);

                if (codegen(scope_info, func) == nullptr) return nullptr;

                ScopeObject obj(scope_info, custom_ty->getDefinition()->getFunctionsSymbolTable());
                node->setLLVMValue(codegen(scope_info, call_expr, left_val));
            }
            else
            {
                // codegen the left value and extract the value at the index stored in the access node
                if((left_val = codegen(scope_info, node->getLeftExpression())) == nullptr) return nullptr;

                node->setLLVMValue(
                    _builder.CreateLoad(
                        _builder.CreateConstInBoundsGEP2_32(
                            nullptr,
                            left_val, 
                            0, 
                            node->getAccessIndex()
                        )
                    )
                );
            }

            break;
        }
        case AbstractSyntaxTree::MemberAccessExpression::AccessType::member_acs:
        {
            llvm::Value* left_val = nullptr;

            if (node->getRightExpression()->getExpressionType() == AbstractSyntaxTree::Expression::expression_type::call_expression)
            {
                if((left_val = codegen_modifiable(scope_info, node->getLeftExpression())) == nullptr) return nullptr;

                auto* call_expr = static_cast<AbstractSyntaxTree::CallExpression*>(node->getRightExpression());

                auto* ty = node->getLeftExpression()->getSynthesizedType();

                if (ty == nullptr || ty->getType() != SynthesizedType::type_category::custom_type)
                {
                    ErrorRegister::reportError(new CodeGenError(
                        "Cannot call a function of a non type",
                        node->getLeftExpression()->getPosition()
                    ));
                    return nullptr;
                }

                string identifier = static_cast<AbstractSyntaxTree::IdentifierExpression*>(call_expr->getFunctionExpression())->getIdentifier();

                auto* custom_ty = static_cast<CustomType*>(ty);

                auto* func = custom_ty->getDefinition()->getFunctionsSymbolTable().at(identifier);

                if (codegen(scope_info, func) == nullptr) return nullptr;

                ScopeObject obj(scope_info, custom_ty->getDefinition()->getFunctionsSymbolTable());
                node->setLLVMValue(codegen(scope_info, call_expr, left_val));
            }
            else
            {
                // codegen the left value and extract the value at the index stored in the access node
                if((left_val = codegen(scope_info, node->getLeftExpression())) == nullptr) return nullptr;

                node->setLLVMValue(_builder.CreateExtractValue(left_val, {node->getAccessIndex()}));
            }

            break;
        }
        default:
            ErrorRegister::reportError(new CodeGenError(
                "Invalid member access operator",
                node->getPosition()
            ));

            break;
    }

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::VarArgsExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getTypeToGet()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    if(scope_info.functions_data.empty() || scope_info.functions_data.top().func_val == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            "Invalid placement of variadic call",
            node->getPosition()
        ));
        return nullptr;
    }
    else if (scope_info.functions_data.top().func_def == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            "Function type can't be accessed for variadic call",
            node->getPosition()
        ));
        return nullptr;
    }
    else if(!scope_info.functions_data.top().func_def->getProperties().is_variadic && 
                scope_info.functions_data.top().variadic_list != nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            "Function without variadic variables can't access variadic calls",
            node->getPosition(),
            "This variadic call "
        ));
        return nullptr;
    }

    node->setLLVMValue(this->_builder.CreateVAArg(scope_info.functions_data.top().variadic_list, node->getSynthesizedType()->getLLVMType()));

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::TypeInstatiation* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getParameters()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    auto* ptr = codegen_modifiable(scope_info, node);

    if (ptr == nullptr)
        return nullptr;

    return _builder.CreateLoad(ptr);
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::CreateFunctionExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getCreationType()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    auto* inner_type = node->getCreationType();
    this->_gc.getOrCreateSafePtrType(node->getSynthesizedType()->getGCName(), inner_type->getSynthesizedType()->getLLVMType());

    auto* create_func = this->_gc.getOrCreateSafeCreateFunction(node->getSynthesizedType()->getGCName());
    auto* created_ptr = this->_builder.CreateCall(llvm::FunctionCallee(create_func));

    // Push back the tmp value -> A strong pointer by default
    scope_info.functions_data.top().creation_objs.front().push_back({static_cast<StrongPointerType*>(node->getSynthesizedType()), created_ptr, true});

    // if the type isn't a custom type then just asign the value
    if (node->getCreationType()->getSynthesizedType()->getType() != SynthesizedType::type_category::custom_type)
    {
        auto* type = static_cast<BasicType*>(node->getCreationType()->getSynthesizedType());
        if(!node->getCreationType()->getParameters()->getExpressions().empty())
        {
            if(codegen(scope_info, node->getCreationType()->getParameters()->getExpressions()[0]) == nullptr) return nullptr;
            
            SynthesizedType::def_safe_assignment(
                created_ptr, node->getCreationType()->getParameters()->getExpressions()[0]->getLLVMValue(),
                node->getSynthesizedType(), node->getCreationType()->getParameters()->getExpressions()[0]->getSynthesizedType(),
                this->_builder,
                this->_gc
            );
        }
    }
    else // if the type is a custom type then call to the constructor
    {
        auto* type = static_cast<CustomType*>(node->getCreationType()->getSynthesizedType());

        // create parameters vector
        std::vector<llvm::Value*> params;

        // add the pointer to the instance
        params.push_back(SynthesizedType::def_get_inner_value(
            nullptr, created_ptr,
            node->getSynthesizedType(), node->getCreationType()->getSynthesizedType(),
            this->_builder,
            this->_gc
        ));

        // add the rest of the parameters
        for (auto* expr : node->getCreationType()->getParameters()->getExpressions())
            params.push_back(codegen(scope_info, expr));

        // call the constructor
        createConstructorCall(scope_info, type, params);
    }

    node->setLLVMValue(created_ptr);
    
    return created_ptr;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::NewFunctionExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getCreationType()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    auto* mallocation_type = node->getCreationType()->getSynthesizedType()->getLLVMType(this->_context, this->_gc);    
    auto size_val = _target_machine->createDataLayout().getTypeAllocSize(mallocation_type);
    auto* int_type = llvm::Type::getInt32Ty(this->_context); // May be 64
    auto* malloc_size = llvm::ConstantExpr::getTruncOrBitCast(this->_builder.getInt64(size_val.getFixedSize()), int_type);
    
    auto* malloc_ptr = this->_builder.Insert(llvm::CallInst::CreateMalloc(this->_builder.GetInsertBlock(),
            int_type, mallocation_type, malloc_size,
            nullptr, nullptr, ""));

    // if the type isn't a custom type then just asign the value
    if (node->getCreationType()->getSynthesizedType()->getType() != SynthesizedType::type_category::custom_type)
    {
        auto* type = static_cast<BasicType*>(node->getCreationType()->getSynthesizedType());
        if(!node->getCreationType()->getParameters()->getExpressions().empty())
        {
            if(codegen(scope_info, node->getCreationType()->getParameters()->getExpressions()[0]) == nullptr) return nullptr;
            this->_builder.CreateStore(node->getCreationType()->getParameters()->getExpressions()[0]->getLLVMValue(),
                malloc_ptr);
        }
    }
    else // if the type is a custom type then call to the constructor
    {
        auto* type = static_cast<CustomType*>(node->getCreationType()->getSynthesizedType());

        // create parameters vector
        std::vector<llvm::Value*> params;

        // add the pointer to the instance
        params.push_back(malloc_ptr);

        // add the rest of the parameters
        for (auto* expr : node->getCreationType()->getParameters()->getExpressions())
            params.push_back(codegen(scope_info, expr));

        // call the constructor
        createConstructorCall(scope_info, type, params);
    }

    node->setLLVMValue(malloc_ptr);
    
    return malloc_ptr;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::DeleteFunctionStatement* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getDeletingObj()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    // The inner type has to be a pointer type
    if (node->getDeletingObj()->getSynthesizedType()->getType() != SynthesizedType::type_category::pointer_type) return nullptr;

    auto* deleting_obj = node->getDeletingObj();
    auto* deleting_type = static_cast<PointerType*>(node->getDeletingObj()->getSynthesizedType());
    if (codegen(scope_info, deleting_obj) == nullptr) return nullptr;

    // Create if not nullptr blocks
    auto* true_block = llvm::BasicBlock::Create(this->_context, "if_not_null", scope_info.functions_data.top().func_val, scope_info.functions_data.top().return_block);
    auto* after_block = llvm::BasicBlock::Create(this->_context, "after_null_block", scope_info.functions_data.top().func_val, scope_info.functions_data.top().return_block);

    // Creating the comparison
    auto* null_cmp = this->_builder.CreateICmpNE(node->getDeletingObj()->getLLVMValue(), llvm::Constant::getNullValue(node->getDeletingObj()->getSynthesizedType()->getLLVMType()));
    this->_builder.CreateCondBr(null_cmp, true_block, after_block);

    this->_builder.SetInsertPoint(true_block);

    // Call to the destructor before freeing the poointer if it is a custom type pointer
    if (deleting_type->getBaseType()->getType() == SynthesizedType::type_category::custom_type)
        createDestructorCalls(scope_info, 
            {{static_cast<CustomType*>(deleting_type->getBaseType()), deleting_obj->getLLVMValue()}});

    // Calling the free function
    auto* free_func = this->_builder.Insert(llvm::CallInst::CreateFree(node->getDeletingObj()->getLLVMValue(), this->_builder.GetInsertBlock()));
    
    // Setting the new insert point
    this->_builder.CreateBr(after_block);
    this->_builder.SetInsertPoint(after_block);

    node->setLLVMValue(llvm::UndefValue::get(node->getSynthesizedType()->getLLVMType()));

    return node->getLLVMValue();
}


llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::VariableDeclaration* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getType()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();
    
    if(this->_builder.GetInsertBlock() ==  nullptr)
    {        
        // the variables are global
        for (AbstractSyntaxTree::VariableDeclarationComponent* decl : node->getVariables())
        {
            if (decl->getLLVMValue() == nullptr)
            {
                // make sure that the llvm type is initialized
                initLLVMType(decl);

                if (!addGlobalVariable(scope_info, decl)) return nullptr;
            }
        }
    }
    else
    {
        CustomType* custom_type = nullptr;
        StrongPointerType* strong_ptr_type = nullptr;
        WeakPointerType* weak_ptr_type = nullptr;
        if (node->getType()->getSynthesizedType()->getType() == SynthesizedType::type_category::custom_type)
            custom_type = static_cast<CustomType*>(node->getType()->getSynthesizedType());
        else if (node->getType()->getSynthesizedType()->getType() == SynthesizedType::type_category::strong_pointer_type)
            strong_ptr_type = static_cast<StrongPointerType*>(node->getType()->getSynthesizedType());
        else if (node->getType()->getSynthesizedType()->getType() == SynthesizedType::type_category::weak_pointer_type)
            weak_ptr_type = static_cast<WeakPointerType*>(node->getType()->getSynthesizedType());

        {
            ScopeObject inner_scope(scope_info, false, node->getSymbolTable());

            // The variable is inside a function
            for (auto* var : node->getVariables())
            {
                if (var->getLLVMValue() == nullptr)
                {
                    initLLVMType(var);
                    if (var->getInitialization() != nullptr &&
                        var->getInitialization()->getExpressionType() == AbstractSyntaxTree::Expression::expression_type::type_instantiation)
                    {
                        // do not allocate memory twice if the expression being assigned is a type instantiation
                        if (codegen_modifiable(scope_info, var->getInitialization()) != nullptr)
                        {
                            // set the value of this node to be the value of the instantiation and rename it
                            var->setLLVMValue(var->getInitialization()->getLLVMValue());
                            var->getIdentifier()->setLLVMValue(var->getLLVMValue());

                            var->getLLVMValue()->setName(var->getIdentifier()->getIdentifier());
                        }
                    }
                    else 
                    {
                        // Creating the allocations in the beginning inorder to optimize the runtime
                        auto* last_ins_block = this->_builder.GetInsertBlock();
                        this->_builder.SetInsertPoint(&(scope_info.functions_data.top().allocation_block->back()));

                        // create a stack allocation
                        var->setLLVMValue(this->_builder.CreateAlloca(
                            var->getSynthesizedType()->getLLVMType(), 
                            nullptr, 
                            var->getIdentifier()->getIdentifier()
                        ));
                        var->getIdentifier()->setLLVMValue(var->getLLVMValue());

                        this->_builder.SetInsertPoint(last_ins_block);

                        // create the initialization (if it exists)
                        if (var->getInitialization() != nullptr)
                        {
                            auto* init_val = codegen(scope_info, var->getInitialization());

                            if (init_val == nullptr) return nullptr;

                            // Checks if can be casted and casts it
                            if ((init_val = var->getInitialization()->getSynthesizedType()->CreateCast(
                                    var->getSynthesizedType(),
                                    init_val, var->getLLVMValue()->getType()->getPointerElementType(),
                                    _builder,
                                    this->_gc
                                )) == nullptr)
                            {
                                reportCantCast(
                                    var->getInitialization()->getSynthesizedType(),
                                    var->getSynthesizedType(),
                                    var->getInitialization()->getPosition());
                                
                                return nullptr;
                            };

                            // Add value undefined casts warning
                            warningIfUndefOrPoison("The implicit cast creates undefined/poisoned llvm value",
                                init_val, var->getInitialization()->getPosition());

                            // store the value inside the allocated stack memory
                            this->_builder.CreateStore(init_val, var->getLLVMValue());

                            // safe pointer retaining
                            if (strong_ptr_type != nullptr)
                            {
                                auto* retain_func = this->_gc.getOrCreateRetainFunction(true);
                                this->_builder.CreateCall(retain_func, {this->_builder.CreateBitCast(init_val, this->_builder.getInt8PtrTy())});
                            }
                            else if (weak_ptr_type != nullptr)
                            {
                                auto* retain_func = this->_gc.getOrCreateRetainFunction(false);
                                this->_builder.CreateCall(retain_func, {this->_builder.CreateBitCast(init_val, this->_builder.getInt8PtrTy())});                            
                            }
                        }
                        else
                        {
                            // if there is no initialization, check if the type is a custom type and call
                            // the default constructor if needed
                            if (custom_type != nullptr)
                                createConstructorCall(scope_info, custom_type, { var->getLLVMValue() });
                            else if (strong_ptr_type != nullptr)
                            {
                                auto* wraped_ptr = SynthesizedType::wrap_pointer(
                                    nullptr, llvm::ConstantPointerNull::get(strong_ptr_type->getBaseType()->getLLVMType()->getPointerTo()),
                                    TypeSystemInfo::getGlobalContainer()->getOrCreatePointerType(strong_ptr_type->getBaseType()),
                                    nullptr, this->_builder, this->_gc
                                );

                                this->_gc.getOrCreateRetainFunction(true);
                                scope_info.functions_data.top().creation_objs.front().push_back({strong_ptr_type, wraped_ptr, true});
                            }
                            else if (weak_ptr_type != nullptr)
                            {
                                auto* wraped_ptr = SynthesizedType::wrap_pointer(
                                    nullptr, llvm::ConstantPointerNull::get(weak_ptr_type->getBaseType()->getLLVMType()->getPointerTo()),
                                    TypeSystemInfo::getGlobalContainer()->getOrCreatePointerType(weak_ptr_type->getBaseType()),
                                    nullptr, this->_builder, this->_gc
                                );

                                this->_gc.getOrCreateRetainFunction(false);
                                scope_info.functions_data.top().creation_objs.front().push_back({weak_ptr_type, wraped_ptr, false});
                            }
                        }
                    }

                    // extend the lifetime of the assigned value if the type is custom
                    if (var->getInitialization() != nullptr && custom_type != nullptr &&
                        scope_info.functions_data.top().alloc_objs.front().size() > 0)
                    {
                        scope_info.functions_data.top().alloc_objs.front().pop_back();
                    }
                }
            }
        }

        // if the type is a custom type, add the variables to the initialization list
        if (custom_type != nullptr)
            for (auto* var : node->getVariables())
                (++(scope_info.functions_data.top().alloc_objs.begin()))->push_back({ custom_type, var->getLLVMValue() });
        else if (strong_ptr_type != nullptr)
            for (auto* var : node->getVariables())
                (++(scope_info.functions_data.top().creation_objs.begin()))->push_back({ strong_ptr_type, var->getLLVMValue(), true, false });
        else if (weak_ptr_type != nullptr)
            for (auto* var : node->getVariables())
                (++(scope_info.functions_data.top().creation_objs.begin()))->push_back({ weak_ptr_type, var->getLLVMValue(), false, false });
    }

    node->setLLVMValue(llvm::UndefValue::get(node->getSynthesizedType()->getLLVMType()));
    
    return node->getLLVMValue(); // A variable declaration does not have an llvm representation
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::ComplexTypeComponent* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    return nullptr;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::ReturnStatement* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getReturnValue() != nullptr && initLLVMType(node->getReturnValue()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    // check if the block already has a terminator
    if (this->_builder.GetInsertBlock() != nullptr && 
        this->_builder.GetInsertBlock()->getTerminator() != nullptr)
        return nullptr;

    if (node->getReturnValue() == nullptr)
    {
        if(scope_info.functions_data.top().func_def->getProperties().is_variadic)
            this->_builder.CreateCall(VarArg::createOrGetEndDec(this->_context, this->_curr_module.get()),
                {scope_info.functions_data.top().variadic_list});
        
        createReleaseCalls(scope_info, scope_info.functions_data.top());

        // The return statement is actually a jump to the return block
        node->setLLVMValue(this->_builder.CreateBr(scope_info.functions_data.top().return_block));
    }
    else
    {
        auto* right_val = codegen(scope_info, node->getReturnValue());

        // Checks if can be casted and casts it
        if ((right_val = node->getReturnValue()->getSynthesizedType()->CreateCast(
                scope_info.functions_data.top().func_def->getReturnType()->getSynthesizedType(),
                right_val, scope_info.functions_data.top().func_val->getReturnType(),
                _builder,
                this->_gc
            )) == nullptr)
        {
            reportCantCast(
                node->getReturnValue()->getSynthesizedType(),
                scope_info.functions_data.top().func_def->getReturnType()->getSynthesizedType(),
                node->getReturnValue()->getPosition());
            
            return nullptr;
        };

        // Add value undefined casts warning
        warningIfUndefOrPoison("The implicit cast creates undefined/poisoned llvm value",
            right_val, node->getReturnValue()->getPosition());


        // If the returned value is a strong safe ptr, then retain it to save its value
        if(node->getReturnValue()->getSynthesizedType()->getType() == SynthesizedType::type_category::strong_pointer_type)
        {
            this->_builder.CreateCall(
                this->_gc.getOrCreateRetainFunction(true),
                {_builder.CreateBitCast(node->getReturnValue()->getLLVMValue(), _builder.getInt8PtrTy())}
            );
        }
        // if the returned value is a custom type, extend the lifetime of the value
        else if (node->getReturnValue()->getSynthesizedType()->getType() == SynthesizedType::type_category::custom_type)
        {
            if (scope_info.functions_data.top().alloc_objs.front().size() > 0)
            {
                scope_info.functions_data.top().alloc_objs.front().pop_back();
            }
        }

        if(scope_info.functions_data.top().func_def->getProperties().is_variadic)
        {
            this->_builder.CreateCall(VarArg::createOrGetEndDec(this->_context, this->_curr_module.get()),
                {scope_info.functions_data.top().variadic_list});
        }

        // create destructor and release calls for all objects in this functions scope
        createDestructorCalls(scope_info, scope_info.functions_data.top());
        createReleaseCalls(scope_info, scope_info.functions_data.top());
        
        // The return statement is actually a jump to the return block
        // Also the value is stored on a different allocated obj
        _builder.CreateStore(right_val, scope_info.functions_data.top().return_allocation);
        node->setLLVMValue(this->_builder.CreateBr(scope_info.functions_data.top().return_block));
    }

    // Raise the count of the return amount
    scope_info.functions_data.top().ret_count++;

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::Expression* node)
{
    if (node == nullptr) return nullptr;

    switch (node->getExpressionType())
    {
    case AbstractSyntaxTree::Expression::expression_type::call_expression:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::CallExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::cast_expression:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::CastExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::member_access:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::MemberAccessExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::namespace_access_expr:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::NamespaceAccessExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::vargs_expression:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::VarArgsExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::type_instantiation:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::TypeInstatiation*>(node));
    case AbstractSyntaxTree::Expression::expression_type::new_function_expression:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::NewFunctionExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::create_function_expression:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::CreateFunctionExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::identifier_expression:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::IdentifierExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::literal_expression:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::LiteralExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::expression_list:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::ExpressionList*>(node));
    case AbstractSyntaxTree::Expression::expression_type::binary_operation:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::BinaryOperatorExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::unary_operation:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::UnaryOperatorExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::ternary_expression:
        //return codegen(scope_info, static_cast<AbstractSyntaxTree::TernaryExpression*>(node));
    default:
        ErrorRegister::reportError(new CodeGenError(
            "Encountered an invalid node",
            node->getPosition()
        ));
        break;
    }

    return nullptr;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getRightExpression()) == nullptr) return nullptr;
    if (initLLVMType(node->getLeftExpression()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    const auto& operator_synthesis = TypeSystemInfo::operators_type_synthesis.find(node->getOperator());
    Operator* op = OperatorsInfo::inst().getOperator(node->getOperator());

    // Check if the operator is defined in the language
    if(op == nullptr ||
        operator_synthesis == TypeSystemInfo::operators_type_synthesis.end())
        ErrorRegister::reportError(new CodeGenError(
            string("Operator ") + node->getOperator() + " is not specified",
            node->getPosition()
        ));

    if(op->op == "&&" || op->op == "||")
    {
        return CodegenLogicalBinaryOperation(scope_info, node, op);
    }

    llvm::Value* left_val = nullptr, * right_val = nullptr;

    // codegen the left value
    if (op->bin_mods.left_input == operator_mod::reference)
        left_val = codegen_modifiable(scope_info, node->getLeftExpression());
    else left_val = codegen(scope_info, node->getLeftExpression());
    
    if (left_val == nullptr) return nullptr;

    // codegen the right value
    if (op->bin_mods.right_input == operator_mod::reference)
        right_val = codegen_modifiable(scope_info, node->getRightExpression());
    else right_val = codegen(scope_info, node->getRightExpression());

    if (right_val == nullptr) return nullptr;

    llvm::Value* left_prev_val = nullptr;

    if(op->op == "=") left_prev_val = this->_builder.CreateLoad(left_val);
    
    AbstractSyntaxTree::Expression* source_expr = nullptr, *dest_expr = nullptr;
    llvm::Value* source_val = nullptr, *dest_val = nullptr;
    
    /*
    
        llvm::Type* source_n_type = nullptr;
        source_n_type = (op->bin_mods.left_input == operator_mod::reference ||
                        op->bin_mods.left_input == operator_mod::pointer) ?
                            left_val->getType()->getPointerElementType() :
                            left_val->getType();
    */
    switch (operator_synthesis->second.at(1))
    {
    case TypeSystemInfo::type_synthesis_action::boolean:
    case TypeSystemInfo::type_synthesis_action::left:
        source_expr = node->getLeftExpression();
        dest_expr = node->getRightExpression();
        source_val = left_val;
        dest_val = right_val;
        break;
    case TypeSystemInfo::type_synthesis_action::right:
        source_expr = node->getRightExpression();
        dest_expr = node->getLeftExpression();
        source_val = right_val;
        dest_val = left_val;
        break;
    default:
        return nullptr; // Already being checked
    }

    // codegen the operation
    auto* res = source_expr->getSynthesizedType()->codegen(
        *op, 
        dest_expr->getSynthesizedType(), 
        source_val, dest_val, this->_builder, this->_gc);

    // No cast throwing
    if (res == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            string("Operator '") + node->getOperator() + string("' does not match ") + node->getRightExpression()->getSynthesizedType()->getName() + 
            string(" and ") + node->getLeftExpression()->getSynthesizedType()->getName(),
            node->getPosition()
        ));

        return nullptr;
    }
    
    // Add value undefined casts warning
    warningIfUndefOrPoison("The implicit codegen creates undefined/poisoned llvm value",
        res, source_expr->getPosition());

    // Retaining and releasing safe ptrs by operator "="
    bool left_strong = node->getLeftExpression()->getSynthesizedType()->getType() == SynthesizedType::type_category::strong_pointer_type, right_strong = false;
    if(op->op == "=")
    {
        if ((left_strong = node->getLeftExpression()->getSynthesizedType()->getType() == SynthesizedType::type_category::strong_pointer_type) ||
            node->getLeftExpression()->getSynthesizedType()->getType() == SynthesizedType::type_category::weak_pointer_type)
        {
            this->_builder.CreateCall(
                this->_gc.getOrCreateRetainFunction(left_strong),
                { this->_builder.CreateBitCast(right_val, this->_builder.getInt8PtrTy())}
            );
        }

        if ((right_strong = node->getRightExpression()->getSynthesizedType()->getType() == SynthesizedType::type_category::strong_pointer_type) ||
            node->getRightExpression()->getSynthesizedType()->getType() == SynthesizedType::type_category::weak_pointer_type)
        {
            auto* base_type = (left_strong) ?
            static_cast<StrongPointerType*>(node->getLeftExpression()->getSynthesizedType())->getBaseType():
            static_cast<WeakPointerType*>(node->getLeftExpression()->getSynthesizedType())->getBaseType();

            if (base_type->getType() == SynthesizedType::type_category::custom_type)
            {
                auto* ty = static_cast<CustomType*>(base_type);
                const auto& destructor = ty->getDefinition()->getFunctionsSymbolTable().find(DESTRUCTOR_KEYWORD);

                if (destructor != ty->getDefinition()->getFunctionsSymbolTable().end())
                {
                    llvm::Value* func_val = nullptr;

                    if ((func_val = codegen(scope_info, destructor->second)) == nullptr) 
                        return nullptr;
                    
                    func_val = constrainValueToModule(this->_curr_module.get(), func_val);

                    _builder.CreateCall(
                        this->_gc.getOrCreateReleaseFunction(left_strong),
                        {
                            this->_builder.CreateBitCast(left_prev_val, this->_builder.getInt8PtrTy()),
                            this->_builder.CreateBitCast(func_val, this->_builder.getInt8PtrTy())
                        }
                    );
                }   
            }
            else
            {
                _builder.CreateCall(
                    this->_gc.getOrCreateReleaseFunction(left_strong),
                    { this->_builder.CreateBitCast(left_prev_val, this->_builder.getInt8PtrTy()), llvm::Constant::getNullValue(llvm::Type::getInt8PtrTy(this->_context)) }
                );
            }
        }
    }

    node->setLLVMValue(op->bin_mods.output == operator_mod::reference ? _builder.CreateLoad(res) : res);

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::UnaryOperatorExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getExpression()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    Operator* op = OperatorsInfo::inst().getOperator(node->getOperator());

    if (op == nullptr)
        ErrorRegister::reportError(new CodeGenError(
            string("Operator ") + node->getOperator() + " is not specified",
            node->getPosition()
        ));

    llvm::Value* inner_val = nullptr;
    auto modify_obj = (node->isPrefix() ? op->unary_mods.prefix_input : op->unary_mods.postfix_input);
    if (modify_obj == operator_mod::reference)
        inner_val = codegen_modifiable(scope_info, node->getExpression());
    else inner_val = codegen(scope_info, node->getExpression());

    if(inner_val == nullptr) return nullptr;

    auto* res = node->getExpression()->getSynthesizedType()->codegen(
        *op, 
        nullptr, 
        node->isPrefix() ? nullptr : inner_val, 
        node->isPrefix() ? inner_val : nullptr, 
        this->_builder,
        this->_gc
    );

    // add wrapped to the creation list
    if (op->op == "#")
    {
        scope_info.functions_data.top().creation_objs.front().push_back({
            static_cast<StrongPointerType*>(node->getSynthesizedType()),
            res,
            true,
            true
        });
    }

    if (res == nullptr)
        ErrorRegister::reportError(new CodeGenError(
            string("Operator '") + node->getOperator() + string("' does not match ") + 
            node->getExpression()->getSynthesizedType()->getName(),
            node->getPosition()
        ));

    // Add value undefined casts warning
    warningIfUndefOrPoison("The result of this expression is an undefined/poisoned llvm value",
        res, node->getExpression()->getPosition());

    // if the operator is prefix, use the inner value, else, use the resulting value
    if (node->isPrefix())
        node->setLLVMValue(op->unary_mods.prefix_output == operator_mod::reference ? 
                    _builder.CreateLoad(res) : res);
    else
        // if the inner value is being used, the codegenerator needs to check the input instead of the output
        node->setLLVMValue(op->unary_mods.postfix_output == operator_mod::reference ?
                    _builder.CreateLoad(res) : res);

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::IdentifierExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    auto* definition = scope_info.getDefinition(node->getIdentifier());

    // if the symbol is defined in the scope info
    if (definition != nullptr)
    {
        // get the llvm::Value* of the definition
        if(codegen(scope_info, definition) == nullptr) return nullptr;
        auto* var_val = definition->getLLVMValue();
        auto* syn_llvm_type = definition->getSynthesizedType()->getLLVMType();

        var_val = constrainValueToModule(this->_curr_module.get(), var_val);

        // check if the type of the definition is a function or the type is not a pointer
        if (syn_llvm_type == var_val->getType() || definition->getSynthesizedType()->getType() == 
                                                    SynthesizedType::type_category::function_type)
            node->setLLVMValue(var_val);
        else if (syn_llvm_type->getPointerTo() == var_val->getType())
            node->setLLVMValue(_builder.CreateLoad(var_val));
        else 
        {
            ErrorRegister::reportError(new CodeGenError(
                "LLVM type mismatch",
                node->getPosition()
            ));

            return nullptr;
        }

        return node->getLLVMValue();
    }

    // the identifier is not defined in the scope info, throw an error
    ErrorRegister::reportError(new CodeGenError(
        node->getIdentifier() + string(" is not defined"),
        node->getPosition()
    ));

    return nullptr;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::ExpressionList* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    llvm::Value* expr_list = llvm::UndefValue::get(node->getSynthesizedType()->getLLVMType());
    node->setLLVMValue(expr_list);
    unsigned int i = 0;

    for (auto* expr : node->getExpressions())    
        expr_list = this->_builder.CreateInsertValue(expr_list, codegen(scope_info, expr), i++);    

    return expr_list;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::CallExpression* node, llvm::Value* this_ptr)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getArguments()) == nullptr) return nullptr;
    if (initLLVMType(node->getFunctionExpression()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    auto* val = codegen(scope_info, node->getFunctionExpression());

    auto* func_call_type = llvm::dyn_cast_or_null<llvm::FunctionType>(llvm::dyn_cast_or_null<llvm::PointerType>(val->getType())->getElementType());
    auto* func_type = dynamic_cast<FunctionType*>(node->getFunctionExpression()->getSynthesizedType());

    // check if there were any errors
    if (func_call_type == nullptr || func_type == nullptr)
    {
        if (val != nullptr)
            ErrorRegister::reportError(new CodeGenError(
                node->getSynthesizedType()->getName() + " cannot be invoked",
                node->getPosition()
            ));

        return nullptr;
    }

    if (func_type->getInputType() == nullptr) return nullptr;

    std::vector<llvm::Value*> res;
    const auto& args = node->getArguments()->getExpressions();
    llvm::Value* arg_val = nullptr;

    // add the this* parameter to the arguments if it exists
    if (this_ptr != nullptr)
        res.push_back(this_ptr);

    if(func_type->getInputType()->getType() == SynthesizedType::type_category::named_complex_type)
    {
        NamedComplexType* func_input_types = nullptr;
        if ((func_input_types = dynamic_cast<NamedComplexType*>(func_type->getInputType())) == nullptr) return nullptr;
        if (node->getArguments() == nullptr) return nullptr;

        const auto& components = func_input_types->getComponents();

        for (unsigned int i = 0; i < args.size(); i++)
        {
            if((arg_val = codegen(scope_info, args[i])) == nullptr) return nullptr;

            // Insert implicit casts if the values are not variadic
            if(i + (this_ptr != nullptr) < components.size())
            {
                if ((arg_val = args[i]->getSynthesizedType()->CreateCast(
                        components[i + (this_ptr != nullptr)].first,
                        arg_val, components[i + (this_ptr != nullptr)].first->getLLVMType(this->_context, this->_gc),
                        _builder,
                        this->_gc
                    )) == nullptr)
                {
                    reportCantCast(
                        args[i]->getSynthesizedType(),
                        components[i + (this_ptr != nullptr)].first,
                        args[i]->getPosition());
                    
                    return nullptr;
                };

                // Add value undefined casts warning
                warningIfUndefOrPoison("The implicit cast creates undefined/poisoned llvm value",
                    arg_val, args[i]->getPosition());
            }

            res.push_back(arg_val);
        }
    }
    else if(func_type->isVariadic())
    {
        for (unsigned int i = 0; i < args.size(); i++)
        {
            if((arg_val = codegen(scope_info, args[i])) == nullptr) return nullptr;
            res.push_back(arg_val);   
        }
    }

    auto* call_val = this->_builder.CreateCall(
                llvm::FunctionCallee(func_call_type, val),
                res
            );

    if (func_type->getOutputType()->getType() == SynthesizedType::type_category::strong_pointer_type)
        scope_info.functions_data.top().creation_objs.front().push_back({
            static_cast<StrongPointerType*>(func_type->getOutputType()),
            call_val,
            true,
            true
        });
    else if (func_type->getOutputType()->getType() == SynthesizedType::type_category::weak_pointer_type)
        scope_info.functions_data.top().creation_objs.front().push_back({
            static_cast<WeakPointerType*>(func_type->getOutputType()),
            call_val,
            false,
            true
        });

    // set the value of the node
    node->setLLVMValue(call_val);

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::LiteralExpression* node)
{
    if (node == nullptr) return nullptr;

    switch(node->getLiteralType())
    {
    case AbstractSyntaxTree::LiteralExpression::literal_type::bool_type:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::BooleanLiteral*>(node));       
    case AbstractSyntaxTree::LiteralExpression::literal_type::char_type:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::CharLiteral*>(node));              
    case AbstractSyntaxTree::LiteralExpression::literal_type::double_type:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::DoubleLiteral*>(node));               
    case AbstractSyntaxTree::LiteralExpression::literal_type::int_type:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::IntegerLiteral*>(node));               
    case AbstractSyntaxTree::LiteralExpression::literal_type::null_type:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::NullLiteral*>(node));               
    case AbstractSyntaxTree::LiteralExpression::literal_type::string_type:
        return codegen(scope_info, static_cast<AbstractSyntaxTree::StringLiteral*>(node));               
    default:
        ErrorRegister::reportError(new CodeGenError(
            "Encountered an invalid node",
            node->getPosition()
        ));
        break;
    }

    return nullptr;
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::BooleanLiteral* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    node->setLLVMValue(llvm::ConstantInt::get(this->_context, llvm::APInt(node->getSynthesizedType()->getLLVMType()->getIntegerBitWidth(), 
            node->getValue(), true)));

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::CharLiteral* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    node->setLLVMValue(llvm::ConstantInt::get(this->_context, llvm::APInt(node->getSynthesizedType()->getLLVMType()->getIntegerBitWidth(), 
            node->getValue(), true)));

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::IntegerLiteral* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();
    
    node->setLLVMValue(llvm::ConstantInt::get(this->_context, llvm::APInt(node->getSynthesizedType()->getLLVMType()->getIntegerBitWidth(), 
            node->getValue(), true)));

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::DoubleLiteral* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    // NEEDS TO BE FIXED! -> Adding support to 128 floats and etc...
    node->setLLVMValue(llvm::ConstantFP::get(this->_context, llvm::APFloat(node->getValue())));

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::NullLiteral* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    // NEEDS TO BE FIXED! -> Adding support to 128 floats and etc...
    node->setLLVMValue(llvm::Constant::getNullValue(llvm::Type::getInt8PtrTy(this->_context)));

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen(CGScopeInfo& scope_info, AbstractSyntaxTree::StringLiteral* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    // create the string literal
    node->setLLVMValue(
        _builder.CreateGlobalStringPtr(
            node->getValue(), "str", 0, this->_curr_module.get()
        )
    );

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::Node* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    if(node->getNodeType() == AbstractSyntaxTree::Node::node_type::statement)
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::Statement*>(node));
    
    ErrorRegister::reportError(new CodeGenError(
        "Encountered an invalid node",
        node->getPosition()
    ));
    
    return nullptr;
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::Statement* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    if (node->getStatementType() == AbstractSyntaxTree::Statement::statement_type::expression)
    {
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::Expression*>(node));
    }

    ErrorRegister::reportError(new CodeGenError(
        "Encountered an invalid node",
        node->getPosition()
    ));

    return nullptr;
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::Expression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    switch (node->getExpressionType())
    {
    case AbstractSyntaxTree::Expression::expression_type::call_expression:
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::CallExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::member_access:
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::MemberAccessExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::namespace_access_expr:
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::NamespaceAccessExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::vargs_expression:
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::VarArgsExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::type_instantiation:
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::TypeInstatiation*>(node));
    case AbstractSyntaxTree::Expression::expression_type::new_function_expression:
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::NewFunctionExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::create_function_expression:
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::CreateFunctionExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::identifier_expression:
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::IdentifierExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::binary_operation:
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::BinaryOperatorExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::unary_operation:
        return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::UnaryOperatorExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::ternary_expression:
        //return codegen_modifiable(scope_info, static_cast<AbstractSyntaxTree::TernaryExpression*>(node));
    default:
        ErrorRegister::reportError(new CodeGenError(
            "Encountered an invalid node",
            node->getPosition()
        ));
        break;
    }
    
    return nullptr;
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getRightExpression()) == nullptr) return nullptr;
    if (initLLVMType(node->getLeftExpression()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();
    if (node->getLeftExpression() == nullptr) return nullptr;
    if (node->getRightExpression() == nullptr) return nullptr;

    return constrainValueToModule(
            this->_curr_module.get(), 
            codegen_modifiable(scope_info, node->getReferencedNode()
        ));
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::TypeInstatiation* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getParameters()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();
    if (node->getParameters() == nullptr) return nullptr;

    // allocate memory for the variable
    auto* last_insert_block = _builder.GetInsertBlock();

    this->_builder.SetInsertPoint(&(scope_info.functions_data.top().allocation_block->back()));

    node->setLLVMValue(
        _builder.CreateAlloca(node->getSynthesizedType()->getLLVMType(), nullptr, "tmp")
    );

    _builder.SetInsertPoint(last_insert_block);

    // check that the type is a custom type
    if (node->getSynthesizedType()->getType() != SynthesizedType::type_category::custom_type)
    {
        ErrorRegister::reportError(new CodeGenError(
            "Cannot call the constructor of a non custom type",
            node->getPosition()
        ));
 
        return nullptr;
    }

    auto* type = static_cast<CustomType*>(node->getSynthesizedType());

    // add this allocation to the allocation list
    scope_info.functions_data.top().alloc_objs.front().push_back({ type, node->getLLVMValue() });

    // create parameters vector
    std::vector<llvm::Value*> params;

    // add the pointer to the instance
    params.push_back(node->getLLVMValue());

    // add the rest of the parameters
    for (auto* expr : node->getParameters()->getExpressions())
        params.push_back(codegen(scope_info, expr));

    // call the constructor
    createConstructorCall(scope_info, type, params);

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::NewFunctionExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getCreationType()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    auto* ptr = codegen(scope_info, node);

    if (ptr == nullptr)
        return nullptr;

    return ptr;
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::CreateFunctionExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getCreationType()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    auto* ptr = codegen(scope_info, node);

    if (ptr == nullptr)
        return nullptr;

    return ptr;
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::MemberAccessExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getLeftExpression()) == nullptr) return nullptr;
    if (initLLVMType(node->getRightExpression()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();
    if (node->getLeftExpression() == nullptr) return nullptr;
    if (node->getRightExpression() == nullptr) return nullptr;

    switch (node->getAccessType())
    {
        case AbstractSyntaxTree::MemberAccessExpression::AccessType::index_acs:
        {
            llvm::Value *left_val = nullptr, *index_val = nullptr;

            if ((left_val = codegen(scope_info, node->getLeftExpression())) == nullptr)
                return nullptr;

            if ((index_val = codegen(scope_info, node->getRightExpression())) == nullptr)
                return nullptr;

            // Cast the index if neccessary
            index_val = createCastIfNeccessary(node->getRightExpression()->getPosition(),
                                    index_val, node->getRightExpression()->getSynthesizedType(),
                                    TypeSystemInfo::getGlobalContainer()->getType("u64"));
            
            if(index_val == nullptr) return nullptr;

            // get the pointer to the element at the index specified by the right expression
            node->setLLVMValue(
                _builder.CreateGEP(
                    left_val,
                    index_val
                )
            );
            break;
        }
        case AbstractSyntaxTree::MemberAccessExpression::AccessType::pointer_member_access:
        {
            llvm::Value* left_val = nullptr;

            if((left_val = codegen(scope_info, node->getLeftExpression())) == nullptr) return nullptr;
            node->setLLVMValue(_builder.CreateConstInBoundsGEP2_32(nullptr,
                left_val, 0, node->getAccessIndex()));
             
            break;
        }
        case AbstractSyntaxTree::MemberAccessExpression::AccessType::member_acs:
        {
            llvm::Value* left_val = nullptr;
            if (node->getLeftExpression()->getModifiable() == AbstractSyntaxTree::Expression::modifiable_type::TRUE)
            {
                if((left_val = codegen_modifiable(scope_info, node->getLeftExpression())) == nullptr) return nullptr;
                node->setLLVMValue(_builder.CreateConstInBoundsGEP2_32(nullptr,
                    left_val, 0, node->getAccessIndex()));
            }
            else
            {
                ErrorRegister::reportError(new CodeGenError(
                    "Left expression has to be a modifiable left value",
                    node->getLeftExpression()->getPosition()
                ));
            }
             
            break;
        }
        default:
            ErrorRegister::reportError(new CodeGenError(
                "Invalid member access operator",
                node->getPosition()
            ));

            break;
    }

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getLeftExpression()) == nullptr) return nullptr;
    if (initLLVMType(node->getRightExpression()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    const auto& operator_synthesis = TypeSystemInfo::operators_type_synthesis.find(node->getOperator());
    Operator* op = OperatorsInfo::inst().getOperator(node->getOperator());

    // Check if the operator is defined in the language
    if(op == nullptr ||
        operator_synthesis == TypeSystemInfo::operators_type_synthesis.end())
        ErrorRegister::reportError(new CodeGenError(
            string("Operator ") + node->getOperator() + " is not specified",
            node->getPosition()
        ));

    if(op->op == "&&" || op->op == "||")
    {
        return CodegenLogicalBinaryOperation(scope_info, node, op);
    }

    llvm::Value* left_val = nullptr, *right_val = nullptr;

    if (op->bin_mods.output != operator_mod::reference)
    {
        ErrorRegister::reportError(new CodeGenError(
            string("Operator ") + node->getOperator() + " doesn't produce a modifiable value",
            node->getPosition()
        ));
        
        return nullptr;
    }

    // codegen the left value    
    if (op->bin_mods.left_input == operator_mod::reference)
        left_val = codegen_modifiable(scope_info, node->getLeftExpression());
    else left_val = codegen(scope_info, node->getLeftExpression());

    if (left_val == nullptr) return nullptr;

    // codegen the right value
    if (op->bin_mods.right_input == operator_mod::reference)
        right_val = codegen_modifiable(scope_info, node->getRightExpression());
    else right_val = codegen(scope_info, node->getRightExpression());

    if (right_val == nullptr) return nullptr;

    AbstractSyntaxTree::Expression* source_expr = nullptr, *dest_expr = nullptr;
    llvm::Value* source_val = nullptr, *dest_val = nullptr;
    
    /*
    
        llvm::Type* source_n_type = nullptr;
        source_n_type = (op->bin_mods.left_input == operator_mod::reference ||
                        op->bin_mods.left_input == operator_mod::pointer) ?
                            left_val->getType()->getPointerElementType() :
                            left_val->getType();
    */
    switch (operator_synthesis->second.at(1))
    {
    case TypeSystemInfo::type_synthesis_action::boolean:
    case TypeSystemInfo::type_synthesis_action::left:
        source_expr = node->getLeftExpression();
        dest_expr = node->getRightExpression();
        source_val = left_val;
        dest_val = right_val;
        break;
    case TypeSystemInfo::type_synthesis_action::right:
        source_expr = node->getRightExpression();
        dest_expr = node->getLeftExpression();
        source_val = right_val;
        dest_val = left_val;
        break;
    default:
        return nullptr; // Already being checked
    }

    // codegen the operation
    auto* res = source_expr->getSynthesizedType()->codegen(
        *op, 
        dest_expr->getSynthesizedType(), 
        source_val, dest_val, this->_builder,
        this->_gc);

    // No cast throwing
    if (res == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            string("Operator '") + node->getOperator() + string("' does not match ") + node->getRightExpression()->getSynthesizedType()->getName() + 
            string(" and ") + node->getLeftExpression()->getSynthesizedType()->getName(),
            node->getPosition()
        ));

        return nullptr;
    }
    
    // Add value undefined casts warning
    warningIfUndefOrPoison("The implicit codegen creates undefined/poisoned llvm value",
        res, source_expr->getPosition());
    
    auto* left_prev_val = _builder.CreateLoad(left_val);

    // Retaining and releasing safe ptrs by operator "="
    bool left_strong = node->getLeftExpression()->getSynthesizedType()->getType() == SynthesizedType::type_category::strong_pointer_type, right_strong = false;
    if(op->op == "=")
    {
        if ((left_strong = node->getLeftExpression()->getSynthesizedType()->getType() == SynthesizedType::type_category::strong_pointer_type) ||
            node->getLeftExpression()->getSynthesizedType()->getType() == SynthesizedType::type_category::weak_pointer_type)
        {
            this->_builder.CreateCall(
                this->_gc.getOrCreateRetainFunction(left_strong),
                { this->_builder.CreateBitCast(right_val, this->_builder.getInt8PtrTy())}
            );
        }

        if ((right_strong = node->getRightExpression()->getSynthesizedType()->getType() == SynthesizedType::type_category::strong_pointer_type) ||
            node->getRightExpression()->getSynthesizedType()->getType() == SynthesizedType::type_category::weak_pointer_type)
        {
            auto* base_type = (left_strong) ?
            static_cast<StrongPointerType*>(node->getLeftExpression()->getSynthesizedType())->getBaseType():
            static_cast<WeakPointerType*>(node->getLeftExpression()->getSynthesizedType())->getBaseType();

            if (base_type->getType() == SynthesizedType::type_category::custom_type)
            {
                auto* ty = static_cast<CustomType*>(base_type);
                const auto& destructor = ty->getDefinition()->getFunctionsSymbolTable().find(DESTRUCTOR_KEYWORD);

                if (destructor != ty->getDefinition()->getFunctionsSymbolTable().end())
                {
                    llvm::Value* func_val = nullptr;

                    if ((func_val = codegen(scope_info, destructor->second)) == nullptr) 
                        return nullptr;
                    
                    func_val = constrainValueToModule(this->_curr_module.get(), func_val);

                    _builder.CreateCall(
                        this->_gc.getOrCreateReleaseFunction(left_strong),
                        {
                            this->_builder.CreateBitCast(left_prev_val, this->_builder.getInt8PtrTy()),
                            this->_builder.CreateBitCast(func_val, this->_builder.getInt8PtrTy())
                        }
                    );
                }   
            }
            else
            {
                _builder.CreateCall(
                    this->_gc.getOrCreateReleaseFunction(left_strong),
                    { this->_builder.CreateBitCast(left_prev_val, this->_builder.getInt8PtrTy()), llvm::Constant::getNullValue(llvm::Type::getInt8PtrTy(this->_context)) }
                );
            }
        }
    }

    node->setLLVMValue(res);
    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::UnaryOperatorExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (initLLVMType(node->getExpression()) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    Operator* op = OperatorsInfo::inst().getOperator(node->getOperator());

    if (op == nullptr)
        ErrorRegister::reportError(new CodeGenError(
            string("Operator ") + node->getOperator() + " is not specified",
            node->getPosition()
        ));

    llvm::Value* inner_val = nullptr;

    if ((node->isPrefix() ? op->unary_mods.prefix_output : op->unary_mods.postfix_output) !=
        operator_mod::reference)
    {
        ErrorRegister::reportError(new CodeGenError(
            string("Operator ") + node->getOperator() + " doesn't produce a modifiable value",
            node->getPosition()
        ));
        
        return nullptr;
    }

    if ((node->isPrefix() ? op->unary_mods.prefix_input : op->unary_mods.postfix_input) ==
        operator_mod::reference)
        inner_val = codegen_modifiable(scope_info, node->getExpression());
    else inner_val = codegen(scope_info, node->getExpression());

    if(inner_val == nullptr) return nullptr;

    auto* res = node->getExpression()->getSynthesizedType()->codegen(
        *op, 
        nullptr, 
        node->isPrefix() ? nullptr : inner_val, 
        node->isPrefix() ? inner_val : nullptr, 
        this->_builder,
        this->_gc
    );

    if (res == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            "Operator '" + node->getOperator() + "' does not match " + 
            node->getExpression()->getSynthesizedType()->getName(),
            node->getPosition()
        ));
        
        return nullptr;
    }

    // Add value undefined casts warning
    warningIfUndefOrPoison("The codegen creates undefined/poisoned llvm value",
        res, node->getExpression()->getPosition());

    if ((node->isPrefix() ? op->unary_mods.prefix_output : op->unary_mods.postfix_output) !=
        operator_mod::reference)
    {
        ErrorRegister::reportError(new CodeGenError(
            "Operator '" + node->getOperator() + "' does not produce a modifiable left value",
            node->getPosition()
        ));
        
        return nullptr;
    }


    // if the operator is prefix, use the inner value, else use the resulting value
    node->setLLVMValue((node->isPrefix()) ? res : inner_val);

    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::IdentifierExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    auto* definition = scope_info.getDefinition(node->getIdentifier());

    llvm::Value* var_val = nullptr;

    // check if the identifier is defined
    if (definition != nullptr)
    {
        if(codegen(scope_info, definition) == nullptr) return nullptr;
        var_val = constrainValueToModule(this->_curr_module.get(), definition->getLLVMValue());
    }
    else
    {
        ErrorRegister::reportError(new CodeGenError(
            "Could not find the definition of '" + node->getIdentifier() + "'",
            node->getPosition()
        ));
        
        return nullptr;
    }

    // set the value of the identifier expression to the value of the definition
    if (definition->getSynthesizedType()->getLLVMType() == var_val->getType())
    {
        ErrorRegister::reportError(new CodeGenError(
            "Cannot set the value of a non modifiable variable",
            node->getPosition()
        ));

        return nullptr;
    }
    else if (definition->getSynthesizedType()->getLLVMType()->getPointerTo() == var_val->getType())
        node->setLLVMValue(var_val); 
    else 
    {
        ErrorRegister::reportError(new CodeGenError(
            "LLVM type mismatch",
            node->getPosition()
        ));

        return nullptr;
    }
 
    return node->getLLVMValue();
}

llvm::Value* CodeGenerator::codegen_modifiable(CGScopeInfo& scope_info, AbstractSyntaxTree::CallExpression* node)
{
    if (initLLVMType(node) == nullptr) return nullptr;
    if (node->getLLVMValue() != nullptr) return node->getLLVMValue();

    ErrorRegister::reportError(new CodeGenError(
        "Function calls never return a modifiable value",
        node->getPosition()
    ));

    return nullptr;

/*
    auto* val = codegen(scope_info, node->getFunctionExpression());

    auto* func_call_type = llvm::dyn_cast_or_null<llvm::FunctionType>(llvm::dyn_cast_or_null<llvm::PointerType>(val->getType())->getElementType());
    auto* func_type = dynamic_cast<FunctionType*>(node->getFunctionExpression()->getSynthesizedType());

    // check if there were any errors
    if (func_call_type == nullptr || func_type == nullptr)
    {
        if (val != nullptr)
            ErrorRegister::reportError(new CodeGenError(
                node->getSynthesizedType()->getName() + " cannot be invoked",
                node->getPosition()
            ));

        return nullptr;
    }

    if (func_type->getInputType() == nullptr) return nullptr;
    
    NamedComplexType* func_input_types = nullptr;
    if (func_type->getInputType()->getType() != SynthesizedType::type_category::void_type)
    {
        func_type_types = static_cast<NamedComplexType*>(func_type->getInputType());
        if (node->getArguments() == nullptr) return nullptr;

        const auto& components = func_input_types->getComponents();
        const auto& args = node->getArguments()->getExpressions();
        std::vector<llvm::Value*> res;
        llvm::Value* arg_val = nullptr;

        for (unsigned int i = 0; i < args.size(); i++)
        {
            if((arg_val = codegen(scope_info, args[i])) == nullptr) return nullptr;
            
            if ((arg_val = addImplicitCasts(scope_info, arg_val,
                components[i].first->getLLVMType(this->_context, this->_gc),
                components[i].first,
                args[i]->getSynthesizedType(),
                args[i]->getPosition()
            )) == nullptr) return nullptr;

            res.push_back(arg_val);
        }
    }

    // set the value of the node
    node->setLLVMValue(
        this->_builder.CreateCall(
            llvm::FunctionCallee(func_call_type, val),
            toValueVector(scope_info, node->getArguments())
        )
    );

    return node->getLLVMValue();
    */
}

bool CodeGenerator::addGlobalVariable(CGScopeInfo& scope_info, AbstractSyntaxTree::VariableDeclarationComponent* declaration)
{
    if (initLLVMType(declaration) == nullptr) return false;
    if (initLLVMType(declaration->getIdentifier()) == nullptr) return false;
    if (declaration->getInitialization() != nullptr && initLLVMType(declaration->getInitialization()) == nullptr) return false;
	if(declaration == nullptr) return false;
	if(declaration->getIdentifier() == nullptr) return false;

    // create the global variable
    llvm::GlobalVariable* variable = new llvm::GlobalVariable(
        *(this->_curr_module),
        declaration->getSynthesizedType()->getLLVMType(),
        false,
        llvm::GlobalValue::LinkageTypes::ExternalLinkage,
        nullptr,
        _name_mangler.getMangaledName(declaration->getIdentifier()->getIdentifier())
    );

    // set the LLVM value
    declaration->setLLVMValue(variable);
    declaration->getIdentifier()->setLLVMValue(variable);

    if (declaration->getProperties().is_extern) return true;

    // set the initializer to be a zero initializer
    variable->setInitializer(llvm::ConstantAggregateZero::get(declaration->getSynthesizedType()->getLLVMType()));

    auto* last_insert_point = this->_builder.GetInsertBlock();

    llvm::Function* init_func = nullptr;
    
    // check if the variable is initialized
    if (declaration->getInitialization() != nullptr)
    {
        // create a global ctor function for the variable
        init_func = llvm::Function::Create(
            llvm::FunctionType::get(llvm::Type::getVoidTy(this->_context), false),
            llvm::GlobalValue::LinkageTypes::InternalLinkage,
            string(GLOBAL_VARIABLE_INITS) + declaration->getIdentifier()->getIdentifier(),
            this->_curr_module.get()
        );

        /*init_func = llvm::getOrCreateInitFunction(
            *(this->_curr_module),
            GLOBAL_VARIABLE_INITS + declaration->getIdentifier()->getIdentifier() + "." +
                    std::to_string(this->_curr_module_node->getModuleId())
        );*/

        // create the body of the function
        llvm::BasicBlock* block = llvm::BasicBlock::Create(
            this->_context, "init_entry", init_func
        );

        scope_info.functions_data.push({init_func, nullptr, nullptr, nullptr, nullptr, nullptr, {}});
        this->_builder.SetInsertPoint(block);

        llvm::Value* val = codegen(scope_info, declaration->getInitialization());

        if (val == nullptr)
        {
            scope_info.functions_data.pop();
            this->_builder.SetInsertPoint(last_insert_point);

            return false;
        }

        if ((val = declaration->getInitialization()->getSynthesizedType()->CreateCast(
            declaration->getSynthesizedType(),
            val,
            declaration->getLLVMValue()->getType()->getPointerElementType(),
            this->_builder,
            this->_gc
        )) == nullptr)
        {
            reportCantCast(declaration->getInitialization()->getSynthesizedType(),
            declaration->getSynthesizedType(), declaration->getInitialization()->getPosition());
            
            return false;
        };

        // Add value undefined casts warning
        warningIfUndefOrPoison("The implicit cast creates undefined/poisoned llvm value",
            val, declaration->getInitialization()->getPosition());
        
        auto* const_val = llvm::dyn_cast_or_null<llvm::Constant>(val);
        
        // set the initializer automatically
        if (const_val != nullptr)
        {
            init_func->eraseFromParent();

            variable->setInitializer(const_val);
        }
        else
        {
            // continue the generation of the function - store the value inside the global
            this->_builder.CreateStore(val, variable);
            this->_builder.CreateRetVoid();

            // verify the function
            if (llvm::verifyFunction(*init_func, &this->_llvm_err_register))
            {
                ErrorRegister::reportError(new CodeGenError(
                    "LLVM error when initializing " + declaration->getIdentifier()->getIdentifier() + ": " + this->_llvm_err,
                    declaration->getPosition()
                ));

                scope_info.functions_data.pop();

                return false;
            }

            // add the function to be generated
            this->_global_init_var_funcs.push_back(init_func);
        }
        
        scope_info.functions_data.pop();
    }

    this->_builder.SetInsertPoint(last_insert_point);

    return true;
}

void CodeGenerator::generateGlobalInitFuncs(CGScopeInfo& scope_info)
{
    auto* last_insert_point = _builder.GetInsertBlock();

    // get / create the global init function for this module
    llvm::Function* global_inits = llvm::getOrCreateInitFunction(
        *(this->_curr_module),
        GLOBAL_FUNC_INIT_VARS
    );

    // push the current function and set the last insert point
    scope_info.functions_data.push({global_inits, nullptr, nullptr, nullptr, nullptr, nullptr, {}});

    llvm::BasicBlock* global_inits_body = nullptr;

    // get the basic block of the function
    if (global_inits->getBasicBlockList().size() <= 0)
        global_inits_body = llvm::BasicBlock::Create(
            this->_context, "init_entry", global_inits
        );
    else if (global_inits->getBasicBlockList().front().getTerminator() == nullptr)
        global_inits_body = &(global_inits->getBasicBlockList().front());
    else
    {
        _builder.SetInsertPoint(&(global_inits->getBasicBlockList().front()));
        
        global_inits_body = llvm::BasicBlock::Create(
            this->_context, "init_entry", global_inits
        );

        _builder.CreateBr(global_inits_body);
    }

    this->_builder.SetInsertPoint(global_inits_body);

    for(auto* init_func : this->_global_init_var_funcs)
        _builder.CreateCall(init_func);

    _builder.SetInsertPoint(&(global_inits->getEntryBlock()));
    _builder.CreateRetVoid();

    // verify the function
    if (llvm::verifyFunction(*global_inits, &this->_llvm_err_register))
    {
        ErrorRegister::reportError(new CodeGenError(
            "LLVM error in global init function: " + this->_llvm_err                
        ));
    }

    scope_info.functions_data.pop();
    this->_builder.SetInsertPoint(last_insert_point);
}

std::vector<llvm::Value*> CodeGenerator::toValueVector(CGScopeInfo& scope_info, AbstractSyntaxTree::ExpressionList* node)
{
    if (initLLVMType(node) == nullptr) return {};

    std::vector<llvm::Value*> res;

    for (auto* expr : node->getExpressions())
        res.push_back(codegen(scope_info, expr));

    return res;
}

void CodeGenerator::mergeLastBlockInto(CGScopeInfo& scope_info, llvm::BasicBlock* block)
{
    auto* last_block = _builder.GetInsertBlock();

    // merge the last block if it is not empty
    if (scope_info.functions_data.top().func_val->getBasicBlockList().size() > 1)
    {
        // get the last basic block of the function
        auto* last_function_block = scope_info.functions_data.top().func_val->getBasicBlockList().getPrevNode(*block);

        // if the last block does not terminate
        if (last_function_block != nullptr && last_function_block->getTerminator() == nullptr)
        {
            // create a branch instruction to this block
            _builder.SetInsertPoint(last_function_block);
            _builder.CreateBr(block);
        }
    }

    _builder.SetInsertPoint(last_block);
}

llvm::BasicBlock* CodeGenerator::allocaLangParams(CGScopeInfo& scope_info, AbstractSyntaxTree::FunctionDefinition* node, llvm::Function* func)
{
    if (node == nullptr || func == nullptr) return nullptr;

    auto* last_insert_block = _builder.GetInsertBlock();
    llvm::BasicBlock* func_entry = nullptr;

    auto create_func_entry = [&]()
    {
        if(func_entry == nullptr)
        {
            // Create basic block for inner language allocations
            func_entry = llvm::BasicBlock::Create(
                this->_context,
                node->getFunctionName()->getIdentifier() + string("_lang_entry"),
                func
            );

            // merge the current block into the last block
            mergeLastBlockInto(scope_info, func_entry);

            _builder.SetInsertPoint(func_entry);
        }
    };

    // Variadic parameter allocation -> va_list
    if(node->getProperties().is_variadic)
    {   
        create_func_entry();
        // Get the variadic list type
        auto* variadic_type = VarArg::getVAListType(this->_context, this->_target_machine);

        if(variadic_type == nullptr)
        {
            ErrorRegister::reportError(new CodeGenError(
                "Invalid variadic backend analysis was performed",
                node->getPosition()
            ));

            _builder.SetInsertPoint(last_insert_block);
            return nullptr;
        }

        // Allocating the variadic list
        auto* list_alloc = _builder.CreateAlloca(variadic_type, nullptr, VarArg::getVariableName());

        // If linux with x86_64 architecture, then bitcast the type
        if (this->_target_machine->getTargetTriple().getOS() == llvm::Triple::Linux && 
            this->_target_machine->getTargetTriple().getArch() == llvm::Triple::x86_64)
            scope_info.functions_data.top().variadic_list = 
                _builder.CreateBitCast(list_alloc, llvm::Type::getInt8PtrTy(this->_context));
        else
            scope_info.functions_data.top().variadic_list = list_alloc;

        // Create call for starting and initializing the variadic list
        _builder.CreateCall(VarArg::createOrGetStartDec(this->_context, this->_curr_module.get()), {scope_info.functions_data.top().variadic_list});
    }

    // Allocate the return value holder
    auto* func_type = scope_info.functions_data.top().func_def->getReturnType()->getSynthesizedType()->getLLVMType();
    if(!func_type->isVoidTy())
    {
        create_func_entry();
        scope_info.functions_data.top().return_allocation = this->_builder.CreateAlloca(func_type, nullptr, "$ret_val");
    }

    this->_builder.SetInsertPoint(last_insert_block);

    return func_entry;
}

llvm::BasicBlock* CodeGenerator::allocaParams(CGScopeInfo& scope_info, AbstractSyntaxTree::FunctionDefinition* node,
                                              llvm::Function* func)
{
    if (node == nullptr || func == nullptr) return nullptr;

    auto* last_insert_block = _builder.GetInsertBlock();
    llvm::BasicBlock* func_entry = nullptr;

    // if there are any parameters
    if (node->getInputType()->getComponents().size() > 0)
    {
        // create a basic block for the entry of the function
        func_entry = llvm::BasicBlock::Create(
            this->_context,
            node->getFunctionName()->getIdentifier() + string("_entry"),
            func
        );

        // merge the current block into the last block
        mergeLastBlockInto(scope_info, func_entry);

        _builder.SetInsertPoint(func_entry);

        // copy the input parameters and allocate memory for them
        int i = 0;
        for (AbstractSyntaxTree::ComplexTypeComponent* arg : node->getInputType()->getComponents())
        {
            // allocate memory
            initLLVMType(arg);
            arg->setLLVMValue(_builder.CreateAlloca(
                arg->getSynthesizedType()->getLLVMType(),
                nullptr,
                arg->getIdentifier()->getIdentifier()
            ));
            arg->getIdentifier()->setLLVMValue(arg->getLLVMValue());

            // store the value
            _builder.CreateStore(func->getArg(i++), arg->getLLVMValue());
        }
    }

    _builder.SetInsertPoint(last_insert_block);
    return func_entry;
}

llvm::BasicBlock* CodeGenerator::insertReturnBlockImpl(CGScopeInfo& scope_info, AbstractSyntaxTree::FunctionDefinition* node, llvm::BasicBlock* return_block)
{
    if (node == nullptr) return nullptr;

    auto* last_point = this->_builder.GetInsertBlock();
    scope_info.functions_data.top().return_block = return_block;

    this->_builder.SetInsertPoint(return_block);

    // Variadic handling
    if(node->getProperties().is_variadic)
        this->_builder.CreateCall(VarArg::createOrGetEndDec(this->_context, this->_curr_module.get()),
            {scope_info.functions_data.top().variadic_list});

    if(node->getReturnType()->getSynthesizedType()->getLLVMType()->isVoidTy())
        this->_builder.CreateRetVoid();
    else
        this->_builder.CreateRet(this->_builder.CreateLoad(scope_info.functions_data.top().return_allocation));

    this->_builder.SetInsertPoint(last_point);

    return return_block;
}

llvm::Value* CodeGenerator::createCastIfNeccessary(const position_data& position, llvm::Value* to_cast, SynthesizedType* source_type, SynthesizedType* dest_type)
{
    if (to_cast == nullptr || source_type == nullptr || dest_type == nullptr) return nullptr;
    if ((to_cast = source_type->CreateCast(dest_type,to_cast, dest_type->getLLVMType(),_builder, this->_gc)) == nullptr)
    {
        reportCantCast(source_type, dest_type, position);
        
        return nullptr;
    };

    // Add value undefined casts warning
    warningIfUndefOrPoison("This cast creates an undefined/poisoned llvm value", to_cast, position);

    return to_cast;
}

llvm::Value* CodeGenerator::getValue(CGScopeInfo& scope_info, AbstractSyntaxTree::MemberAccessExpression* expr)
{
    if (expr == nullptr || expr->getLeftExpression() == nullptr || expr->getRightExpression() == nullptr) return nullptr;
    if (expr->getLLVMValue() != nullptr) return expr->getLLVMValue();

    if (expr->getLeftExpression()->getExpressionType() != AbstractSyntaxTree::Expression::expression_type::identifier_expression)
    {
        ErrorRegister::reportError((new CodeGenError(
            "This must be an identifier",
            expr->getLeftExpression()->getPosition()
        ))->withNote("Before '::' there should always be an identifier", expr->getPosition()));
        return nullptr;
    }

    string identifier = static_cast<AbstractSyntaxTree::IdentifierExpression*>(expr->getLeftExpression())->getIdentifier();
    auto* definition = scope_info.getDefinition(identifier);

    if (definition == nullptr)
    {
        ErrorRegister::reportError(new CodeGenError(
            "'" + identifier + "'" + " is not defined", 
            expr->getLeftExpression()->getPosition()
        ));
        return nullptr;
    }

    switch (definition->getNodeType())
    {
    case AbstractSyntaxTree::Node::node_type::module:
    {
        // use the scope of the referened module instead
        auto* referenced_module = static_cast<AbstractSyntaxTree::Module*>(definition);
        auto module_scope = CGScopeInfo(
            referenced_module,
            referenced_module->getSymbolTable()
        );

        try {
            module_scope.includeImportedSymbols();
        } catch(SymbolRedefinitionException& err) {
            ErrorRegister::reportError((new CodeGenError(
                "Redefined Symbol",
                err.previous_definition->getPosition()
            ))->withNote(err.redefined_symbol + " is already defined here", err.new_definition->getPosition()));
        }

        // set the definition to be the definition of the right node
        if (expr->getRightExpression()->getExpressionType() == AbstractSyntaxTree::Expression::expression_type::member_access)
        {
            expr->setLLVMValue(getValue(
                module_scope,
                static_cast<AbstractSyntaxTree::MemberAccessExpression*>(expr->getRightExpression())
            ));

            return expr->getLLVMValue();
        } else if (expr->getRightExpression()->getExpressionType() == AbstractSyntaxTree::Expression::expression_type::identifier_expression) {
            if(expr->getRightExpression()->getLLVMValue() == nullptr)
            {
                auto* right_ref = static_cast<AbstractSyntaxTree::IdentifierExpression*>(expr->getRightExpression());
                definition = module_scope.getDefinition(right_ref->getIdentifier());
            
                if(definition == nullptr) return nullptr;

                string new_name;

                // If function or namespace, there is a need to add datat to the vector
                if(definition->getNodeType() == Node::node_type::function_definition || 
                    definition->getNodeType() == Node::node_type::namespace_definition)
                {
                    ManglingObj mang_obj(_name_mangler, right_ref->getIdentifier(), definition->getSynthesizedType());
                    new_name = _name_mangler.getMangaledName();
                }
                else // A simple variable
                    new_name = _name_mangler.getMangaledName(right_ref->getIdentifier());
                
                auto* new_var = getOrCreateVarLinkageIfGlobal(scope_info, new_name, definition);
            
                if (new_var == nullptr) 
                {
                    ErrorRegister::reportError(new CodeGenError(
                        "The expected global does not exist",
                        expr->getRightExpression()->getPosition()
                    ));
                    return nullptr;
                }

                expr->getRightExpression()->setLLVMValue(new_var);
            }

            definition = expr->getRightExpression();
        } else {
            ErrorRegister::reportError(new CodeGenError(
                "Could not find the definition of this variable",
                expr->getRightExpression()->getPosition()
            ));

            return nullptr;
        } 

        if (definition == nullptr) return nullptr;

        // get the llvm::Value* of the definition as a constant
        auto* other_module_val = definition->getLLVMValue();

        auto* global_val = llvm::dyn_cast_or_null<llvm::GlobalValue>(other_module_val);

        if (other_module_val == nullptr)
        {
            ErrorRegister::reportError(new CodeGenError(
                "Could not link this reference to its definition",
                expr->getPosition()
            ));
        }

        if (global_val != nullptr)
            expr->setLLVMValue(global_val);
        else 
        {
            ErrorRegister::reportError(new CodeGenError(
                "Not accessible in this context",
                expr->getLeftExpression()->getPosition()
            ));
        }
    }
        break;   
    default:
        break;
    }

    return expr->getLLVMValue();
}

void CodeGenerator::reportCantCast(SynthesizedType* source, SynthesizedType* dest, const position_data& pos)
{
    ErrorRegister::reportError(new CodeGenError(
        "Can't cast from type: " + source->getName() + " to type " + dest->getName(),
        pos,
        "Can't cast to " + dest->getName()
    ));
}

void CodeGenerator::warningIfUndefOrPoison(const string& warning, llvm::Value* val, const position_data& pos)
{
    if(val == nullptr) return;

    if (llvm::isa_and_nonnull<llvm::PoisonValue>(val) ||
        llvm::isa_and_nonnull<llvm::UndefValue>(val))
    {
        ErrorRegister::reportWarning(new CodeGenWarning(
            warning,
            pos,
            "Creates a value with undefined behaviour"
        ));
    }
}

/*
if (llvm::isa_and_nonnull<llvm::PoisonValue>(res) ||
        llvm::isa_and_nonnull<llvm::UndefValue>(res))
    {
        ErrorRegister::reportWarning(new CodeGenWarning(
            "The implicit cast creates undefined/poisoned llvm value",
            pos,
            "Creates a value with undefined behaviour"
        ));
    }
*/

llvm::Type* CodeGenerator::initLLVMType(AbstractSyntaxTree::Node* node)
{
    if (node == nullptr) 
    {
        ErrorRegister::reportError(new CodeGenError(
            "Missing a required node"
        ));

        return nullptr;
    }

    if (node->getSynthesizedType() == nullptr) 
    {
        ErrorRegister::reportError(new CodeGenError(
            "Could not generate type info",
            node->getPosition()
        ));

        return nullptr;
    }

    if(node->getSynthesizedType()->getLLVMType(this->_context, this->_gc) == nullptr)
        ErrorRegister::reportError(new CodeGenError(
            "LLVM Invalid Type",
            node->getPosition()
        ));

    return node->getSynthesizedType()->getLLVMType();
}

llvm::Type* CodeGenerator::initLLVMType(SynthesizedType* type, const position_data& position)
{
    if (type == nullptr) 
    {
        ErrorRegister::reportError(new CodeGenError(
            "Missing a required type",
            position
        ));

        return nullptr;
    }
    
    if(type->getLLVMType(this->_context, this->_gc) == nullptr)
        ErrorRegister::reportError(new CodeGenError(
            "LLVM Invalid Type",
            position
        ));

    return type->getLLVMType();
}

llvm::GlobalValue* CodeGenerator::getOrCreateVarLinkageIfGlobal(CGScopeInfo& scope_info, const string& name, AbstractSyntaxTree::Node* node)
{
    if (initLLVMType(node) == nullptr || name.empty()) return nullptr;
    if (node->getLLVMValue() == nullptr) return nullptr;

    // Check if the node is a global function
    auto* func_type = llvm::dyn_cast_or_null<llvm::FunctionType>(llvm::dyn_cast_or_null<llvm::PointerType>(node->getLLVMValue()->getType())->getPointerElementType());
    if(func_type != nullptr)
    {
        auto func = this->_curr_module->getOrInsertFunction(name, func_type);
        if(func.getCallee() == nullptr) return nullptr;

        return llvm::cast_or_null<llvm::GlobalValue>(func.getCallee());
    }

    // Check if the node is a global variable
    llvm::GlobalValue* curr_global_val = nullptr;
    if((curr_global_val = llvm::dyn_cast_or_null<llvm::GlobalValue>(this->_curr_module->getOrInsertGlobal(name, node->getSynthesizedType()->getLLVMType()))) == nullptr) return nullptr;

    if(curr_global_val == nullptr) return nullptr;

    curr_global_val->setLinkage(llvm::GlobalValue::LinkageTypes::ExternalLinkage);

    return curr_global_val;
}

void CodeGenerator::createDestructorCalls(CGScopeInfo& scope_info, const FunctionMetaData& metadata)
{
    const auto& allocations = metadata.alloc_objs;

    for (auto alloc_list = allocations.rbegin(); alloc_list != allocations.rend(); alloc_list++)
        createDestructorCalls(scope_info, *alloc_list);
}

void CodeGenerator::createReleaseCalls(CGScopeInfo& scope_info, const FunctionMetaData& metadata)
{
    const auto& allocations = metadata.creation_objs;

    for (auto alloc_list = allocations.rbegin(); alloc_list != allocations.rend(); alloc_list++)
        createReleaseCalls(scope_info, *alloc_list);
}

void CodeGenerator::createDestructorCalls(CGScopeInfo& scope_info, const AllocationList& alloc_list)
{
    // call the destructors
    for (auto itr = alloc_list.rbegin(); itr != alloc_list.rend(); itr++)
        createDestructorCall(scope_info, itr->first, itr->second);
}

void CodeGenerator::createDestructorCall(CGScopeInfo& scope_info, CustomType* type, llvm::Value* self_ptr)
{
    // call the destructor if it is defined
    const auto& destructor = type->getDefinition()->getFunctionsSymbolTable().find(DESTRUCTOR_KEYWORD);

    if (destructor != type->getDefinition()->getFunctionsSymbolTable().end())
    {
        AbstractSyntaxTree::Node* func = destructor->second;
        llvm::Value* func_val = nullptr;

        if ((func_val = codegen(scope_info, func)) == nullptr) 
            return;

        _builder.CreateCall(
            llvm::FunctionCallee(
                static_cast<llvm::FunctionType*>(func->getSynthesizedType()->getLLVMType()),
                constrainValueToModule(this->_curr_module.get(), func_val)
            ), 
            { self_ptr }
        );
    }
}

void CodeGenerator::createReleaseCalls(CGScopeInfo& scope_info, const SafeCreationList& alloc_list)
{
    // call the destructors if they are defined
    for (auto itr = alloc_list.rbegin(); itr != alloc_list.rend(); itr++)
        createReleaseCall(scope_info, *itr);
}

void CodeGenerator::createReleaseCall(CGScopeInfo& scope_info, const SafeCreationInfo& alloc_info)
{
    SynthesizedType* base_type = (alloc_info.is_strong) ? (alloc_info.safe_ptr_type.strong_ptr_type->getBaseType()) : (alloc_info.safe_ptr_type.weak_ptr_type->getBaseType());
    auto* ptr_val = (alloc_info.is_tmp) ? (alloc_info.safe_ptr) : (this->_builder.CreateLoad(alloc_info.safe_ptr));
    if (base_type->getType() == SynthesizedType::type_category::custom_type)
    {
        auto* ty = static_cast<CustomType*>(base_type);
        const auto& destructor = ty->getDefinition()->getFunctionsSymbolTable().find(DESTRUCTOR_KEYWORD);

        if (destructor != ty->getDefinition()->getFunctionsSymbolTable().end())
        {
            llvm::Value* func_val = nullptr;

            if ((func_val = codegen(scope_info, destructor->second)) == nullptr) 
                return;

            auto* release_func = this->_gc.getOrCreateReleaseFunction(alloc_info.is_strong);

            func_val = constrainValueToModule(this->_curr_module.get(), func_val);

            _builder.CreateCall(
                llvm::FunctionCallee(release_func), 
                {
                    this->_builder.CreateBitCast(ptr_val, this->_builder.getInt8PtrTy()),
                    this->_builder.CreateBitCast(func_val, this->_builder.getInt8PtrTy())
                }
            );
        }   
    }
    else
    {
        auto* release_func = this->_gc.getOrCreateReleaseFunction(alloc_info.is_strong);
        _builder.CreateCall(
            llvm::FunctionCallee(release_func), 
            { this->_builder.CreateBitCast(ptr_val, this->_builder.getInt8PtrTy()), llvm::Constant::getNullValue(llvm::Type::getInt8PtrTy(this->_context)) }
        );
    }    
}

void CodeGenerator::createConstructorCall(CGScopeInfo& scope_info, CustomType* type, llvm::ArrayRef<llvm::Value*> parameters)
{
    llvm::Value* func_val = nullptr;
    const auto& found = type->getDefinition()->getFunctionsSymbolTable().find(CONSTRUCTOR_KEYWORD);

    // if there is a constructor
    if (found != type->getDefinition()->getFunctionsSymbolTable().end())
    {
        AbstractSyntaxTree::Node* func = found->second;

        if ((func_val = codegen(scope_info, func)) == nullptr) 
            return;

        _builder.CreateCall(
            llvm::FunctionCallee(
                static_cast<llvm::FunctionType*>(func->getSynthesizedType()->getLLVMType()),
                constrainValueToModule(this->_curr_module.get(), func_val)
            ), 
            parameters
        );
    }
}

llvm::Value* CodeGenerator::constrainValueToModule(llvm::Module* mod, llvm::Value* value)
{
    // if the value is not a global object, referencing it can only be done from the same module
    if (value == nullptr) return nullptr;
    if (!llvm::isa<llvm::GlobalValue>(value)) return value;
    if (!llvm::isa<llvm::PointerType>(value->getType())) return value;
    auto* glob_obj = llvm::cast<llvm::GlobalValue>(value);

    // check if the global object is already defined in the module given
    if (glob_obj->getParent() == mod) return glob_obj;

    // Check if the value is a global function
    auto* func_type = llvm::dyn_cast_or_null<llvm::FunctionType>(llvm::dyn_cast_or_null<llvm::PointerType>(value->getType())->getPointerElementType());
    if(func_type != nullptr)
    {
        auto func = this->_curr_module->getOrInsertFunction(value->getName(), func_type);
        if(func.getCallee() == nullptr) return nullptr;

        return llvm::cast_or_null<llvm::GlobalValue>(func.getCallee());
    }

    // Check if the node is a global variable
    llvm::GlobalValue* curr_global_val = nullptr;
    if((curr_global_val = llvm::dyn_cast_or_null<llvm::GlobalValue>(
        mod->getOrInsertGlobal(value->getName(), value->getType()->getPointerElementType()))) == nullptr) return nullptr;

    curr_global_val->setLinkage(llvm::GlobalValue::LinkageTypes::ExternalLinkage);

    return llvm::cast<llvm::Value>(curr_global_val);
}

void CodeGenerator::insertDefaultConstructorImpl(CGScopeInfo& scope_info, CustomType* type, llvm::Value* self_ptr)
{
    if (type == nullptr) return;
    if (self_ptr == nullptr) return;

    const auto& data_members = type->getDataMembers();

    for (unsigned int i = 0; i < data_members.size(); i++)
    {
        switch (data_members[i].second->getType())
        {
        case SynthesizedType::type_category::custom_type:
            createConstructorCall(
                scope_info, 
                static_cast<CustomType*>(data_members[i].second), 
                { _builder.CreateConstInBoundsGEP2_32(nullptr, self_ptr, 0, i) }
            );
            break;
        case SynthesizedType::type_category::strong_pointer_type:
        {
            auto* base = static_cast<StrongPointerType*>(data_members[i].second)->getBaseType();
            
            _builder.CreateStore(
                SynthesizedType::wrap_pointer(
                    nullptr,
                    llvm::Constant::getNullValue(base->getLLVMType()->getPointerTo()),
                    TypeSystemInfo::getGlobalContainer()->getOrCreatePointerType(base),
                    nullptr,
                    this->_builder,
                    this->_gc
                ),
                _builder.CreateConstInBoundsGEP2_32(nullptr, self_ptr, 0, i)
            );
            break;
        }
        case SynthesizedType::type_category::weak_pointer_type:
        {
            auto* base = static_cast<WeakPointerType*>(data_members[i].second)->getBaseType();
            auto* rc_ptr = _builder.CreateConstInBoundsGEP2_32(nullptr, self_ptr, 0, i);

            _builder.CreateStore(
                SynthesizedType::wrap_pointer(
                    nullptr,
                    llvm::Constant::getNullValue(base->getLLVMType()->getPointerTo()),
                    TypeSystemInfo::getGlobalContainer()->getOrCreatePointerType(base),
                    nullptr,
                    this->_builder,
                    this->_gc
                ),
                rc_ptr
            );

            this->_gc.setRC(rc_ptr, _builder.getInt64(0));
            break;
        }
        }
    }
}

void CodeGenerator::insertDefaultDestructorImpl(CGScopeInfo& scope_info, CustomType* type, llvm::Value* self_ptr)
{
    if (type == nullptr) return;
    if (self_ptr == nullptr) return;

    const auto& data_members = type->getDataMembers();

    for (unsigned int i = 0; i < data_members.size(); i++)
    {
        switch (data_members[i].second->getType())
        {
        case SynthesizedType::type_category::custom_type:
            createDestructorCall(
                scope_info,
                static_cast<CustomType*>(data_members[i].second), 
                { _builder.CreateConstInBoundsGEP2_32(nullptr, self_ptr, 0, i) }
            );
            break;
        case SynthesizedType::type_category::strong_pointer_type:
        {
            createReleaseCall(scope_info, 
                {
                    static_cast<StrongPointerType*>(data_members[i].second),
                    _builder.CreateConstInBoundsGEP2_32(nullptr, self_ptr, 0, i),
                    true,
                    false
                });
            break;
        }
        case SynthesizedType::type_category::weak_pointer_type:
        {
            createReleaseCall(scope_info, 
                {
                    static_cast<WeakPointerType*>(data_members[i].second),
                    _builder.CreateConstInBoundsGEP2_32(nullptr, self_ptr, 0, i),
                    false,
                    false
                });
            break;
        }
        }
    }
}

void CGScopeInfo::enterScope(const SymbolTable& table, const AllocationList& list, const SafeCreationList& safe_list) noexcept
{
    ScopeInformation::enterScope(table);
    if (!functions_data.empty())
    {
        functions_data.top().alloc_objs.push_front(list);
        functions_data.top().creation_objs.push_front(safe_list);
    }
}

void CGScopeInfo::exitScope(bool create_alloc_block) noexcept
{
    ScopeInformation::exitScope();
    if (create_alloc_block && !functions_data.empty() && !functions_data.top().alloc_objs.empty())
        functions_data.top().alloc_objs.erase(functions_data.top().alloc_objs.begin());

    if (create_alloc_block && !functions_data.empty() && !functions_data.top().creation_objs.empty())
        functions_data.top().creation_objs.erase(functions_data.top().creation_objs.begin());
}

void CGScopeInfo::enterFunction(const FunctionMetaData& function) noexcept
{
    this->functions_data.push(function);
}

void CGScopeInfo::exitFunction() noexcept
{
    this->functions_data.pop();
}