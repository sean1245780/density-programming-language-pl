import os
import subprocess
import sys
import re


executable_name = "./density"
main_testfile_name = "main"
extensions = ["dens", "dny", "denh", "dnh"]
log_folder_name = "TestLogs"
outputs_folder = "TestOutput"
output_file_extension = ".txt"
generate_output_flag = "--generate"
override_output_flag = "--override"
no_testfile_flag = "no-test"
no_testfile_regex = f"/\\*\s*{no_testfile_flag}\s*\\*/"

override_output = False

found_any = False
any_error = False

DEFAULT_TEST_TIMEOUT = 0.5
MAX_OUTPUT_LEN = 65536

COLOR_BOLD = "\033[1m"

COLOR_RED = COLOR_BOLD + "\033[31m"
COLOR_GREEN = COLOR_BOLD + "\033[32m"
COLOR_YELLOW = COLOR_BOLD + "\033[33m"
COLOR_CYAN = COLOR_BOLD + "\033[36m"
COLOR_WHITE = COLOR_BOLD + "\033[37m"

COLOR_RESET = "\033[0m"

def main():
	global override_output

	# check if the compiler exists
	if not os.path.isfile(executable_name):
		print(COLOR_RED + "Could not find " + executable_name + "!" + COLOR_RESET)
		return

	# check if the program is requested to override the previous files
	override_output = override_output_flag in sys.argv

	# check if the program is requested to generate output files
	if generate_output_flag in sys.argv:
		if generate_outputs():
			if found_any:
				print(COLOR_GREEN + "Successfully generated output data for all test files!" + COLOR_RESET)
			else: 
				print(COLOR_CYAN + "No testfiles found" + COLOR_RESET)
		else:
			print(COLOR_YELLOW + "Could not generate output data for all tests files" + COLOR_RESET)

		return

	# run the tests
	if run_tests_recursive():
		if found_any:
			print(COLOR_GREEN + "All tests passed!" + COLOR_RESET)
		else:
			print(COLOR_CYAN + "No testfiles found" + COLOR_RESET)
	else:
		print(COLOR_YELLOW + "Some tests failed" + COLOR_RESET)


def generate_outputs(path = "./"):
	global found_any
	global any_error

	has_main = False
	printed_msg = False
	dir_content = os.listdir(path)
	dirs = []

	# check if there is a main testfile, if so, generate the output for it
	for entry in dir_content:
		if os.path.isfile(path + entry) and is_testfile(path, entry, True):
			found_any = True
			has_main = True

			print(COLOR_WHITE + f"Generating output for folder {path}:" + COLOR_RESET)

			# generate output for the test
			if generate_output(path, entry, False):
				print(COLOR_GREEN + "\tGenerated output successfully!" + COLOR_RESET)
			else:
				print(COLOR_RED + "\tFailed to generate output!" + COLOR_RESET)
				any_error = True

	# generate output for all tests if there is no main file
	if not has_main:
		for entry in dir_content:
			# check if the file is a testfile
			if os.path.isfile(path + entry) and is_testfile(path, entry):
				found_any = True
				# print message only if there is a file to test in the directory
				if path != "" and not printed_msg:
					print(COLOR_WHITE + f"Generating output for test files in folder {path}:" + COLOR_RESET)
					printed_msg = True

				# generate output for the test
				if generate_output(path, entry, True):
					print(COLOR_GREEN + f"\t- Successfully generated output for {entry}!" + COLOR_RESET)
				else:
					print(COLOR_RED + f"\t- Failed to generate output for {entry}!" + COLOR_RESET)
					any_error = True
			elif os.path.isdir(path + entry):
				# add the directory to be checked later
				dirs.append(path + entry + "/")

		# run the tests on the subdirectories
		for directory in dirs:
			if not generate_outputs(directory):
				any_error = True

	return not any_error


def generate_output(path, filename, use_folder):
	new_filename = '.'.join(filename.split('.')[:-1])

	# check if the output file already exists
	if not override_output and os.path.isfile(path + outputs_folder + "/" + new_filename + output_file_extension):
		print(COLOR_CYAN + f"\t* Output file for {filename} already exists" + COLOR_CYAN)
		return True

	# compile the file
	error = compile_file(path, filename, new_filename)
	
	if error != "":
		log_error(path, new_filename, error, use_folder)
		return False

	# run the executable
	error, out = run_executable(path + new_filename)
	
	if error != "":
		log_error(path, new_filename, error, use_folder)
		return False

	# write the result to the expected results file
	write_expected_output(path, new_filename, out)

	return True


def write_expected_output(path, new_filename, output):
	# check if the outputs folder exists, if not, create it
	if not os.path.isdir(path + outputs_folder):
		print(COLOR_CYAN + "\t* Creating outputs folder: " + path + outputs_folder + COLOR_RESET)

		os.mkdir(path + outputs_folder)

	result_filepath = path + outputs_folder + "/" + new_filename + output_file_extension

	# write the output into the file
	with open(result_filepath, "wb") as output_file:
		output_file.write(output)


def is_testfile(path, filename, check_main = False):
	name_data = filename.split(".")

	if len(name_data) <= 1:
		return False

	before_ext = name_data[0]
	after_ext = name_data[-1]

	# check if the name of the file is a testfile name
	if check_main and before_ext != main_testfile_name:
		return False

	# check if the extension is allowed
	return after_ext in extensions and not has_no_test_comment(path + filename)


def has_no_test_comment(file_path):
	# open the test file and check if there is a comment saying that this file should not be a testfile
	with open(file_path, "r") as test_file:
		matched_data = re.match(no_testfile_regex, test_file.readline())

		# check if the no testfile comment exists
		if matched_data is None:
			return False
		
		return len(matched_data.groups()) >= 0


def run_tests_recursive(path = "./"):
	global found_any
	global any_error

	has_main = False
	printed_msg = False
	dir_content = os.listdir(path)
	dirs = []

	# check if there is a main testfile, if so, run it
	for entry in dir_content:
		if os.path.isfile(path + entry) and is_testfile(path, entry, True):
			found_any = True
			has_main = True

			print(COLOR_WHITE + f"Running main test file in folder {path}:" + COLOR_RESET)

			# run the test
			if run_test(path, entry, False):
				print(COLOR_GREEN + "\tTest passed!" + COLOR_RESET)
			else:
				print(COLOR_RED + "\tTest failed!" + COLOR_RESET)
				any_error = True

	# run all tests in the folder if there is no main file
	if not has_main:
		for entry in dir_content:
			# check if the file is a testfile
			if os.path.isfile(path + entry) and is_testfile(path, entry):
				found_any = True
				# print message only if there is a file to test in the directory
				if path != "" and not printed_msg:
					print(COLOR_WHITE + f"Running tests in folder {path}:" + COLOR_RESET)
					printed_msg = True

				# run the test
				if run_test(path, entry, True):
					print(COLOR_GREEN + f"\t- {entry} passed!" + COLOR_RESET)
				else:
					print(COLOR_RED + f"\t- {entry} failed!" + COLOR_RESET)
					any_error = True
			elif os.path.isdir(path + entry):
				# add the directory to be checked later
				dirs.append(path + entry + "/")

	# run the tests on the subdirectories
	for directory in dirs:
		if not run_tests_recursive(directory):
			any_error = True

	return not any_error


def run_test(path, filename, use_folder):
	new_filename = '.'.join(filename.split('.')[:-1])

	# get the expected output from the testfile
	expected_output = get_expected_output(path, filename, new_filename)

	if expected_output is None:
		return False

	# compile the file
	error = compile_file(path, filename, new_filename)
	
	if error != "":
		log_error(path, new_filename, error, use_folder)
		return False

	# run the executable
	error, _ = run_executable(path + new_filename, expected_output)
	
	if error != "":
		log_error(path, new_filename, error, use_folder)
		return False

	return True


def compile_file(path, filename, new_filename):
	error = ""
	args = [executable_name, path + filename, "-o", path + new_filename, "-vr"]

	# create a child process to compile the test
	proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	out, err = proc.communicate()
	out = out.decode('latin-1')
	err = err.decode('latin-1')

	exit_code = proc.wait()

	# if the compilation failed, set the error
	if exit_code != 0 or len(out) > 0 or len(err) > 0:
		error = f"Compilation Failed. (compiler exited with exit code: {exit_code})\n<stdout>:\n{out}\n<stderr>:\n{err}"

	return error


def run_executable(executable, expected_output = None):
	error = ""

	fix_len = lambda output: output if len(output) <= MAX_OUTPUT_LEN else (output[:MAX_OUTPUT_LEN] + b'\n...')

	# check if the executable exists
	if not os.path.isfile(executable):
		return f"Could not find executable {executable}", ""

	# create a child process to run the executable
	try:
		proc = subprocess.run([executable], stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=DEFAULT_TEST_TIMEOUT)

		out = fix_len(proc.stdout if proc.stdout is not None else b'')
		err = fix_len(proc.stderr if proc.stderr is not None else b'')
		exit_code = proc.returncode

		# check if the executable crashed
		if len(err) != 0 or exit_code != 0:
			error = f"Executable crashed. (exit code: { exit_code })\n<stdout>:\n{ out.decode('latin-1') }\n<stderr>:\n{ err.decode('latin-1') }"
		# compare the expected output and the actual output
		elif expected_output is not None and out != expected_output:
			error = f"Expected:\n{ expected_output.decode('latin-1') }\nGot:\n{ out.decode('latin-1') }\n<stderr>:\n{ err.decode('latin-1') }"
	except subprocess.TimeoutExpired as exception:
		out = fix_len(exception.stdout if exception.stdout is not None else b'')
		err = fix_len(exception.stderr if exception.stderr is not None else b'')
		error = f"Timeout ({DEFAULT_TEST_TIMEOUT}s) expired\n<stdout>:\n{ out.decode('latin-1') }\n<stderr>:\n{ err.decode('latin-1') }"

	return error, out


def get_expected_output(path, filename, new_filename):
	# check if the outputs folder exists
	if not os.path.isdir(path + outputs_folder):
		print(COLOR_YELLOW + "\t* Could not find the expected outputs folder " + path + outputs_folder + COLOR_RESET)
		print(COLOR_YELLOW + "\t* Use the --generate flag to generate expected outputs for all testfiles "+ COLOR_RESET)
		return None

	result_filepath = path + outputs_folder + "/" + new_filename + output_file_extension

	# check if the expected output for the test file exists
	if not os.path.isfile(result_filepath):
		print(COLOR_YELLOW + "\t* Could not find " + result_filepath + COLOR_RESET)
		return None

	# read the file and return the content
	with open(result_filepath, "rb") as output_file:
		return output_file.read()


def log_error(path, filename, error, use_folder):
	# put the log files in a folder
	if use_folder:
		# create the folder if it does not exist
		if not os.path.isdir(path + log_folder_name):
			os.mkdir(path + log_folder_name)
		
		# create the log file inside the folder
		with open(path + log_folder_name + "/" + filename + "_log.txt", 'w') as file:
			file.write(error)
	else:
		# create a single log file
		with open(path + "log.txt", 'w') as file:
			file.write(error)


if __name__ == "__main__":
	main()
