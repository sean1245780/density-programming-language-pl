#include "Parser.h"

ParserOutput Parser::parse(const std::vector<Token*>& tokens)
{
	Parser p(tokens);
	AbstractSyntaxTree::Node* parsed_node = nullptr;
	ParserOutput result;

	while ((parsed_node = p.parseNext()) != nullptr)
	{
		result.parsed_data.push_back(parsed_node);
	}

	result.import_requests = p.getImportRequests();

	return result;
}

AbstractSyntaxTree::Node* Parser::parseNext()
{
	if (peekToken() == nullptr)
		return nullptr;	

	// try parsing using standrad denotation
	AbstractSyntaxTree::Node* last = std();

	if (last != nullptr)
		return last;

	// parse an expression since left denotation did not match anything
	last = parseExpression();
	
	if (last != nullptr)
		if (expect(';') == nullptr && this->_curr_position < this->_tokens.size())
			this->_curr_position--;

	return last;
}

AbstractSyntaxTree::Expression* Parser::nud()
{
	AbstractSyntaxTree::Expression* result = nullptr;
	Token* curr_token = getToken();

	if (curr_token == nullptr)
	{
		this->reportExpected("an expression");

		return nullptr;
	}

	// check the type of the token
	switch (curr_token->type)
	{
	case token_type::literal_bool:
		result = new AbstractSyntaxTree::BooleanLiteral(curr_token->file_pos, (static_cast<LiteralBooleanToken*>(curr_token))->value);
		break;
	case token_type::literal_char:
		result = new AbstractSyntaxTree::CharLiteral(curr_token->file_pos, (static_cast<LiteralCharacterToken*>(curr_token))->value);
		break;
	case token_type::literal_double:
		result = new AbstractSyntaxTree::DoubleLiteral(curr_token->file_pos, (static_cast<LiteralFloatToken*>(curr_token))->value);
		break;
	case token_type::literal_int:
	{
		unsigned long long before_point = (static_cast<LiteralIntToken*>(curr_token))->value;

		// check if there is a "." after the integer 
		if (accept(".") != nullptr)
		{
			this->_curr_position++;
			Token* next = expect(token_type::literal_int, "integer literal");

			// parse as a double literal
			if (next != nullptr)
			{
				auto integer_value = static_cast<LiteralIntToken*>(next)->value;

				result = new AbstractSyntaxTree::DoubleLiteral(curr_token->file_pos, 
							createDouble(before_point, integer_value));
			}
		}
		else 
			result = new AbstractSyntaxTree::IntegerLiteral(curr_token->file_pos, before_point);

		break;
	}
	case token_type::literal_string:
		result = new AbstractSyntaxTree::StringLiteral(curr_token->file_pos, (static_cast<LiteralStringToken*>(curr_token))->value);
		break;
	case token_type::literal_null:
		result = new AbstractSyntaxTree::NullLiteral(curr_token->file_pos, (unsigned int)((static_cast<LiteralCharacterToken*>(curr_token))->value));
		break;
	case token_type::identifier:
		if (accept(":") != nullptr)
		{
			// skip the ":" operator
			this->_curr_position++;
			auto* params = parseExpressionList();
			if (params != nullptr)
			{
				auto* inst = new AbstractSyntaxTree::TypeInstatiation(params->getPosition());
				inst->setParameters(params);
				inst->setType(new AbstractSyntaxTree::SimpleType(
					curr_token->file_pos,
					(static_cast<IdentifierToken*>(curr_token))->name
				));
				inst->setOwnsType(true);
				result = inst;
			}
		}
		else 
			result = new AbstractSyntaxTree::IdentifierExpression(curr_token->file_pos, (static_cast<IdentifierToken*>(curr_token))->name);
		break;
	case token_type::op:
	{
		if (static_cast<OperatorToken*>(curr_token)->op == ".")
		{
			Token* next = expect(token_type::literal_int, "integer literal");

			// parse as a double literal (.71, .004 ...)
			if (next != nullptr)
			{
				auto integer_value = static_cast<LiteralIntToken*>(next)->value;

				result = new AbstractSyntaxTree::DoubleLiteral(curr_token->file_pos, createDouble(0, integer_value));
			}
		}
		else
		{
			// parse as unary expression
			PrecedenceInfo precedence = curr_token->getPrecedenceInfo();

			if (precedence.prefix_precedence != NULL_PRECEDENCE)
			{
				unsigned int last_precedence = curr_token->getRightPrecedence((unsigned int)precedence_type::prefix);

				auto* un_op_expr = new AbstractSyntaxTree::UnaryOperatorExpression(curr_token->file_pos);

				un_op_expr->setPrefix(true);
				un_op_expr->setOperator(((OperatorToken*)curr_token)->op);

				un_op_expr->setExpression(parseExpression(last_precedence));

				result = un_op_expr;
			}
			else
			{
				ErrorRegister::reportError(new ParserError(std::string("Expected prefix operator"), curr_token->file_pos,
					std::string("Operator \"") + ((OperatorToken*)curr_token)->op + std::string("\" is not defined as prefix")));
			}
		}
		
		break;
	}
	case token_type::separator:
	{
		switch (((SeparatorToken*)curr_token)->separator)
		{
		case '(':
		{
			unsigned int last_precedence = curr_token->getRightPrecedence((unsigned int)precedence_type::prefix);

			// parse the inner expression
			result = this->parseExpression(last_precedence);

			// report an error if the closing separator is not ')'
			this->expect(')');
			break;
		}
		default:
			ErrorRegister::reportError(new ParserError("Separator mismatch", curr_token->file_pos, "Expected an expression"));
			break;
		}

		break;
	}
	case token_type::create_kw: // Parsing the same as "new" keyword
	case token_type::new_kw:
	{
		auto* creation_type = parseTypeExpression();
		
		if (creation_type == nullptr) break;

		this->expect(":");
		
		if(this->expect('(') != nullptr)
		{
			this->_curr_position--;

			auto* params = parseExpressionList();
			if (params != nullptr)
			{
				auto* inst = new AbstractSyntaxTree::TypeInstatiation(params->getPosition());
				inst->setParameters(params);
				inst->setType(creation_type);
				inst->setOwnsType(false);
				
				if(curr_token->type == token_type::create_kw)
					result = new AbstractSyntaxTree::CreateFunctionExpression(curr_token->file_pos, inst);
				if(curr_token->type == token_type::new_kw)
					result = new AbstractSyntaxTree::NewFunctionExpression(curr_token->file_pos, inst);
			}
		}

		break;
	}
	case token_type::vargs_kw:
	{
		result = parseVArgsExpression(curr_token->file_pos); // Function call
		
		break;
	}
	default:
		ErrorRegister::reportError(new ParserError("Expected an expression", curr_token->file_pos,
			"Expected an expression"));
		break;
	}

	return result;
}

AbstractSyntaxTree::Expression* Parser::led(AbstractSyntaxTree::Expression* left)
{
	AbstractSyntaxTree::Expression* result = nullptr;

	Token* curr_token = peekToken();

	if (curr_token == nullptr)
		return nullptr;

	PrecedenceInfo precedence = curr_token->getPrecedenceInfo();

	switch (curr_token->type)
	{
	case token_type::op:
	{
		OperatorToken* op_token = static_cast<OperatorToken*>(curr_token);
		if (precedence.binary_precedence != NULL_PRECEDENCE)
		{
			// parse as binary operator expression if the operator has binary precedence
			unsigned int last_precedence = curr_token->getRightPrecedence((unsigned int)precedence_type::binary);

			if(op_token->op == "to" || op_token->op == "as" || op_token->op == "bitcast")
			{
				this->_curr_position++;
				result = parseCastExpression(op_token, left);
			}
			else if (op_token->op == "." || op_token->op == "->")
			{
				auto* member_access = new AbstractSyntaxTree::MemberAccessExpression(op_token->file_pos);
				this->_curr_position++;

				member_access->setLeftExpression(left);
				member_access->setRightExpression(this->parseExpression(last_precedence));
				member_access->setAccessType(
					op_token->op == "." ?
					AbstractSyntaxTree::MemberAccessExpression::AccessType::member_acs : 
					AbstractSyntaxTree::MemberAccessExpression::AccessType::pointer_member_access
				);

				result = static_cast<AbstractSyntaxTree::Expression*>(member_access);
			}
			else if (op_token->op == "::")
			{
				auto* ns_access = new AbstractSyntaxTree::NamespaceAccessExpression(op_token->file_pos);
				this->_curr_position++;

				ns_access->setLeftExpression(left);
				ns_access->setRightExpression(this->parseExpression(last_precedence));
				result = static_cast<AbstractSyntaxTree::Expression*>(ns_access);
			}
			else
			{
				auto* bin_expr = new AbstractSyntaxTree::BinaryOperatorExpression(curr_token->file_pos);

				// skip the operator token
				this->_curr_position++;

				bin_expr->setLeftExpression(left);
				bin_expr->setOperator(((OperatorToken*)curr_token)->op);
				bin_expr->setRightExpression(this->parseExpression(last_precedence));
				result = bin_expr;
			}
		}
		else if (precedence.postfix_precedence != NULL_PRECEDENCE)
		{
			// parse as unary operator expression if the operator has postfix precedence
			auto* unary_expr = new AbstractSyntaxTree::UnaryOperatorExpression(curr_token->file_pos);

			// skip the operator token
			this->_curr_position++;

			unary_expr->setExpression(left);
			unary_expr->setPrefix(false);
			unary_expr->setOperator(((OperatorToken*)curr_token)->op);
			result = static_cast<AbstractSyntaxTree::Expression*>(unary_expr);
		}
		else
		{
			ErrorRegister::reportError((new ParserError("Encountered prefix operator", curr_token->file_pos,
				"Expected postfix / binary operator"))->withNote("Prefix operators must come before an expression"));
		}
		
		break;
	}
	case token_type::separator:
	{
		const char& separator_obj = ((SeparatorToken*)curr_token)->separator;
		switch (separator_obj)
		{
		case  '[':
		{
			// skip the '['
			this->_curr_position++;
			auto* inside_brackets = parseExpression(curr_token->getRightPrecedence((unsigned int)precedence_type::prefix));
			
			if(inside_brackets == nullptr) break;
			
			if(this->expect(']') == nullptr)
			{
				delete inside_brackets;
				break;
			}

			auto* member_acc = new AbstractSyntaxTree::MemberAccessExpression(curr_token->file_pos, AbstractSyntaxTree::MemberAccessExpression::AccessType::index_acs);
			member_acc->setLeftExpression(left);
			member_acc->setRightExpression(inside_brackets);

			result = member_acc;

			break;
		}
		case '(':
			// parse call expression

			result = new AbstractSyntaxTree::CallExpression(left->getPosition());

			((AbstractSyntaxTree::CallExpression*)result)->setFunctionExpression(left);
			((AbstractSyntaxTree::CallExpression*)result)->setArguments(parseExpressionList());
			break;
		case ',':
		{
			// parse expression list
			AbstractSyntaxTree::ExpressionList* expr_lst = new AbstractSyntaxTree::ExpressionList(left->getPosition());
			AbstractSyntaxTree::Expression* expr = nullptr;

			expr_lst->addExpression(left);

			do {
				// skip the ',' separator
				this->_curr_position++;

				expr = parseExpression(curr_token->getRightPrecedence((int)precedence_type::binary));

				if (expr == nullptr)
				{
					// remove the first expression to avoid double deletion
					expr_lst->setExpression(0, nullptr);
					delete expr_lst;

					return nullptr;
				}

				expr_lst->addExpression(expr);
			} while(accept(',') != nullptr);

			result = expr_lst;

			break;
		}
		default:
			ErrorRegister::reportError(new ParserError("Separator mismatch", curr_token->file_pos,
				std::string("The separator '") + separator_obj + 
				std::string("' cannot come after an expression")));
			break;
		}
		break;
	}
	case token_type::vargs_kw:
	{
		this->_curr_position++;

		result = parseVArgsExpression(curr_token->file_pos); // Function call

		break;
	}

	default:
		ErrorRegister::reportError(new ParserError("Failed to match left denotation", curr_token->file_pos,
			"Expected operator / separator"));

		break;
	}

	return result;
}

AbstractSyntaxTree::Node* Parser::std()
{	
	AbstractSyntaxTree::Node* result = nullptr;
	AbstractSyntaxTree::Properties properties;

	Token* curr_token = peekToken();

	if (curr_token == nullptr)
		return nullptr;

	switch (curr_token->type)
	{
	case token_type::namespace_kw:
	{
		this->_curr_position++;
		
		Token* identifier_token = nullptr;

		// make sure that there is an identifier
		if ((identifier_token = accept(token_type::identifier)) == nullptr)
		{
			reportExpected("an identifier");
			
			return nullptr;
		}

		this->_curr_position++;
		if(this->expect('{') == nullptr) break;

		auto* identifier = new AbstractSyntaxTree::IdentifierExpression
			(identifier_token->file_pos, static_cast<IdentifierToken*>(identifier_token)->name);

		AbstractSyntaxTree::NamespaceDefinition* namespace_def =
			new AbstractSyntaxTree::NamespaceDefinition(curr_token->file_pos, identifier);

		AbstractSyntaxTree::Node* parsed_node = nullptr;

		while (accept('}') == nullptr && (parsed_node = this->parseNext()) != nullptr)
			namespace_def->AddCommand(parsed_node);

		if(this->expect('}') == nullptr)
		{
			delete namespace_def;
			namespace_def = nullptr;
		}

		result = namespace_def;

		break;
	}
	case token_type::type_kw:
	{
		this->_curr_position++;

		// parse the name of the type
		auto* name = parseIdentifierExpression();

		if (name == nullptr) break;

		// check that there is a '{'
		if (this->expect('{') == nullptr) 
		{
			delete name;
			break;
		}

		// return an empty type if there is a '}' immediately after the '{'
		if (this->accept('}') != nullptr)
		{
			this->_curr_position++;

			AbstractSyntaxTree::TypeDefinition* type_definition = new AbstractSyntaxTree::TypeDefinition(name->getPosition());

			type_definition->setTypeName(name);

			result = type_definition;
			break;
		}

		vector<AbstractSyntaxTree::Node*> type_content = { parseNext() };
		AbstractSyntaxTree::Node* curr_node;

		// parse the content of the type
		while (type_content.back() != nullptr && this->accept('}') == nullptr)
		{
			type_content.push_back(parseNext());
		}

		// if parseNext failed, delete allocated memory 
		if (type_content.back() == nullptr)
		{
			delete name;
			for (auto* node : type_content)
				delete node;
			break;
		}

		this->expect('}');

		AbstractSyntaxTree::TypeDefinition* type_definition = new AbstractSyntaxTree::TypeDefinition(name->getPosition());

		type_definition->setTypeName(name);

		vector<AbstractSyntaxTree::VariableDeclaration*> vars;
		vector<AbstractSyntaxTree::FunctionDefinition*> funcs;

		for (auto* node : type_content)
		{
			bool invalid = false;

			// add variable declarations and functions to the type 
            if (node->getNodeType() == AbstractSyntaxTree::Node::node_type::statement)
            {
                if (static_cast<AbstractSyntaxTree::Statement*>(node)->getStatementType() == 
                    AbstractSyntaxTree::Statement::statement_type::variable_declaration)
                {
					vars.push_back(static_cast<AbstractSyntaxTree::VariableDeclaration*>(node));
                }
				else invalid = true;
            }
			// add the member functions
            else if (node->getNodeType() == AbstractSyntaxTree::Node::node_type::function_definition)
            {
                funcs.push_back(static_cast<AbstractSyntaxTree::FunctionDefinition*>(node));
            }
			else invalid = true;

            if (invalid)
            {
                ErrorRegister::reportError(new ParserError(
                    "Only functions and variables are allowed inside a type",
                    node->getPosition()
                ));
            }
		}

		type_definition->setTypeDataMembers(vars);
		type_definition->setTypeFunctions(funcs);

		result = type_definition;
		break;
	}
	case token_type::delete_kw:
	{
		this->_curr_position++;

		auto* expr = parseExpression();

		if (expr == nullptr) break;
		if (this->expect(';') == nullptr)
		{
			delete expr;
			break;
		} 

		result = new AbstractSyntaxTree::DeleteFunctionStatement(curr_token->file_pos, expr);

		break;
	}
	case token_type::while_kw:
	{
		// skip the while keyword
		this->_curr_position++;

		// parse: "(expression)"
		if (this->expect('(') == nullptr) break;

		auto* condition = parseExpression();
		if (condition == nullptr) break;

		if (this->expect(')') == nullptr) 
		{
			delete condition;
			break;
		}

		// parse the commands of the while
		auto* commands = parseNext();

		if (commands == nullptr) 
		{
			delete condition;
			break;
		}

		// check that the parsed node is a statement
		if (commands->getNodeType() != AbstractSyntaxTree::Node::node_type::statement) 
		{
			ErrorRegister::reportError((new ParserError(
				"Expected a statement after 'while'",
				commands->getPosition()
			))->withNote("Only statements are allowed inside a while statement"));

			delete condition;
			break;
		}

		auto* while_statement = new AbstractSyntaxTree::WhileStatement(curr_token->file_pos);

		while_statement->setCondition(condition);
		while_statement->setCommands(static_cast<AbstractSyntaxTree::Statement*>(commands));

		result = while_statement;
		break;
	}
	case token_type::for_kw:
		result = parseForStatement();
		break;
	case token_type::constructor_kw:
		this->_curr_position++;
		result = parseFunctionDefinition(
				new AbstractSyntaxTree::NamelessComplexType(curr_token->file_pos),
				new AbstractSyntaxTree::IdentifierExpression(curr_token->file_pos, CONSTRUCTOR_KEYWORD)
			);
		break;
	case token_type::destructor_kw:
		this->_curr_position++;
		result = parseFunctionDefinition(
				new AbstractSyntaxTree::NamelessComplexType(curr_token->file_pos),
				new AbstractSyntaxTree::IdentifierExpression(curr_token->file_pos, DESTRUCTOR_KEYWORD)
			);
		break;
	case token_type::import_kw:		// parse import statement
		{
			this->_curr_position++;

			// parse the name of the import
			auto* module_str = this->expect(token_type::literal_string, "a module name (string)");

			if (module_str == nullptr)
				break;

			string module_name = static_cast<LiteralStringToken*>(module_str)->value;

			// make sure that there is a ';' separator at the end of the command
			if (this->expect(';') != nullptr)
			{
				// create the import statement
				result = new AbstractSyntaxTree::ImportStatement(
					curr_token->file_pos,
					module_name
				);

				this->_import_requests.push_back(static_cast<AbstractSyntaxTree::ImportStatement*>(result));
			}
		}
		break;
	// parse properties and proceede to parse the identifier
	case token_type::extern_kw:
		properties = acceptProperties();
	case token_type::self_kw:	// "Self" is used as a type expression
	case token_type::strong_kw:
	case token_type::weak_kw:
	case token_type::identifier:
		// check:	<Type Expression> <identifier> <Complex type>	==> function
		//			<Type Expression> <identifier>					==> variable declaration
		//			<identifier> 	  <anything else>				==> not handled by standard denotation
		{			
			unsigned int last_pos = this->_curr_position;

			AbstractSyntaxTree::TypeExpression* type_expr = parseTypeExpression();

			// check if the type was successfully parsed			
			if (type_expr == nullptr)
				break;

			if (this->accept(token_type::identifier) != nullptr)
			{
				if (accept('(', 1) != nullptr)
					// it's a function
					result = parseFunctionDefinition(type_expr);
				else
				{
					// its a variable declaration
					result = parseVariableDeclaration(type_expr);

					if (result != nullptr)
						this->expect(';');
				}

				if(result != nullptr)
				{
					result->getProperties().updateProperties(properties, true);
					result->setProperties(result->getProperties());
				}
			}
			else 
			{
				delete type_expr;
				this->_curr_position = last_pos;
			}
		}

		break;
	case token_type::separator:
		switch (((SeparatorToken*)(curr_token))->separator)
		{
		case '{': {	// parse statement block
			// consume the '{' separator
			this->_curr_position++;

			AbstractSyntaxTree::StatementBlock* block = new AbstractSyntaxTree::StatementBlock(curr_token->file_pos);
			AbstractSyntaxTree::Node* next = nullptr;

			while (accept('}') == nullptr && ((next = parseNext()) != nullptr))
			{
				if (next->getNodeType() == AbstractSyntaxTree::Node::node_type::statement)
					block->addStatement((AbstractSyntaxTree::Statement*)next);
				else
					ErrorRegister::reportError((new ParserError("Expected a statement", next->getPosition(),
						"Expected a statement"))->withNote("Only statements are allowed inside a statement block"));
			}

			// make sure that there is a closing '}'
			expect('}');

			result = block;
		}
			break;
		case '@':
		{
			// check:	<complex type> <identifier> '(' ==> function definition
			//			<complex type> <identifier> 	==> variable declaration
			//			<complex type> <not identifier>	==> not handled by standard denotation

			// make sure that there is a type
			auto* type = parseTypeExpression();

			if (type == nullptr)
				return nullptr;

			// make sure that there is an identifier
			if (accept(token_type::identifier) == nullptr)
			{
				reportExpected("an identifier");
				
				delete type;	
				return nullptr;
			}

			// if there is '(', it is a function, else, it is a variable declaration
			if (accept('(', 1) != nullptr)
				result = parseFunctionDefinition(type);
			else
			{
				result = parseVariableDeclaration(type);

				if (result != nullptr)
					this->expect(';');	
			}

			break;	
		}
	}

	break;
	case token_type::return_kw:
	{
		this->_curr_position++;
		auto* val = new AbstractSyntaxTree::ReturnStatement(curr_token->file_pos);
		
		if (this->accept(';') == nullptr)
		{
			val->setReturnValue(parseExpression());
		}
		
		this->expect(';');

		result = val;
		break;
	}
	case token_type::if_kw:
	{
		this->_curr_position++;
		auto* if_token = curr_token;
		
		// expect the starting '('
		if ((curr_token = this->expect('(')) == nullptr)
			break;

		// parse the inner condition
		auto* if_condition = this->parseExpression(curr_token->getRightPrecedence((int)precedence_type::prefix));

		if (if_condition == nullptr) break;

		// expect the ending ')'
		if (this->expect(')') == nullptr)
		{
			delete if_condition;
			break;
		}

		// parse the commands to be executed if the condition is true
		auto* if_commands = parseNext();

		if (if_commands == nullptr) 
		{
			delete if_condition;
			break;
		}

		if (if_commands->getNodeType() != AbstractSyntaxTree::Node::node_type::statement) 
		{
			ErrorRegister::reportError((new ParserError(
				"Expected a statement after 'if'",
				if_commands->getPosition()
			))->withNote("Only statements are allowed inside an if statement"));

			delete if_condition;
			break;
		}

		// allow else 
		AbstractSyntaxTree::Node* else_commands = nullptr;
		if (accept(token_type::else_kw) != nullptr)
		{
			// skip the else keyword
			this->_curr_position++;
			else_commands = parseNext();

			// check that the else command is valid
			if (else_commands != nullptr && else_commands->getNodeType() != AbstractSyntaxTree::Node::node_type::statement) 
				ErrorRegister::reportError((new ParserError(
					"Expected a statement after 'else'",
					else_commands->getPosition()
				))->withNote("Only statements are allowed after 'else'"));
		}
		
		auto* if_cond = new AbstractSyntaxTree::ConditionalStatement(if_token->file_pos);
		if_cond->setCondition(if_condition);
		if_cond->setIfCommands(static_cast<AbstractSyntaxTree::Statement*>(if_commands)); 
		if_cond->setElseCommands(static_cast<AbstractSyntaxTree::Statement*>(else_commands));
		
		result = if_cond;
		break;
	}
		
	}

	return result;
}

AbstractSyntaxTree::TypeExpression* Parser::type_nud(bool report)
{
	auto* curr_tok = peekToken();

	if (curr_tok == nullptr) return nullptr;

	// check the type of the token
	switch (curr_tok->type)
	{
		case token_type::self_kw:
			this->_curr_position++;
			return new AbstractSyntaxTree::SimpleType(curr_tok->file_pos, SELF_TYPE_KEYWORD);
		case token_type::identifier:
			this->_curr_position++;
			return new AbstractSyntaxTree::SimpleType(curr_tok->file_pos, 
								static_cast<IdentifierToken*>(curr_tok)->name);
		case token_type::separator:
			if ((static_cast<SeparatorToken*>(curr_tok))->separator == '@')
				return parseComplexType(true);
		case token_type::strong_kw:
		{
			this->_curr_position++;
			auto* safe_ptr = parseTypeExpression();
			if (safe_ptr != nullptr) {
				safe_ptr->getProperties().is_strong = true;

				return safe_ptr;
			}
		}
		case token_type::weak_kw:
		{
			this->_curr_position++;
			auto* safe_ptr = parseTypeExpression();
			if (safe_ptr != nullptr) {
				safe_ptr->getProperties().is_weak = true;
				
				return safe_ptr;
			}
		}
	}

	if (report)
		ErrorRegister::reportError(new ParserError(
			"Expected a type expression",
			curr_tok->file_pos
		));

	this->_curr_position++;
	return nullptr;
}

AbstractSyntaxTree::TypeExpression* Parser::type_led(AbstractSyntaxTree::TypeExpression* left, bool report)
{
	AbstractSyntaxTree::TypeExpression* result = nullptr;

	Token* curr_token = peekToken();

	if (curr_token == nullptr) return nullptr;

	PrecedenceInfo precedence = curr_token->getPrecedenceInfo();

	switch (curr_token->type)
	{
		case token_type::op:
		{
			auto op =(static_cast<OperatorToken*>(curr_token))->op;
			if (op == "*")
			{
				this->_curr_position++;

				return new AbstractSyntaxTree::PointerType(curr_token->file_pos, left);
			}
			else
			{
				ErrorRegister::reportError(new ParserError(
					"Operator " + op + " cannot be a chained to a type", 
					curr_token->file_pos,
					"Expected valid type operator"));				
			}
			break;
		}
		default:
				ErrorRegister::reportError(new ParserError("Failed to match left denotation", curr_token->file_pos,
				"Expected operator"));

		break;
	}

	return result;
}

AbstractSyntaxTree::FunctionDefinition* Parser::parseFunctionDefinition()
{
	// parse the return type of the function
	AbstractSyntaxTree::TypeExpression* return_type = parseTypeExpression();

	if (return_type == nullptr)
		return nullptr;

	return parseFunctionDefinition(return_type);
}

AbstractSyntaxTree::ConditionalStatement* Parser::parseConditional()
{
	return nullptr;
}

AbstractSyntaxTree::Properties Parser::parseProperties()
{
	AbstractSyntaxTree::Properties properties{};
	bool is_property = true;
	Token* curr_token = peekToken();

	if(curr_token == nullptr) return {};

	while(is_property && curr_token != nullptr)
	{
		switch (curr_token->type)
		{
		case token_type::vargs_kw:
			properties.is_variadic = true;
			break;
		default:
			is_property = false;
			break;
		}
		
		if(is_property)
		{	
			this->_curr_position++;
			curr_token = peekToken();
		}	
	}
	
	return std::move(properties);
}

AbstractSyntaxTree::VarArgsExpression* Parser::parseVArgsExpression(const position_data& keyword_pos)
{
	if(this->expect('(') == nullptr);

	auto* type = parseTypeExpression();

	if(type == nullptr) return nullptr;

	if(this->expect(')') == nullptr) return nullptr;

	return new AbstractSyntaxTree::VarArgsExpression(keyword_pos, type);
}

inline AbstractSyntaxTree::FunctionDefinition* Parser::parseFunctionDefinition(AbstractSyntaxTree::TypeExpression* return_type)
{
	// make sure that the return type exists
	if (return_type == nullptr)
		return nullptr;

	// parse the name of the function
	AbstractSyntaxTree::IdentifierExpression* function_name = parseIdentifierExpression();
	
	if (function_name == nullptr)
	{
		delete return_type;

		return nullptr;
	}

	return parseFunctionDefinition(return_type, function_name);
}

inline AbstractSyntaxTree::FunctionDefinition* Parser::parseFunctionDefinition(AbstractSyntaxTree::TypeExpression* return_type, AbstractSyntaxTree::IdentifierExpression* function_name)
{
	if (return_type == nullptr)
		return nullptr;

	if (function_name == nullptr)
	{
		delete return_type;
		return nullptr;
	}
	
	// parse the input type of the function
	AbstractSyntaxTree::ComplexType* input_type = parseNamedComplexType(false);

	if (input_type == nullptr)
	{
		delete return_type;
		delete function_name;

		return nullptr;
	}

	auto properties = parseProperties();

	AbstractSyntaxTree::Node* function_body = nullptr;

	if (this->accept(';'))
	{
		this->_curr_position++;
	}
	else 
	{
		// parse the body of the function
		function_body = parseNext();

		if (function_body == nullptr || function_body->getNodeType() != AbstractSyntaxTree::Node::node_type::statement)
		{
			delete return_type;
			delete function_name;
			delete input_type;
			if (function_body != nullptr) delete function_body;

			return nullptr;
		}
	}

	AbstractSyntaxTree::FunctionDefinition* result = new AbstractSyntaxTree::FunctionDefinition(function_name->getPosition());

	result->setCommands(static_cast<AbstractSyntaxTree::Statement*>(function_body));
	result->setFunctionName(function_name);
	result->setInputType(input_type);
	result->setReturnType(return_type);
	result->setProperties(properties);
	
	return result;
}

AbstractSyntaxTree::TypeExpression* Parser::parseTypeExpression(unsigned int last_precedence)
{
	int last_pos = this->_curr_position;
	AbstractSyntaxTree::TypeExpression* result = type_nud();

	if (result == nullptr)
	{
		this->_curr_position = last_pos;
		return nullptr;
	}

	AbstractSyntaxTree::TypeExpression* temp_root = nullptr;

	while (this->_curr_position < this->_tokens.size() &&
		last_precedence < this->_tokens[this->_curr_position]->getTypePrecedenceInfo()
							.leftPrecedence(((unsigned int)precedence_type::binary) | ((unsigned int)precedence_type::postfix)))
	{
		// try parsing using left denotation
		temp_root = type_led(result);

		// check if left denotation succeeded
		if (temp_root == nullptr)
		{
			this->_curr_position = last_pos;
			delete result;
			return nullptr;
		}

		// set the root node to be the new node
		result = temp_root;
	}
	
	return result;
}

AbstractSyntaxTree::ExpressionList* Parser::parseExpressionList()
{
	Token* curr_tok = nullptr;

	// make sure that there is '(' starting the complex type
	if ((curr_tok = this->expect('(')) == nullptr)
		return nullptr;

	// if there is a ')', create an empty expression list
	if (this->accept(')') != nullptr)
	{
		this->_curr_position++;

		return new AbstractSyntaxTree::ExpressionList(curr_tok->file_pos);
	}

	AbstractSyntaxTree::Expression* expr = parseExpression(curr_tok->getRightPrecedence((int)precedence_type::prefix));
	AbstractSyntaxTree::ExpressionList* result = dynamic_cast<AbstractSyntaxTree::ExpressionList*>(expr);

	// if the parsed expression is not an expression list, create a new expression list with a single expression element
	if (result == nullptr)
	{
		result = new AbstractSyntaxTree::ExpressionList(curr_tok->file_pos);
		result->addExpression(expr);
	}

	// make sure that there is ')' ending the complex type
	this->expect(')');

	return result;
}

AbstractSyntaxTree::CastExpression* Parser::parseCastExpression(OperatorToken* cast_token, AbstractSyntaxTree::Expression* inner)
{
	if(cast_token == nullptr) return nullptr;

	auto casting_type = AbstractSyntaxTree::CastExpression::cast_type::cast_static;
	if (cast_token->op == "to") {} // The same as the above
	else if (cast_token->op == "as") casting_type = AbstractSyntaxTree::CastExpression::cast_type::cast_dynamic;
	else if (cast_token->op == "bitcast") casting_type = AbstractSyntaxTree::CastExpression::cast_type::cast_bitcast;
	else
	{
		ErrorRegister::reportError(new ParserError(
			"Invalid casting type",
			cast_token->file_pos,
			"Expected to/as/bitcast casting operators"
		));

		return nullptr;
	}

	auto* type_expr = parseTypeExpression();
	if (type_expr == nullptr) return nullptr;

	return new AbstractSyntaxTree::CastExpression(cast_token->file_pos, inner, type_expr, casting_type);
}

AbstractSyntaxTree::Expression* Parser::parseExpression(unsigned int last_precedence)
{
	// try parsing using null denotation
	AbstractSyntaxTree::Expression* result = nud();

	if (result == nullptr)
		return nullptr;	

	AbstractSyntaxTree::Expression* temp_root = nullptr;

	// repeat left denotation parsing until the precedence of the next token is smaller
	while (this->_curr_position < this->_tokens.size() &&
		last_precedence < this->_tokens[this->_curr_position]->getLeftPrecedence(((unsigned int)precedence_type::binary) |
																				 ((unsigned int)precedence_type::postfix)))
	{
		// try parsing using left denotation
		temp_root = led(result);

		// check if left denotation succeeded
		if (temp_root == nullptr)
		{
			this->_curr_position++;
			delete result;
			return nullptr;
		}

		// set the root node to be the new node
		result = temp_root;
	}

	return result;
}

AbstractSyntaxTree::ComplexType* Parser::parseNamedComplexType(bool expect_at)
{
	Token* curr_tok = nullptr;

	if (expect_at)
	{
		if ((curr_tok = this->expect('@')) == nullptr) return nullptr;
	
		// return an empty complex type if there is no '('
		if (this->accept('(') == nullptr)
			return new AbstractSyntaxTree::ComplexType(curr_tok->file_pos);

		this->_curr_position++;
	}
	else if ((curr_tok = this->expect('(')) == nullptr) return nullptr;
	
	AbstractSyntaxTree::ComplexType* result = new AbstractSyntaxTree::ComplexType(curr_tok->file_pos);
	AbstractSyntaxTree::TypeExpression* type = nullptr;
	AbstractSyntaxTree::IdentifierExpression* name = nullptr;
	bool has_comma = false;

	if (this->accept(')') != nullptr)
	{
		this->_curr_position++;

		return result;
	}

	do {
		has_comma = false;

		type = parseTypeExpression();
		name = parseIdentifierExpression();

		if (type != nullptr && name != nullptr)
			result->addComponent(type, name);

		if ((has_comma = (accept(',') != nullptr)))
			this->_curr_position++;
	} while (type != nullptr && name != nullptr && has_comma);

	// make sure that there is ')' ending the complex type
	this->expect(')');	

	return result;
}

AbstractSyntaxTree::NamelessComplexType* Parser::parseNamelessComplexType(bool expect_at)
{
	Token* curr_tok = nullptr;

	if (expect_at)
	{
		if ((curr_tok = this->expect('@')) == nullptr) return nullptr;
	
		// return an empty complex type if there is no '('
		if (this->accept('(') == nullptr)
			return new AbstractSyntaxTree::NamelessComplexType(curr_tok->file_pos);

		this->_curr_position++;
	}
	else if ((curr_tok = this->expect('(')) == nullptr) return nullptr;

	AbstractSyntaxTree::NamelessComplexType* result = new AbstractSyntaxTree::NamelessComplexType(curr_tok->file_pos);
	AbstractSyntaxTree::TypeExpression* type = nullptr;
	bool has_comma = false;

	if (this->accept(')') != nullptr)
	{
		this->_curr_position++;

		return result;
	}

	do {
		has_comma = false;

		type = parseTypeExpression();

		if (type != nullptr)
			result->addComponent(type);

		if ((has_comma = (accept(',') != nullptr)))
			this->_curr_position++;
	} while (type != nullptr && has_comma);

	// make sure that there is ')' ending the complex type
	this->expect(')');	

	return result;
}

AbstractSyntaxTree::IdentifierExpression* Parser::parseIdentifierExpression()
{
	IdentifierToken* curr_tok = (IdentifierToken*)expect(token_type::identifier, "an identifier");

	if (curr_tok == nullptr)
		return nullptr;

	return new AbstractSyntaxTree::IdentifierExpression(curr_tok->file_pos, curr_tok->name);
}

AbstractSyntaxTree::VariableDeclaration* Parser::parseVariableDeclaration(AbstractSyntaxTree::TypeExpression* type)
{
	if (type == nullptr) return nullptr;

	// create a variable declaration node
	AbstractSyntaxTree::VariableDeclaration* result = new AbstractSyntaxTree::VariableDeclaration(type->getPosition());
	((AbstractSyntaxTree::VariableDeclaration*)result)->setType(type);

	AbstractSyntaxTree::IdentifierExpression* variable_name = nullptr;
	AbstractSyntaxTree::Expression* value = nullptr;
	bool has_comma = false;
	
	do {
		has_comma = false;
		value = nullptr;

		// parse the name of the variable
		variable_name = parseIdentifierExpression();

		if (variable_name != nullptr)
		{
			// allow initialization
			if (accept("=") != nullptr)
			{
				// skip the "=" operator
				this->_curr_position++;
				value = parseExpression(OperatorsInfo::inst().getOperator("=")->precedence.rightPrecedence((unsigned int)precedence_type::binary));
			}
			else if (accept(":") != nullptr)
			{
				// skip the ":" operator
				this->_curr_position++;
				auto* params = parseExpressionList();
				if (params != nullptr)
				{
					auto* inst = new AbstractSyntaxTree::TypeInstatiation(params->getPosition());
					inst->setParameters(params);
					inst->setType(type);
					inst->setOwnsType(false);
					value = inst;
				}
			}

			result->addVariable(variable_name, value);
		}

		// allow multiple variable initialization
		if (variable_name != nullptr && accept(',') != nullptr) 
		{
			this->_curr_position++;
			has_comma = true;
		}
	} while (variable_name != nullptr && has_comma);

	return result;
}

AbstractSyntaxTree::TypeExpression* Parser::parseComplexType(bool expect_at)
{
	Token* curr_tok = nullptr;

	if (expect_at)
	{
		if ((curr_tok = this->expect('@')) == nullptr) return nullptr;
	
		// return an empty complex type if there is no '('
		if (this->accept('(') == nullptr)
			return new AbstractSyntaxTree::ComplexType(curr_tok->file_pos);

		this->_curr_position++;
	}
	else if ((curr_tok = this->expect('(')) == nullptr) return nullptr;

	AbstractSyntaxTree::TypeExpression* type = nullptr, *final_result = nullptr;
	AbstractSyntaxTree::IdentifierExpression* name = nullptr;
	bool has_comma = false;

	if (this->accept(')') != nullptr)
	{
		this->_curr_position++;

		return new AbstractSyntaxTree::ComplexType(curr_tok->file_pos);
	}

	bool is_named = true;

	// determine the type of the complex type
	type = parseTypeExpression();

	if (accept(',') != nullptr) {
		is_named = false;
		this->_curr_position++;
	}
	else if (accept(token_type::identifier) != nullptr){
		name = parseIdentifierExpression();

		if (accept(')') != nullptr)
		{
			this->_curr_position++;

			auto* named_complex = new AbstractSyntaxTree::ComplexType(curr_tok->file_pos);
			
			named_complex->addComponent(type, name);

			return named_complex;
		}
		else
			expect(',');
	}

	if (is_named)
	{
		// parse named complex type
		auto* result = new AbstractSyntaxTree::ComplexType(curr_tok->file_pos);
		
		if (type != nullptr && name != nullptr)
		{
			result->addComponent(type, name);

			do {
				has_comma = false;

				type = parseTypeExpression();
			
				if (accept(token_type::identifier))
					name = parseIdentifierExpression();

				if (type != nullptr && name != nullptr)
					result->addComponent(type, name);

				if ((has_comma = (accept(',') != nullptr)))
					this->_curr_position++;
			} while (type != nullptr && name != nullptr && has_comma);
		}

		final_result = result;
	}
	else 
	{
		// parse unnamed complex type
		auto* result = new AbstractSyntaxTree::NamelessComplexType(curr_tok->file_pos);
		
		if (type != nullptr)
		{
			result->addComponent(type);

			do {
				has_comma = false;

				type = parseTypeExpression();
			
				if (type != nullptr)
					result->addComponent(type);

				if ((has_comma = (accept(',') != nullptr)))
					this->_curr_position++;
			} while (type != nullptr && has_comma);
		}

		final_result = result;
	}

	// make sure that there is ')' ending the complex type
	this->expect(')');	

	return final_result;
}

AbstractSyntaxTree::ForStatement* Parser::parseForStatement()
{
	Token* for_keyword = nullptr;
	AbstractSyntaxTree::VariableDeclaration* init = nullptr;
	AbstractSyntaxTree::Expression* cond = nullptr;
	AbstractSyntaxTree::Expression* inc = nullptr;
	AbstractSyntaxTree::Node* commands = nullptr;
	auto cleanup = [&](){
		if (init != nullptr) delete init;
		if (cond != nullptr) delete cond;
		if (inc != nullptr) delete inc;
		if (commands != nullptr) delete commands;
	};

	if ((for_keyword = expect(token_type::for_kw, "for keyword")) == nullptr)
		return nullptr;

	if (expect('(') == nullptr)
		return nullptr;

	// parse the initialization
	if (accept(';') == nullptr)
	{
		init = parseVariableDeclaration(parseTypeExpression());

		if (init == nullptr || expect(';') == nullptr) { cleanup(); return nullptr; }
	} else this->_curr_position++;

	// parse the condition
	cond = parseExpression();

	if (cond == nullptr || expect(';') == nullptr) { cleanup(); return nullptr; }

	// parse the incrementation
	if (accept(')') == nullptr)
	{
		inc = parseExpression();

		if (inc == nullptr) { cleanup(); return nullptr; }
	}

	if (expect(')') == nullptr) { cleanup(); return nullptr; }

	commands = parseNext();

	if (commands == nullptr) { cleanup(); return nullptr; }

	if (commands->getNodeType() != AbstractSyntaxTree::Node::node_type::statement)
	{
		ErrorRegister::reportError((new ParserError(
			"Expected a statement after 'for'",
			commands->getPosition()
		))->withNote("Only statements are allowed inside a for statement"));

		cleanup();
		return nullptr;
	}

	AbstractSyntaxTree::ForStatement* result = new AbstractSyntaxTree::ForStatement(for_keyword->file_pos);

	result->setInitialization(init);
	result->setCondition(cond);
	result->setIncrementation(inc);
	result->setCommands(static_cast<AbstractSyntaxTree::Statement*>(commands));

	return result;
}

inline Token* Parser::expect(token_type expected, const std::string& expected_name)
{
	if (this->_curr_position >= this->_tokens.size())
	{
		if (this->_tokens.size() > 0)
			ErrorRegister::reportError(new ParserError(std::string("Expected ") + expected_name, this->_tokens[this->_tokens.size() - 1]->file_pos,
				std::string("Expected ") + expected_name + std::string(" after this token")));

		return nullptr;
	}

	if (this->_tokens[this->_curr_position]->type != expected)
	{
		ErrorRegister::reportError(new ParserError(std::string("Expected ") + expected_name, this->_tokens[this->_curr_position]->file_pos,
			std::string("Expected ") + expected_name));

		this->_curr_position++;

		return nullptr;
	}

	return this->_tokens[this->_curr_position++];
}

inline Token* Parser::accept(token_type expected, const unsigned int& lookahead)
{
	if (this->_curr_position + lookahead >= this->_tokens.size() || this->_tokens[this->_curr_position + lookahead]->type != expected)
		return nullptr;

	return this->_tokens[this->_curr_position + lookahead];
}

inline Token* Parser::getToken()
{
	if (this->_curr_position >= this->_tokens.size())
		return nullptr;

	return this->_tokens[this->_curr_position++];
}

inline Token* Parser::peekToken(const unsigned int& lookahead)
{
	if (this->_curr_position + lookahead >= this->_tokens.size())
		return nullptr;

	return this->_tokens[this->_curr_position + lookahead];
}

inline OperatorToken* Parser::expect(std::string op)
{
	OperatorToken* result = (OperatorToken*)expect(token_type::op, string("'") + op + string("'"));

	if (result == nullptr)
		return nullptr;

	if (result->op != op)
	{
		reportExpected(std::string("'") + op + std::string("'"), false, -1);
		return nullptr;
	}

	return result;
}

inline OperatorToken* Parser::accept(std::string op, const unsigned int& lookahead)
{
	OperatorToken* result = (OperatorToken*)accept(token_type::op, lookahead);

	if (result == nullptr)
		return nullptr;

	return (result->op == op ? result : nullptr);
}

SeparatorToken* Parser::expect(char separator)
{
	SeparatorToken* result = (SeparatorToken*)expect(token_type::separator, std::string("\'") + separator + std::string("\'"));

	if (result == nullptr)
		return nullptr;

	if (result->separator != separator)
	{
		reportExpected(std::string("'") + separator + std::string("'"), false, -1);
		return nullptr;
	}

	return result;
}

inline SeparatorToken* Parser::accept(char separator, const unsigned int& lookahead)
{
	SeparatorToken* result = (SeparatorToken*)accept(token_type::separator, lookahead);

	if (result == nullptr)
		return nullptr;

	return (result->separator == separator ? result : nullptr);
}

inline long double createDouble(unsigned long long before_point, unsigned long long after_point)
{
	if (after_point == 0) return before_point;

	return before_point + after_point * powl(10, -std::floor(std::log10(after_point) + 1));
}

inline void Parser::reportExpected(const std::string& expected_name, bool increment_pos, int offset)
{
	if (this->_curr_position + offset >= this->_tokens.size())
	{
		if (this->_tokens.size() > 0)
		{
			ErrorRegister::reportError(new ParserError(std::string("Expected ") + expected_name, this->_tokens[this->_tokens.size() - 1]->file_pos,
				std::string("Expected ") + expected_name + std::string(" after this token")));			
		}

		return;
	}

	ErrorRegister::reportError(new ParserError(std::string("Expected ") + expected_name, this->_tokens[this->_curr_position + offset]->file_pos,
		std::string("Expected ") + expected_name));

	if (increment_pos)
		this->_curr_position++;
}

inline void Parser::reportPropertyRedefinition(const std::string& property_name, const position_data& pos)
{
	ErrorRegister::reportError((new ParserError(
			property_name + " redefinition",
			pos,
			"The property " + property_name + "has already been defined"
		))->withHelp("Remove redundant " + property_name + " property", pos));
}

AbstractSyntaxTree::Properties Parser::acceptProperties()
{
	AbstractSyntaxTree::Properties properties;
	bool invalid_prop = false;

	while (!invalid_prop)
	{
		auto* curr_token = peekToken();

		// if there is a token at this position
		if (curr_token != nullptr)
		{
			switch (curr_token->type)
			{
			case token_type::extern_kw:		// parse extern property
				this->_curr_position++;

				// if this property has not been defined yet
				if (properties.is_extern)
				{
					invalid_prop = true;
					reportPropertyRedefinition("extern", curr_token->file_pos);
				}
				else 
					properties.is_extern = true;
				break;
			default:
				invalid_prop = true;
				break;
			}
		}
	}

	return properties;
}