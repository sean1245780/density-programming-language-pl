cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

set(parser_source_files
Parser.h
Parser.cpp
)

add_library(parser ${parser_source_files})
target_link_libraries(parser lexer)