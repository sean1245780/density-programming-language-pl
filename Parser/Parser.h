#pragma once

#include "../Utilities/DefinedObjs/Tokens.h"
#include "../Utilities/AbstractSyntaxTree/Nodes.h"
#include "../Utilities/Errors/CompilerErrors.h"

using CompilerErrors::ErrorRegister;
using CompilerErrors::ParserError;

typedef struct ParserOutput {
	vector<AbstractSyntaxTree::Node*> parsed_data;
	vector<AbstractSyntaxTree::ImportStatement*> import_requests;
} ParserOutput;

class Parser {
private:
	const std::vector<Token*>& _tokens;
	unsigned int _curr_position;

	vector<AbstractSyntaxTree::ImportStatement*> _import_requests;

public:
	/*
		The function will parse the tokens given and return a vector of abstract syntax trees
		@ the tokens will not be deleted automatically
	*/
	static ParserOutput parse(const std::vector<Token*>& tokens);

private:
	Parser(const std::vector<Token*>& tokens) : _tokens(tokens), _curr_position(0) { };

	/*
		This function parses the next node to be inserted into the tree using std, nud, and led.
		@ the function reports errors and returns a nullptr when a syntax error is found
		@ the function increments the position state of the parser
	*/
	AbstractSyntaxTree::Node* parseNext();

	/*
		This function parses an expression without direct use of led.
		@ the function expects an expression to be present at the current position
		@ the function reports errors and returns a nullptr when a syntax error is found
		@ the function increments the position state of the parser
	*/
	AbstractSyntaxTree::Expression* nud();

	/*
		This function appends operators and other left denotable elements according to the 
		language syntax. It uses the current token and the left expression to create the 
		new root node that is returned.
		@ the function expects an appendable element to be present at the current position
		@ the function reports errors and returns a nullptr when a syntax error is found
		@ the function increments the position state of the parser
	*/
	AbstractSyntaxTree::Expression* led(AbstractSyntaxTree::Expression* left);

	/*
		This function accepts elements which can be parsed using a well defined denotation.
		@ the function returnes nulltptr when the current token cannot be parsed according
		to the logic defined above.
		@ the function increments the position state of the parser if parsing was successful.
	*/
	AbstractSyntaxTree::Node* std();

	// NOTE: if an invalid string is parsed, and the string represents an invalid type expression,
	// the functions will report and error anyway
	AbstractSyntaxTree::TypeExpression* type_nud(bool report = false);
	AbstractSyntaxTree::TypeExpression* type_led(AbstractSyntaxTree::TypeExpression* left, bool report = false);

	/*
		The function parses an expression using nud and led
		@ last_precedence is the precedence of the first operator parsed in this expression
	*/
	AbstractSyntaxTree::Expression* parseExpression(unsigned int last_precedence = MIN_PRECEDENCE);

	/*
		The function will parse an identifier expression
		@ the function reports an error if the identifier is missing
	*/
	AbstractSyntaxTree::IdentifierExpression* parseIdentifierExpression();

	/*
		The function will parse a type expression
		@ the function reports an error if any essential element of the statement is missing
		@ the function does not account for ';'
		@ the function accounts for the elements needed to fill the VariableDeclaration class members
		@ the function will keep the pointer at the same position unless it fails
	*/
	AbstractSyntaxTree::TypeExpression* parseTypeExpression(unsigned int last_precedence = MIN_PRECEDENCE);

	/*
		The function will parse a variable declaration line
		@ the function reports an error if any essential element of the statement is missing
		@ the function does not account for ';'
		@ the function accounts for the elements needed to fill the VariableDeclaration class members
	*/
	AbstractSyntaxTree::VariableDeclaration* parseVariableDeclaration(AbstractSyntaxTree::TypeExpression* type);

	/*
		The function will parse an expression list
		@ the function reports an error if any essential element of the expression list is missing
		@ the function does not account for ';'
	*/
	AbstractSyntaxTree::ExpressionList* parseExpressionList();

	/*
		The function will parse a cast expression
		@ the function reports an error if any essential element of the expression list is missing
		@ the function does not account for ';'
	*/
	AbstractSyntaxTree::CastExpression* parseCastExpression(OperatorToken* cast_token, AbstractSyntaxTree::Expression* inner = nullptr);

	/*
		The function will parse a complex type expression
		@ the function reports an error if any essential element of the type expression is missing
		@ the function does not account for ';'
	*/
	AbstractSyntaxTree::TypeExpression* parseComplexType(bool expect_at = true);

	/*
		The function will parse a nameless complex type expression
		@ the function reports an error if any essential element of the type expression is missing
		@ the function does not account for ';'
	*/
	AbstractSyntaxTree::NamelessComplexType* parseNamelessComplexType(bool expect_at = true);

	/*
		The function will parse a named complex type expression
		@ the function reports an error if any essential element of the type expression is missing
		@ the function does not account for ';'
	*/
	AbstractSyntaxTree::ComplexType* parseNamedComplexType(bool expect_at = true);

	/*
		The function will parse a function definition
		@ the function reports an error if any essential element of the function is missing
	*/
	AbstractSyntaxTree::FunctionDefinition* parseFunctionDefinition();

	/*
		The function will parse a conditional
		@ the function reports an error if any essential element of the conditional is missing
	*/
	AbstractSyntaxTree::ConditionalStatement* parseConditional();

	/*
		The check if it has keywords of properties.
		@ the function reports an error if any essential element of the conditional is missing
	*/
	AbstractSyntaxTree::Properties parseProperties();

	/*
		The check if it has keywords of properties.
		@ the function reports an error if any essential element of the conditional is missing
	*/
	AbstractSyntaxTree::VarArgsExpression* parseVArgsExpression(const position_data& keyword_pos);

	/*
		The function will parse a for statement and return it
		@ the function reports errors if any essential part of the statement is missing
		@ the function returns nullptr if parsing failed
	*/
	AbstractSyntaxTree::ForStatement* parseForStatement();

	/*
		The function will parse a function definition
		@ the function reports an error if any essential element of the function is missing
	*/
	inline AbstractSyntaxTree::FunctionDefinition* parseFunctionDefinition(AbstractSyntaxTree::TypeExpression* ret_type);

	/*
		The function will parse a function definition
		@ the function reports an error if any essential element of the function is missing
	*/
	inline AbstractSyntaxTree::FunctionDefinition* parseFunctionDefinition(AbstractSyntaxTree::TypeExpression* return_type, AbstractSyntaxTree::IdentifierExpression* function_name);

	/*
		The function will return the current token if it is the given operator, else it will return nullptr
		@ this function increments the current position on every call
		@ this function reports an error when the current operator is not the one specified
		@ expected_name is the name of the expected component to be reported missing
	*/
	inline Token* expect(token_type expected, const std::string& expected_name);

	/*
		The function will return the current token if it is the given type, else it will return nullptr
		@ this function never increments the current position
		@ this function does not report an error when returning nullptr
	*/
	inline Token* accept(token_type expected, const unsigned int& lookahead = 0);

	/*
		The function returns the token in the current position
		@ the function does array bounds check
		@ the function increments the current position after returning
	*/
	inline Token* getToken();

	/*
		The function returns the token in the current position
		@ the function does array bounds check
		@ the function does not increment the current position after returning
	*/
	inline Token* peekToken(const unsigned int& lookahead = 0);

	/*
		The function will return the current token if it is the given operator, else it will return nullptr
		@ this function increments the current position on every call
		@ this function reports an error when the current operator is not the one specified
	*/
	inline OperatorToken* expect(std::string op);

	/*
		The function will return the current token if it is the given operator, else it will return nullptr
		@ this function never increments the current position
		@ this function does not report an error when returning nullptr
	*/
	inline OperatorToken* accept(std::string op, const unsigned int& lookahead = 0);

	/*
		The function will return the current token if it is the given separator, else it will return nullptr
		@ this function increments the current position on every call
		@ this function reports an error when the current operator is not the one specified
	*/
	inline SeparatorToken* expect(char separator);

	/*
		The function will return the current token if it is the given separator, else it will return nullptr
		@ this function never increments the current position
		@ this function does not report an error when returning nullptr
	*/
	inline SeparatorToken* accept(char separator, const unsigned int& lookahead = 0);

	/*
		The function will report that an expected component with the name specified was not found
		@ the position of the error that will be reported is the current position (if it exists)
		@ the function accounts for array bounds
	*/
	inline void reportExpected(const std::string& expected_name, bool increment_pos = false, int offset = 0);

	/*
		The function will report that an the property at the current position is redefined
		@ the position of the error that will be reported is the current position (if it exists)
		@ the function accounts for array bounds
	*/
	inline void reportPropertyRedefinition(const std::string& property_name, const position_data& pos);

	/*
		The function will return the import requests that were collected in the parser
		@ the function does not empty the requests vector
	*/
	vector<AbstractSyntaxTree::ImportStatement*> getImportRequests() { return this->_import_requests; }

	/*
		The function will try to parse a property sequence.
		@ the function returns a default property struct when no properties were found.
		@ the function increments the position state of the parser if parsing was successful.
	*/
	AbstractSyntaxTree::Properties acceptProperties();
};

/*
	The function will create a double value using the integers defining the digits before and
	after the point
*/
static inline long double createDouble(unsigned long long before_point, unsigned long long after_point);