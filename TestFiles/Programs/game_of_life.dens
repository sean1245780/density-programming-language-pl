import "../stdlib.dens";

extern i32 rand();

var alive_cell_char = 'O';
var dead_cell_char = ' ';
var max_iter = 10;

i32 main()
{
	var size = (20, 10);

	char** buf1 = initializeBuffer(size);
	char** buf2 = initializeBuffer(size);
	
	for (var curr_iter = 0; curr_iter < max_iter; curr_iter++)
	{
		printf("Iteration: %d\n", curr_iter);

		if (curr_iter % 2 == 0)
		{
			iterateGame(buf1, buf2, size);
			printState(buf2, size);
		} else {
			iterateGame(buf2, buf1, size);
			printState(buf1, size);
		} 
	}

	ret 0;
}

@iterateGame(char** last_buffer, char** next_buffer, @(i32 width, i32 height) size)
{
	for (var i = 0; i < size.height; i++)
	{
		for (var j = 0; j < size.width; j++)
		{
			if (cellShouldLive(last_buffer, size, j, i))
				next_buffer[i][j] = alive_cell_char;
			else 
				next_buffer[i][j] = dead_cell_char;
		}
	}
}

bool cellShouldLive(char** buf, @(i32 width, i32 height) size, i32 x, i32 y)
{
	var count = neighbors(buf, size, x, y);

	ret count == 3 || (count == 2 && alive(buf, size, x, y));
}

i32 neighbors(char** buf, @(i32 width, i32 height) size, i32 x, i32 y)
{
	ret alive(buf, size, x - 1	, y - 1) to u32 + 
		alive(buf, size, x		, y - 1) to u32 + 
		alive(buf, size, x + 1	, y - 1) to u32 + 
		alive(buf, size, x + 1	, y) 	 to u32 + 
		alive(buf, size, x + 1	, y + 1) to u32 + 
		alive(buf, size, x		, y + 1) to u32 + 
		alive(buf, size, x - 1	, y + 1) to u32 + 
		alive(buf, size, x - 1	, y) 	 to u32;
}

bool alive(char** buf, @(i32 width, i32 height) size, i32 x, i32 y)
{
	if (x < 0 || y < 0 || x >= size.width || y >= size.height)
		ret false;

	ret buf[y][x] == alive_cell_char;
}

@printState(char** state, @(i32 width, i32 height) size)
{
	for (var i = 0; i < size.height; i++)
	{
		for (var j = 0; j < size.width; j++)
		{
			printf("%c", state[i][j]);
		}
		
		printf("\n");
	}
}

char** initializeBuffer(@(i32 width, i32 height) size)
{
	char** result = calloc(size.height, 8);
	var a = 0;
	
	for (var i = 0; i < size.height; i++)
	{
		// allocate the line
		result[i] = calloc(size.width, 1);

		// set cells to be randomly alive or dead
		for (var j = 0; j < size.width; j++)
		{
			if (rand() % 2 == 0)
				result[i][j] = alive_cell_char;
			else 
				result[i][j] = dead_cell_char;
		}
	}
	
	ret result;
}