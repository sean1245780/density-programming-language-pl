cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

set(abstract_syn_tree_sf
Nodes.h
Nodes.cpp
)

add_library(abstract_syn_tree ${abstract_syn_tree_sf})

target_link_libraries(abstract_syn_tree defs)
