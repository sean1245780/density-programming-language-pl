#include "Nodes.h"

namespace AbstractSyntaxTree {
	int Node::print_indentation = 0;

	void Properties::updateProperties(const Properties& prop, bool main_val)
	{
		if(!(prop.is_extern ^ main_val)) this->is_extern = main_val;
		if(!(prop.is_variadic ^ main_val)) this->is_variadic = main_val;
		if(!(prop.is_const ^ main_val)) this->is_const = main_val;
	}

	void Module::print() const noexcept
	{
		std::cout << "Module " << this->getPosition().resource.getFilePath() << ":"<< '\n';

		for (const auto& symbol : this->_symbol_table)
		{
			std::cout << "Symbol: " << symbol.first << '\n';
		}

		std::cout << "Contents: \n";

		for (Node* command : this->_commands)
		{
			command->print();
			std::cout << '\n';
		}
	}

	NamespaceDefinition::~NamespaceDefinition()
	{
		delete this->_identifier;
		this->_identifier = nullptr;

		this->CleanAllCommands();
		this->_symbol_table.clear();
	}

	void NamespaceDefinition::CleanAllCommands() noexcept
	{
		for(auto* command : this->_commands)
			delete command;
		
		this->_commands.clear();
	}

	void NamespaceDefinition::print() const noexcept
	{
		Node::print();

		std::cout << "Namespace Definition\n";

		Node::print_indentation++;

		if(this->_identifier != nullptr) this->_identifier->print();

		for(auto* command : this->_commands)
		{
			if(command != nullptr) command->print();
		}

		Node::print_indentation--;
	}

	ImportStatement::~ImportStatement() { }

	void ImportStatement::print() const noexcept
	{
		Node::print();
		
		std::cout << "Import statement: \"" << this->_to_import << "\"";
	}

	void NamelessComplexType::print() const noexcept
	{
		Node::print();

		std::cout << "Nameless complex type\n";
		
		Node::print_indentation++;
		
		for (TypeExpression* component : this->_type_components)
		{
			component->print();
		}

		Node::print_indentation--;
	}


	void SimpleType::print() const noexcept
	{
		Node::print();
		std::cout << "Simple type: " << this->_identifier_type << '\n';
	}

	ArrayType::~ArrayType()
	{
		if (this->_base_type != nullptr)
			delete this->_base_type;
	}

	void ArrayType::print() const noexcept
	{
		Node::print();

		std::cout << "Array type\n";
		
		Node::print_indentation++;
		this->_base_type->print();
		Node::print_indentation--;
	}

	PointerType::~PointerType()
	{
		if (this->_base_type != nullptr)
			delete this->_base_type;
	}

	void PointerType::print() const noexcept
	{
		Node::print();

		std::cout << "Pointer type\n";

		Node::print_indentation++;
		this->_base_type->print();
		Node::print_indentation--;
	}

	StatementBlock::~StatementBlock()
	{
		for (auto command : this->_commands)
			if (command != nullptr)
				delete command;
	}

	void StatementBlock::print() const noexcept
	{
		Node::print();

		std::cout << "Statement block\n";

		Node::print_indentation++;

		for (Statement* statement : this->_commands)
			if (statement != nullptr)
				statement->print();

		Node::print_indentation--;	
	}

	ConditionalStatement::~ConditionalStatement()
	{
		if (this->_condition != nullptr) delete this->_condition;
		if (this->_if_commands != nullptr) delete this->_if_commands;
		if (this->_else_commands != nullptr) delete this->_else_commands;
	}

	void ConditionalStatement::setCondition(Expression* condition) noexcept
	{
		if (this->_condition != nullptr) delete this->_condition;

		this->_condition = condition;
	}

	void ConditionalStatement::setIfCommands(Statement* commands) noexcept
	{
		if (this->_if_commands != nullptr) delete this->_if_commands;

		this->_if_commands = commands;
	}

	void ConditionalStatement::setElseCommands(Statement* commands) noexcept
	{
		if (this->_else_commands != nullptr) delete this->_else_commands;

		this->_else_commands = commands;
	}

	void ConditionalStatement::print() const noexcept
	{
		Node::print();

		std::cout << "Conditional statement\n";

		Node::print_indentation++;

		if (this->_condition != nullptr) this->_condition->print();
		if (this->_if_commands != nullptr) this->_if_commands->print();
		if (this->_else_commands != nullptr) this->_else_commands->print();

		Node::print_indentation--;
	}

	WhileStatement::~WhileStatement()
	{
		if (this->_condition != nullptr) delete this->_condition;
		if (this->_commands != nullptr) delete this->_commands;
	}

	void WhileStatement::setCondition(Expression* condition) noexcept
	{
		if (this->_condition != nullptr) delete this->_condition;

		this->_condition = condition;
	}

	void WhileStatement::setCommands(Statement* commands) noexcept
	{
		if (this->_commands != nullptr) delete this->_commands;

		this->_commands = commands;
	}

	void WhileStatement::print() const noexcept
	{
		Node::print();

		std::cout << "While statement\n";

		Node::print_indentation++;

		if (this->_condition != nullptr) this->_condition->print();
		if (this->_commands != nullptr) this->_commands->print();

		Node::print_indentation--;
	}

	ComplexTypeComponent::~ComplexTypeComponent()
	{
		if (this->_type != nullptr) delete this->_type;
		if (this->_identifier != nullptr) delete this->_identifier;
	}

	void ComplexTypeComponent::print() const noexcept
	{
		Node::print();

		std::cout << "Complex Type Component\n";

		Node::print_indentation++;

		if (this->_type != nullptr) this->_type->print();
		if (this->_identifier != nullptr) this->_identifier->print();

		Node::print_indentation--;
	}

	void VariableDeclarationComponent::print() const noexcept
	{
		Node::print();

		std::cout << "Variable Declaration Component\n";

		Node::print_indentation++;

		if (this->_identifier != nullptr) this->_identifier->print();
		if (this->_initialization != nullptr) this->_initialization->print();

		Node::print_indentation--;
	}

	VariableDeclarationComponent::~VariableDeclarationComponent()
	{
		if (this->_identifier != nullptr) delete this->_identifier;
		if (this->_initialization != nullptr) delete this->_initialization;
	}

	VariableDeclaration::~VariableDeclaration()
	{
		if (this->_type != nullptr) delete this->_type;

		for (VariableDeclarationComponent* var_decl : this->_variables)
		{
			if (var_decl != nullptr) delete var_decl;
		}
	}

	void VariableDeclaration::setType(TypeExpression* type)
	{
		if (this->_type != nullptr) delete this->_type;

		this->_type = type;
	}

	void VariableDeclaration::print() const noexcept
	{
		Node::print();

		std::cout << "Variable declaration\n";

		Node::print_indentation++;

		if (this->_type != nullptr) this->_type->print();

		for (VariableDeclarationComponent* var_decl : this->_variables)
		{
			if (var_decl != nullptr) var_decl->print();
		}

		Node::print_indentation--;
	}

	void VariableDeclaration::setProperties(const Properties& properties) noexcept
	{
		Node::setProperties(properties);

		// set the same properties to the contained nodes
		for (auto* decl : this->_variables)
			decl->setProperties(properties);
	}

	ReturnStatement::~ReturnStatement()
	{
		if (this->_return_value != nullptr) delete this->_return_value;
	}

	void ReturnStatement::setReturnValue(Expression* return_value)
	{
		if (this->_return_value != nullptr) delete this->_return_value;

		this->_return_value = return_value;
	}

	void ReturnStatement::print() const noexcept
	{
		Node::print();

		std::cout << "Return statement\n";

		Node::print_indentation++;

		if (this->_return_value != nullptr) this->_return_value->print();

		Node::print_indentation--;
	}

	NamespaceAccessExpression::~NamespaceAccessExpression()
	{
		if (this->_left_expression != nullptr) delete this->_left_expression;
		if (this->_right_expression != nullptr) delete this->_right_expression;
	}

	void NamespaceAccessExpression::setLeftExpression(Expression* expression) noexcept
	{
		if (this->_left_expression != nullptr) delete this->_left_expression;

		this->_left_expression = expression;
	}

	void NamespaceAccessExpression::setRightExpression(Expression* expression) noexcept
	{
		if (this->_right_expression != nullptr) delete this->_right_expression;

		this->_right_expression = expression;
	}

	void NamespaceAccessExpression::print() const noexcept
	{
		Node::print();

		std::cout << "Namespace Access Expression\n";

		Node::print_indentation++;

		if (this->_left_expression != nullptr) this->_left_expression->print();
		if (this->_right_expression != nullptr) this->_right_expression->print();

		Node::print_indentation--;
	}

	MemberAccessExpression::~MemberAccessExpression()
	{
		if (this->_left_expression != nullptr) delete this->_left_expression;
		if (this->_right_expression != nullptr) delete this->_right_expression;
	}

	void MemberAccessExpression::setLeftExpression(Expression* expression) noexcept
	{
		if (this->_left_expression != nullptr) delete this->_left_expression;

		this->_left_expression = expression;
	}

	void MemberAccessExpression::setRightExpression(Expression* expression) noexcept
	{
		if (this->_right_expression != nullptr) delete this->_right_expression;

		this->_right_expression = expression;
	}

	void MemberAccessExpression::setAccessType(AccessType access_type) noexcept
	{
		this->_access_type = access_type;
	}

	void MemberAccessExpression::setAccessIndex(unsigned int index) noexcept
	{
		this->_index = index;
	}

	void MemberAccessExpression::print() const noexcept
	{
		Node::print();

		std::cout << "Member access (";

		switch (this->_access_type)
		{
		case AccessType::member_acs:
			std::cout << "member -> '.'";
			break;
		case AccessType::pointer_member_access:
			std::cout << "member -> '->'";
			break;
		case AccessType::index_acs:
			std::cout << "index -> '[]'";
			break;
		default:
			std::cout << "invalid";
			break;
		}
		
		std::cout << ")\n";
		
		Node::print_indentation++;

		if (this->_left_expression != nullptr) this->_left_expression->print();
		if (this->_right_expression != nullptr) this->_right_expression->print();

		Node::print_indentation--;
	}

	BinaryOperatorExpression::~BinaryOperatorExpression()
	{
		if (this->_left_expression != nullptr) delete this->_left_expression;
		if (this->_right_expression != nullptr) delete this->_right_expression;
	}

	void BinaryOperatorExpression::setLeftExpression(Expression* expression)
	{
		if (this->_left_expression != nullptr) delete this->_left_expression;

		this->_left_expression = expression;
	}

	void BinaryOperatorExpression::setRightExpression(Expression* expression)
	{
		if (this->_right_expression != nullptr) delete this->_right_expression;

		this->_right_expression = expression;
	}

	void BinaryOperatorExpression::print() const noexcept
	{
		Node::print();

		std::cout << "Binary operator expression (" << this->_operator << ")\n";

		Node::print_indentation++;

		if (this->_left_expression != nullptr) this->_left_expression->print();
		if (this->_right_expression != nullptr) this->_right_expression->print();

		Node::print_indentation--;
	}

	UnaryOperatorExpression::~UnaryOperatorExpression()
	{
		if (this->_expression != nullptr) delete this->_expression;
	}

	void UnaryOperatorExpression::setExpression(Expression* expression)
	{
		if (this->_expression != nullptr) delete this->_expression;
		
		this->_expression = expression;
	}

	void UnaryOperatorExpression::print() const noexcept
	{
		Node::print();

		std::cout << "Unary operator expression (" << this->_operator << ")\n";

		Node::print_indentation++;

		if (this->_expression != nullptr) this->_expression->print();

		Node::print_indentation--;
	}

	ExpressionList::~ExpressionList()
	{
		for (auto expr : this->_expressions)
			if (expr != nullptr)
				delete expr;
	}

	void ExpressionList::print() const noexcept
	{
		Node::print();

		std::cout << "Expression list\n";

		Node::print_indentation++;

		for (auto expr : this->_expressions)
			if (expr != nullptr)
				expr->print();

		Node::print_indentation--;
	}

	void TypeInstatiation::setParameters(ExpressionList* params) noexcept
	{
		if (this->_constructor_params != nullptr) delete this->_constructor_params;

		this->_constructor_params = params;
	}

	void TypeInstatiation::setType(TypeExpression* type) noexcept
	{
		if (this->_owns_type && this->_type != nullptr) delete this->_type;

		this->_type = type;
	}

	TypeInstatiation::~TypeInstatiation() noexcept
	{
		if (this->_constructor_params != nullptr) delete this->_constructor_params;
		if (this->_type != nullptr && _owns_type) delete this->_type;
	}

	void TypeInstatiation::print() const noexcept
	{
		Node::print();

		std::cout << "Type Instantiation\n";

		Node::print_indentation++;

		if (this->_type != nullptr) this->_type->print();
		if (this->_constructor_params != nullptr) this->_constructor_params->print();

		Node::print_indentation--;
	}

	CallExpression::~CallExpression()
	{
		if (this->_arguments != nullptr) delete this->_arguments;
		if (this->_function_expression != nullptr) delete this->_function_expression;
	}

	void CallExpression::setFunctionExpression(Expression* func) noexcept
	{
		if (this->_function_expression != nullptr) delete this->_function_expression;

		this->_function_expression = func;
	}

	void CallExpression::setArguments(ExpressionList* arguments) noexcept
	{
		if (this->_arguments != nullptr) delete this->_arguments;

		this->_arguments = arguments;
	}

	void CallExpression::print() const noexcept
	{
		Node::print();

		std::cout << "Call expression\n";

		Node::print_indentation++;

		if (this->_arguments != nullptr) this->_arguments->print();
		if (this->_function_expression != nullptr) this->_function_expression->print();

		Node::print_indentation--;
	}

	TernaryExpression::~TernaryExpression()
	{
		if (this->_condition != nullptr) delete this->_condition;
		if (this->_true_expression != nullptr) delete this->_true_expression;
		if (this->_false_expression != nullptr) delete this->_false_expression;
	}

	void TernaryExpression::setCondition(Expression* condition)
	{
		if (this->_condition != nullptr) delete this->_condition;

		this->_condition = condition;
	}

	void TernaryExpression::setTrueExpression(Expression* true_expr)
	{
		if (this->_true_expression != nullptr) delete this->_true_expression;

		this->_true_expression = true_expr;
	}

	void TernaryExpression::setFalseExpression(Expression* false_expr)
	{
		if (this->_false_expression != nullptr) delete this->_false_expression;

		this->_false_expression = false_expr;
	}

	void TernaryExpression::print() const noexcept
	{
		Node::print();

		std::cout << "Ternary expression\n";

		Node::print_indentation++;

		if (this->_condition != nullptr) this->_condition->print();
		if (this->_true_expression != nullptr) this->_true_expression->print();
		if (this->_false_expression != nullptr) this->_false_expression->print();

		Node::print_indentation--;
	}

	bool operator==(const TypeExpression& first, const TypeExpression& second)
	{
		auto val1 = first.getTypeExpressionCategory(), val2 = second.getTypeExpressionCategory();
		if(val1 != val2)
			return false;
				
		switch (val1)
		{
		case TypeExpression::type_expression_category::simple_type:
		{
			const auto* obj1 = dynamic_cast<const SimpleType*>(&first);
			const auto* obj2 = dynamic_cast<const SimpleType*>(&second);
			if(obj1 != nullptr && obj2 != nullptr)
				return (*obj1) == (*obj2);
			break;
		}
		case TypeExpression::type_expression_category::complex_type:
		{
			const auto* obj1 = dynamic_cast<const ComplexType*>(&first);
			const auto* obj2 = dynamic_cast<const ComplexType*>(&second);
			if(obj1 != nullptr && obj2 != nullptr)
				return (*obj1) == (*obj2);
			break;
		}
		case TypeExpression::type_expression_category::array_type:
		{
			const auto* obj1 = dynamic_cast<const ArrayType*>(&first);
			const auto* obj2 = dynamic_cast<const ArrayType*>(&second);
			if(obj1 != nullptr && obj2 != nullptr)
				return (*obj1) == (*obj2);
			break;
		}
		case TypeExpression::type_expression_category::pointer_type:
		{
			const auto* obj1 = dynamic_cast<const PointerType*>(&first);
			const auto* obj2 = dynamic_cast<const PointerType*>(&second);
			if(obj1 != nullptr && obj2 != nullptr)
				return (*obj1) == (*obj2);
			break;
		}
		case TypeExpression::type_expression_category::nameless_complex_type:
		{
			const auto* obj1 = dynamic_cast<const NamelessComplexType*>(&first);
			const auto* obj2 = dynamic_cast<const NamelessComplexType*>(&second);
			if(obj1 != nullptr && obj2 != nullptr)
				return (*obj1) == (*obj2);
			break;
		}
		default:
			return false;
			break;
		}
		return false;
	}

	FunctionDefinition::~FunctionDefinition()
	{
		if (this->_commands != nullptr) delete this->_commands;
		if (this->_input_type != nullptr) delete this->_input_type;
		if (this->_return_type != nullptr) delete this->_return_type;
		if (this->_funtion_name != nullptr) delete this->_funtion_name;
	}

	void FunctionDefinition::setFunctionName(IdentifierExpression* name) noexcept
	{
		if (this->_funtion_name != nullptr) delete this->_funtion_name;

		this->_funtion_name = name;
	}

	void FunctionDefinition::setReturnType(TypeExpression* return_type) noexcept
	{
		if (this->_return_type != nullptr) delete this->_return_type;
		
		this->_return_type = return_type;
	}

	void FunctionDefinition::setInputType(ComplexType* input_type) noexcept
	{
		if (this->_input_type != nullptr) delete this->_input_type;
		
		this->_input_type = input_type;
	}

	void FunctionDefinition::setCommands(Statement* commands) noexcept
	{
		if (this->_commands != nullptr) delete this->_commands;

		this->_commands = commands;
	}

	void FunctionDefinition::print() const noexcept
	{
		Node::print();

		std::cout << "Function definition\n";

		Node::print_indentation++;

		if (this->_funtion_name != nullptr) this->_funtion_name->print();
		if (this->_commands != nullptr) this->_commands->print();
		if (this->_input_type != nullptr) this->_input_type->print();
		if (this->_return_type != nullptr) this->_return_type->print();

		Node::print_indentation--;
	}

	TypeDefinition::~TypeDefinition()
	{
		for (auto* node : this->_type_data_members)
			if (node != nullptr) delete node;
		for (auto* node : this->_type_functions)
			if (node != nullptr) delete node;

		if (this->_type_name != nullptr) delete this->_type_name;
	}

	void TypeDefinition::setTypeName(IdentifierExpression* name) noexcept
	{
		if (this->_type_name != nullptr) delete this->_type_name;

		this->_type_name = name;
	}

	void TypeDefinition::setTypeFunctions(const vector<FunctionDefinition*>& funcs) noexcept
	{
		for (auto* node : this->_type_functions)
			if (node != nullptr) delete node;

		this->_type_functions = funcs;
	}

	void TypeDefinition::setTypeDataMembers(const vector<VariableDeclaration*>& vars) noexcept
		{
		for (auto* node : this->_type_data_members)
			if (node != nullptr) delete node;

		this->_type_data_members = vars;
	}

	void TypeDefinition::print() const noexcept
	{
		Node::print();

		std::cout << "Type definition\n";

		Node::print_indentation++;

		if (this->_type_name != nullptr) this->_type_name->print();
		for (auto* node : this->_type_data_members)
			if (node != nullptr) 
				node->print();
		for (auto* node : this->_type_functions)
			if (node != nullptr) 
				node->print();

		Node::print_indentation--;
	}

	ComplexType::~ComplexType()
	{
		for (ComplexTypeComponent* component : this->_type_components)
		{
			if (component != nullptr) delete component;
		}

		this->_type_components.clear();
	}

	void ComplexType::print() const noexcept
	{
		Node::print();

		std::cout << "Complex type\n";

		Node::print_indentation++;

		for (ComplexTypeComponent* component : this->_type_components)
		{
			if (component != nullptr) component->print();
		}

		Node::print_indentation--;
	}

	ForStatement::~ForStatement()
	{
		if (this->_initialization != nullptr) delete this->_initialization;
		if (this->_condition != nullptr) delete this->_condition;
		if (this->_incrementation != nullptr) delete this->_incrementation;
		if (this->_commands != nullptr) delete this->_commands;
	}

	void ForStatement::setInitialization(VariableDeclaration* init) noexcept
	{
		if (this->_initialization != nullptr) delete this->_initialization;
		
		this->_initialization = init;
	}

	void ForStatement::setCondition(Expression* condition) noexcept
	{
		if (this->_condition != nullptr) delete this->_condition;

		this->_condition = condition;
	}

	void ForStatement::setIncrementation(Expression* increment) noexcept
	{
		if (this->_incrementation != nullptr) delete this->_incrementation;

		this->_incrementation = increment;
	}

	void ForStatement::setCommands(Statement* commands) noexcept
	{
		if (this->_commands != nullptr) delete this->_commands;

		this->_commands = commands;
	}

	void ForStatement::print() const noexcept
	{
		Node::print();

		std::cout << "For statement\n";

		Node::print_indentation++;

		if (this->_initialization != nullptr) this->_initialization->print();
		if (this->_condition != nullptr) this->_condition->print();
		if (this->_incrementation != nullptr) this->_incrementation->print();
		if (this->_commands != nullptr) this->_commands->print();

		Node::print_indentation--;
	}

	void Node::print() const noexcept
	{
		for (int i = 0; i < Node::print_indentation; i++)
			std::cout << '\t';

		if (this->_synthesized_type != nullptr)
			std::cout << "(type=" << this->_synthesized_type->getName() << ") ";
	}

	void IdentifierExpression::print() const noexcept
	{
		Node::print();

		std::cout << "Identifier expression (" << this->_identifier << ")\n";
	}

	void CastExpression::print() const noexcept
	{
		Node::print();

		string type_str;
		std::cout << "Cast expression (";

		switch (this->_casting_type)
		{
		case cast_type::cast_static:
			type_str = "static";
			break;
		case cast_type::cast_dynamic:
			type_str = "dynamic";
			break;
		case cast_type::cast_bitcast:
			type_str = "bitcast";
			break;
		default:
			type_str = "-unknown";
			break;
		}

		std::cout << type_str << ")\n";

		this->print_indentation++;

		if(this->_inner != nullptr) this->_inner->print();
		if(this->_target_type != nullptr) this->_target_type->print();
		
		this->print_indentation--;
	}


	void IntegerLiteral::print() const noexcept
	{
		Node::print();

		std::cout << "Integer literal (" << this->_value << ")\n";
	}

	void DoubleLiteral::print() const noexcept
	{
		Node::print();

		std::cout << "Double literal (" << this->_value << ")\n";
	}

	void CharLiteral::print() const noexcept
	{
		Node::print();

		std::cout << "Char literal (" << this->_value << ")\n";
	}

	void BooleanLiteral::print() const noexcept
	{
		Node::print();

		std::cout << "Boolean literal (" << (this->_value ? "true" : "false") << ")\n";
	}

	void StringLiteral::print() const noexcept
	{
		Node::print();

		std::cout << "String literal (" << this->_value << ")\n";
	}

	void NullLiteral::print() const noexcept
	{
		Node::print();

		std::cout << "Null literal (null = " << this->_value << ")\n";
	}

	void VarArgsExpression::print() const noexcept
	{
		Node::print();

		std::cout << "Variadic Args (Keyword)\n";

		if (_type_get == nullptr) return;

		this->print_indentation++;
		_type_get->print();
		this->print_indentation--;
	}

	void CreateFunctionExpression::print() const noexcept
	{
		Node::print();

		std::cout << "Create Function Call (Keyword)\n";

		if (_creation_type == nullptr) return;

		this->print_indentation++;
		_creation_type->print();
		this->print_indentation--;
	}

	void NewFunctionExpression::print() const noexcept
	{
		Node::print();

		std::cout << "New Function Call (Keyword)\n";

		if (_creation_type == nullptr) return;

		this->print_indentation++;
		_creation_type->print();
		this->print_indentation--;
	}

	void DeleteFunctionStatement::print() const noexcept
	{
		Node::print();

		std::cout << "Delete Function Call (Keyword)\n";

		if (_deleting_obj == nullptr) return;

		this->print_indentation++;
		_deleting_obj->print();
		this->print_indentation--;
	}
}
