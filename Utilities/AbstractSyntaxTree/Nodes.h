#pragma once

#include "../Errors/CompilerErrors.h"
#include "../DefinedObjs/TypeSystemInfo.h"
#include "../DataHandler/IDataAccess.h"
#include "llvm/IR/Value.h"
#include <string>
#include <vector>

using std::vector;
using std::string;
using std::pair;

namespace AbstractSyntaxTree
{

	typedef struct Properties {
		bool is_extern = false; // Getting external functions / variables
		bool is_const = false; // Setting the function / variable as const
		bool is_variadic = false; // Setting the only functions if have variadic
		bool is_strong = false;
		bool is_weak = false;

		//inline bool is_all_export_extern() const noexcept { return is_extern && is_export; }
		void updateProperties(const Properties& prop, bool main_val = true);
	} Properties;

	/* This class represents a node of an abstract syntax tree */
	class Node
	{
	protected:
		static int print_indentation;

	private:
		position_data _position;
		SynthesizedType* _synthesized_type = nullptr;
		llvm::Value* _llvm_value = nullptr;
		Properties _properties;

	public:
		enum class node_type { type_expression, statement, function_definition, module,
								variable_declaration_component, complex_type_component,
								import_statement, namespace_definition, type_definition };
		
		Node(const position_data& pos) : _position(pos) {}

		virtual ~Node() = default;

		virtual node_type getNodeType() const noexcept = 0;

		virtual void print() const noexcept;

		const position_data& getPosition() const noexcept { return this->_position; }

		SynthesizedType* getSynthesizedType() { return this->_synthesized_type; }
		llvm::Value* getLLVMValue() { return this->_llvm_value; }
		void setSynthesizedType(SynthesizedType* synthesized_type) { this->_synthesized_type = synthesized_type; }
		void setLLVMValue(llvm::Value* val) { this->_llvm_value = val; }

		Properties& getProperties() noexcept { return this->_properties; }
		virtual void setProperties(const Properties& properties) noexcept { this->_properties = properties; }
	};

	typedef unordered_map<string, Node*> SymbolTable;

	class Module : public Node
	{
	private:
		unsigned int _module_id;
		std::vector<Node*> _commands;			// the abstract syntax tree for this module
		SymbolTable _symbol_table;				// symbol table for globals in this
		unordered_set<Module*> _imported_mods;	// the modules that this module is importing

	public:
		Module(IDataAccess& module_code, const std::vector<Node*>& commands) : 
					Node(position_data(module_code, true)), _commands(commands) {
			static unsigned int current_id = 0;
			this->_module_id = current_id++;
		}

		std::vector<Node*>& getCommands() noexcept { return this->_commands; }
		SymbolTable& getSymbolTable() noexcept { return this->_symbol_table; }

		void setSymbolTable(const SymbolTable& table) noexcept { this->_symbol_table = table; }
		unsigned int getModuleId() const noexcept { return this->_module_id; }
		unordered_set<Module*>& getImportedModules() { return this->_imported_mods; }
		void setImportedModules(const unordered_set<Module*>& mods) { this->_imported_mods = mods; }

		virtual node_type getNodeType() const noexcept { return node_type::module;  }
		void print() const noexcept;
	};

	/* This class represents type expressions, which evaluate to a type: int, string, (string a, int b)*/
	class TypeExpression : public Node {
	public:
		enum class type_expression_category { simple_type, complex_type, array_type, pointer_type,
											  nameless_complex_type };
		
		TypeExpression(const position_data& pos) : Node(pos) {}

		virtual ~TypeExpression() = default;
		
		virtual node_type getNodeType() const noexcept { return node_type::type_expression;  }
		virtual type_expression_category getTypeExpressionCategory() const noexcept = 0;

		friend bool operator==(const TypeExpression& first, const TypeExpression& second);
	};

	/* This class represents complex types: (string a, int b, (int x, int y) c)*/
	class NamelessComplexType : public TypeExpression {
	private:
		vector<TypeExpression*> _type_components;

	public:
		NamelessComplexType(const position_data& pos) : TypeExpression(pos) {}

		virtual ~NamelessComplexType()
		{
			for(auto& obj : this->_type_components)
			{
				if(obj != nullptr) delete obj;
			}

			this->_type_components.clear();
		}

		virtual type_expression_category getTypeExpressionCategory() const noexcept { return type_expression_category::nameless_complex_type; }

		void addComponent(TypeExpression* type_expr) { this->_type_components.push_back(type_expr); }

		const vector<TypeExpression*>& getComponents() const noexcept { return this->_type_components; }

		virtual void print() const noexcept;

		friend bool operator==(const NamelessComplexType& first, const NamelessComplexType& second)
		{
			if(first._type_components.empty() || second._type_components.empty() ||
					(first._type_components.size() != second._type_components.size())) // Maybe make some problems
				return false;

			auto it1 = first._type_components.begin();
			auto end1 = first._type_components.end();
			auto it2 = second._type_components.begin();
			auto end2 = second._type_components.end();

			for(;it1 != end1 && it2 != end2; it1++, it2++)
			{
				if(!((*(*(it1))) == (*(*(it2))))) return false;
			}

			return true;
		}
	};

	/* This class represents an array type: [int], [(int x, int y)], [string*] */
	// AN OPEN IDEA!
	class ArrayType : public TypeExpression {
	private:
		TypeExpression* _base_type = nullptr;

	public:
		ArrayType(const position_data& pos, TypeExpression* base_type) : TypeExpression(pos), _base_type(base_type) {}

		virtual ~ArrayType();

		virtual type_expression_category getTypeExpressionCategory() const noexcept { return type_expression_category::array_type; }

		TypeExpression* getBaseType() const noexcept { return this->_base_type; }

		virtual void print() const noexcept;

		friend bool operator==(const ArrayType& first, const ArrayType& second)
		{
			if(first._base_type == nullptr || second._base_type == nullptr)
				return false;

			return ((*(first._base_type)) == (*(second._base_type)));
		}
	};
	
	/* This class represents pointer types: int*, string*, (vector x, char* y)* */
	class PointerType : public TypeExpression {
	private:
		TypeExpression* _base_type = nullptr;

	public:
		PointerType(const position_data& pos, TypeExpression* base_type) : TypeExpression(pos), _base_type(base_type) {}

		virtual ~PointerType();

		virtual type_expression_category getTypeExpressionCategory() const noexcept { return type_expression_category::pointer_type; }

		TypeExpression* getBaseType() const noexcept { return this->_base_type; }
		void setBaseType(TypeExpression* base_type) noexcept { this->_base_type = base_type; }

		virtual void print() const noexcept;

		friend bool operator==(const PointerType& first, const PointerType& second)
		{
			if(first._base_type == nullptr || second._base_type == nullptr)
				return false;

			return ((*(first._base_type)) == (*(second._base_type)));
		}
	};

	/* This class represents statements which are commands to be executed: int x; x += 5; */
	class Statement : public Node { 
	private:
		bool _has_return = false;

	public:
		enum class statement_type { conditional_statement, while_statement, for_statement, variable_declaration,
				return_statement, expression, statement_block, delete_function_statement };

		Statement(const position_data& pos) : Node(pos) {}

		virtual ~Statement() = default;

		virtual node_type getNodeType() const noexcept { return node_type::statement; }

		virtual statement_type getStatementType() const noexcept = 0;

		void setHasReturn(bool has_return) { this->_has_return = has_return; }
		bool getHasReturn() { return this->_has_return; }
	};

	/* This class represents an ordered list of commands to be executed in sequence: { int x = 0; x++; f(x); }*/
	class StatementBlock : public Statement {
	private:
		std::vector<Statement*> _commands;
		SymbolTable _symbol_table;

	public:
		StatementBlock(const position_data& pos) : Statement(pos) {}

		virtual ~StatementBlock();

		virtual statement_type getStatementType() const noexcept { return statement_type::statement_block; }

		void addStatement(Statement* statement) { this->_commands.push_back(statement); }

		void setSymbolTable(const SymbolTable& symbol_table) { this->_symbol_table = symbol_table; }

		const std::vector<Statement*>& getCommands() const noexcept { return this->_commands; }
		const SymbolTable& getSymbolTable() const noexcept { return this->_symbol_table; }

		virtual void print() const noexcept;
	};

	/* This class represents expressions, which evaluate to a value: -5, x + 8, f(0), x */
	class Expression : public Statement 
	{
	public:
			enum class modifiable_type {NONE, FALSE, TRUE};

	private:
		modifiable_type _modifiable;
		
	public:
		enum class expression_type {binary_operation, unary_operation, expression_list, call_expression, ternary_expression,
				identifier_expression, literal_expression, cast_expression, member_access, vargs_expression, namespace_access_expr,
				type_instantiation, create_function_expression, new_function_expression };

		Expression(const position_data& pos, modifiable_type modifiable = modifiable_type::NONE) : Statement(pos), _modifiable(modifiable) {}

		virtual ~Expression() = default;

		virtual statement_type getStatementType() const noexcept { return statement_type::expression; }
		virtual expression_type getExpressionType() const noexcept = 0;
		void setModifiable(const modifiable_type& modifiable) noexcept { this->_modifiable = modifiable; };
		const modifiable_type& getModifiable() const noexcept { return this->_modifiable; }
	};

	class NamespaceAccessExpression : public Expression
	{
	private:
		Expression* _left_expression = nullptr;
		Expression* _right_expression = nullptr;

		Node* _referenced_node = nullptr;

	public:
		NamespaceAccessExpression(const position_data& pos) : Expression(pos) {}

		virtual ~NamespaceAccessExpression();

		virtual expression_type getExpressionType() const noexcept { return expression_type::namespace_access_expr; }

		void setLeftExpression(Expression* expression) noexcept;
		void setRightExpression(Expression* expression) noexcept;

		void setReferencedNode(Node* referenced) noexcept { this->_referenced_node = referenced; }

		Expression* getLeftExpression() const noexcept { return this->_left_expression; }
		Expression* getRightExpression() const noexcept { return this->_right_expression; }
		Node* getReferencedNode() const noexcept { return this->_referenced_node; }

		virtual void print() const noexcept;
	};

	/* This class represents a member access expression: a.x = 5; b->y = r; c.0 = 5; */
	class MemberAccessExpression : public Expression {
	public:
		enum class AccessType { member_acs, index_acs, pointer_member_access };

	private:
		Expression* _left_expression = nullptr;
		Expression* _right_expression = nullptr;
		AccessType _access_type = AccessType::member_acs;
		unsigned int _index = 0;

	public:
		MemberAccessExpression(const position_data& pos, AccessType access_type = AccessType::member_acs)
			: Expression(pos), _access_type(access_type) {}

		virtual ~MemberAccessExpression();

		virtual expression_type getExpressionType() const noexcept { return expression_type::member_access; }

		void setLeftExpression(Expression* expression) noexcept;
		void setRightExpression(Expression* expression) noexcept;
		void setAccessType(AccessType access_type) noexcept;
		void setAccessIndex(unsigned int index) noexcept;

		Expression* getLeftExpression() const noexcept { return this->_left_expression; }
		Expression* getRightExpression() const noexcept { return this->_right_expression; }
		AccessType getAccessType() const noexcept { return this->_access_type; }
		unsigned int getAccessIndex() const noexcept { return this->_index; }

		virtual void print() const noexcept;
	};

	/* This class represents a conditional statement: if (x < 5) { x++; } else { x--; } */
	class ConditionalStatement : public Statement {
	private:
		Expression* _condition = nullptr;
		Statement* _if_commands = nullptr;
		Statement* _else_commands = nullptr;

	public:		
		ConditionalStatement(const position_data& pos) : Statement(pos) {}
		
		virtual ~ConditionalStatement();

		virtual statement_type getStatementType() const noexcept { return statement_type::conditional_statement; }

		void setCondition(Expression* condition) noexcept;
		void setIfCommands(Statement* commands) noexcept;
		void setElseCommands(Statement* commands) noexcept;

		Expression* getCondition() const noexcept { return this->_condition; }
		Statement* getIfCommands() const noexcept { return this->_if_commands; }
		Statement* getElseCommands() const noexcept { return this->_else_commands; }

		virtual void print() const noexcept;
	};

	/* This class represents a while statement: while(true) {}, while (x < 0) x++; */
	class WhileStatement : public Statement {
	private:
		Expression* _condition = nullptr;
		Statement* _commands = nullptr;

	public:
		WhileStatement(const position_data& pos) : Statement(pos) {}
		
		virtual ~WhileStatement();

		virtual statement_type getStatementType() const noexcept { return statement_type::while_statement; }

		void setCondition(Expression* condition) noexcept;
		void setCommands(Statement* commands) noexcept;

		Expression* getCondition() const noexcept { return this->_condition; }
		Statement* getCommands() const noexcept { return this->_commands; }

		virtual void print() const noexcept;
	};

	/* This class represents a return statement: return 5; return "abcd"; return 8 * y / x; */
	class ReturnStatement : public Statement {
	private:
		Expression* _return_value = nullptr;

	public:
		ReturnStatement(const position_data& pos) : Statement(pos) {}

		virtual ~ReturnStatement();
		
		virtual statement_type getStatementType() const noexcept { return statement_type::return_statement; }

		void setReturnValue(Expression* return_value);

		Expression* getReturnValue() const noexcept { return this->_return_value; }

		virtual void print() const noexcept;
	};

	/* This class represents binary operator expression: x = 2, x + 5, x **= 8, y * z */
	class BinaryOperatorExpression : public Expression {
	private:
		std::string _operator;
		Expression* _left_expression = nullptr;
		Expression* _right_expression = nullptr;
		bool _left_dir = true;

	public:
		BinaryOperatorExpression(const position_data& pos) : Expression(pos) {}

		virtual ~BinaryOperatorExpression();

		virtual expression_type getExpressionType() const noexcept { return expression_type::binary_operation; }

		void setOperator(const std::string& op) { this->_operator = op; }
		void setLeftExpression(Expression* expression);
		void setRightExpression(Expression* expression);

		std::string getOperator() const noexcept { return this->_operator; }
		Expression* getLeftExpression() const noexcept { return this->_left_expression; }
		Expression* getRightExpression() const noexcept { return this->_right_expression; }

		virtual void print() const noexcept;
	};

	/* This class represents unary operator expression: x++, --5, !true */
	class UnaryOperatorExpression : public Expression {
	private:
		std::string _operator;
		Expression* _expression = nullptr;
		bool _is_prefix = false;

	public:
		UnaryOperatorExpression(const position_data& pos) : Expression(pos) {}

		virtual ~UnaryOperatorExpression();

		virtual expression_type getExpressionType() const noexcept { return expression_type::unary_operation; }

		void setPrefix(bool is_prefix) { this->_is_prefix = is_prefix; }
		void setExpression(Expression* expression);
		void setOperator(std::string op) { this->_operator = op; }

		std::string getOperator() const noexcept { return this->_operator; }
		Expression* getExpression() const noexcept { return this->_expression; }
		bool isPrefix() const noexcept { return this->_is_prefix; }

		virtual void print() const noexcept;
	};

	/* This class represents a list of expressions: (5, 7, 4), (x, 7, "abc", 'a') */
	class ExpressionList : public Expression {
	private:
		std::vector<Expression*> _expressions;

	public:
		ExpressionList(const position_data& pos) : Expression(pos) {}

		virtual ~ExpressionList();

		virtual expression_type getExpressionType() const noexcept { return expression_type::expression_list; }

		void addExpression(Expression* expression) noexcept { this->_expressions.push_back(expression); }
		const vector<Expression*>& getExpressions() const noexcept { return this->_expressions; }
		
		void setExpression(int position, Expression* expr) { 
			if (position >= this->_expressions.size()) return;
			this->_expressions[position] = expr; 
		}

		virtual void print() const noexcept;
	};

	class TypeInstatiation : public Expression {
	private:
		ExpressionList* _constructor_params = nullptr;
		TypeExpression* _type = nullptr;
		bool _owns_type = false;

	public:
		TypeInstatiation(const position_data& pos) : Expression(pos) {}

		ExpressionList* getParameters() const noexcept { return this->_constructor_params; }
		TypeExpression* getType() const noexcept { return this->_type; }
		bool getOwnsType() const noexcept { return this->_owns_type; }

		void setParameters(ExpressionList* params) noexcept;
		void setType(TypeExpression* type) noexcept;
		void setOwnsType(bool owns) noexcept { this->_owns_type = owns; }

		virtual ~TypeInstatiation();

		virtual expression_type getExpressionType() const noexcept { return expression_type::type_instantiation; }

		virtual void print() const noexcept;
	};

	/* This class represents a function call expression: f(5, 2), g(x), (func_array[0])("abcd") */
	class CallExpression : public Expression {
	private:
		Expression* _function_expression = nullptr;
		ExpressionList* _arguments = nullptr;

	public:
		CallExpression(const position_data& pos) : Expression(pos) {}

		virtual ~CallExpression();

		virtual expression_type getExpressionType() const noexcept { return expression_type::call_expression; }

		void setFunctionExpression(Expression* func) noexcept;
		void setArguments(ExpressionList* arguments) noexcept;

		Expression* getFunctionExpression() const noexcept { return this->_function_expression; }
		ExpressionList* getArguments() const noexcept { return this->_arguments; }

		virtual void print() const noexcept;
	};

	/* This class represents a ternary expression: true ? 1 : 2; x > 5 ? (0, 1) : (2, 28); */
	class TernaryExpression : public Expression {
	private:
		Expression* _condition = nullptr;
		Expression* _true_expression = nullptr;
		Expression* _false_expression = nullptr;

	public:
		TernaryExpression(const position_data& pos) : Expression(pos) { }
		
		virtual ~TernaryExpression();

		virtual expression_type getExpressionType() const noexcept { return expression_type::ternary_expression; }

		void setCondition(Expression* condition);
		void setTrueExpression(Expression* true_expr);
		void setFalseExpression(Expression* false_expr);

		Expression* getCondition() const noexcept { return this->_condition; }
		Expression* getTrueExpression() const noexcept { return this->_true_expression; }
		Expression* getFalseExpression() const noexcept { return this->_false_expression; }

		virtual void print() const noexcept;
	};

	/* This class represents an identifier expression: my_class, some_identifier */
	class IdentifierExpression : public Expression {
	private:
		std::string _identifier;

	public:
		IdentifierExpression(const position_data& pos, const string& identifier) : Expression(pos), _identifier(identifier) {}

		virtual ~IdentifierExpression() = default;

		virtual expression_type getExpressionType() const noexcept { return expression_type::identifier_expression; }

		void setIdentifier(const std::string& identifier) { this->_identifier = identifier; }
		const std::string& getIdentifier() const noexcept { return this->_identifier; }

		virtual void print() const noexcept;
	};

	class CastExpression : public Expression {
	public:
		enum class cast_type { cast_static, cast_dynamic, cast_bitcast };

	private:
		Expression* _inner = nullptr;
		TypeExpression* _target_type = nullptr;
		cast_type _casting_type;

	public:
		CastExpression(const position_data& pos, Expression* inner, TypeExpression* target_type, cast_type casting_type = cast_type::cast_static)
				: Expression(pos), _inner(inner), _target_type(target_type), _casting_type(casting_type)  {}
		
		virtual ~CastExpression() = default;

		virtual expression_type getExpressionType() const noexcept { return expression_type::cast_expression; }

		void setInnerExpression(Expression* inner) noexcept { this->_inner = inner; }
		Expression* getInnerExpression() const noexcept { return this->_inner; }

		void setTargetType(TypeExpression* target_type) noexcept { this->_target_type = target_type; }
		TypeExpression* getTargetType() const noexcept { return this->_target_type; }

		void setCastingType(const cast_type& casting_type) noexcept { this->_casting_type = casting_type; }
		cast_type getCastingType() const noexcept { return this->_casting_type; }

		virtual void print() const noexcept;
	};

	/* This class represents a simple type expression: int, string, vector, some_identifier, char */
	class SimpleType : public TypeExpression {
	private:
		string _identifier_type;

	public:
		SimpleType(const position_data& pos, const string& idntf_type) : TypeExpression(pos) { this->_identifier_type = idntf_type; }
		virtual ~SimpleType() = default;

		void setIdentifierType(const string& identifier_type) { this->_identifier_type = identifier_type; }

		virtual type_expression_category getTypeExpressionCategory() const noexcept { return type_expression_category::simple_type; }

		const string& getIdentifier() const noexcept { return this->_identifier_type; }

		virtual void print() const noexcept;

		friend bool operator==(const SimpleType& first, const SimpleType& second)
		{
			if(first._identifier_type.empty() || second._identifier_type.empty()) // Maybe make some problems
				return false;
			
			return first._identifier_type == second._identifier_type;
		}
	};

	class VariableDeclarationComponent : public Node
	{
	private:
		IdentifierExpression* _identifier = nullptr;
		Expression* _initialization = nullptr;

	public:
		VariableDeclarationComponent(IdentifierExpression* identifier, Expression* initialization) :
		 						Node(identifier->getPosition()), _identifier(identifier), _initialization(initialization) { }

		virtual ~VariableDeclarationComponent();

		virtual node_type getNodeType() const noexcept { return node_type::variable_declaration_component; };
		
		IdentifierExpression* getIdentifier() const noexcept { return this->_identifier; }
		Expression* getInitialization() const noexcept { return this->_initialization; }

		virtual void print() const noexcept;
	};

	class ComplexTypeComponent : public Node
	{
	private:
		TypeExpression* _type = nullptr;
		IdentifierExpression* _identifier = nullptr;

	public:
		ComplexTypeComponent(TypeExpression* type, IdentifierExpression* identifier) :
					Node(identifier->getPosition()), _identifier(identifier), _type(type) { }

		virtual ~ComplexTypeComponent();

		virtual node_type getNodeType() const noexcept { return node_type::complex_type_component; };

		IdentifierExpression* getIdentifier() const noexcept { return this->_identifier; }
		TypeExpression* getType() const noexcept { return this->_type; }

		virtual void print() const noexcept;

		friend bool operator==(const ComplexTypeComponent& first, const ComplexTypeComponent& second)
		{
			if(first._type == nullptr || second._type == nullptr  || first._identifier == nullptr || second._identifier == nullptr) // Maybe make some problems
				return false;
			
			return (*(first._type)) == (*(second._type));
		}
	};

	/* This class represents complex types: (string a, int b, (int x, int y) c)*/
	class ComplexType : public TypeExpression {
	private:
		vector<ComplexTypeComponent*> _type_components;
		SymbolTable _symbol_table;

	public:
		ComplexType(const position_data& pos) : TypeExpression(pos) {}

		virtual ~ComplexType();

		virtual type_expression_category getTypeExpressionCategory() const noexcept { return type_expression_category::complex_type; }

		void addComponent(TypeExpression* type_expr, IdentifierExpression* name_id) { this->_type_components.push_back(new ComplexTypeComponent(type_expr, name_id)); }

		void setSymbolTable(const SymbolTable& symbol_table) { this->_symbol_table = symbol_table; }

		const SymbolTable& getSymbolTable() const noexcept { return this->_symbol_table; }

		vector<ComplexTypeComponent*>& getComponents() noexcept { return this->_type_components; }

		virtual void print() const noexcept;

		friend bool operator==(const ComplexType& first, const ComplexType& second)
		{
			if(first._type_components.empty() || second._type_components.empty() ||
					(first._type_components.size() != second._type_components.size())) // Maybe make some problems
				return false;

			auto it1 = first._type_components.begin();
			auto end1 = first._type_components.end();
			auto it2 = second._type_components.begin();
			auto end2 = second._type_components.end();

			for(;it1 != end1 && it2 != end2; it1++, it2++)
			{
				if(!((*(*(it1))) == (*(*(it2))))) return false;
			}

			return true;
		}
	};

	/* This class represents an import statement: import "my_module.dens" as my_module; */
	class ImportStatement : public Node {
	private:
		string _to_import;
		string _full_path;

	public:
		ImportStatement(const position_data& pos, const string& module_name) : Node(pos) {
			this->_to_import = module_name;
		}

		virtual ~ImportStatement();

		virtual node_type getNodeType() const noexcept { return node_type::import_statement; }

		const string& getImportModuleName() const noexcept { return this->_to_import; }
		const string& getFullPath() const noexcept { return this->_full_path; }

		void setFullPath(const string& full_path) { this->_full_path = full_path; }
		void setImportModuleName(const string& to_import) { this->_to_import = to_import; }

		virtual void print() const noexcept;
	};

	/* This class represents a declaration of a variable: int x; double y; (int x, int y) a; */
	class VariableDeclaration : public Statement {
	private:
		TypeExpression* _type = nullptr;
		std::vector<VariableDeclarationComponent*> _variables;
		SymbolTable _table;
	
	public:
		VariableDeclaration(const position_data& pos) : Statement(pos) {}

		virtual ~VariableDeclaration();

		virtual statement_type getStatementType() const noexcept { return statement_type::variable_declaration; }

		void setType(TypeExpression* type);
		void setSymbolTable(const SymbolTable& table) { this->_table = table; }
		void addVariable(IdentifierExpression* name, Expression* value = nullptr) { this->_variables.push_back(new VariableDeclarationComponent(name, value)); }

		std::vector<VariableDeclarationComponent*>& getVariables() noexcept { return this->_variables; }
		TypeExpression* getType() const noexcept { return this->_type; }
		SymbolTable& getSymbolTable() { return this->_table; }

		virtual void print() const noexcept;
		virtual void setProperties(const Properties& properties) noexcept;
	};

	/* This class represents a for statement: for (int i = 0; i < 5; i++) x += 5; */
	class ForStatement : public Statement {
	private:
		VariableDeclaration* _initialization = nullptr;
		Expression* _condition = nullptr;
		Expression* _incrementation = nullptr;
		Statement* _commands = nullptr;

		SymbolTable _table;

	public:
		ForStatement(const position_data& pos) : Statement(pos) {}
		
		virtual ~ForStatement();

		virtual statement_type getStatementType() const noexcept { return statement_type::for_statement; }

		void setInitialization(VariableDeclaration* init) noexcept;
		void setCondition(Expression* condition) noexcept;
		void setIncrementation(Expression* increment) noexcept;
		void setCommands(Statement* commands) noexcept;
		void setSymbolTable(const SymbolTable& table) { this->_table = table; }

		VariableDeclaration* getInitialization() const noexcept { return this->_initialization; }
		Expression* getCondition() const noexcept { return this->_condition; }
		Expression* getIncrementation() const noexcept { return this->_incrementation; }
		Statement* getCommands() const noexcept { return this->_commands; }
		SymbolTable& getSymbolTable() { return this->_table; }

		virtual void print() const noexcept;
	};

	/* This class represents a literal expression: 5, 'a', "abcd" */
	class LiteralExpression : public Expression {
	public:
		LiteralExpression(const position_data& pos) : Expression(pos){}

		enum class literal_type { int_type, double_type, char_type, bool_type, string_type, null_type };

		virtual expression_type getExpressionType() const noexcept { return expression_type::literal_expression; }
		virtual literal_type getLiteralType() const noexcept = 0;
	};

	/* This class represents an integer literal expression: 5, 8, -12 */
	class IntegerLiteral : public LiteralExpression {
	private:
		int _value;

	public:
		IntegerLiteral(const position_data& pos, int value) : LiteralExpression(pos), _value(value) {}

		virtual ~IntegerLiteral() = default;

		virtual literal_type getLiteralType() const noexcept { return literal_type::int_type; }

		int getValue() const noexcept { return this->_value; }

		virtual void print() const noexcept;
	};

	/* This class represents a double literal expression: 5.0, 8.5, -12.1 */
	class DoubleLiteral : public LiteralExpression {
	private:
		double _value;
		
	public:
		DoubleLiteral(const position_data& pos, double value) : LiteralExpression(pos), _value(value) {}

		virtual ~DoubleLiteral() = default;

		virtual literal_type getLiteralType() const noexcept { return literal_type::double_type; }

		double getValue() const noexcept { return this->_value; }

		virtual void print() const noexcept;
	};

	/* This class represents a char literal expression: 'a', '&', ' ' */
	class CharLiteral : public LiteralExpression {
	private:
		char _value;

	public:
		CharLiteral(const position_data& pos, char value) : LiteralExpression(pos), _value(value){}
		
		virtual ~CharLiteral() = default;

		virtual literal_type getLiteralType() const noexcept { return literal_type::char_type; }

		char getValue() const noexcept { return this->_value; }

		virtual void print() const noexcept;
	};

	/* This class represents a boolean literal expression: true, false */
	class BooleanLiteral : public LiteralExpression {
	private:
		bool _value;

	public:
		BooleanLiteral(const position_data& pos, bool value) : LiteralExpression(pos), _value(value) {}

		virtual ~BooleanLiteral() = default;

		virtual literal_type getLiteralType() const noexcept { return literal_type::bool_type; }

		bool getValue() const noexcept { return this->_value; }

		virtual void print() const noexcept;
	};

	/* This class represents a string literal expression: "abcd", "1234" */
	class StringLiteral : public LiteralExpression {
	private:
		std::string _value;
		PointerType* _type = nullptr;

	public:
		StringLiteral(const position_data& pos, std::string value) : LiteralExpression(pos), _value(value) {}

		virtual literal_type getLiteralType() const noexcept { return literal_type::string_type; }

		std::string getValue() const noexcept { return this->_value; }
	
		virtual void print() const noexcept;
	};

	/* This class represents a string literal expression: "abcd", "1234" */
	class NullLiteral : public LiteralExpression {
	private:
		unsigned int _value;

	public:
		NullLiteral(const position_data& pos, unsigned int value = 0) : LiteralExpression(pos), _value(value) {}

		virtual ~NullLiteral() = default;

		virtual literal_type getLiteralType() const noexcept { return literal_type::null_type; }

		unsigned int getValue() const noexcept { return this->_value; }
	
		virtual void print() const noexcept;
	};

	/* This class represents a "varadic call" */
	class VarArgsExpression : public Expression {
	private:
		TypeExpression* _type_get = nullptr;

	public:
		VarArgsExpression(const position_data& pos, TypeExpression* type_get) : Expression(pos), _type_get(type_get) {}
		
		virtual ~VarArgsExpression() { if(_type_get != nullptr) delete _type_get; _type_get = nullptr; }

		virtual expression_type getExpressionType() const noexcept { return expression_type::vargs_expression; }

		TypeExpression* getTypeToGet() const noexcept { return this->_type_get; }
		void setTypeToGet(TypeExpression* type_get) noexcept { this->_type_get = type_get; }

		virtual void print() const noexcept;
	};

	/* This class represents a "create call" */
	class CreateFunctionExpression : public Expression {
	private:
		TypeInstatiation* _creation_type = nullptr;

	public:
		CreateFunctionExpression(const position_data& pos, TypeInstatiation* creation_type) : Expression(pos), _creation_type(creation_type) {}
		
		virtual ~CreateFunctionExpression() { if(_creation_type != nullptr) delete _creation_type; _creation_type = nullptr; }

		virtual expression_type getExpressionType() const noexcept { return expression_type::create_function_expression; }

		TypeInstatiation* getCreationType() const noexcept { return this->_creation_type; }
		void setCreationType(TypeInstatiation* creation_type) noexcept { this->_creation_type = creation_type; }

		virtual void print() const noexcept;
	};

	/* This class represents a "new call" */
	class NewFunctionExpression : public Expression {
	private:
		TypeInstatiation* _creation_type = nullptr;

	public:
		NewFunctionExpression(const position_data& pos, TypeInstatiation* creation_type) : Expression(pos), _creation_type(creation_type) {}
		
		virtual ~NewFunctionExpression() { if(_creation_type != nullptr) delete _creation_type; _creation_type = nullptr; }

		virtual expression_type getExpressionType() const noexcept { return expression_type::new_function_expression; }

		TypeInstatiation* getCreationType() const noexcept { return this->_creation_type; }
		void setCreationType(TypeInstatiation* creation_type) noexcept { this->_creation_type = creation_type; }

		virtual void print() const noexcept;
	};

	/* This class represents a "delete call" */
	class DeleteFunctionStatement : public Statement {
	private:
		Expression* _deleting_obj = nullptr;

	public:
		DeleteFunctionStatement(const position_data& pos, Expression* deleting_obj) : Statement(pos), _deleting_obj(deleting_obj) {}
		
		virtual ~DeleteFunctionStatement() { if(_deleting_obj != nullptr) delete _deleting_obj; _deleting_obj = nullptr; }

		virtual statement_type getStatementType() const noexcept { return statement_type::delete_function_statement; }

		Expression* getDeletingObj() const noexcept { return this->_deleting_obj; }
		void setDeletingObj(Expression* deleting_obj) noexcept { this->_deleting_obj = deleting_obj; }

		virtual void print() const noexcept;
	};

	class FunctionDefinition;

	/* This class represents a type declaration: type vector { i64 length; i8* buffer = null; } */
	class TypeDefinition : public Node {
	private:
		IdentifierExpression* _type_name = nullptr;
		vector<FunctionDefinition*> _type_functions;
		vector<VariableDeclaration*> _type_data_members;
		SymbolTable _data_symbol_table;
		SymbolTable _functions_symbol_table;

	public:
		TypeDefinition(const position_data& pos) : Node(pos) {}

		virtual ~TypeDefinition();

		virtual node_type getNodeType() const noexcept { return node_type::type_definition; }

		IdentifierExpression* getTypeName() const noexcept { return this->_type_name; }
		vector<FunctionDefinition*>& getTypeFunctions() noexcept { return this->_type_functions; }
		vector<VariableDeclaration*>& getTypeDataMembers() noexcept { return this->_type_data_members; }
		SymbolTable& getDataSymbolTable() { return this->_data_symbol_table; }
		SymbolTable& getFunctionsSymbolTable() { return this->_functions_symbol_table; }

		void setTypeName(IdentifierExpression* name) noexcept;
		void setTypeFunctions(const vector<FunctionDefinition*>& funcs) noexcept;
		void setTypeDataMembers(const vector<VariableDeclaration*>& vars) noexcept;
		void setDataSymbolTable(const SymbolTable& table) { this->_data_symbol_table = table;}
		void setFunctionsSymbolTable(const SymbolTable& table) { this->_functions_symbol_table = table;}

		virtual void print() const noexcept;
	};

	/* This class represents a function: (int a, int b) x(int y) { return (y, 0); } */
	class FunctionDefinition : public Node {
	private:
		IdentifierExpression* _funtion_name = nullptr;
		TypeExpression* _return_type = nullptr;
		ComplexType* _input_type = nullptr;
		Statement* _commands = nullptr;

		TypeDefinition* _parent = nullptr;

	public:
		FunctionDefinition(const position_data& pos) : Node(pos) {}

		virtual ~FunctionDefinition();

		virtual node_type getNodeType() const noexcept { return node_type::function_definition; }

		IdentifierExpression* getFunctionName() const noexcept { return this->_funtion_name; }
		TypeExpression* getReturnType() const noexcept { return this->_return_type; }
		ComplexType* getInputType() const noexcept { return this->_input_type; }
		Statement* getCommands() const noexcept { return this->_commands; }
		TypeDefinition* getParentType() const noexcept { return this->_parent; }

		void setFunctionName(IdentifierExpression* name) noexcept;
		void setReturnType(TypeExpression* return_type) noexcept;
		void setInputType(ComplexType* input_type) noexcept;
		void setCommands(Statement* commands) noexcept;
		void setParentType(TypeDefinition* type) noexcept { this->_parent = type; }

		virtual void print() const noexcept;
	};

	class NamespaceDefinition : public Node
	{
	private:
		IdentifierExpression* _identifier = nullptr;
		std::vector<Node*> _commands;
		SymbolTable _symbol_table;

	public:
		NamespaceDefinition(const position_data& pos, IdentifierExpression* identifier) 
			: Node(pos), _identifier(identifier) {}
		~NamespaceDefinition();

		IdentifierExpression* getIdentifier() const noexcept { return this->_identifier; }
		std::vector<Node*>& getCommands() noexcept { return this->_commands; }
		SymbolTable& getSymbolTable() noexcept { return this->_symbol_table; }

		void setIdentifier(IdentifierExpression* identifier) noexcept { this->_identifier = identifier; }
		void setCommands(const std::vector<Node*>& commands) noexcept { this->_commands = commands; }
		void setSymbolTable(const SymbolTable& table) noexcept { this->_symbol_table = table; }

		void AddCommand(Node* node) noexcept { this->_commands.push_back(node); }
		void CleanAllCommands() noexcept;

		virtual node_type getNodeType() const noexcept { return node_type::namespace_definition;  }
		void print() const noexcept;
	};

}