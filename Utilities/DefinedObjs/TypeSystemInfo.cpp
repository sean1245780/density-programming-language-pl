#include "TypeSystemInfo.h"

TypeContainer* TypeSystemInfo::global_container = nullptr;
llvm::LLVMContext TypeSystemInfo::global_context;

unordered_map<std::string, TypeSystemInfo::primitive_type> TypeSystemInfo::primitive_type_mapping =                       
                        {
                            {"i8", primitive_type::int8_type},
                            {"i16", primitive_type::int16_type},
                            {"i32", primitive_type::int32_type},
                            {"i64", primitive_type::int64_type},
                            {"i128", primitive_type::int128_type},
                            {"u8", primitive_type::uint8_type},
                            {"u16", primitive_type::uint16_type},
                            {"u32", primitive_type::uint32_type},
                            {"u64", primitive_type::uint64_type},
                            {"u128", primitive_type::uint_128},
                            //{"f16", primitive_type::float16_type},
                            {"f32", primitive_type::float32_type},
                            {"f64", primitive_type::float64_type},
                            {"f128", primitive_type::float128_type},
                            {"char", primitive_type::char_type},
                            {"uchar", primitive_type::uchar_type},
                            {"bool", primitive_type::bool_type},
                            // And var by default
                        };

unordered_map<string, array<TypeSystemInfo::type_synthesis_action, 3>> TypeSystemInfo::operators_type_synthesis = {
                                            // PREFIX                          BINARY                       POSTFIX
                                    {".",   {type_synthesis_action::none, type_synthesis_action::access, type_synthesis_action::none} },
                                    {"::",	{type_synthesis_action::none, type_synthesis_action::access, type_synthesis_action::none} },
                                    {"->",	{type_synthesis_action::none, type_synthesis_action::dref_access, type_synthesis_action::none} },
                                    {"!",	{type_synthesis_action::boolean, type_synthesis_action::none, type_synthesis_action::none} },
                                    {"~",	{type_synthesis_action::right, type_synthesis_action::none, type_synthesis_action::none} },
                                    {"++",	{type_synthesis_action::right, type_synthesis_action::none, type_synthesis_action::left} }, 
                                    {"--",	{type_synthesis_action::right, type_synthesis_action::none, type_synthesis_action::left} },
                                    {"^^",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"%",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"/",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"*",	{type_synthesis_action::dereference, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"$",	{type_synthesis_action::safe_dref_access, type_synthesis_action::none, type_synthesis_action::none} },
                                    {"#",	{type_synthesis_action::safe_wrap, type_synthesis_action::none, type_synthesis_action::none} },
                                    {"+",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"-",	{type_synthesis_action::right, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"[]",	{type_synthesis_action::none, type_synthesis_action::right, type_synthesis_action::none} },
                                    {">>",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"<<",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"<=",	{type_synthesis_action::none, type_synthesis_action::boolean, type_synthesis_action::none} },
                                    {">=",	{type_synthesis_action::none, type_synthesis_action::boolean, type_synthesis_action::none} },
                                    {"<",	{type_synthesis_action::none, type_synthesis_action::boolean, type_synthesis_action::none} },
                                    {">",	{type_synthesis_action::none, type_synthesis_action::boolean, type_synthesis_action::none} },
                                    {"!=",	{type_synthesis_action::none, type_synthesis_action::boolean, type_synthesis_action::none} },
                                    {"==",	{type_synthesis_action::none, type_synthesis_action::boolean, type_synthesis_action::none} },
                                    {"&",	{type_synthesis_action::pointer_of, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"^",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"|",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"&&",	{type_synthesis_action::none, type_synthesis_action::boolean, type_synthesis_action::none} },
                                    {"||",	{type_synthesis_action::none, type_synthesis_action::boolean, type_synthesis_action::none} },
                                    {"=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"**=", {type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"+=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} }, 
                                    {"-=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"/=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"*=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} }, 
                                    {"%=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} }, 
                                    {"|=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"&=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"^=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"<<=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {">>=",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"to",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"as",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} },
                                    {"bitcast",	{type_synthesis_action::none, type_synthesis_action::left, type_synthesis_action::none} }
                                };

unordered_map<TypeSystemInfo::primitive_type, std::string> TypeSystemInfo::primitive_type_mapping_rev = []()
{
    unordered_map<TypeSystemInfo::primitive_type, std::string> val;
    
    for(const auto& obj : TypeSystemInfo::primitive_type_mapping)
    {
        val.insert({obj.second, obj.first});
    }

    return val;  
}();

TypeContainer::~TypeContainer()
{ 
    for(auto* val : this->unnamed_definitions)
    {
        delete val;
        val = nullptr;
    }

    for(auto& val : this->definitions)
    {
        delete val.second;
        val.second = nullptr;
    }
}

void TypeContainer::enterDefinition(const string& definition) noexcept
{
    if (!this->position.empty())
        this->position += ":";
    this->position += definition;
}

string TypeContainer::exitDefinition() noexcept
{
    if (this->position.empty()) return "";

    size_t colons_pos = this->position.find(":");
    string ret_val;
    
    if (colons_pos == string::npos) 
    {
        ret_val = this->position;
        this->position = "";
    }
    else 
    {
        ret_val = this->position.substr(colons_pos + 1);
        this->position = this->position.substr(0, colons_pos);
    }

    return ret_val;
}

string TypeContainer::exitToScopeOf(const string& identifier) noexcept
{
    string exited = "";
    string curr_exited = "";

    while ((curr_exited = exitDefinition()) != identifier && curr_exited != "")
        exited = curr_exited + exited;

    return exited + curr_exited;
}

string TypeContainer::exitToGlobalScope() noexcept
{
    string tmp = position;
    this->position = "";
    return tmp;
}

PointerType* TypeContainer::getOrCreatePointerType(SynthesizedType* base) noexcept
{
    if(base == nullptr) return nullptr;

    return static_cast<PointerType*>(safeGetOrCreateType(
            base->getName() + "*", 
            [&](){ return new PointerType(base); }, 
            SynthesizedType::type_category::pointer_type)
        );
}

StrongPointerType* TypeContainer::getOrCreateStrongPointerType(SynthesizedType* base) noexcept
{
    if(base == nullptr) return nullptr;

    return static_cast<StrongPointerType*>(safeGetOrCreateType(
            "strong" + base->getName() + "*", 
            [&](){ return new StrongPointerType(base); }, 
            SynthesizedType::type_category::strong_pointer_type)
        );
}

WeakPointerType* TypeContainer::getOrCreateWeakPointerType(SynthesizedType* base) noexcept
{
    if(base == nullptr) return nullptr;

    return static_cast<WeakPointerType*>(safeGetOrCreateType(
            "weak" + base->getName() + "*", 
            [&](){ return new WeakPointerType(base); }, 
            SynthesizedType::type_category::weak_pointer_type)
        );
}


FunctionType* TypeContainer::getOrCreateFunctionType(SynthesizedType* input, SynthesizedType* output, bool is_variadic, CustomType* parent) noexcept
{
    if (input == nullptr || output == nullptr) return nullptr;

    return static_cast<FunctionType*>(safeGetOrCreateType(
            string("(") + input->getName() + " + (" + ((is_variadic) ? ("vargs") : (""))
                + ") -> " + output->getName() + ")" + (parent != nullptr ? " in " + parent->getName() : ""), 
            [&](){ return new FunctionType(input, output, is_variadic); }, 
            SynthesizedType::type_category::function_type)
        );
}

BasicType* TypeContainer::getOrCreateBasicType(const string& name, llvm::Type* llvm_type, bool modifiable) noexcept
{
    return static_cast<BasicType*>(safeGetOrCreateType(
            this->position + name, 
            [&](){ return new BasicType(this->position + name, llvm_type, modifiable); }, 
            SynthesizedType::type_category::basic_type)
        );
}

SynthesizedType* TypeContainer::getOrCreateType(const string& name, std::function<SynthesizedType*()> creation_func) noexcept
{
    // find the definition
    const auto found = this->definitions.find(name);

    // if it was not found, add it
    if (found == this->definitions.end()) 
    {
        auto* type = creation_func();
        this->definitions.insert({name, type});

        return type;
    }

    return found->second;
}

SynthesizedType* TypeContainer::safeGetOrCreateType(const string& name, std::function<SynthesizedType*()> creation_func, SynthesizedType::type_category type_category) noexcept
{
    auto* type = getOrCreateType(name, creation_func);

    return type->getType() == type_category ? type : nullptr; 
}

NamedComplexType* TypeContainer::getOrCreateNamedComplexType(NamedComplexType* type)
{
    auto* created = static_cast<NamedComplexType*>(safeGetOrCreateType(type->getName(), [&](){ return type; },
        SynthesizedType::type_category::named_complex_type));

    if (created != type) delete type;

    return created;
}

UnnamedComplexType* TypeContainer::getOrCreateUnnamedComplexType(UnnamedComplexType* type)
{
    auto* created = static_cast<UnnamedComplexType*>(safeGetOrCreateType(type->getName(), [&](){ return type; },
        SynthesizedType::type_category::unnamed_complex_type));

    if (created != type) delete type;

    return created;    
}

SynthesizedType* TypeContainer::getType(const string& definition) const noexcept
{
    // find the definition
    const auto found = this->definitions.find(definition);

    // if it was not found, return nullptr
    if (found == this->definitions.end()) 
        return nullptr;

    return found->second;
}

void TypeSystemInfo::init() noexcept
{
    bool isSigned = true;
    auto def_int_inc = [isSigned](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* type, SynthesizedType* other_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
            llvm::Value* val = (lhs == nullptr) ? rhs : lhs;
            llvm::Value* before_addition = nullptr;

            builder.CreateStore(
                builder.CreateAdd(
                    before_addition = builder.CreateLoad(val),
                    llvm::ConstantInt::get(builder.getContext(), llvm::APInt(type->getLLVMType()->getIntegerBitWidth(), 1, isSigned)
                )), 
                val
            );

            return lhs == nullptr ? val : before_addition;
        };

    auto def_int_dec = [isSigned](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* type, SynthesizedType* other_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
            llvm::Value* val = (lhs == nullptr) ? rhs : lhs;
            llvm::Value* before_addition = nullptr;

            builder.CreateStore(
                builder.CreateSub(
                    before_addition = builder.CreateLoad(val),
                    llvm::ConstantInt::get(builder.getContext(), llvm::APInt(type->getLLVMType()->getIntegerBitWidth(), 1, isSigned)
                )), 
                val
            );

            return lhs == nullptr ? val : before_addition;
        };

    auto def_int_neg = [isSigned](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* { 
                if(rhs == nullptr) return nullptr;
                return builder.CreateNeg(rhs, "", true, true);
            };

    auto def_float_gt = [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFCmpOGT(lhs, rhs);};
    auto def_float_lt =[](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFCmpOLT(lhs, rhs);};
    auto def_float_ge =[](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFCmpOGE(lhs, rhs);};
    auto def_float_le =[](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFCmpOLE(lhs, rhs);};
    auto def_float_add =[](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFAdd(lhs, rhs);};
    auto def_float_sub =[](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFSub(lhs, rhs);};
    auto def_float_mul =[](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFMul(lhs, rhs);};
    auto def_float_div =[](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFDiv(lhs, rhs);};

    global_container = new TypeContainer();

    auto* int8_type = global_container->getOrCreateBasicType("i8", llvm::Type::getInt8Ty(TypeSystemInfo::global_context), false);
    auto* int16_type = global_container->getOrCreateBasicType("i16", llvm::Type::getInt16Ty(TypeSystemInfo::global_context), false);
    auto* int32_type = global_container->getOrCreateBasicType("i32", llvm::Type::getInt32Ty(TypeSystemInfo::global_context), false);
    auto* int64_type = global_container->getOrCreateBasicType("i64", llvm::Type::getInt64Ty(TypeSystemInfo::global_context), false);
    auto* int128_type = global_container->getOrCreateBasicType("i128", llvm::Type::getInt128Ty(TypeSystemInfo::global_context), false);
    auto* uint8_type = global_container->getOrCreateBasicType("u8", llvm::Type::getInt8Ty(TypeSystemInfo::global_context), false);
    auto* uint16_type = global_container->getOrCreateBasicType("u16", llvm::Type::getInt16Ty(TypeSystemInfo::global_context), false);
    auto* uint32_type = global_container->getOrCreateBasicType("u32", llvm::Type::getInt32Ty(TypeSystemInfo::global_context), false);
    auto* uint64_type = global_container->getOrCreateBasicType("u64", llvm::Type::getInt64Ty(TypeSystemInfo::global_context), false);
    auto* uint128_type = global_container->getOrCreateBasicType("u128", llvm::Type::getInt128Ty(TypeSystemInfo::global_context), false);
    //auto* float16_type = global_container->getOrCreateBasicType("f16", llvm::Type::getHalfTy(TypeSystemInfo::global_context), false);
    auto* float32_type = global_container->getOrCreateBasicType("f32", llvm::Type::getFloatTy(TypeSystemInfo::global_context), false);
    auto* float64_type = global_container->getOrCreateBasicType("f64", llvm::Type::getDoubleTy(TypeSystemInfo::global_context), false);
    auto* float128_type = global_container->getOrCreateBasicType("f128", llvm::Type::getFP128Ty(TypeSystemInfo::global_context), false);
    auto* bool_type = global_container->getOrCreateBasicType("bool", llvm::Type::getInt1Ty(TypeSystemInfo::global_context), false);
    auto* char_type = global_container->getOrCreateBasicType("char", llvm::Type::getInt8Ty(TypeSystemInfo::global_context), false);
    auto* uchar_type = global_container->getOrCreateBasicType("uchar", llvm::Type::getInt8Ty(TypeSystemInfo::global_context), false);

    global_container->safeGetOrCreateType("module",
        [&]() -> SynthesizedType* { return new ModuleType(); },
        SynthesizedType::type_category::module_type
    );

    global_container->safeGetOrCreateType("namespace",
        [&]() -> SynthesizedType* { return new NamespaceType(); },
        SynthesizedType::type_category::namespace_type
    );
    
    global_container->safeGetOrCreateType("void", 
        [&]() -> SynthesizedType* { return new VoidType(llvm::Type::getVoidTy(global_context)); },
        SynthesizedType::type_category::void_type
    );

    unordered_map<string, SynthesizedType::op_codegen_function> bin_type_ops = {
        {"=", SynthesizedType::def_assignment}, 
        {"==", SynthesizedType::def_equ}, 
        {"!=", SynthesizedType::def_not_equ}, 
        {">", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateICmpSGT(lhs, rhs);}},
        {"<", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateICmpSLT(lhs, rhs);}}, 
        {">=", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateICmpSGE(lhs, rhs);}}, 
        {"<=", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateICmpSLE(lhs, rhs);}}, 
        {"+", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateAdd(lhs, rhs);}}, 
        {"-", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateSub(lhs, rhs);}},
        {"*", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateMul(lhs, rhs);}},
        {"/", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateSDiv(lhs, rhs);}},
        {"%", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateSRem(lhs, rhs);}}
//      {"^^", [](llvm::Value* lhs, llvm::Value* rhs, llvm::IRBuilder<>& builder){return builder.CreateAdd(lhs, rhs);}}  
    };

    unordered_map<string, SynthesizedType::op_codegen_function> unr_type_ops = {
        {"++", def_int_inc},
        {"--", def_int_dec},
        {"-", def_int_neg}
    };

    int8_type->addOperators(int8_type, bin_type_ops);
    int8_type->addOperators(nullptr, unr_type_ops);
    int16_type->addOperators(int16_type, bin_type_ops);
    int16_type->addOperators(nullptr, unr_type_ops);
    int32_type->addOperators(int32_type, bin_type_ops);
    int32_type->addOperators(nullptr, unr_type_ops);
    int64_type->addOperators(int64_type, bin_type_ops);
    int64_type->addOperators(nullptr, unr_type_ops);
    int128_type->addOperators(int128_type, bin_type_ops);
    int128_type->addOperators(nullptr, unr_type_ops);
    char_type->addOperators(char_type, bin_type_ops);
    char_type->addOperators(nullptr, unr_type_ops);

    isSigned = false;
    bin_type_ops =  
    {
        {"=", SynthesizedType::def_assignment}, 
        {"==", SynthesizedType::def_equ}, 
        {"!=", SynthesizedType::def_not_equ}, 
        {">", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateICmpUGT(lhs, rhs);}},
        {"<", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateICmpULT(lhs, rhs);}}, 
        {">=", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateICmpUGE(lhs, rhs);}}, 
        {"<=", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateICmpULE(lhs, rhs);}}, 
        {"+", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateAdd(lhs, rhs);}}, 
        {"-", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateSub(lhs, rhs);}},
        {"*", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateMul(lhs, rhs);}},
        {"/", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateUDiv(lhs, rhs);}},
        {"%", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateURem(lhs, rhs);}}
//      {"^^", [](llvm::Value* lhs, llvm::Value* rhs, llvm::IRBuilder<>& builder){return builder.CreateAdd(lhs, rhs);}}
    };

    unr_type_ops = {
        {"++", def_int_inc},
        {"--", def_int_dec},
        {"-", def_int_neg}
    };

    uint8_type->addOperators(uint8_type, bin_type_ops);
    uint8_type->addOperators(nullptr, unr_type_ops);
    uint16_type->addOperators(uint16_type, bin_type_ops);
    uint16_type->addOperators(nullptr, unr_type_ops);
    uint32_type->addOperators(uint32_type, bin_type_ops);
    uint32_type->addOperators(nullptr, unr_type_ops);
    uint64_type->addOperators(uint64_type, bin_type_ops);
    uint64_type->addOperators(nullptr, unr_type_ops);
    uint128_type->addOperators(uint128_type, bin_type_ops);
    uint128_type->addOperators(nullptr, unr_type_ops);
    uchar_type->addOperators(uchar_type, bin_type_ops);
    uchar_type->addOperators(nullptr, unr_type_ops);
    

    bin_type_ops = {
        {"=",  SynthesizedType::def_assignment}, 
        {"==", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFCmpOEQ(lhs, rhs);}},
        {"!=", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFCmpONE(lhs, rhs);}},
        {">",  def_float_gt},
        {"<",  def_float_lt},
        {">=", def_float_ge},
        {"<=", def_float_le},
        {"+",  def_float_add},
        {"-",  def_float_sub},
        {"*",  def_float_mul},
        {"/",  def_float_div},
        {"%", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateFRem(lhs, rhs);}}
//      {"^^", [](llvm::Value* lhs, llvm::Value* rhs, llvm::IRBuilder<>& builder){return builder.CreateAdd(lhs, rhs);}}
    };

    unr_type_ops = {
        {"++", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
            llvm::Value* val = (lhs == nullptr) ? rhs : lhs;
            llvm::Value* before_addition = nullptr;

            builder.CreateStore(
                builder.CreateFAdd(
                    before_addition = builder.CreateLoad(val),
                    llvm::ConstantFP::get(builder.getContext(), llvm::APFloat(1.0f))
                ), 
                val
            );

            return lhs == nullptr ? val : before_addition;
        }},
        {"--", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
            llvm::Value* val = (lhs == nullptr) ? rhs : lhs;
            llvm::Value* before_addition = nullptr;

            builder.CreateStore(
                builder.CreateFSub(
                    before_addition = builder.CreateLoad(val),
                    llvm::ConstantFP::get(builder.getContext(), llvm::APFloat(1.0f))
                ), 
                val
            );

            return lhs == nullptr ? val : before_addition;
        }},
        {"-", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* { 
                if(rhs == nullptr) return nullptr;
                return builder.CreateFNeg(rhs);
        }}
    };

    //float16_type->addOperators(float16_type, bin_type_ops);
    //float16_type->addOperators(nullptr, unr_type_ops);
    float32_type->addOperators(float32_type, bin_type_ops);
    float32_type->addOperators(nullptr, unr_type_ops);
    float64_type->addOperators(float64_type, bin_type_ops);
    float64_type->addOperators(nullptr, unr_type_ops);
    float128_type->addOperators(float128_type, bin_type_ops);
    float128_type->addOperators(nullptr, unr_type_ops);

    bool_type->addOperators(bool_type, {
        {"=", SynthesizedType::def_assignment}, 
        {"==", SynthesizedType::def_equ},
        {"!=", SynthesizedType::def_not_equ},
        {"&&", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
            return builder.CreateAnd(lhs, rhs);
        }},
        {"||", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
            return builder.CreateOr(lhs, rhs);
        }},
    });

    bool_type->addOperators(nullptr, {
        {"!", [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {return builder.CreateICmpNE(rhs, llvm::ConstantInt::get(llvm::Type::getInt1Ty(builder.getContext()), 0, false));}}
    });

    auto int_to_float_cast = [](llvm::Value* int_val, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
                return builder.CreateSIToFP(int_val, target_type);};
    auto uint_to_float_cast = [](llvm::Value* int_val, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* { 
                return builder.CreateUIToFP(int_val, target_type);};
    auto integer_to_bool = [](llvm::Value* int_val, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* { 
                return builder.CreateICmpNE(int_val, llvm::ConstantInt::get(int_val->getType(), 0, false));};
    auto float_to_bool = [](llvm::Value* int_val, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* { 
                return builder.CreateFCmpONE(int_val, llvm::ConstantInt::get(int_val->getType(), 0, false));};
    
    int8_type->addStaticCasts(
        { 
            //{ float16_type, int_to_float_cast},
            { float32_type, int_to_float_cast},
            { float64_type, int_to_float_cast},
            { float128_type, int_to_float_cast},
            { int8_type, SynthesizedType::def_fit_size_int},
            { int16_type, SynthesizedType::def_fit_size_int},
            { int32_type, SynthesizedType::def_fit_size_int},
            { int64_type, SynthesizedType::def_fit_size_int},
            { int128_type, SynthesizedType::def_fit_size_int},
            { char_type, SynthesizedType::def_fit_size_int},
            { uint8_type, SynthesizedType::def_fit_size_int},
            { uint16_type, SynthesizedType::def_fit_size_int},
            { uint32_type, SynthesizedType::def_fit_size_int},
            { uint64_type, SynthesizedType::def_fit_size_int},
            { uint128_type, SynthesizedType::def_fit_size_int},
            { uchar_type, SynthesizedType::def_fit_size_int},
            { bool_type, integer_to_bool},
        }
    );

    int16_type->setStaticCasts(int8_type->getStaticCasts());
    int16_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_int);
    int32_type->setStaticCasts(int8_type->getStaticCasts());
    int32_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_int);
    int64_type->setStaticCasts(int8_type->getStaticCasts());
    int64_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_int);
    int128_type->setStaticCasts(int8_type->getStaticCasts());
    int128_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_int);
    char_type->setStaticCasts(int8_type->getStaticCasts());
    char_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_int);
    uint16_type->setStaticCasts(int8_type->getStaticCasts());
    uint16_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_uint);
    uint32_type->setStaticCasts(int8_type->getStaticCasts());
    uint32_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_uint);
    uint64_type->setStaticCasts(int8_type->getStaticCasts());
    uint64_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_uint);
    uint128_type->setStaticCasts(int8_type->getStaticCasts());
    uint128_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_uint);
    uchar_type->setStaticCasts(int8_type->getStaticCasts());
    uchar_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_uint);
    bool_type->setStaticCasts(int8_type->getStaticCasts());
    bool_type->addStaticCast(int8_type, SynthesizedType::def_fit_size_int);

    uint8_type->addStaticCasts(
        { 
            //{ float16_type, uint_to_float_cast},
            { float32_type, uint_to_float_cast},
            { float64_type, uint_to_float_cast},
            { float128_type, uint_to_float_cast},
            { uint8_type, SynthesizedType::def_fit_size_uint},
            { uint16_type, SynthesizedType::def_fit_size_uint},
            { uint32_type, SynthesizedType::def_fit_size_uint},
            { uint64_type, SynthesizedType::def_fit_size_uint},
            { uint128_type, SynthesizedType::def_fit_size_uint},
            { uchar_type, SynthesizedType::def_fit_size_uint},
            { int8_type, SynthesizedType::def_fit_size_int},
            { int16_type, SynthesizedType::def_fit_size_int},
            { int32_type, SynthesizedType::def_fit_size_int},
            { int64_type, SynthesizedType::def_fit_size_int},
            { int128_type, SynthesizedType::def_fit_size_int},
            { char_type, SynthesizedType::def_fit_size_uint},
            { bool_type, integer_to_bool},
        }
    );

    uint16_type->setStaticCasts(uint8_type->getStaticCasts());
    uint16_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_uint);
    uint32_type->setStaticCasts(uint8_type->getStaticCasts());
    uint32_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_uint);
    uint64_type->setStaticCasts(uint8_type->getStaticCasts());
    uint64_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_uint);
    uint128_type->setStaticCasts(uint8_type->getStaticCasts());
    uint128_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_uint);
    uchar_type->setStaticCasts(uint8_type->getStaticCasts());
    uchar_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_uint);
    int16_type->setStaticCasts(uint8_type->getStaticCasts());
    int16_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_int);
    int32_type->setStaticCasts(uint8_type->getStaticCasts());
    int32_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_int);
    int64_type->setStaticCasts(uint8_type->getStaticCasts());
    int64_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_int);
    int128_type->setStaticCasts(uint8_type->getStaticCasts());
    int128_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_int);
    char_type->setStaticCasts(uint8_type->getStaticCasts());
    char_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_int);
    bool_type->setStaticCasts(uint8_type->getStaticCasts());
    bool_type->addStaticCast(uint8_type, SynthesizedType::def_fit_size_uint);

    auto float_to_int_cast = [](llvm::Value* int_val, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* { 
                return builder.CreateFPToSI(int_val, target_type);
            };
    auto float_to_uint_cast = [](llvm::Value* int_val, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* { 
                return builder.CreateFPToUI(int_val, target_type);
            };

    // no float16_type
    float32_type->addStaticCasts(
        {   { float32_type, SynthesizedType::def_fit_size_float },
            { float64_type, SynthesizedType::def_fit_size_float },
            { float128_type, SynthesizedType::def_fit_size_float },
            { int8_type, float_to_int_cast},
            { int16_type, float_to_int_cast},
            { int32_type, float_to_int_cast},
            { int64_type, float_to_int_cast},
            { int128_type, float_to_int_cast},
            { char_type, float_to_int_cast},
            { uint8_type, float_to_uint_cast},
            { uint16_type, float_to_uint_cast},
            { uint32_type, float_to_uint_cast},
            { uint64_type, float_to_uint_cast},
            { uint128_type, float_to_uint_cast},
            { uchar_type, float_to_uint_cast},
            { bool_type, float_to_int_cast}
        }
    );

    //float32_type->setStaticCasts(float16_type->getStaticCasts());
    float64_type->setStaticCasts(float32_type->getStaticCasts());
    float128_type->setStaticCasts(float32_type->getStaticCasts());

    /*float16_type->addStaticCasts({
        { float32_type, SynthesizedType::def_fit_size_float },
        { float64_type, SynthesizedType::def_fit_size_float },
        { float128_type, SynthesizedType::def_fit_size_float }
    });

    float32_type->addStaticCasts({
        //{ float16_type, SynthesizedType::def_fit_size_float },
        { float64_type, SynthesizedType::def_fit_size_float },
        { float128_type, SynthesizedType::def_fit_size_float }
    });

    float64_type->addStaticCasts({
        //{ float16_type, SynthesizedType::def_fit_size_float },
        { float32_type, SynthesizedType::def_fit_size_float },
        { float128_type, SynthesizedType::def_fit_size_float }
    });

    float128_type->addStaticCasts({
        //{ float16_type, SynthesizedType::def_fit_size_float },
        { float32_type, SynthesizedType::def_fit_size_float },
        { float64_type, SynthesizedType::def_fit_size_float }
    });*/
}

void TypeSystemInfo::cleanup() noexcept
{
    delete global_container;
    global_container = nullptr;
}

