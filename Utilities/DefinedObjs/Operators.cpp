#include "Operators.h"

unsigned int PrecedenceInfo::leftPrecedence(unsigned int type)
{
	switch (type)
	{
	case (unsigned int)precedence_type::prefix:
		return prefix_precedence;
	case (unsigned int)precedence_type::binary:
		return binary_precedence;
	case (unsigned int)precedence_type::postfix:
		return postfix_precedence;
	case (((unsigned int)precedence_type::postfix) | ((unsigned int)precedence_type::binary)):
		return binary_precedence == NULL_PRECEDENCE ? postfix_precedence : binary_precedence;
	case (((unsigned int)precedence_type::prefix) | ((unsigned int)precedence_type::binary)):
		return binary_precedence == NULL_PRECEDENCE ? prefix_precedence : binary_precedence;
	}

	return NULL_PRECEDENCE;
}

unsigned int PrecedenceInfo::rightPrecedence(unsigned int type)
{
	return leftPrecedence(type) + (associativity == associativity_type::right ? -1 : 0);
}

OperatorsInfo::OperatorsInfo()
{
    // insert the data of the operators
    this->_operators.insert({".", new Operator{
            ".",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::left, NULL_PRECEDENCE, 180,           NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"::", new Operator{
            "::",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::left, 210,				210,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"->", new Operator{
            "->",
            BINARY_DEF_MOD(
                operator_mod::pointer,
                operator_mod::reference
            ),
            {associativity_type::left, NULL_PRECEDENCE, 180,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"!", new Operator{
            "!",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, 160,				NULL_PRECEDENCE, NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"~", new Operator{
            "~",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, 160,				NULL_PRECEDENCE, NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"$", new Operator{
            "$",
            UNARY_OP_MOD(
                operator_mod::value,
                operator_mod::reference,
                operator_mod::value,
                operator_mod::value                
            ),
            {associativity_type::left, 180,				NULL_PRECEDENCE,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"#", new Operator{
            "#",
            UNARY_OP_MOD(
                operator_mod::value,
                operator_mod::value,
                operator_mod::value,
                operator_mod::value                
            ),
            {associativity_type::left, 180,				NULL_PRECEDENCE,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"++", new Operator{
            "++",
            UNARY_OP_MOD(
                operator_mod::reference,
                operator_mod::reference,
                operator_mod::reference,
                operator_mod::value
            ),
            {associativity_type::left, 160,				NULL_PRECEDENCE, 160}
        }
    });

    this->_operators.insert({"--", new Operator{
            "--",
            UNARY_OP_MOD(
                operator_mod::reference,
                operator_mod::reference,
                operator_mod::reference,
                operator_mod::value
            ),
            {associativity_type::left, 160,				NULL_PRECEDENCE, 160}
        }
    });

    this->_operators.insert({"^^", new Operator{
            "^^",
            OPERATOR_ALL_VALUE,
            {associativity_type::right, NULL_PRECEDENCE, 150,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"%", new Operator{
            "%",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 140,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"/", new Operator{
            "/",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 140,			NULL_PRECEDENCE}
        }
    });

    // the default modifier is value, so there is no need to change it
    this->_operators.insert({"*", new Operator{
            "*",
            UNARY_OP_MOD(
                operator_mod::pointer,
                operator_mod::reference,
                operator_mod::value,
                operator_mod::value                
            ),
            {associativity_type::left, 180,				140,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"+", new Operator{
            "+",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, 180             ,130,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"-", new Operator{
            "-",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, 180			  , 130,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({">>", new Operator{
            ">>",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 120,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"<<", new Operator{
            "<<",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 120,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"<=", new Operator{
            "<=",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 110,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({">=", new Operator{
            ">=",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 110,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"<", new Operator{
            "<",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 110,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({">", new Operator{
            ">",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 110,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"!=", new Operator{
            "!=",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 100,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"==", new Operator{
            "==",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 90,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"&", new Operator{
            "&",
            UNARY_OP_MOD(
                operator_mod::reference,
                operator_mod::pointer,
                operator_mod::value,
                operator_mod::value
            ),
            {associativity_type::left, 160,				80,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"^", new Operator{
            "^",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 70,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"|", new Operator{
            "|",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 60,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"&&", new Operator{
            "&&",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 50,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"||", new Operator{
            "||",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 40,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({":", new Operator{
            ":",
            OPERATOR_ALL_VALUE,
            {associativity_type::left, NULL_PRECEDENCE, 30,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"?", new Operator{
            "?",
            OPERATOR_ALL_VALUE,
            {associativity_type::right, NULL_PRECEDENCE, 30,			NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"=", new Operator{
            "=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"^^=", new Operator{
            "^^=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"+=", new Operator{
            "+=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"-=", new Operator{
            "-=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"/=", new Operator{
            "/=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"*=", new Operator{
            "*=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"%=", new Operator{
            "%=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"|=", new Operator{
            "|=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"&=", new Operator{
            "&=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"^=", new Operator{
            "^=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"<<=", new Operator{
            "<<=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({">>=", new Operator{
            ">>=",
            BINARY_DEF_MOD(
                operator_mod::reference,
                operator_mod::reference
            ),
            {associativity_type::right, NULL_PRECEDENCE, 20,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"to", new Operator{
            "to",
            OPERATOR_ALL_VALUE,
            {associativity_type::right, NULL_PRECEDENCE, 180,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"as", new Operator{
            "as",
            BINARY_DEF_MOD(
                operator_mod::pointer,
                operator_mod::pointer
            ),
            {associativity_type::right, NULL_PRECEDENCE, 180,				NULL_PRECEDENCE}
        }
    });

    this->_operators.insert({"bitcast", new Operator{
            "bitcast",
            OPERATOR_ALL_VALUE,
            {associativity_type::right, NULL_PRECEDENCE, 180,				NULL_PRECEDENCE}
        }
    });
}

OperatorsInfo::~OperatorsInfo()
{
    for (auto& itr : this->_operators)
        if (itr.second != nullptr) 
            delete itr.second;

    this->_operators.clear();
}

Operator* OperatorsInfo::getOperator(const string& op)
{
    auto itr = this->_operators.find(op);

    if (itr == this->_operators.end()) return nullptr;

    return itr->second;
}

unsigned int OperatorsInfo::getLongestOperatorSize() const
{
    unsigned int max = 0;

    for(const auto& val : this->_operators)
        max = (val.first.size() > max) ? val.first.size() : max;

    return max;
}
