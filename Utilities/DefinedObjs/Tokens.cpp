#include "Tokens.h"

std::unordered_set<char> Token::separators = {'{', '}', '(', ')', '[', ']', ';', ',', '@'};

const unsigned int Token::longest_operator = OperatorsInfo::inst().getLongestOperatorSize();
				 
std::unordered_map<std::string, token_type> Token::keywords = {{"while", 				token_type::while_kw}, 
															   {"for",					token_type::for_kw }, 
															   {"if",					token_type::if_kw }, 
															   {"else",					token_type::else_kw },
															   {"set",					token_type::set_kw },
															   {"get",					token_type::get_kw },
															   {"contine",				token_type::continue_kw },
															   {"break",				token_type::break_kw },
															   {"namespace",			token_type::namespace_kw },
															   {"enum",					token_type::enum_kw },
															   {"type",					token_type::type_kw },
															   {"throw",				token_type::throw_kw },
															   {"unset",				token_type::unset_kw },
															   {"vset",					token_type::vset_kw },
															   {"public",				token_type::public_kw },
															   {"private",				token_type::private_kw },
															   {"protected",			token_type::protected_kw },
															   {"catch",				token_type::catch_kw },
															   {"ret",					token_type::return_kw },
															   {"static",				token_type::static_kw },
															   {"import", 				token_type::import_kw},
															   {"extern", 				token_type::extern_kw},
															   {"vargs", 				token_type::vargs_kw},
															   {"create", 				token_type::create_kw},
															   {"new", 					token_type::new_kw},
															   {"delete", 				token_type::delete_kw},
															   {"strong", 				token_type::strong_kw},
															   {"weak", 				token_type::weak_kw},
															   {SELF_TYPE_KEYWORD, 		token_type::self_kw},
															   {CONSTRUCTOR_KEYWORD, 	token_type::constructor_kw},
															   {DESTRUCTOR_KEYWORD, 	token_type::destructor_kw}
															};

std::unordered_map<std::string, token_type> Token::literal_keywords	= {{"null", token_type::literal_null},
																		{"true", token_type::literal_bool},
																		{"false", token_type::literal_bool}};

void Token::print()
{
	std::cout << "General Token(type = " << (int)type << ")" << " \t-->  Position: " << file_pos << cendl;
}

void OperatorToken::print()
{
	std::cout << "Operator: \t" << op << " \t--> Position: " << file_pos << cendl;
}

void SeparatorToken::print()
{
	std::cout << "Separator: \t" << separator << " \t--> Position: " << file_pos << cendl;
}

void IdentifierToken::print()
{
	std::cout << "Identifier: \t" << name << " \t--> Position: " << file_pos << cendl;
}

void UnknownToken::print()
{
	std::cout << "Unknown: \t" << unknown_identifier << " \t--> Position: " << file_pos << cendl;
}

void KeywordToken::print()
{
	std::cout << "Keyword: \ttype = " << (int)type << " \t--> Position: " << file_pos << cendl;
}

PrecedenceInfo UnknownToken::getPrecedenceInfo()
{
	return {associativity_type::none, NULL_PRECEDENCE, NULL_PRECEDENCE, NULL_PRECEDENCE};
}

PrecedenceInfo IdentifierToken::getPrecedenceInfo()
{
	return { associativity_type::left, 165, 165, 165 };
}

PrecedenceInfo SeparatorToken::getPrecedenceInfo()
{
	if (this->separator == '(' || this->separator == '[')
		return { associativity_type::right, 10, 200, NULL_PRECEDENCE };

	if (this->separator == ',')
		return { associativity_type::left, 15, 15, 15 };

	return { associativity_type::left, MIN_PRECEDENCE, MIN_PRECEDENCE, MIN_PRECEDENCE };
}

PrecedenceInfo OperatorToken::getPrecedenceInfo()
{
	auto op_info = OperatorsInfo::inst().getOperator(this->op);
	
	if (op_info == nullptr)
		return { associativity_type::none, NULL_PRECEDENCE, NULL_PRECEDENCE, NULL_PRECEDENCE };

	return op_info->precedence;
}

PrecedenceInfo KeywordToken::getPrecedenceInfo()
{
	return {associativity_type::none, NULL_PRECEDENCE, NULL_PRECEDENCE, NULL_PRECEDENCE};
}