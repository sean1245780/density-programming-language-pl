#pragma once

#include "llvm/IR/Value.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Constants.h"

#include "GC.h"

#include "Constants.h"
#include "Operators.h"

#include <vector>
#include <unordered_map>
#include <string>
#include <sstream>
#include <unordered_set>
#include <functional>
#include <map>

using std::string;
using std::stringstream;
using std::unordered_map;
using std::vector;
using std::pair;
using std::unordered_set;
using std::function;
using std::map;

namespace AbstractSyntaxTree{ class TypeDefinition; }

/*
    This class represents the type of any component of the language, it 
    describes the underlying type of the written code - not the code itself.
*/
class SynthesizedType {
public:
    typedef function<llvm::Value*(llvm::Value*, llvm::Value*, SynthesizedType*, SynthesizedType*, llvm::IRBuilder<>&, GarbageCollector&)> op_codegen_function;
    typedef function<llvm::Value*(llvm::Value*, llvm::Type*, SynthesizedType*, SynthesizedType*, llvm::IRBuilder<>&, GarbageCollector&)> cast_codegen_function;

    static op_codegen_function def_equ;
    static op_codegen_function def_not_equ;
    static op_codegen_function def_assignment;
    static op_codegen_function def_safe_equ;
    static op_codegen_function def_safe_not_equ;
    static op_codegen_function def_safe_assignment;
    static op_codegen_function def_dereference;
    static op_codegen_function def_get_inner_value;
    static op_codegen_function def_address;

    static function<op_codegen_function(Operator*)> create_def_complex_and;

    static op_codegen_function def_complex_equ;
    static op_codegen_function def_complex_not_equ;
    static op_codegen_function def_complex_greater;
    static op_codegen_function def_complex_greater_equ;
    static op_codegen_function def_complex_smaller;
    static op_codegen_function def_complex_smaller_equ;
    static op_codegen_function wrap_pointer;

    static cast_codegen_function def_bitcast;
    static cast_codegen_function def_safe_ptr_to_ptr;
    static cast_codegen_function def_fit_size_int;
    static cast_codegen_function def_fit_size_uint;
    static cast_codegen_function def_fit_size_float;
    static cast_codegen_function def_elementwise_cast;
    static cast_codegen_function def_get_inner_pointer;
    
private:
    unordered_map<SynthesizedType*, unordered_map<string, op_codegen_function>> _defined_operators;
    unordered_map<SynthesizedType*, cast_codegen_function> _defined_static_casts;

protected:
    string _type_name;
    string _type_mangling_name;
    llvm::Type* _llvm_type = nullptr;

    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc) = 0;

    op_codegen_function* getCodegenFunction(const string& op, SynthesizedType* other);

public:
    enum class type_category { 
                                void_type, pointer_type, named_complex_type, unnamed_complex_type,
                                basic_type, function_type, namespace_type, module_type,
                                custom_type, strong_pointer_type, weak_pointer_type
                            };
    
    string getName() const noexcept { return _type_name; } // Get the type's name
    string getManglingName() const noexcept { return _type_mangling_name; } // Get the type's name
    virtual string getGCName() const noexcept { return _type_mangling_name; }
    virtual type_category getType() const noexcept  = 0;
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept = 0;
    void addOperator(SynthesizedType* type, const string& op, op_codegen_function func) noexcept;
    void addOperators(SynthesizedType* type, const unordered_map<string, op_codegen_function>& ops) noexcept;
    void setOperators(const unordered_map<SynthesizedType*, unordered_map<string, op_codegen_function>>& ops) noexcept { this->_defined_operators = ops; }
    const unordered_map<SynthesizedType*, unordered_map<string, op_codegen_function>>& getOperators() const noexcept { return this->_defined_operators; }
    void addStaticCast(SynthesizedType* type, cast_codegen_function func) noexcept;
    void addStaticCasts(const unordered_map<SynthesizedType*, cast_codegen_function>& casts) noexcept;
    void setStaticCasts(const unordered_map<SynthesizedType*, cast_codegen_function>& casts) noexcept 
        { this->_defined_static_casts = casts; };
    const unordered_map<SynthesizedType*, cast_codegen_function>& getStaticCasts() const noexcept { return this->_defined_static_casts; }

    virtual cast_codegen_function getStaticCast(SynthesizedType* type);

    virtual bool isDefined(const string& op, SynthesizedType* other = nullptr); // If nullptr -> A unary operator
    bool isCastCompatible(SynthesizedType* other) const noexcept;
    virtual bool isStaticCastCompatible(SynthesizedType* other) const noexcept;
    bool isBitPtrCastCompatible(SynthesizedType* other) const noexcept;
    llvm::Value* CreateCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept;
    virtual llvm::Value* CreateStaticCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept;
    llvm::Value* CreateBitPtrCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept;
    virtual llvm::Type* getLLVMType(llvm::LLVMContext& context, GarbageCollector& gc) { if(this->_llvm_type == nullptr) { this->initLLVMType(context, gc); } return this->_llvm_type; }; 
    virtual llvm::Type* getLLVMType() { return this->_llvm_type; };
    virtual void setLLVMType(llvm::Type* type) { this->_llvm_type = type; };

    virtual llvm::Value* codegen(Operator& op, SynthesizedType* rhs_type, llvm::Value* lhs, llvm::Value* rhs, llvm::IRBuilder<>& builder, GarbageCollector& gc) {
        auto* func = getCodegenFunction(op.op, rhs_type);

        if (func != nullptr)
            return (*func)(lhs, rhs, this, rhs_type, builder, gc);
        
        if(rhs_type == nullptr) return nullptr;

        llvm::Type* left_n_type = (op.bin_mods.left_input == operator_mod::reference ||
                        op.bin_mods.left_input == operator_mod::pointer) ?
                            lhs->getType()->getPointerElementType() :
                            lhs->getType();
    
        llvm::Value* right_val = nullptr;
        if((right_val = rhs_type->CreateCast(this, rhs, left_n_type, builder, gc)) != nullptr)
            if((func = getCodegenFunction(op.op, this)) != nullptr)
                return (*func)(lhs, right_val, this, this, builder, gc);
                
        return nullptr;
    }

    virtual bool modifiableByDefault() const noexcept { return false; }
};

class VoidType : public SynthesizedType
{
protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc){ this->_llvm_type = llvm::Type::getVoidTy(context); }

public:
    VoidType(llvm::Type* type) noexcept : SynthesizedType() { this->_llvm_type = type; this->_type_name = "void"; this->_type_mangling_name = "void"; }

    virtual type_category getType() const noexcept { return SynthesizedType::type_category::void_type; };
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept
    {
        if(other == nullptr) return false;
        if(other == this) return true;
        
        return other->getType() == this->getType();
    }

};

class PointerType : public SynthesizedType
{
private:
    SynthesizedType* _base_type = nullptr;

protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc);

public:
    PointerType(SynthesizedType* base_type) noexcept
    {
        this->setBaseType(base_type);
        this->addOperator(nullptr, "*", SynthesizedType::def_dereference);
        this->addOperator(nullptr, "#", SynthesizedType::wrap_pointer);
        this->addOperator(nullptr, "&", SynthesizedType::def_address);
        this->addOperator(this, "=", SynthesizedType::def_assignment);
        this->addOperator(this, "==", SynthesizedType::def_equ);
        this->addOperator(this, "!=", SynthesizedType::def_not_equ);
    }

    virtual cast_codegen_function getStaticCast(SynthesizedType* type);
    
    SynthesizedType* getBaseType() const noexcept { return this->_base_type; }
    void setBaseType(SynthesizedType* base_type) noexcept
    {
        this->_base_type = base_type;
        
        if(_base_type != nullptr)
        {
            this->_type_name = base_type->getName() + "*";
            this->_type_mangling_name = base_type->getName() + "$p";
        }
        else
        {
            this->_type_name = "[null base type]";
        }
    }

    virtual type_category getType() const noexcept { return SynthesizedType::type_category::pointer_type; };
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept
    {
        if(other == nullptr) return false;
        if(other == this) return true;

        if(other->getType() != SynthesizedType::type_category::pointer_type) return false;
        
        auto* other_ptr = static_cast<PointerType*>(other);
        if(this->_base_type == nullptr || other_ptr->_base_type == nullptr) return false;

        return this->_base_type->compare(other_ptr->_base_type, static_cast_chk, bit_cast_ptr);
    }
};

class StrongPointerType : public SynthesizedType
{
private:
    SynthesizedType* _base_type = nullptr;

protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc);

public:
    StrongPointerType(SynthesizedType* base_type) noexcept
    {
        this->setBaseType(base_type);
        this->addOperator(nullptr, "$", SynthesizedType::def_get_inner_value);
        this->addOperator(nullptr, "&", SynthesizedType::def_address);
        this->addOperator(this, "=", SynthesizedType::def_assignment);
        this->addOperator(this, "==", SynthesizedType::def_safe_equ);
        this->addOperator(this, "!=", SynthesizedType::def_safe_not_equ);
    }

    virtual cast_codegen_function getStaticCast(SynthesizedType* type);
    virtual bool isStaticCastCompatible(SynthesizedType* other) const noexcept;
    virtual llvm::Value* CreateStaticCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept;
    
    SynthesizedType* getBaseType() const noexcept { return this->_base_type; }
    void setBaseType(SynthesizedType* base_type) noexcept
    {
        this->_base_type = base_type;
        
        if(_base_type != nullptr)
        {
            this->_type_name = "strong " + base_type->getName() + "*";
            this->_type_mangling_name = base_type->getName() + "$sp";
        }
        else
        {
            this->_type_name = "[null base type]";
        }
    }

    virtual string getGCName() const noexcept
    {
        return string("$s$") + ((this->_base_type != nullptr) ? this->_base_type->getGCName() : ("[null base type]"));
    }

    virtual type_category getType() const noexcept { return SynthesizedType::type_category::strong_pointer_type; };
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept
    {
        if(other == nullptr) return false;
        if(other == this) return true;

        if(other->getType() != SynthesizedType::type_category::strong_pointer_type) return false;
        
        auto* other_ptr = static_cast<StrongPointerType*>(other);
        if(this->_base_type == nullptr || other_ptr->_base_type == nullptr) return false;

        return this->_base_type->compare(other_ptr->_base_type, static_cast_chk, bit_cast_ptr);
    }
};

class WeakPointerType : public SynthesizedType
{
private:
    SynthesizedType* _base_type = nullptr;

protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc);

public:
    WeakPointerType(SynthesizedType* base_type) noexcept
    {
        this->setBaseType(base_type);
        this->addOperator(nullptr, "$", SynthesizedType::def_get_inner_value);
        this->addOperator(nullptr, "&", SynthesizedType::def_address);
        this->addOperator(this, "=", SynthesizedType::def_assignment);
        this->addOperator(this, "==", SynthesizedType::def_safe_equ);
        this->addOperator(this, "!=", SynthesizedType::def_safe_not_equ);
    }

    virtual cast_codegen_function getStaticCast(SynthesizedType* type);
    virtual bool isStaticCastCompatible(SynthesizedType* other) const noexcept;
    virtual llvm::Value* CreateStaticCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept;
    
    SynthesizedType* getBaseType() const noexcept { return this->_base_type; }
    void setBaseType(SynthesizedType* base_type) noexcept
    {
        this->_base_type = base_type;
        
        if(_base_type != nullptr)
        {
            this->_type_name = "weak " + base_type->getName() + "*";
            this->_type_mangling_name = base_type->getName() + "$wp";
        }
        else
        {
            this->_type_name = "[null base type]";
        }
    }

    virtual string getGCName() const noexcept
    {
        return string("$s$") + ((this->_base_type != nullptr) ? this->_base_type->getGCName() : ("[null base type]"));
    }

    virtual type_category getType() const noexcept { return SynthesizedType::type_category::weak_pointer_type; };
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept
    {
        if(other == nullptr) return false;
        if(other == this) return true;

        if(other->getType() != SynthesizedType::type_category::weak_pointer_type) return false;
        
        auto* other_ptr = static_cast<WeakPointerType*>(other);
        if(this->_base_type == nullptr || other_ptr->_base_type == nullptr) return false;

        return this->_base_type->compare(other_ptr->_base_type, static_cast_chk, bit_cast_ptr);
    }
};

class NamedComplexType : public SynthesizedType {
private:
    vector<pair<SynthesizedType*, string>> _components;

protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc);

public:
    NamedComplexType() noexcept { 
        this->_type_name = "()"; 
        this->_type_mangling_name = "$NC()"; // Named Complex

        this->addOperator(nullptr, "&", SynthesizedType::def_address);
        this->addOperator(this, "=", SynthesizedType::def_assignment);
        this->addOperator(this, "==", SynthesizedType::def_complex_equ);
        this->addOperator(this, "!=", SynthesizedType::def_complex_not_equ);
        this->addOperator(this, "<=", SynthesizedType::def_complex_smaller_equ);
        this->addOperator(this, ">=", SynthesizedType::def_complex_greater_equ);
        this->addOperator(this, "<", SynthesizedType::def_complex_greater);
        this->addOperator(this, ">", SynthesizedType::def_complex_smaller);
    }

    void addComponent(const pair<SynthesizedType*, string>& type);
    const vector<pair<SynthesizedType*, string>>& getComponents() const noexcept { return this->_components; } 

    bool isDefined(const string& op, SynthesizedType* other = nullptr); // If nullptr -> A unary operator
    virtual cast_codegen_function getStaticCast(SynthesizedType* type);

    virtual bool isStaticCastCompatible(SynthesizedType* other) const noexcept;
    virtual llvm::Value* CreateStaticCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept;

    virtual type_category getType() const noexcept { return SynthesizedType::type_category::named_complex_type; };
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept;

    virtual bool modifiableByDefault() const noexcept { return true; }

    virtual llvm::Value* codegen(Operator& op, SynthesizedType* rhs_type, llvm::Value* lhs, llvm::Value* rhs, llvm::IRBuilder<>& builder, GarbageCollector& gc);
};

class UnnamedComplexType : public SynthesizedType {
private:
    vector<SynthesizedType*> _components;

protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc);

public:
    UnnamedComplexType() noexcept { 
        this->_type_name = "()"; 
        this->_type_mangling_name = "$UC()"; // Unnamed Complex

        this->addOperator(nullptr, "&", SynthesizedType::def_address);
        this->addOperator(this, "=", SynthesizedType::def_assignment);
        this->addOperator(this, "==", SynthesizedType::def_complex_equ);
        this->addOperator(this, "!=", SynthesizedType::def_complex_not_equ);
        this->addOperator(this, "<=", SynthesizedType::def_complex_smaller_equ);
        this->addOperator(this, ">=", SynthesizedType::def_complex_greater_equ);
        this->addOperator(this, "<", SynthesizedType::def_complex_greater);
        this->addOperator(this, ">", SynthesizedType::def_complex_smaller);
    }

    virtual bool isDefined(const string& op, SynthesizedType* other = nullptr); // If nullptr -> A unary operator
    virtual cast_codegen_function getStaticCast(SynthesizedType* type);
    virtual bool isStaticCastCompatible(SynthesizedType* other) const noexcept;
    virtual llvm::Value* CreateStaticCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept;

    void addComponent(SynthesizedType* type);
    const vector<SynthesizedType*>& getComponents() const noexcept { return this->_components; } 

    virtual type_category getType() const noexcept { return SynthesizedType::type_category::unnamed_complex_type; };
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept;

    virtual bool modifiableByDefault() const noexcept { return true; }

    virtual llvm::Value* codegen(Operator& op, SynthesizedType* rhs_type, llvm::Value* lhs, llvm::Value* rhs, llvm::IRBuilder<>& builder, GarbageCollector& gc);
};

class BasicType : public SynthesizedType {
protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc) {}
    bool _modifiable;

public:
    BasicType(const string& type_name, llvm::Type* type, bool modifiable = true) noexcept : _modifiable(modifiable)
    { 
        this->_type_name = type_name; this->_llvm_type = type;
        this->_type_mangling_name = type_name;
        this->addOperator(nullptr, "&", SynthesizedType::def_address); 
    }
    virtual type_category getType() const noexcept { return SynthesizedType::type_category::basic_type; }
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept;

    virtual bool modifiableByDefault() const noexcept { return this->_modifiable; }
};

class CustomType;

class FunctionType : public SynthesizedType {
private:
    SynthesizedType* input_type = nullptr;
    SynthesizedType* output_type = nullptr;
    bool _is_variadic = false;
    CustomType* parent_type = nullptr;

protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc);

public:
    FunctionType(SynthesizedType* input, SynthesizedType* output, bool is_varadic = false) noexcept;
    
    SynthesizedType* getInputType() const noexcept { return input_type; }
    SynthesizedType* getOutputType() const noexcept { return output_type; }
    CustomType* getParentType() const noexcept { return parent_type; }
    bool isVariadic() const noexcept { return this->_is_variadic; }

    void setVariadic(bool is_variadic) noexcept;
    void setParentType(CustomType* type) { this->parent_type = type; }

    bool compareArgs(SynthesizedType* other, bool with_static_c, bool with_bit_ptr_c, PointerType* called_object = nullptr) const noexcept;

    virtual type_category getType() const noexcept { return SynthesizedType::type_category::function_type; };
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept;
};

class CustomType : public SynthesizedType {
protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc);
    bool _modifiable;

private:
    vector<pair<string, SynthesizedType*>> _data_members;
    unordered_map<string, FunctionType*> _functions;
    AbstractSyntaxTree::TypeDefinition* definition = nullptr;

public:
    CustomType(const string& type_name, bool modifiable = true) noexcept : _modifiable(modifiable)
    { 
        this->_llvm_type = nullptr;
        this->_type_mangling_name = type_name;
        this->_type_name = type_name;
        
        this->addOperator(nullptr, "&", SynthesizedType::def_address); 
        this->addOperator(this, "=", SynthesizedType::def_assignment); 
    }

    void setDataMembers(const vector<pair<string, SynthesizedType*>>& members) { this->_data_members = members; }
    void setFunctions(const unordered_map<string, FunctionType*>& funcs) { this->_functions = funcs; }
    void setDefinition(AbstractSyntaxTree::TypeDefinition* def) { this->definition = def; }

    vector<pair<string, SynthesizedType*>>& getDataMembers() { return this->_data_members;}
    unordered_map<string, FunctionType*>& getFunctions() { return this->_functions; }
    AbstractSyntaxTree::TypeDefinition* getDefinition() { return this->definition; }

    virtual type_category getType() const noexcept { return SynthesizedType::type_category::custom_type; }
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept;

    virtual bool modifiableByDefault() const noexcept { return this->_modifiable; }
};

class NamespaceType : public SynthesizedType {
protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc) { this->_llvm_type = llvm::Type::getVoidTy(context); }

public:
    NamespaceType() noexcept { this->_type_name = "namespace"; }
    virtual type_category getType() const noexcept { return SynthesizedType::type_category::namespace_type; };
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept
    {
        if(other == nullptr) return false;
        return other->getType() == SynthesizedType::type_category::namespace_type;
    }
};

class ModuleType : public SynthesizedType {
protected:
    virtual void initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc) { this->_llvm_type = llvm::Type::getVoidTy(context); }

public:    
    virtual type_category getType() const noexcept { return SynthesizedType::type_category::module_type; };
    virtual bool compare(SynthesizedType* other, bool static_cast_chk = false, bool bit_cast_ptr = false) const noexcept { return this == other; }
};

static bool iterate_complex(SynthesizedType* calling, SynthesizedType* other,
                            std::function<bool(SynthesizedType*, SynthesizedType*, unsigned int index)> func, bool has_size_check = true)
{
    if(calling == nullptr || other == nullptr) return false;

    if (calling->getType() == SynthesizedType::type_category::named_complex_type &&
        other->getType() == SynthesizedType::type_category::named_complex_type)
    {
        const auto& comp_calling = static_cast<NamedComplexType*>(calling)->getComponents();
        const auto& comp_other = static_cast<NamedComplexType*>(other)->getComponents();

        if(has_size_check && comp_calling.size() != comp_other.size()) return false;

        for (unsigned int i = 0; i < comp_calling.size(); i++)
        {
            if (!func(comp_calling[i].first, comp_other[i].first, i)) return false;
        }

        return true;
    }
    else if (calling->getType() == SynthesizedType::type_category::named_complex_type &&
        other->getType() == SynthesizedType::type_category::unnamed_complex_type)
    {
        const auto& comp_calling = static_cast<NamedComplexType*>(calling)->getComponents();
        const auto& comp_other = static_cast<UnnamedComplexType*>(other)->getComponents();

        if(has_size_check && comp_calling.size() != comp_other.size()) return false;

        for (unsigned int i = 0; i < comp_calling.size(); i++)
        {
            if (!func(comp_calling[i].first, comp_other[i], i)) return false;
        }

        return true;
    }
    else if (calling->getType() == SynthesizedType::type_category::unnamed_complex_type &&
        other->getType() == SynthesizedType::type_category::named_complex_type)
    {
        const auto& comp_calling = static_cast<UnnamedComplexType*>(calling)->getComponents();
        const auto& comp_other = static_cast<NamedComplexType*>(other)->getComponents();

        if(has_size_check && comp_calling.size() != comp_other.size()) return false;

        for (unsigned int i = 0; i < comp_calling.size(); i++)
        {
            if (!func(comp_calling[i], comp_other[i].first, i)) return false;
        }

        return true;
    }
    else if (calling->getType() == SynthesizedType::type_category::unnamed_complex_type &&
        other->getType() == SynthesizedType::type_category::unnamed_complex_type)
    {
        const auto& comp_calling = static_cast<UnnamedComplexType*>(calling)->getComponents();
        const auto& comp_other = static_cast<UnnamedComplexType*>(other)->getComponents();

        if(has_size_check && comp_calling.size() != comp_other.size()) return false;

        for (unsigned int i = 0; i < comp_calling.size(); i++)
        {
            if (!func(comp_calling[i], comp_other[i], i)) return false;
        }

        return true;
    }

    return false;  
}

#include "../AbstractSyntaxTree/Nodes.h"