#pragma once

#define NULL_PRECEDENCE		        0
#define MIN_PRECEDENCE		        5

#define cendl                       '\n'
#define sendl                       "\n"
#define version_cpp                 __cplusplus
#define extension_1                 ".dny"
#define extension_2                 ".dens"
#define extension_h_1               ".dnh"
#define extension_h_2               ".denh"
#define density_version             "0.3.1V"
#define development_version         "Beta"

#define OBJ_FILE_EXT                ".o"
#define EXECUTABLE_FILE_EXT         ".out"
#define IR_FILE_EXT                 ".ll"
#define IR_BITCODE_FILE_EXT         ".bc"

#define RAW_DATA_MODULE_NAME        "<Raw Data>"

#define GLOBAL_VARIABLE_INITS       "$global_init_"
#define GLOBAL_FUNC_INIT_VARS       "global$vars$init"

#define SELF_TYPE_KEYWORD           "Self"
#define CONSTRUCTOR_KEYWORD         "init"
#define DESTRUCTOR_KEYWORD          "deinit"

typedef char sbyte; // Signed byte
typedef unsigned char ubyte; // Unsigned byte

enum class CompliationOptm { O0, O1, O2, O3, Os, Oz };
