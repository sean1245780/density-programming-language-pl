#include "Types.h"

SynthesizedType::op_codegen_function SynthesizedType::def_assignment = 
    [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        builder.CreateStore(rhs, lhs);
        return lhs;
    };

SynthesizedType::op_codegen_function SynthesizedType::def_safe_assignment = 
    [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if(rhs == nullptr || lhs == nullptr) return nullptr;
        builder.CreateStore(rhs, builder.CreateLoad(builder.CreateConstInBoundsGEP2_32(lhs->getType()->getPointerElementType(), lhs, 0, 2)));
        return lhs;
    };
SynthesizedType::op_codegen_function SynthesizedType::def_equ = 
    [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        return builder.CreateICmpEQ(lhs, rhs);
    };
SynthesizedType::op_codegen_function SynthesizedType::def_safe_equ = 
    [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if(rhs == nullptr || lhs == nullptr) return nullptr;
        return builder.CreateICmpEQ(builder.CreateLoad(builder.CreateConstInBoundsGEP2_32(rhs->getType()->getPointerElementType(), rhs, 0, 2)),
            builder.CreateLoad(builder.CreateConstInBoundsGEP2_32(lhs->getType()->getPointerElementType(), lhs, 0, 2)));
    };
SynthesizedType::op_codegen_function SynthesizedType::def_not_equ = 
    [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        return builder.CreateICmpNE(lhs, rhs);
    };
SynthesizedType::op_codegen_function SynthesizedType::def_safe_not_equ = 
    [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if(rhs == nullptr || lhs == nullptr) return nullptr;
        return builder.CreateICmpNE(builder.CreateLoad(builder.CreateConstInBoundsGEP2_32(rhs->getType()->getPointerElementType(), rhs, 0, 2)),
            builder.CreateLoad(builder.CreateConstInBoundsGEP2_32(lhs->getType()->getPointerElementType(), lhs, 0, 2)));
    };
SynthesizedType::op_codegen_function SynthesizedType::def_dereference = 
    [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if (rhs == nullptr) return nullptr;
        return rhs;
    };
SynthesizedType::op_codegen_function SynthesizedType::def_get_inner_value = 
    [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if (rhs == nullptr) return nullptr;
        return builder.CreateLoad(builder.CreateConstInBoundsGEP2_32(rhs->getType()->getPointerElementType(), rhs, 0, 2));
    };
SynthesizedType::op_codegen_function SynthesizedType::def_address = 
    [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if (rhs == nullptr) return nullptr;
        return builder.CreateConstInBoundsGEP1_32(rhs->getType()->getPointerElementType(), rhs, 0);
    };
SynthesizedType::op_codegen_function SynthesizedType::wrap_pointer = 
    [](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if (calling->getType() != SynthesizedType::type_category::pointer_type) return nullptr;

        auto* base_ty = static_cast<PointerType*>(calling)->getBaseType();
        auto* gc_ty = gc.getOrCreateSafePtrType(
            TypeSystemInfo::getGlobalContainer()->getOrCreateStrongPointerType(base_ty)->getGCName(), 
            base_ty->getLLVMType());

        // obtain wrapping function
        auto* wrap_func = gc.getOrCreateWrapFunction((string)(gc_ty->getName()));
        auto* wrapped_ptr = builder.CreateCall(
            llvm::FunctionCallee(wrap_func),
            { builder.CreateBitCast(rhs, builder.getInt8PtrTy()) }
        );

        return wrapped_ptr;
    };
function<SynthesizedType::op_codegen_function(Operator*)> SynthesizedType::create_def_complex_and = 
    [](Operator* op) {
        return [op](llvm::Value* lhs, llvm::Value* rhs, SynthesizedType* calling, SynthesizedType* type, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
            if(op == nullptr) return nullptr;

            llvm::Value* res = llvm::ConstantInt::get(
                builder.getContext(), 
                llvm::APInt(1, 1, true)
            );
            
            return iterate_complex(calling, type, [&, op](SynthesizedType* calling_inner, SynthesizedType* other_inner, unsigned int index){
                if (!calling_inner->isDefined(op->op, other_inner)) return false;
                
                auto curr_val = calling_inner->codegen(
                    *op,
                    other_inner,
                    builder.CreateExtractValue(lhs, index),
                    builder.CreateExtractValue(rhs, index),
                    builder,
                    gc
                );

                if (curr_val == nullptr) return false;
                
                res = builder.CreateAnd(res, curr_val);

                return res != nullptr;
            }) ? res : nullptr;
        };
    };

SynthesizedType::op_codegen_function SynthesizedType::def_complex_equ = create_def_complex_and(OperatorsInfo::inst().getOperator("=="));
SynthesizedType::op_codegen_function SynthesizedType::def_complex_not_equ = create_def_complex_and(OperatorsInfo::inst().getOperator("!="));
SynthesizedType::op_codegen_function SynthesizedType::def_complex_greater = create_def_complex_and(OperatorsInfo::inst().getOperator(">"));
SynthesizedType::op_codegen_function SynthesizedType::def_complex_greater_equ = create_def_complex_and(OperatorsInfo::inst().getOperator(">="));
SynthesizedType::op_codegen_function SynthesizedType::def_complex_smaller = create_def_complex_and(OperatorsInfo::inst().getOperator("<"));
SynthesizedType::op_codegen_function SynthesizedType::def_complex_smaller_equ = create_def_complex_and(OperatorsInfo::inst().getOperator("<="));

SynthesizedType::cast_codegen_function SynthesizedType::def_bitcast = 
    [](llvm::Value* to_cast, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        return builder.CreateBitCast(to_cast, target_type);
    };

SynthesizedType::cast_codegen_function SynthesizedType::def_safe_ptr_to_ptr = 
    [](llvm::Value* to_cast, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        return builder.CreateConstInBoundsGEP2_32(to_cast->getType()->getPointerElementType(), to_cast, 0, 2);
    };

SynthesizedType::cast_codegen_function SynthesizedType::def_fit_size_int = 
    [](llvm::Value* to_cast, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if (to_cast->getType()->getIntegerBitWidth() > target_type->getIntegerBitWidth())
            return builder.CreateTrunc(to_cast, target_type);
        else if (to_cast->getType()->getIntegerBitWidth() < target_type->getIntegerBitWidth())
            return builder.CreateSExtOrBitCast(to_cast, target_type);
        return to_cast;
    };
SynthesizedType::cast_codegen_function SynthesizedType::def_fit_size_uint = 
    [](llvm::Value* to_cast, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if (to_cast->getType()->getIntegerBitWidth() > target_type->getIntegerBitWidth())
            return builder.CreateTrunc(to_cast, target_type);
        else if (to_cast->getType()->getIntegerBitWidth() < target_type->getIntegerBitWidth())
            return builder.CreateZExtOrBitCast(to_cast, target_type);
        return to_cast;
    };

SynthesizedType::cast_codegen_function SynthesizedType::def_fit_size_float = 
    [](llvm::Value* to_cast, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if (to_cast->getType() == target_type) return to_cast;
        return builder.CreateFPCast(to_cast, target_type);
    };

SynthesizedType::cast_codegen_function SynthesizedType::def_get_inner_pointer = 
    [](llvm::Value* to_cast, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        if (to_cast == nullptr) return nullptr;
        if (to_cast->getType() == target_type) return to_cast;
        
        return builder.CreateConstInBoundsGEP2_32(to_cast->getType()->getPointerElementType(), to_cast, 0, 2);
    };

SynthesizedType::cast_codegen_function SynthesizedType::def_elementwise_cast = 
    [](llvm::Value* to_cast, llvm::Type* target_type, SynthesizedType* cast_st, SynthesizedType* type_st, llvm::IRBuilder<>& builder, GarbageCollector& gc) -> llvm::Value* {
        llvm::Value* last = llvm::UndefValue::get(target_type);
        return iterate_complex(cast_st, type_st, [&](SynthesizedType* calling_inner, SynthesizedType* other_inner, unsigned int index){
                llvm::Value* cast_op = nullptr;
                if(calling_inner->compare(other_inner))
                    return (last = builder.CreateInsertValue(last, builder.CreateExtractValue(to_cast, {index}), {index})) != nullptr;

                // Create implicit casts if can
                if ((cast_op = calling_inner->CreateCast(other_inner,
                    builder.CreateExtractValue(to_cast, {index}),
                    other_inner->getLLVMType(), builder, gc)) == nullptr)
                    return false;
                
                return ((last = builder.CreateInsertValue(last, cast_op, {index})) != nullptr);
            }) ? last : nullptr;
        };

void SynthesizedType::addOperator(SynthesizedType* type, const string& op, op_codegen_function func) noexcept
{
    auto val = this->_defined_operators.find(type);
    if(val == this->_defined_operators.end())
        this->_defined_operators.insert({type, {{op, func}}});
    else
        (*val).second.insert({op, func});
}

void SynthesizedType::addOperators(SynthesizedType* type, const unordered_map<string, op_codegen_function >& ops) noexcept
{
    auto type_obj = this->_defined_operators.find(type);

    if(type_obj == this->_defined_operators.end())
        this->_defined_operators.insert({type, ops});
    else
    {
        for(auto& op : ops)
        {
            (*type_obj).second.insert(op);
        }
    }
}

void SynthesizedType::addStaticCast(SynthesizedType* type, cast_codegen_function func) noexcept
{
    if (type == nullptr) return;

    this->_defined_static_casts.insert({type, func});
}

void SynthesizedType::addStaticCasts(const unordered_map<SynthesizedType*, cast_codegen_function>& casts) noexcept
{
    for (const auto& cast : casts)
        if (cast.first != nullptr)
            this->_defined_static_casts.insert(cast);
}

SynthesizedType::cast_codegen_function SynthesizedType::getStaticCast(SynthesizedType* type)
{
    auto found = this->_defined_static_casts.find(type);

    if (found == this->_defined_static_casts.end())
        return nullptr;

    return found->second;
}

SynthesizedType::cast_codegen_function PointerType::getStaticCast(SynthesizedType* type)
{
    if (type->getType() == SynthesizedType::type_category::pointer_type)
        return SynthesizedType::def_bitcast;

    return SynthesizedType::getStaticCast(type);
}

SynthesizedType::cast_codegen_function StrongPointerType::getStaticCast(SynthesizedType* type)
{
    if (type->getType() == SynthesizedType::type_category::strong_pointer_type ||
        type->getType() == SynthesizedType::type_category::weak_pointer_type)
        return SynthesizedType::def_bitcast;

    if(type->getType() == SynthesizedType::type_category::pointer_type)
        return SynthesizedType::def_safe_ptr_to_ptr;
    
    return SynthesizedType::getStaticCast(type);
}

SynthesizedType::cast_codegen_function WeakPointerType::getStaticCast(SynthesizedType* type)
{
    if (type->getType() == SynthesizedType::type_category::weak_pointer_type ||
        type->getType() == SynthesizedType::type_category::strong_pointer_type)
        return SynthesizedType::def_bitcast;

    if(type->getType() == SynthesizedType::type_category::pointer_type)
        return SynthesizedType::def_safe_ptr_to_ptr;

    return SynthesizedType::getStaticCast(type);
}

SynthesizedType::op_codegen_function* SynthesizedType::getCodegenFunction(const string& op, SynthesizedType* other)
{    
    // check if the function is defined for the other type
    const auto& op_mapping = this->_defined_operators.find(other);

    if (op_mapping != this->_defined_operators.end())
    {
        const auto& func = op_mapping->second.find(op);

        if (func != op_mapping->second.end())
            return &(func->second);
    }

    return nullptr;
}

bool SynthesizedType::isDefined(const string& op, SynthesizedType* other)
{
    if(this->getCodegenFunction(op, other) != nullptr) return true;
    if(other != nullptr && !other->isCastCompatible(this)) return false;

    return this->getCodegenFunction(op, this) != nullptr; // If can cast the other to this type
}

bool SynthesizedType::isCastCompatible(SynthesizedType* other) const noexcept
{
    if(other == nullptr) return false;
    if (this == other) return true;

    // Techincally, static cast virtually already checking it, but for
    // performance reasons, is being used again
    if((this->getType() == SynthesizedType::type_category::named_complex_type || 
            this->getType() == SynthesizedType::type_category::unnamed_complex_type) && 
        (other->getType() == SynthesizedType::type_category::named_complex_type || 
            other->getType() == SynthesizedType::type_category::unnamed_complex_type))
        return this->compare(other, true, true);

    return this->isStaticCastCompatible(other) || this->isBitPtrCastCompatible(other);
}

bool SynthesizedType::isStaticCastCompatible(SynthesizedType* other) const noexcept
{
    if (other == nullptr) return false;
    if (this->compare(other)) return true;

    // Static cast check
    return this->_defined_static_casts.find(other) != this->_defined_static_casts.end();
}

bool SynthesizedType::isBitPtrCastCompatible(SynthesizedType* other) const noexcept
{
    if (other == nullptr) return false;
    if (this->compare(other)) return true;

    if(this->getType() == SynthesizedType::type_category::pointer_type &&
        this->getType() == other->getType()) return true;

    return (this->getType() == SynthesizedType::type_category::strong_pointer_type ||
        this->getType() == SynthesizedType::type_category::weak_pointer_type) &&
        (other->getType() == SynthesizedType::type_category::strong_pointer_type ||
        other->getType() == SynthesizedType::type_category::weak_pointer_type);
}

llvm::Value* SynthesizedType::CreateCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept
{
    if(other == nullptr || val == nullptr || cast_type == nullptr) return nullptr;

    auto* st_cast = CreateStaticCast(other, val, cast_type, builder, gc);
    if(st_cast != nullptr) return st_cast;

    // If no static cast exists, check other bitcasts    
    return CreateBitPtrCast(other, val, cast_type, builder, gc);
}

llvm::Value* WeakPointerType::CreateStaticCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept
{
    if(other == nullptr || val == nullptr || cast_type == nullptr) return nullptr;

    if (other->getType() == SynthesizedType::type_category::strong_pointer_type)
        return def_bitcast(val, cast_type, this, other, builder, gc);

    if (other->getType() == SynthesizedType::type_category::pointer_type)
        return def_get_inner_pointer(val, cast_type, this, other, builder, gc);
    
    return SynthesizedType::CreateStaticCast(other, val, cast_type, builder, gc);
}

llvm::Value* StrongPointerType::CreateStaticCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept
{
    if(other == nullptr || val == nullptr || cast_type == nullptr) return nullptr;

    if (other->getType() == SynthesizedType::type_category::weak_pointer_type)
        return def_bitcast(val, cast_type, this, other, builder, gc);

    if (other->getType() == SynthesizedType::type_category::pointer_type)
        return def_get_inner_pointer(val, cast_type, this, other, builder, gc);
    
    return SynthesizedType::CreateStaticCast(other, val, cast_type, builder, gc);
}

llvm::Value* SynthesizedType::CreateStaticCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept
{
    if(other == nullptr || val == nullptr || cast_type == nullptr) return nullptr;
    if (this->compare(other)) return val;

    auto found = this->_defined_static_casts.find(other);

    if (found == this->_defined_static_casts.end())
        return nullptr;

    return found->second(val, cast_type, this, other, builder, gc);
}

llvm::Value* SynthesizedType::CreateBitPtrCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept
{
    if(!this->isBitPtrCastCompatible(other)) return nullptr;
    if(other == this) return val;
    if(other->compare(this)) return val;

    return SynthesizedType::def_bitcast(val, cast_type, this, other, builder, gc);
}

bool StrongPointerType::isStaticCastCompatible(SynthesizedType* other) const noexcept
{  
    if (other->getType() == SynthesizedType::type_category::weak_pointer_type)
        return this->_base_type->compare(static_cast<WeakPointerType*>(other)->getBaseType(), true, false);
 
    if (other->getType() == SynthesizedType::type_category::pointer_type)
        return this->_base_type->compare(static_cast<PointerType*>(other)->getBaseType(), true, false);
 
    return SynthesizedType::isStaticCastCompatible(other);
}

bool WeakPointerType::isStaticCastCompatible(SynthesizedType* other) const noexcept
{  
    if (other->getType() == SynthesizedType::type_category::strong_pointer_type)
        return this->_base_type->compare(static_cast<StrongPointerType*>(other)->getBaseType(), true, false);
 
    if (other->getType() == SynthesizedType::type_category::pointer_type)
        return this->_base_type->compare(static_cast<PointerType*>(other)->getBaseType(), true, false);

    return SynthesizedType::isStaticCastCompatible(other);
}

void NamedComplexType::addComponent(const pair<SynthesizedType*, string>& type)
{
    this->_components.push_back(type);
    this->_type_name.pop_back();
    if(this->_type_name.size() > 1) this->_type_name += ", "; 
    this->_type_name += (type.first->getName() + " " + type.second + ")");
    
    this->_type_mangling_name.pop_back(); // Remove ')'
    if(this->_components.size() == 1) this->_type_mangling_name += type.first->getManglingName();
    else this->_type_mangling_name += ("." + type.first->getManglingName());
    this->_type_mangling_name += ")";
}

bool NamedComplexType::compare(SynthesizedType* other, bool static_cast_chk, bool bit_cast_ptr) const noexcept
{
    if(other == nullptr) return false;
    if(other == this) return true;

    if(other->getType() == SynthesizedType::type_category::named_complex_type)
    {
        // convert to named complex type
        auto* other_val = static_cast<NamedComplexType*>(other);
        if(other_val == nullptr) return false;

        if(this->_components.size() != other_val->_components.size()) return false;

        // compare the subtypes

        auto val1 = this->_components.begin();
        auto end_val1 = this->_components.end();
        auto val2 = other_val->_components.begin();
        auto end_val2 = other_val->_components.end();

        for(; val1 != end_val1 && val2 != end_val2; val1++, val2++)
        {
            if(!(val1->first->compare(val2->first, static_cast_chk, bit_cast_ptr))) return false;
        }

        return true;
    }
    else if(other->getType() == SynthesizedType::type_category::unnamed_complex_type)
    {
        auto* other_val = static_cast<UnnamedComplexType*>(other);
        if(other_val == nullptr) return false;

        if(this->_components.size() != other_val->getComponents().size()) return false;

        // compare the subtypes
        auto val1 = this->_components.begin();
        auto end_val1 = this->_components.end();
        auto val2 = other_val->getComponents().begin();
        auto end_val2 = other_val->getComponents().end();

        for(; val1 != end_val1 && val2 != end_val2; val1++, val2++)
        {
            if(!((val1->first)->compare(*val2, static_cast_chk, bit_cast_ptr))) return false;
        }

        return true;
    }

    return false;
}

bool NamedComplexType::isDefined(const string& op, SynthesizedType* other)
{
    if(this->getCodegenFunction(op, other) != nullptr) return true;

    // Create check for (1, 2) * 2 => (2, 4)
    bool is_complex_op = true;
    for (unsigned int i = 0; i < this->_components.size() && is_complex_op; i++)
        if(!this->_components[i].first->isDefined(op, other)) is_complex_op = false;
    
    if(is_complex_op) return true;
    
    // Cast & then codegen func check
    if(!other->isCastCompatible(this)) return false;

    return this->getCodegenFunction(op, this); // If can cast the other to this type
}

SynthesizedType::cast_codegen_function NamedComplexType::getStaticCast(SynthesizedType* type)
{
    if(type == nullptr) return nullptr;
    return (this->isStaticCastCompatible(type)) ? def_elementwise_cast : nullptr;
}

bool NamedComplexType::isStaticCastCompatible(SynthesizedType* other) const noexcept
{
    if(other == nullptr) return false;

    // Is either one of them is complex type, then check inner type static compatability
    if(other->getType() == SynthesizedType::type_category::named_complex_type || 
        other->getType() == SynthesizedType::type_category::unnamed_complex_type)
            return other->compare(other, true, false);
        
    return false;
}

llvm::Value* NamedComplexType::CreateStaticCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept
{
    if (other == nullptr || val == nullptr || cast_type == nullptr) return nullptr;
    if (this->compare(other)) return val;

    return (this->isStaticCastCompatible(other)) ?
        SynthesizedType::def_elementwise_cast(val, cast_type, this, other, builder, gc) : nullptr;
}

void UnnamedComplexType::addComponent(SynthesizedType* type)
{
    this->_components.push_back(type);
    this->_type_name.pop_back();
    if(this->_type_name.size() > 1) this->_type_name += ", "; 
    this->_type_name += (type->getName() + ")");

    this->_type_mangling_name.pop_back(); // Remove ')'
    if(this->_components.size() == 1) this->_type_mangling_name += type->getManglingName();
    else this->_type_mangling_name += ("." + type->getManglingName());
    this->_type_mangling_name += ")";
}

bool UnnamedComplexType::compare(SynthesizedType* other, bool static_cast_chk, bool bit_cast_ptr) const noexcept
{
    if(other == nullptr) return false;
    if(other == this) return true;
    
    if(other->getType() == SynthesizedType::type_category::unnamed_complex_type)
    {
        // convert to named complex type
        auto* other_val = static_cast<UnnamedComplexType*>(other);
        if(other_val == nullptr) return false;

        if(this->_components.size() != other_val->_components.size()) return false;

        // compare the subtypes
        auto val1 = this->_components.begin();
        auto end_val1 = this->_components.end();
        auto val2 = other_val->_components.begin();
        auto end_val2 = other_val->_components.end();

        for(; val1 != end_val1 && val2 != end_val2; val1++, val2++)
        {
            if(!((*val1)->compare(*val2, static_cast_chk, bit_cast_ptr))) return false;
        }

        return true;
    }
    else if(other->getType() == SynthesizedType::type_category::named_complex_type)
    {
        // convert to named complex type
        auto* other_val = static_cast<NamedComplexType*>(other);
        if(other_val == nullptr) return false;

        // compare the subtypes

        if(this->_components.size() != other_val->getComponents().size()) return false;

        auto val1 = this->_components.begin();
        auto end_val1 = this->_components.end();
        auto val2 = other_val->getComponents().begin();
        auto end_val2 = other_val->getComponents().end();

        for(; val1 != end_val1 && val2 != end_val2; val1++, val2++)
        {
            if(!((*val1)->compare(val2->first, static_cast_chk, bit_cast_ptr))) return false;
        }

        return true;
    }

    return false;
}

bool UnnamedComplexType::isDefined(const string& op, SynthesizedType* other)
{
    if(this->getCodegenFunction(op, other) != nullptr) return true;

    // Create check for (1, 2) * 2 => (2, 4)
    bool is_complex_op = true;
    for (unsigned int i = 0; i < this->_components.size() && is_complex_op; i++)
        if(!this->_components[i]->isDefined(op, other)) is_complex_op = false;
    
    if(is_complex_op) return true;
    
    // Cast & then codegen func check
    if(!other->isCastCompatible(this)) return false;

    return this->getCodegenFunction(op, this); // If can cast the other to this type
}

SynthesizedType::cast_codegen_function UnnamedComplexType::getStaticCast(SynthesizedType* type)
{
    if(type == nullptr) return nullptr;
    return (this->isStaticCastCompatible(type)) ? def_elementwise_cast : nullptr;
}

bool UnnamedComplexType::isStaticCastCompatible(SynthesizedType* other) const noexcept
{
    if(other == nullptr) return false;

    // Is either one of them is complex type, then check inner type static compatability
    if(other->getType() == SynthesizedType::type_category::named_complex_type || 
        other->getType() == SynthesizedType::type_category::unnamed_complex_type)
            return other->compare(other, true, false);
        
    return false;
}

llvm::Value* UnnamedComplexType::CreateStaticCast(SynthesizedType* other, llvm::Value* val, llvm::Type* cast_type, llvm::IRBuilder<>& builder, GarbageCollector& gc) noexcept
{
    if (other == nullptr || val == nullptr || cast_type == nullptr) return nullptr;
    if (this->compare(other)) return val;

    return (this->isStaticCastCompatible(other)) ?
        SynthesizedType::def_elementwise_cast(val, cast_type, this, other, builder, gc) : nullptr;
}

bool BasicType::compare(SynthesizedType* other, bool static_cast_chk, bool bit_cast_ptr) const noexcept
{
    if(other == nullptr) return false;
    if(other == this) return true;
    if(static_cast_chk && bit_cast_ptr) return this->isCastCompatible(other);
    if(static_cast_chk) return this->isStaticCastCompatible(other);
    if(bit_cast_ptr) return this->isBitPtrCastCompatible(other);

    return false;
}

FunctionType::FunctionType(SynthesizedType* input, SynthesizedType* output, bool is_variadic) noexcept :
    input_type(input), output_type(output), parent_type(nullptr), _is_variadic(is_variadic)
{
    this->_type_name = "(";
    this->_type_name += ((input_type == nullptr) ? ("invalid") : (input_type->getName()));
    this->_type_name += " + (";
    if(this->_is_variadic) this->_type_name += "vargs";
    this->_type_name += ") -> ";
    this->_type_name += ((output_type == nullptr) ? ("invalid") : (output_type->getName()));
    this->_type_name += ")";

    if(input != nullptr && input->getType() == SynthesizedType::type_category::named_complex_type)
    {
        auto* name_comp = static_cast<NamedComplexType*>(input);
        
        for(const auto& inner_type : name_comp->getComponents())
            this->_type_mangling_name += inner_type.first->getManglingName() + ".";

        if(!this->_type_mangling_name.empty() && this->_type_mangling_name[this->_type_mangling_name.size() - 1] == '.')
            this->_type_mangling_name.pop_back(); // remove redundant '.'
    }
    else
        this->_type_mangling_name += ((input_type == nullptr) ? ("invalid") : (input_type->getManglingName()));

    if(this->_is_variadic) this->_type_name += ".$varg";

    this->addOperator(nullptr, "&", SynthesizedType::def_address);
}

bool CustomType::compare(SynthesizedType* other, bool static_cast_chk, bool bit_cast_ptr) const noexcept
{
    return other == this;
}

void CustomType::initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc)
{
    vector<llvm::Type*> types;
    
    // set the llvm type
    auto* struct_llvm_type = llvm::StructType::create(
        { llvm::Type::getInt8Ty(context) },
        _type_name
    );

    this->_llvm_type = struct_llvm_type;

    // add all of the non function members of the type to the struct type
    for (auto& component : this->_data_members)
        types.push_back(component.second->getLLVMType(context, gc));

    if (!types.empty())
        struct_llvm_type->setBody(types);        
}

bool FunctionType::compare(SynthesizedType* other, bool static_cast_chk, bool bit_cast_ptr) const noexcept
{
    if(other == nullptr) return false;
    if(other == this) return true;

    if(other->getType() == SynthesizedType::type_category::function_type)
    {
        // convert to named complex type
        auto* other_val = static_cast<FunctionType*>(other);
        if(other_val == nullptr) return false;
        if(other_val->parent_type != this->parent_type) return false;

        return (!(this->_is_variadic ^ other_val->_is_variadic) && 
                    this->input_type->compare(other_val->input_type, static_cast_chk, bit_cast_ptr) &&
                    this->output_type->compare(other_val->output_type, static_cast_chk, bit_cast_ptr)
                 );
    }

    return false;
}

void FunctionType::initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc)
{
    if (this->input_type == nullptr || this->output_type == nullptr) return;
    if (this->input_type->getLLVMType(context, gc) == nullptr || 
        this->output_type->getLLVMType(context, gc) == nullptr) return;

    auto* input_llvm_type = llvm::dyn_cast_or_null<llvm::StructType>(this->input_type->getLLVMType());
    auto* output_llvm_type = this->output_type->getLLVMType();
    vector<llvm::Type*> args;

    // add the arguments
    if (input_llvm_type != nullptr)
        for (auto* ty : input_llvm_type->elements())
            args.push_back(ty);
    else if (input_type->getLLVMType() != nullptr && !input_type->getLLVMType()->isVoidTy())
        args.push_back(this->input_type->getLLVMType());

    if (!args.empty())
        this->_llvm_type = llvm::FunctionType::get(
            output_llvm_type,
            args,
            this->_is_variadic
        );
    else
        this->_llvm_type = llvm::FunctionType::get(
            output_llvm_type,
            this->_is_variadic
        );
}

void FunctionType::setVariadic(bool is_variadic) noexcept
{
    this->_is_variadic = is_variadic;

    this->_type_name = "(";
    this->_type_name += ((input_type == nullptr) ? ("invalid") : (input_type->getName()));
    this->_type_name += " + (";
    if(this->_is_variadic) this->_type_name += "varg";
    this->_type_name += ") -> ";
    this->_type_name += ((output_type == nullptr) ? ("invalid") : (output_type->getName()));
    this->_type_name += ")";
}

bool FunctionType::compareArgs(SynthesizedType* other, bool static_cast_chk, bool bit_cast_ptr, PointerType* called_object) const noexcept
{
    if (other == nullptr || input_type == nullptr) return false;

    std::vector<SynthesizedType*> other_types;
    std::vector<SynthesizedType*> this_types;

    if (called_object != nullptr)
        other_types.push_back(called_object);

    if (other->getType() == SynthesizedType::type_category::named_complex_type)
        for (auto type : static_cast<NamedComplexType*>(other)->getComponents())
            other_types.push_back(type.first);
    else if (other->getType() == SynthesizedType::type_category::unnamed_complex_type)
        for (auto* type : static_cast<UnnamedComplexType*>(other)->getComponents())
            other_types.push_back(type);

    if (this->getInputType()->getType() == SynthesizedType::type_category::named_complex_type)
        for (auto type : static_cast<NamedComplexType*>(this->getInputType())->getComponents())
            this_types.push_back(type.first);
    else if (this->getInputType()->getType() == SynthesizedType::type_category::unnamed_complex_type)
        for (auto* type : static_cast<UnnamedComplexType*>(this->getInputType())->getComponents())
            this_types.push_back(type);

    // if not enough arguments were provided, return false
    if (other_types.size() < this_types.size())
        return false;

    unsigned int i = 0;
    for (; i < this_types.size(); i++)
        if (!this_types[i]->compare(other_types[i], static_cast_chk, bit_cast_ptr))
            return false;

    // return true if all types matched exactly or if the function is variadic
    return (this_types.size() == other_types.size()) || this->_is_variadic;
}

void UnnamedComplexType::initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc)
{
    vector<llvm::Type*> elements;

    for (auto* component : this->_components)
        if(component != nullptr)
            elements.push_back(component->getLLVMType(context, gc));

    this->_llvm_type = llvm::StructType::get(context, elements);
}

llvm::Value* UnnamedComplexType::codegen(Operator& op, SynthesizedType* rhs_type, llvm::Value* lhs, llvm::Value* rhs, llvm::IRBuilder<>& builder, GarbageCollector& gc)
{
    if (SynthesizedType::isDefined(op.op, rhs_type) || SynthesizedType::isDefined(op.op, this)) 
        return SynthesizedType::codegen(op, rhs_type, lhs, rhs, builder, gc);

    if (rhs_type != nullptr && rhs_type->getType() == SynthesizedType::type_category::named_complex_type)
    {
        if (!this->compare(rhs_type, true, true)) return nullptr;

        llvm::UndefValue* res = llvm::UndefValue::get(this->getLLVMType());
        auto* named_complex = static_cast<NamedComplexType*>(rhs_type);
        for (unsigned int i = 0; i < this->_components.size(); i++)
        {
            // apply the codegeneration function to the inner values
            auto* curr_val = this->_components[i]->codegen(
                op,
                named_complex->getComponents()[i].first,
                builder.CreateExtractValue(lhs, i),
                builder.CreateExtractValue(rhs, i),
                builder, 
                gc
            );
            
            if (curr_val == nullptr) return nullptr;

            // add the calculated value
            builder.CreateInsertValue(res, curr_val, i);
        }

        return res;
    }
    else if(rhs_type != nullptr && rhs_type->getType() == SynthesizedType::type_category::unnamed_complex_type)
    {
        if (!this->compare(rhs_type, true, true)) return nullptr;

        llvm::UndefValue* res = llvm::UndefValue::get(this->getLLVMType());
        auto* unnamed_complex = static_cast<UnnamedComplexType*>(rhs_type);
        for (unsigned int i = 0; i < this->_components.size(); i++)
        {
            // apply the codegeneration function to the inner values
            auto* curr_val = this->_components[i]->codegen(
                op,
                unnamed_complex->_components[i],
                builder.CreateExtractValue(lhs, i),
                builder.CreateExtractValue(rhs, i),
                builder, 
                gc
            );
            
            if (curr_val == nullptr) return nullptr;

            // add the calculated value
            builder.CreateInsertValue(res, curr_val, i);
        }

        return res;
    }
    else
    {
        llvm::UndefValue* res = llvm::UndefValue::get(this->getLLVMType());

        // apply the codegen function for each element
        for (unsigned int i = 0; i < this->_components.size(); i++)
        {
            auto* curr_res = this->_components[i]->codegen(
                op,
                rhs_type,
                builder.CreateExtractValue(lhs, i),
                rhs,
                builder, 
                gc
            );

            if (curr_res == nullptr) return nullptr;

            builder.CreateInsertValue(res, curr_res, i);
        }

        return res;
    }

    return nullptr;
}

void NamedComplexType::initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc)
{
    vector<llvm::Type*> elements;

    for (auto& component : this->_components)
        if(component.first != nullptr)
            elements.push_back(component.first->getLLVMType(context, gc));

    this->_llvm_type = llvm::StructType::get(context, elements);
}

llvm::Value* NamedComplexType::codegen(Operator& op, SynthesizedType* rhs_type, llvm::Value* lhs, llvm::Value* rhs, llvm::IRBuilder<>& builder, GarbageCollector& gc)
{
    // Checking if the operator exists, if not, cast and check again
    if (SynthesizedType::isDefined(op.op, rhs_type) || SynthesizedType::isDefined(op.op, this)) 
        return SynthesizedType::codegen(op, rhs_type, lhs, rhs, builder, gc);

    if (rhs_type != nullptr && rhs_type->getType() == SynthesizedType::type_category::named_complex_type)
    {
        if (!this->compare(rhs_type, true, true)) return nullptr;

        llvm::UndefValue* res = llvm::UndefValue::get(this->getLLVMType());
        auto* named_complex = static_cast<NamedComplexType*>(rhs_type);

        for (unsigned int i = 0; i < this->_components.size(); i++)
        {
            // apply the codegeneration function to the inner values
            auto* curr_val = this->_components[i].first->codegen(
                op,
                named_complex->_components[i].first,
                builder.CreateExtractValue(lhs, i), builder.CreateExtractValue(rhs, i),
                builder, 
                gc
            );
            
            if (curr_val == nullptr) return nullptr;

            // add the calculated value
            builder.CreateInsertValue(res, curr_val, i);
        }

        return res;
    }
    else if(rhs_type != nullptr && rhs_type->getType() == SynthesizedType::type_category::unnamed_complex_type)
    {
        if (!this->compare(rhs_type, true, true)) return nullptr;

        llvm::UndefValue* res = llvm::UndefValue::get(this->getLLVMType());
        auto* unnamed_complex = static_cast<UnnamedComplexType*>(rhs_type);
        for (unsigned int i = 0; i < this->_components.size(); i++)
        {
            // apply the codegeneration function to the inner values
            auto* curr_val = this->_components[i].first->codegen(
                op,
                unnamed_complex->getComponents()[i],
                builder.CreateExtractValue(lhs, i), builder.CreateExtractValue(rhs, i),
                builder, 
                gc
            );
            
            if (curr_val == nullptr) return nullptr;

            // add the calculated value
            builder.CreateInsertValue(res, curr_val, i);
        }

        return res;
    }
    else
    {
        llvm::UndefValue* res = llvm::UndefValue::get(this->getLLVMType());

        // apply the codegen function for each element
        for (unsigned int i = 0; i < this->_components.size(); i++)
        {
            auto* curr_res = this->_components[i].first->codegen(
                op,
                rhs_type,
                builder.CreateExtractValue(lhs, i),
                rhs,
                builder, 
                gc
            );

            if (curr_res == nullptr) return nullptr;

            builder.CreateInsertValue(res, curr_res, i);
        }

        return res;
    }

    return nullptr;
}


void PointerType::initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc)
{
    if(this->_base_type == nullptr) return;

    if(this->_base_type->getLLVMType(context, gc) != nullptr)
    {
        this->_llvm_type = this->_base_type->getLLVMType()->getPointerTo();
    }
}

void StrongPointerType::initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc)
{
    if(this->_base_type == nullptr) return;

    this->_llvm_type = gc.getOrCreateSafePtrType(getGCName(), this->_base_type->getLLVMType(context, gc))->getPointerTo();
}

void WeakPointerType::initLLVMType(llvm::LLVMContext& context, GarbageCollector& gc)
{
    if(this->_base_type == nullptr) return;

    this->_llvm_type = gc.getOrCreateSafePtrType(getGCName(), this->_base_type->getLLVMType(context, gc))->getPointerTo();
}
