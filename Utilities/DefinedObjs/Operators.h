#pragma once

#include "Constants.h"
#include <string>
#include <unordered_map>

#define BINARY_OP_MOD(left_in, right_in, out)                           {left_in, right_in, out}, {}
#define UNARY_OP_MOD(prefix_in, prefix_out, postfix_in, postfix_out)    {}, {prefix_in, prefix_out, postfix_in, postfix_out}

#define BINARY_DEF_MOD(left_in, out)                                    {left_in, operator_mod::value, out}, {}

#define OPERATOR_ALL_VALUE                                              {}, {}

using std::string;
using std::unordered_map;

enum class associativity_type { left, right, none };
enum class precedence_type { prefix = 1 << 0, binary = 1 << 1, postfix = 1 << 2 };

typedef char operator_mod_byte;

namespace operator_mod
{
    enum operator_modifier { pointer = 0, reference, value};
}

typedef struct PrecedenceInfo {
	associativity_type associativity;
	unsigned int prefix_precedence;
	unsigned int binary_precedence;
	unsigned int postfix_precedence;

	unsigned int leftPrecedence(unsigned int type);
	unsigned int rightPrecedence(unsigned int type);
} PrecedenceInfo;

typedef struct Operator
{
    string op = "";
    struct {
        operator_mod_byte left_input = operator_mod::value;
        operator_mod_byte right_input = operator_mod::value;
        operator_mod_byte output = operator_mod::value;
    } bin_mods;
    struct {
        operator_mod_byte prefix_input = operator_mod::value;
        operator_mod_byte prefix_output = operator_mod::value;
        operator_mod_byte postfix_input = operator_mod::value;
        operator_mod_byte postfix_output = operator_mod::value;
    } unary_mods;
    PrecedenceInfo precedence = {};
} Operator;

class OperatorsInfo
{
private:
    unordered_map<string, Operator*> _operators;

    OperatorsInfo();
    ~OperatorsInfo();

public:
    // get the singleton instance
    static OperatorsInfo& inst() { static OperatorsInfo info; return info; }

    Operator* getOperator(const string& op);

    unsigned int getLongestOperatorSize() const;
};

enum class kwOperators { vargs_op };

static std::unordered_map<kwOperators, string> keywords_operators =
{
    {kwOperators::vargs_op, "vargs"}
};