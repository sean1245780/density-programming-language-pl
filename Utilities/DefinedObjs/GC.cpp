#include "GC.h"

GarbageCollector::GarbageCollector(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::TargetMachine* target, llvm::Module* curr_module)
    : _builder(builder), _context(context), _target(target), _curr_module(curr_module)
{
    // Target machine can't be nullptr
    //if(this->_target == nullptr)
       // throw std::runtime_error("Invalid target machine data was given");
}

llvm::StructType* GarbageCollector::getOrCreateSafePtrType(const string& name, llvm::Type* inner_type)
{
    if(name.empty() || inner_type == nullptr || this->_curr_module == nullptr) return nullptr;
    
    // Looking up if it already exists, then take it
    const auto& existing_type = this->_safe_pointers.find(name);
    if(existing_type != this->_safe_pointers.end())
        return existing_type->second.safe_type;
    

    // getOrCreate inner types of the given safe type
    // {i64, i64, inner_type*}
    llvm::ArrayRef<llvm::Type *>  inner_pointer_types
        {this->_builder.getInt64Ty(), this->_builder.getInt64Ty(), inner_type->getPointerTo()};
    
    auto* new_type = llvm::StructType::create(_context, inner_pointer_types, name);
    this->_safe_pointers.insert({name, {inner_type, new_type, this->_curr_module}});

    return new_type;
}

llvm::Function* GarbageCollector::getOrCreateSafeCreateFunction(const string& name)
{
    if (name.empty() || this->_curr_module == nullptr || this->_target == nullptr) return nullptr;

    // Looking up if it exists
    const auto& existing_type = this->_safe_pointers.find(name);
    if(existing_type == this->_safe_pointers.end()) return nullptr;
    
    llvm::Function* function = nullptr;
    SafePtrData new_safe_ptr_data = existing_type->second;

    // Return the function if already exists
    if(existing_type->second.create_ptr_func != nullptr)
    {
        function = llvm::cast_or_null<llvm::Function>(
            this->_curr_module->getOrInsertFunction(
                name + "&create", existing_type->second.create_ptr_func->getFunctionType()
            ).getCallee());
        
        if(function != nullptr)
            function->setLinkage(llvm::GlobalValue::LinkageTypes::ExternalLinkage);
            
    }
    else
    {
        auto* last_pos = this->_builder.GetInsertBlock();

        // Create the function
        auto* func_type = llvm::FunctionType::get(
            existing_type->second.safe_type->getPointerTo(),
            llvm::ArrayRef<llvm::Type*> {},
            false
        );
        
        function = llvm::Function::Create(
            func_type, llvm::GlobalValue::LinkageTypes::ExternalLinkage,
            name + "&create", *(this->_curr_module)
        );

        // create the entry point and get the allocation size
        auto* entry_block = llvm::BasicBlock::Create(this->_context, "entry", function);
        this->_builder.SetInsertPoint(entry_block);
        
        // A malloc size updated by the malloc function and used later by memset
        llvm::Constant* malloc_size = nullptr;

        // Create the malloc call function
        auto malloc_func = [&](llvm::Type* mallocated_ptr) -> llvm::Instruction* {
            auto size_val = _target->createDataLayout().getTypeAllocSize(existing_type->second.safe_type);
            auto* int_type = llvm::Type::getInt32Ty(this->_context); // May be 64
            malloc_size = llvm::ConstantExpr::getTruncOrBitCast(this->_builder.getInt64(size_val.getFixedSize()), int_type);
            
            return llvm::CallInst::CreateMalloc(entry_block,
                    int_type, mallocated_ptr, malloc_size,
                    nullptr, nullptr, "");
        };

        // Create the safe ptr type && Set the all counter and strong counter to 1 (Assuming they have are of a type 64)
        auto* malloc_inst = this->_builder.Insert(malloc_func(existing_type->second.safe_type));
        this->_builder.CreateStore(this->_builder.getInt64(1), this->_builder.CreateConstInBoundsGEP2_32(malloc_inst->getType()->getPointerElementType(), malloc_inst, 0, 0));
        this->_builder.CreateStore(this->_builder.getInt64(1), this->_builder.CreateConstInBoundsGEP2_32(malloc_inst->getType()->getPointerElementType(), malloc_inst, 0, 1));
        
        // Create the inner ptr && don't set anything inside
        auto* inner_malloc = this->_builder.Insert(malloc_func(existing_type->second.obj_type));

        // Set the inner ptr to the allocated ptr
        this->_builder.CreateStore(
            inner_malloc,
            this->_builder.CreateConstInBoundsGEP2_32(malloc_inst->getType()->getPointerElementType(), malloc_inst, 0, 2)
        );
        
        // retrun the malloc obj
        this->_builder.CreateRet(malloc_inst);

        this->_builder.SetInsertPoint(last_pos);

        // Setting the new data
        new_safe_ptr_data.create_ptr_func = function;
        this->_safe_pointers.insert_or_assign(name, new_safe_ptr_data);
    }

    return function;
}

llvm::Function* GarbageCollector::getOrCreateRetainFunction(bool is_strong)
{
    if (this->_curr_module == nullptr || this->_target == nullptr) return nullptr;
    auto** using_func = ((is_strong) ? (&retain_ptr_func_strong) : (&retain_ptr_func_weak));
    string type_name = ((is_strong) ? ("strong") : ("weak"));

    // Return the function if already exists
    if((*using_func) != nullptr)
    {
        auto* function = llvm::cast_or_null<llvm::Function>(
            this->_curr_module->getOrInsertFunction(
                "&retain" + type_name, (*using_func)->getFunctionType()
            ).getCallee());
        
        if(function != nullptr)
            function->setLinkage(llvm::GlobalValue::LinkageTypes::ExternalLinkage);

        return function;
    }
    else
    {
        auto* last_pos = this->_builder.GetInsertBlock();

        // Create the function
        auto* func_type = llvm::FunctionType::get(
            llvm::Type::getVoidTy(this->_context),
            llvm::ArrayRef<llvm::Type*>
                { this->_builder.getInt8PtrTy() },
            false
        );
        
        (*using_func) = llvm::Function::Create(
            func_type, llvm::GlobalValue::LinkageTypes::ExternalLinkage,
            "&retain" + type_name, *(this->_curr_module)
        );

        // Setting the argument to be assumed as non null, always
        (*using_func)->addFnAttr(llvm::Attribute::AttrKind::ArgMemOnly);
        (*using_func)->getArg(0)->addAttr(llvm::Attribute::AttrKind::NonNull);

        // create the entry point and get the allocation size
        auto* entry_block = llvm::BasicBlock::Create(this->_context, "entry", (*using_func));
        this->_builder.SetInsertPoint(entry_block);
        
        // bitcasting the void pointer to the safe struct
        auto* struct_type = getOrCreateSafePtrType(this->_safe_i8_type_name, _builder.getInt8Ty());
        auto* safe_obj = this->_builder.CreateBitCast(
            (*using_func)->getArg(0), struct_type->getPointerTo());
        
        // Increase the total count
        auto* total_count = this->_builder.CreateConstInBoundsGEP2_32(struct_type, safe_obj, 0, 0);
        this->_builder.CreateStore(
            this->_builder.CreateAdd(this->_builder.CreateLoad(total_count), this->_builder.getInt64(1)),
            total_count
        );

        if (is_strong) // Increase reference count, only if strong
        {  
            auto* ref_count = this->_builder.CreateConstInBoundsGEP2_32(struct_type, safe_obj, 0, 1);
            this->_builder.CreateStore(
                this->_builder.CreateAdd(this->_builder.CreateLoad(ref_count), this->_builder.getInt64(1)),
                ref_count
            );
        }

        // Return void
        this->_builder.CreateRetVoid();
        
        this->_builder.SetInsertPoint(last_pos);
    }

    return (*using_func);

}

llvm::Function* GarbageCollector::getOrCreateReleaseFunction(bool is_strong)
{
    if (this->_curr_module == nullptr || this->_target == nullptr) return nullptr;
    auto** using_func = ((is_strong) ? (&release_ptr_func_strong) : (&release_ptr_func_weak));
    string type_name = ((is_strong) ? ("strong") : ("weak"));
    
    // Return the function if already exists
    if((*using_func) != nullptr)
    {
        auto* function = llvm::cast_or_null<llvm::Function>(
            this->_curr_module->getOrInsertFunction(
                "&release" + type_name, (*using_func)->getFunctionType()
            ).getCallee());
        
        if(function != nullptr)
            function->setLinkage(llvm::GlobalValue::LinkageTypes::ExternalLinkage);

        return function;
    }
    else
    {
        auto* last_pos = this->_builder.GetInsertBlock();

        // Create the function
        auto* func_type = llvm::FunctionType::get(
            llvm::Type::getVoidTy(this->_context),
            llvm::ArrayRef<llvm::Type*>
                { this->_builder.getInt8PtrTy(), this->_builder.getInt8PtrTy() },
            false
        );
        
        (*using_func) = llvm::Function::Create(
            func_type, llvm::GlobalValue::LinkageTypes::ExternalLinkage,
            "&release" + type_name, *(this->_curr_module)
        );

        /*
        void strongrelease(i8** ptr, i8* destructor){

            (*ptr)->ref--; // because it is strong release, assume refcount >= 1
            if((*ptr)->ref == 0)
            {
                if(destructor != null) // cast destructor to: void(i8*)* and calls it
                    (desctructor bitcast void(i8*)*)((*ptr)->ptr);
                free((*ptr)->ptr);
                (*ptr)->ptr = null;
            }

            (*ptr)->total--;
        
            if((*ptr)->total == 0)
            {
                free(*ptr);
                *ptr = null;
            }

            return ans;
        }

        void weakrelease(i8** ptr){
            (*ptr)->total--;
        
            if((*ptr)->total == 0)
            {
                free(*ptr);
                *ptr = null;
            }
        }
        */

        // Setting the argument to be assumed as non null, always
        (*using_func)->addFnAttr(llvm::Attribute::AttrKind::ArgMemOnly);
        (*using_func)->getArg(0)->addAttr(llvm::Attribute::AttrKind::NonNull);

        // create the entry point and get the allocation size
        auto* entry_block = llvm::BasicBlock::Create(this->_context, "entry", (*using_func));
        this->_builder.SetInsertPoint(entry_block);

        // bitcasting the void pointer to the safe struct
        auto* struct_type = getOrCreateSafePtrType(this->_safe_i8_type_name, _builder.getInt8Ty());
        auto* safe_obj = this->_builder.CreateConstGEP1_64(this->_builder.CreateBitCast(
            (*using_func)->getArg(0), struct_type->getPointerTo()), 0);
        
        if(is_strong)
        {
            auto* ref_count_ptr = this->_builder.CreateConstGEP2_32(struct_type, safe_obj, 0, 1);
            auto* ref_count = this->_builder.CreateLoad(ref_count_ptr);

            // Subtruct 1 from the refrence count
            auto* sub_op = this->_builder.CreateSub(ref_count, this->_builder.getInt64(1));
            this->_builder.CreateStore(sub_op, ref_count_ptr);

            // Create the needed blocks
            auto* if_ref_block = llvm::BasicBlock::Create(this->_context, "if_ref_count", (*using_func));
            auto* if_destrucor_block = llvm::BasicBlock::Create(this->_context, "if_destructor", (*using_func));
            auto* if_ref_free = llvm::BasicBlock::Create(this->_context, "free_ptr", (*using_func));
            auto* if_after_ref = llvm::BasicBlock::Create(this->_context, "after_ref", (*using_func));
            
            // Get inner ptr for future use
            auto* inner_ptr_ptr = this->_builder.CreateConstGEP2_32(struct_type, safe_obj, 0, 2);
            auto* inner_ptr = this->_builder.CreateLoad(inner_ptr_ptr);

            // Create the condition: if(ref_count == 0)
            auto* and_op = this->_builder.CreateAnd(
                this->_builder.CreateICmpEQ(sub_op, this->_builder.getInt64(0)),
                this->_builder.CreateICmpNE(inner_ptr, llvm::ConstantPointerNull::get(llvm::cast_or_null<llvm::PointerType>(inner_ptr->getType())))
            );

            this->_builder.CreateCondBr(and_op, if_ref_block, if_after_ref);

            this->_builder.SetInsertPoint(if_ref_block);

            // Create the condition: if(desctructor != null)
            this->_builder.CreateCondBr(
                this->_builder.CreateIsNull((*using_func)->getArg(1)),
                if_ref_free, if_destrucor_block
            );

            this->_builder.SetInsertPoint(if_destrucor_block);

            // Create call to the destructor after bitcasting it: i8* -> void (i8*)
            auto* desctructor = this->_builder.CreateBitCast(
                (*using_func)->getArg(1),
                llvm::FunctionType::get(this->_builder.getVoidTy(), {this->_builder.getInt8PtrTy()})->getPointerTo()
            );
            
            this->_builder.CreateCall(llvm::cast_or_null<llvm::FunctionType>(desctructor->getType()->getPointerElementType()), desctructor, { inner_ptr });

            // Jumping to free the inner ptr
            this->_builder.CreateBr(if_ref_free);
            this->_builder.SetInsertPoint(if_ref_free);

            // Create free call and insert null to the pointer
            this->_builder.Insert(llvm::CallInst::CreateFree(inner_ptr, if_ref_free));
            this->_builder.CreateStore(
                llvm::ConstantPointerNull::get(llvm::cast_or_null<llvm::PointerType>(inner_ptr->getType())),
                inner_ptr_ptr
            );

            // Jump to after block
            this->_builder.CreateBr(if_after_ref);
            this->_builder.SetInsertPoint(if_after_ref);
        }

        auto* total_count_ptr = this->_builder.CreateConstGEP2_32(struct_type, safe_obj, 0, 0);
        auto* total_count = this->_builder.CreateLoad(total_count_ptr);
        
        llvm::Value* total_sub = this->_builder.CreateSub(total_count, this->_builder.getInt64(1));

        // Subtruct 1 from the total count
        this->_builder.CreateStore(
            total_sub,
            total_count_ptr
        );

        // Create the total check blocks
        auto* if_total_block = llvm::BasicBlock::Create(this->_context, "if_total_count", (*using_func));
        auto* if_after_total = llvm::BasicBlock::Create(this->_context, "after_total", (*using_func));

        // Create the condition: if(total_count == 0)
        this->_builder.CreateCondBr(
            this->_builder.CreateICmpEQ(total_sub, this->_builder.getInt64(0)),
            if_total_block, if_after_total
        );

        // The if total block
        this->_builder.SetInsertPoint(if_total_block);

        // Create free call
        this->_builder.Insert(llvm::CallInst::CreateFree(safe_obj, if_total_block));

        // Return void
        this->_builder.CreateBr(if_after_total); // Create jump to after block
        this->_builder.SetInsertPoint(if_after_total);
        this->_builder.CreateRetVoid();
        
        this->_builder.SetInsertPoint(last_pos);
    }

    return (*using_func);
}

llvm::Function* GarbageCollector::getOrCreateWrapFunction(const string& name)
{
    if (name.empty() || this->_curr_module == nullptr || this->_target == nullptr) return nullptr;

    // Looking up if it exists
    const auto& existing_type = this->_safe_pointers.find(name);
    if(existing_type == this->_safe_pointers.end()) return nullptr;
    
    llvm::Function* function = nullptr;
    SafePtrData new_safe_ptr_data = existing_type->second;

    // Return the function if already exists
    if(existing_type->second.wrap_ptr_func != nullptr)
    {
        function = llvm::cast_or_null<llvm::Function>(
            this->_curr_module->getOrInsertFunction(
                name + "&wrap", existing_type->second.wrap_ptr_func->getFunctionType()
            ).getCallee());
        
        if(function != nullptr)
            function->setLinkage(llvm::GlobalValue::LinkageTypes::ExternalLinkage);
    }
    else
    {
        auto* last_pos = this->_builder.GetInsertBlock();

        // Create the function
        auto* func_type = llvm::FunctionType::get(
            existing_type->second.safe_type->getPointerTo(),
            llvm::ArrayRef<llvm::Type*> { _builder.getInt8PtrTy() },
            false
        );
        
        function = llvm::Function::Create(
            func_type, llvm::GlobalValue::LinkageTypes::ExternalLinkage,
            name + "&wrap", *(this->_curr_module)
        );

        // create the entry point and get the allocation size
        auto* entry_block = llvm::BasicBlock::Create(this->_context, "entry", function);
        this->_builder.SetInsertPoint(entry_block);
        
        // A malloc size updated by the malloc function and used later by memset
        llvm::Constant* malloc_size = nullptr;

        // Create the malloc call function
        auto malloc_func = [&](llvm::Type* mallocated_ptr) -> llvm::Instruction* {
            auto size_val = _target->createDataLayout().getTypeAllocSize(existing_type->second.safe_type);
            auto* int_type = llvm::Type::getInt32Ty(this->_context); // May be 64
            malloc_size = llvm::ConstantExpr::getTruncOrBitCast(this->_builder.getInt64(size_val.getFixedSize()), int_type);
            
            return llvm::CallInst::CreateMalloc(entry_block,
                    int_type, mallocated_ptr, malloc_size,
                    nullptr, nullptr, "");
        };

        // Create the safe ptr type && Set the all counter and strong counter to 1 (Assuming they have are of a type 64)
        auto* malloc_inst = this->_builder.Insert(malloc_func(existing_type->second.safe_type));
        this->_builder.CreateStore(this->_builder.getInt64(1), this->_builder.CreateConstInBoundsGEP2_32(malloc_inst->getType()->getPointerElementType(), malloc_inst, 0, 0));
        this->_builder.CreateStore(this->_builder.getInt64(1), this->_builder.CreateConstInBoundsGEP2_32(malloc_inst->getType()->getPointerElementType(), malloc_inst, 0, 1));

        // Set the inner ptr to the pointer given
        this->_builder.CreateStore(
            _builder.CreateBitCast(function->getArg(0), new_safe_ptr_data.obj_type->getPointerTo()),
            this->_builder.CreateConstInBoundsGEP2_32(malloc_inst->getType()->getPointerElementType(), malloc_inst, 0, 2)
        );
        
        // retrun the malloc obj
        this->_builder.CreateRet(malloc_inst);

        this->_builder.SetInsertPoint(last_pos);

        // Setting the new data
        new_safe_ptr_data.wrap_ptr_func = function;
        this->_safe_pointers.insert_or_assign(name, new_safe_ptr_data);
    }

    return function;
}

llvm::StoreInst* GarbageCollector::setRC(llvm::Value* ptr, llvm::Value* count)
{
    if (ptr == nullptr) return nullptr;

    return this->_builder.CreateStore(
        count, 
        this->_builder.CreateConstInBoundsGEP2_32(
            ptr->getType()->getPointerElementType()->getPointerElementType(), _builder.CreateLoad(ptr), 0, 1
        )
    );
}

llvm::StoreInst* GarbageCollector::setTotalRC(llvm::Value* ptr, llvm::Value* count)
{
    if (ptr == nullptr) return nullptr;

    return this->_builder.CreateStore(
        count, 
        this->_builder.CreateConstInBoundsGEP2_32(
            ptr->getType()->getPointerElementType()->getPointerElementType(), _builder.CreateLoad(ptr), 0, 0
        )
    );
}
