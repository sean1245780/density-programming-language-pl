#pragma once

#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/GlobalAlias.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/Type.h"

#include "llvm/Support/Host.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"

#include <vector>
#include <unordered_map>
#include <string>

using std::vector;
using std::string;
using std::unordered_map;
using std::pair;

typedef struct SafePtrData
{
    llvm::Type* obj_type = nullptr;
    llvm::StructType* safe_type = nullptr;
    llvm::Module* define_module = nullptr;
    llvm::Function* create_ptr_func = nullptr;
    llvm::Function* wrap_ptr_func = nullptr;
} SafePtrData;

class GarbageCollector
{
private:
    llvm::IRBuilder<>& _builder;
    llvm::LLVMContext& _context;
    llvm::TargetMachine* _target;

    llvm::Function* retain_ptr_func_strong = nullptr;
    llvm::Function* retain_ptr_func_weak = nullptr;
    llvm::Function* release_ptr_func_strong = nullptr;
    llvm::Function* release_ptr_func_weak = nullptr;
    string _safe_i8_type_name = "safe_i8";

    unordered_map<string, SafePtrData> _safe_pointers;
    llvm::Module* _curr_module;

public:

    GarbageCollector(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::TargetMachine* target, llvm::Module* curr_module = nullptr);
    
    // Defining the safe pointer type - Fitting both strong and weak pointer types
    llvm::StructType* getOrCreateSafePtrType(const string& name, llvm::Type* inner_type);

    // Defining the 3 main functions to manipulate the safe ptr - create, retain, release
    llvm::Function* getOrCreateSafeCreateFunction(const string& name);
    llvm::Function* getOrCreateRetainFunction(bool is_strong = true);
    llvm::Function* getOrCreateReleaseFunction(bool is_strong = true);

    // Defining a function to wrap a non refrence counted pointer to a safe pointer
    llvm::Function* getOrCreateWrapFunction(const string& name);

    llvm::StoreInst* setRC(llvm::Value* ptr, llvm::Value* count);
    llvm::StoreInst* setTotalRC(llvm::Value* ptr, llvm::Value* count);

    /*
        To get the inner ptr.
        ptr: safe_ptr = <exists>
        ptr: inner_ptr = null

        if(safe_ptr != null)
            inner_ptr = get pos(2) safe_ptr
    */
    
    //llvm::Function* getOrCreateSafeInnerPtr(const string& name);

    /*
        strong int* func(int d) 
        {
            // retain - ref = 1
            var x = create int(); // strong

            if(d > 5)
            {
                // retain - ref = 2
                return x; // store
                //release - ref = 1;
            }

            return null;
            // release - ref = 0
        }

        i32 main()
        {
            // ref count - 2
            var x = func(); // store - ref count 1
            // ref count x = 1

            // ref count 1
            func(); // release
            // ref count 0 - have to release

            var something = (x = func()); // final ref = 1
            //release - ref = 0;
            if(func() == null)
            {
                
            }
            
            
        }

        {
            strong int* x = create int();
            strong int* yap = x;
            strong int* zap = yap;
        }*/

        /*
        strong int* yap;  // stack allocation
        strong int* zap;
       
        store x in yap      // storing the safe pointer as it is
        strong retain (yap)   // retaining the safe pointer
        
        store yap in zap
        strong retain (zap)
    */

    // Getters and Setters
    inline llvm::Module* getCurrentModule() const noexcept { return this->_curr_module; }
    //static constexpr const char* getMangledName() noexcept { return "$S"; }
    
    inline void setCurrentModule(llvm::Module* curr_module) noexcept { this->_curr_module = curr_module; }
    inline void setTarget(llvm::TargetMachine* target) noexcept { this->_target = target; }
    inline void setSafeI8PtrNameType(const string& name) noexcept { this->_safe_i8_type_name = name; }

};

