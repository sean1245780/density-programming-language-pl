#pragma once

#include "Types.h"
#include "llvm/IR/IRBuilder.h"

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <array>
#include <functional>

using std::string;
using std::unordered_map;
using std::unordered_set;
using std::vector;
using std::pair;
using std::array;

class TypeContainer
{
private:
    vector<SynthesizedType*> unnamed_definitions;
    unordered_map<string, SynthesizedType*> definitions;
    string position = "";

public:
    TypeContainer() = default;
    ~TypeContainer();

    void enterDefinition(const string& definition) noexcept;
    string exitDefinition() noexcept;

    string exitToScopeOf(const string& identifier) noexcept;
    string exitToGlobalScope() noexcept;
    
    PointerType* getOrCreatePointerType(SynthesizedType* base) noexcept;
    StrongPointerType* getOrCreateStrongPointerType(SynthesizedType* base) noexcept;
    WeakPointerType* getOrCreateWeakPointerType(SynthesizedType* base) noexcept;
    FunctionType* getOrCreateFunctionType(SynthesizedType* input, SynthesizedType* output, bool is_variadic, CustomType* parent = nullptr) noexcept;
    BasicType* getOrCreateBasicType(const string& name, llvm::Type* llvm_type, bool modifiable) noexcept;

    SynthesizedType* getOrCreateType(const string& name, std::function<SynthesizedType*()> creation_func) noexcept;
    SynthesizedType* getOrCreateType(std::function<SynthesizedType*()> creation_func) noexcept;

    SynthesizedType* safeGetOrCreateType(const string& name, std::function<SynthesizedType*()> creation_func, SynthesizedType::type_category type_category) noexcept;

    SynthesizedType* getType(const string& definition) const noexcept;

    NamedComplexType* getOrCreateNamedComplexType(NamedComplexType* type);
    UnnamedComplexType* getOrCreateUnnamedComplexType(UnnamedComplexType* type);
};

class TypeSystemInfo
{
private:
    TypeSystemInfo() = default;

    static TypeContainer* global_container;
    static llvm::LLVMContext global_context;

public:
    enum class primitive_type { int8_type, int16_type, int32_type, int64_type, int128_type,
                                uint8_type, uint16_type, uint32_type, uint64_type, uint_128,
                                float16_type, float32_type, float64_type, float128_type,
                                bool_type, char_type, uchar_type };

    enum class type_synthesis_action { none, left, right, boolean, pointer_of, dereference, access, dref_access, safe_dref_access, safe_wrap };

    static constexpr const char* generic_type = "var";

    static unordered_map<string, primitive_type> primitive_type_mapping;
    static unordered_map<primitive_type, string> primitive_type_mapping_rev;

    static unordered_map<string, array<type_synthesis_action, 3>> operators_type_synthesis;

    static constexpr const unsigned int size_opertors = 22;
    static constexpr const char* operators_str[size_opertors] = // By using the values of enum we can get the wanted index
    {
        "+", "-", "++" , "--", "*", "/", "%", "!", "||", "&&",
        "~", "|", "&", "^", "&", "*", "==", ">=", "<=", ">", "<", "="
    };

    static TypeContainer* getGlobalContainer() { return global_container; }
    static llvm::LLVMContext& getGlobalLLVMContext() { return global_context; }

    static void init() noexcept;
    static void cleanup() noexcept;
};