#pragma once

#include "../DataHandler/IDataAccess.h"
#include "Constants.h"
#include "Operators.h"
#include <iostream>
#include <string>
#include <exception>
#include <unordered_set>
#include <unordered_map>
#include <vector>

enum class token_type { // general
						identifier, op, separator, 
						
						// literal
						literal_int, literal_string, literal_char, literal_double, literal_null, literal_bool,
						
						// keywords
						if_kw, else_kw, while_kw, for_kw, continue_kw, break_kw,
						
						namespace_kw, enum_kw, type_kw,
						create_kw, new_kw, delete_kw,
						strong_kw, weak_kw,
						set_kw, get_kw, unset_kw, vset_kw,
						public_kw, protected_kw, private_kw,
						throw_kw, catch_kw, return_kw, static_kw,
						
						import_kw, extern_kw, vargs_kw, self_kw,
						constructor_kw, destructor_kw,

						//unknown
						unknown };

typedef struct Token {
	static std::unordered_set<char> separators;

	// The length of the longest operator
	static const unsigned int longest_operator;

	static std::unordered_map<std::string, token_type> keywords;
	
	static std::unordered_map<std::string, token_type> literal_keywords;
	
	token_type type;
	position_data file_pos;
	
	Token(const position_data& data) : file_pos(data) {}

	/*
		The function will print the information of this token.
	*/
	virtual void print();
	
	virtual PrecedenceInfo getPrecedenceInfo() = 0;
	virtual PrecedenceInfo getTypePrecedenceInfo() { 
		return {associativity_type::none, NULL_PRECEDENCE, NULL_PRECEDENCE, NULL_PRECEDENCE}; 
	}
	unsigned int getLeftPrecedence(unsigned int type) { return this->getPrecedenceInfo().leftPrecedence(type); }
	unsigned int getRightPrecedence(unsigned int type) { return this->getPrecedenceInfo().rightPrecedence(type); }
} Token;

typedef struct OperatorToken : public Token {
	std::string op;
	
	OperatorToken(const position_data& data) : Token(data) {}

	/*
		The function will print the information of this token.
	*/
	virtual void print();

	virtual PrecedenceInfo getPrecedenceInfo();
	virtual PrecedenceInfo getTypePrecedenceInfo() { 
		if (this->op == "*") 
			return {associativity_type::none, NULL_PRECEDENCE, NULL_PRECEDENCE, 200};

		return {associativity_type::none, NULL_PRECEDENCE, NULL_PRECEDENCE, NULL_PRECEDENCE}; 
	}
} OperatorToken;

typedef struct SeparatorToken : public Token {
	char separator;

	SeparatorToken(const position_data& data) : Token(data) {}
	/*
		The function will print the information of this token.
	*/
	virtual void print();

	virtual PrecedenceInfo getPrecedenceInfo();
} SeparatorToken;

typedef struct IdentifierToken : public Token {
	std::string name;

	IdentifierToken(const position_data& data) : Token(data) {}
	
	/*
		The function will print the information of this token.
	*/
	virtual void print();

	virtual PrecedenceInfo getPrecedenceInfo();
	virtual PrecedenceInfo getTypePrecedenceInfo() {
		return {associativity_type::left, MIN_PRECEDENCE, MIN_PRECEDENCE, MIN_PRECEDENCE}; 
	}	
} IdentifierToken;

typedef struct KeywordToken : public Token {

	KeywordToken(const position_data& data) : Token(data) {}

	/*
		The function will print the information of this token.
	*/
	virtual void print();

	virtual PrecedenceInfo getPrecedenceInfo();
} KeywordToken;

typedef struct UnknownToken : public Token {
	std::string unknown_identifier;

	UnknownToken(const position_data& data) : Token(data) {}
	/*
		The function will print the information of this token.
	*/
	virtual void print();

	virtual PrecedenceInfo getPrecedenceInfo();
} UnknownToken;

template <class T>
struct LiteralToken : public Token {
	T value;
	
	LiteralToken(const position_data& data) : Token(data) {}

	/*
		The function will initialize an instance of this class.
		@ new_value is the literal value to be stored.
		@ tok_type is the type of the literal stored in this instance.
	*/
	LiteralToken(T new_value, token_type tok_type, const position_data& data) : Token(data)
	{
		this->value = new_value;
		this->type = tok_type;
	}
	
	/*
		The function will print the information of this token.
	*/
	virtual void print()
	{
		std::cout << "Literal: \t" << value << " \t--> Position: " << file_pos << std::endl;
	}

	virtual PrecedenceInfo getPrecedenceInfo()
	{
		return {associativity_type::left, 155, 155, 155};
	}
};

typedef LiteralToken<unsigned long long> LiteralIntToken;
typedef LiteralToken<std::string> LiteralStringToken;
typedef LiteralToken<bool> LiteralBooleanToken;
typedef LiteralToken<char> LiteralCharacterToken;
typedef LiteralToken<long double> LiteralFloatToken;