#pragma once

#include "reporter.hpp"
#include "../DataHandler/IDataAccess.h"
#include <iostream>
#include <exception>
#include <sstream>

using std::stringstream;
using std::vector;

namespace CompilerErrors {

	class CompilerOutInfo : public std::exception
	{
	public:
		virtual std::ostream& operator<<(std::ostream& output_stream) = 0;
	};

	class CompilationError : public CompilerOutInfo, public reporter::Error {
	public:
		CompilationError(const std::string& error, const position_data& position, const std::string& submessage = "") noexcept :
				reporter::Error(string("Compilation Error ") += error, submessage,(Location)position) {}

		CompilationError(const std::string& error, const std::string& submessage = "") noexcept :
				reporter::Error(string("Compilation Error ") += error, submessage, {}) {}

		virtual std::ostream& operator<<(std::ostream& output_stream);

		CompilationError* withNote(const std::string& message, const position_data& location) { reporter::Error::withNote(message, (Location)location); return this; }

		CompilationError* withHelp(const std::string& message, const position_data& location) { reporter::Error::withHelp(message, (Location)location); return this; }

		CompilationError* withNote(const std::string& message) { reporter::Error::withNote(message); return this; }

		CompilationError* withHelp(const std::string& message) { reporter::Error::withHelp(message); return this; }
	};

	class ParserError : public CompilationError {
	public:
		ParserError(const std::string& error, const position_data& position, const std::string& submessage = "") noexcept :
				CompilationError(string("> Parser Error: ") += error, position, (submessage.empty() ? error : submessage)) {}

	};

	class LexerError : public CompilationError {
	public:
		LexerError(const std::string& error, const position_data& position, const std::string& submessage = "") noexcept :
				CompilationError(string("> Lexer Error: ") += error, position, (submessage.empty() ? error : submessage)) {}

	};

	class SemanticError : public CompilationError {
	public:
		SemanticError(const std::string& error, const position_data& position, const std::string& submessage = "") noexcept :
				CompilationError(string("> Semantic Error: ") += error, position, (submessage.empty() ? error : submessage)) {}

		SemanticError(const std::string& error, const std::string& submessage = "") noexcept :
				CompilationError(string("> Semantic Error: ") += error, (submessage.empty() ? error : submessage)) {}
	};

	class CodeGenError : public CompilationError {
	public:
		CodeGenError(const std::string& error, const position_data& position, const std::string& submessage = "") noexcept :
				CompilationError(string("> Code Generation Error: ") += error, position, (submessage.empty() ? error : submessage)) {}

		CodeGenError(const std::string& error, const std::string& submessage = "") noexcept :
				CompilationError(string("> Code Generation Error: ") += error, (submessage.empty() ? error : submessage)) {}
	};

	class CompilationWarning : public CompilerOutInfo, public reporter::Warning {
	public:
		CompilationWarning(const std::string& error, const position_data& position, const std::string& submessage = "") noexcept :
				reporter::Warning(string("Compilation Error ") += error, submessage,(Location)position) {}

		virtual std::ostream& operator<<(std::ostream& output_stream);

		CompilationWarning* withNote(const std::string& message, const position_data& location) { reporter::Warning::withNote(message, (Location)location); return this; }

		CompilationWarning* withHelp(const std::string& message, const position_data& location) { reporter::Warning::withHelp(message, (Location)location); return this; }

		CompilationWarning* withNote(const std::string& message) { reporter::Warning::withNote(message); return this; }

		CompilationWarning* withHelp(const std::string& message) { reporter::Warning::withHelp(message); return this; }
	};

	class ParserWarning : public CompilationWarning {
	public:
		ParserWarning(const std::string& error, const position_data& position, const std::string& submessage = "") noexcept :
				CompilationWarning(string("> Parser Warning: ") += error, position, (submessage.empty() ? error : submessage)) {}

	};

	class LexerWarning : public CompilationWarning {
	public:
		LexerWarning(const std::string& error, const position_data& position, const std::string& submessage = "") noexcept :
				CompilationWarning(string("> Lexer Warning: ") += error, position, (submessage.empty() ? error : submessage)) {}

	};

	class SemanticWarning : public CompilationWarning {
	public:
		SemanticWarning(const std::string& error, const position_data& position, const std::string& submessage = "") noexcept :
				CompilationWarning(string("> Semantic Warning: ") += error, position, (submessage.empty() ? error : submessage)) {}

	};

	class CodeGenWarning : public CompilationWarning {
	public:
		CodeGenWarning(const std::string& error, const position_data& position, const std::string& submessage = "") noexcept :
				CompilationWarning(string("> Code Generation Warning: ") += error, position, (submessage.empty() ? error : submessage)) {}

	};

	class ErrorRegister
	{
	private:
		bool _has_warnings = false;
		bool _has_errors = false;
		bool _ignore_warnings = false;
		vector<CompilerOutInfo*> _all_data_registered;
		reporter::Config _cfg;
	
		ErrorRegister(bool ignore_warnings = false);
		ErrorRegister(const ErrorRegister&) = delete;
		ErrorRegister(ErrorRegister&&) = delete;
		ErrorRegister& operator = (const ErrorRegister&) = delete;
		ErrorRegister& operator = (ErrorRegister&&) = delete;

		static inline ErrorRegister& getInstance() 
		{
			static ErrorRegister singleton;
			return singleton;
		}

		~ErrorRegister();

	public:
		static ErrorRegister& reportError(CompilationError* error);
		static ErrorRegister& reportWarning(CompilationWarning* error);

		static inline bool hasWarnings() { return getInstance()._has_warnings; }
		static inline bool hasErrors() { return getInstance()._has_errors; }
		static inline bool isIgnoringWarnings() { return getInstance()._ignore_warnings; }
		static inline void setIgnoringWarnings(bool ignore_warnings) { getInstance()._ignore_warnings = ignore_warnings; }
		static void printCompilingOutputInfo();
		static void flush();
		static inline const reporter::Config& getConfigurations() { return getInstance()._cfg; }
	};
}