#include "CompilerErrors.h"

namespace CompilerErrors {

	std::ostream& CompilationError::operator<<(std::ostream& output_stream)
	{
		this->print(output_stream, ErrorRegister::getConfigurations());
		return output_stream;
	}

	std::ostream& CompilationWarning::operator<<(std::ostream& output_stream)
	{
		this->print(output_stream, ErrorRegister::getConfigurations());
		return output_stream;
	}

	ErrorRegister::ErrorRegister(bool ignore_warnings) : _ignore_warnings(ignore_warnings)
	{
		this->_cfg.style = reporter::DisplayStyle::RICH;

		this->_cfg.chars.beforeFileName = "|--> ";
		this->_cfg.chars.afterFileName = " <--";
		this->_cfg.chars.borderVertical = L'|';
		this->_cfg.chars.borderHorizontal = L' ';
		this->_cfg.chars.borderBottomRight = L'|';
		this->_cfg.chars.noteBullet = L'*';
		this->_cfg.chars.lineVertical = L'|';
		this->_cfg.chars.lineBottomLeft = "\\_";

		this->_cfg.colors.help = reporter::colors::fggreen & reporter::colors::bold;
		this->_cfg.colors.note = reporter::colors::fgcyan & reporter::colors::bold;
	}

	ErrorRegister::~ErrorRegister()
	{
		getInstance().flush();
	}

	ErrorRegister& ErrorRegister::reportError(CompilationError* error)
	{
		ErrorRegister& errReg = getInstance();

		if (error != nullptr) {
			errReg._all_data_registered.push_back(error);
			errReg._has_errors =  true;
		}

		return errReg;
	}

	ErrorRegister& ErrorRegister::reportWarning(CompilationWarning* warning)
	{
		ErrorRegister& errReg = getInstance();

		if(errReg._ignore_warnings)
		{
			delete warning;
			return errReg;
		}

		if (warning != nullptr) {
			errReg._all_data_registered.push_back(warning);
			errReg._has_warnings =  true;
		}

		return errReg;
	}

	void ErrorRegister::printCompilingOutputInfo()
	{
		ErrorRegister& errReg = getInstance();

		for (CompilerOutInfo* err : errReg._all_data_registered)
		{
			if (err != nullptr)
			{
				err->operator<<(std::cerr);
				std::cerr << "\n";	
			}
		}
	}
	
	void ErrorRegister::flush()
	{
		ErrorRegister& errReg = getInstance();

		for (auto& err : errReg._all_data_registered)
		{
			if (err != nullptr)
			{
				delete err;
				err = nullptr;
			}
		}

		errReg._all_data_registered.clear();
		errReg._has_errors = false;
		errReg._has_warnings = false;
	}
}