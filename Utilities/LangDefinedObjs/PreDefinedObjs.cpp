#include "PreDefinedObjs.h"

string VarArg::_name = "vargs";

string VarArg::getVariableName() noexcept { return VarArg::_name; }

llvm::StructType* VarArg::getVAListType(llvm::LLVMContext& context, llvm::TargetMachine* target_machine)
{
    if(target_machine == nullptr) return nullptr;

    if (target_machine->getTargetTriple().getOS() == llvm::Triple::Linux && 
        target_machine->getTargetTriple().getArch() == llvm::Triple::x86_64)
    {
        auto* int_type = llvm::Type::getInt32Ty(context);
        auto* char_type = llvm::Type::getInt8Ty(context)->getPointerTo();
        return llvm::StructType::get(context, {int_type, int_type, char_type, char_type});
    }
   
    return llvm::StructType::get(context, { llvm::Type::getInt8Ty(context)->getPointerTo() });
}

llvm::AllocaInst* VarArg::createVAList(llvm::IRBuilder<>& builder, llvm::StructType* va_list_type)
{
    return builder.CreateAlloca(va_list_type, nullptr, VarArg::_name);
}

llvm::AllocaInst* VarArg::createVAList(llvm::LLVMContext& context, llvm::TargetMachine* target_machine, llvm::IRBuilder<>& builder)
{
    if (target_machine == nullptr) return nullptr;
    return builder.CreateAlloca(VarArg::getVAListType(context, target_machine), nullptr, VarArg::_name);
}

llvm::Function* VarArg::createOrGetStartDec(llvm::LLVMContext& context, llvm::Module* module)
{
    if (module == nullptr) return nullptr;

    return llvm::Intrinsic::getDeclaration(module, llvm::Intrinsic::vastart);
}

llvm::Function* VarArg::createOrGetCopyDec(llvm::LLVMContext& context, llvm::Module* module)
{
    if (module == nullptr) return nullptr;

    return llvm::Intrinsic::getDeclaration(module, llvm::Intrinsic::vacopy);
}

llvm::Function* VarArg::createOrGetEndDec(llvm::LLVMContext& context, llvm::Module* module)
{
    if (module == nullptr) return nullptr;

    return llvm::Intrinsic::getDeclaration(module, llvm::Intrinsic::vaend);
}
