#pragma once

#include "llvm/IR/Value.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/Type.h"

#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/TargetRegistry.h"

#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"

#include <string>

using std::string;

//enum class ObjUse : int { identifier = 1, functioncall = 0x2 };

class VarArg 
{
private:
    static string _name;

    VarArg() = default;

public:
    static string getVariableName() noexcept;
    static llvm::StructType* getVAListType(llvm::LLVMContext& context, llvm::TargetMachine* target_machine);
    static llvm::AllocaInst* createVAList(llvm::IRBuilder<>& builder, llvm::StructType* va_list_type);
    static llvm::AllocaInst* createVAList(llvm::LLVMContext& context, llvm::TargetMachine* target_machine, llvm::IRBuilder<>& builder);
    static llvm::Function* createOrGetStartDec(llvm::LLVMContext& context, llvm::Module* module);
    static llvm::Function* createOrGetCopyDec(llvm::LLVMContext& context, llvm::Module* module);
    static llvm::Function* createOrGetEndDec(llvm::LLVMContext& context, llvm::Module* module);
};