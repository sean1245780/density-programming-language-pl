#pragma once

// Those are includes and constants that meant to be in the top of the main file!

#include "Errors/reporter.hpp"
#include "FileHandler/FileHandler.h"
#include "DataHandler/StringAccess.h"
#include "DataHandler/FileAccess.h"
#include "DefinedObjs/Constants.h"

