#pragma once

#include "../Errors/reporter.hpp"
#include <exception>
#include <thread>
#include <chrono>
#include <set>
#include <sstream>
#include "../FileHandler/FileHandler.h"
#include "../DefinedObjs/Constants.h"

using std::pair;
using reporter::Location;
using std::string;
using std::stringstream;

enum class data_access_type {string_access, file_access};

/*
This class represents an interface to access a binary / textual data resource.
*/
class IDataAccess
{
protected:

	/*
		The function constructs an instance of this class.
		@The default constructor is protected to prevent instantiation of this class.
	*/
	IDataAccess() = default;

public:
	/*
		The function will return the byte at the index given.
	*/
	virtual char operator[](const int index) = 0;
	
	/*
		The function will return the string starting at the index given with the length given.
	*/
	virtual string substr(const int index, const int len = 1) = 0;

	/*
		The function will return the length (in bytes) of the data resource.
	*/
	virtual unsigned int length() const noexcept = 0;

	/*
		The function will return the path to the data access
	*/
	virtual string getFilePath() const noexcept = 0;

	/*
		The function will return the type of the data access
	*/
	virtual data_access_type getType() const noexcept = 0;

};

typedef struct position_data
{
	unsigned int total_pos;
	unsigned int x;
	unsigned int x_end; // First character to not include
	unsigned int y;
	bool undefined_pos;
	IDataAccess& resource;

	position_data(IDataAccess& data, bool undefined = false) noexcept : resource(data), total_pos(0), x(0), x_end(0), y(0), undefined_pos(undefined) {}

	void increment(const unsigned int& count = 1) noexcept;

	operator Location() const { 
		OpFile* op_file = nullptr;

		if (this->undefined_pos)
			return {};

		if (this->resource.getType() == data_access_type::file_access)
		{
			op_file = new OpFile(resource.getFilePath(), true);
		}
		else if (this->resource.getType() == data_access_type::string_access)
		{
			op_file = new OpFile(resource.substr(0, resource.length()), false);
		}

		return Location(y + 1, x, x_end, op_file, false);
	}

	friend std::ostream& operator<<(std::ostream& stream, const position_data& pos) noexcept;

	operator string() const noexcept;

} position_data;