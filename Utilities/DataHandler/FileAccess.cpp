#include "FileAccess.h"

FileAccess::FileAccess(string file_name)
{
	if (!this->_file.openFile(file_name, openFileModes::read_b))
		throw std::invalid_argument((std::string("Could not open ") + file_name).c_str());

	this->_length = (unsigned int)this->_file.getFilesLength();
}

char FileAccess::operator[](const int index)
{
	auto res = this->_file.readFromFile(1, index, true, true);
	
	if (!res.success)
		throw std::runtime_error("Failed to read from file");


	return res.obj[0];
}

string FileAccess::substr(const int index, const int len)
{
	auto res = this->_file.readFromFile(len, index, true, true);

	if (!res.success)
		throw std::runtime_error("Failed to read from file");

	return res.obj;
}

unsigned int FileAccess::length() const noexcept
{
	return this->_length;
}
