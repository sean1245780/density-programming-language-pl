#pragma once

#include "IDataAccess.h"
#include "../FileHandler/FileHandler.h"

class FileAccess : public IDataAccess
{
private:
	FileOperation _file;
	unsigned int _length;

public:

	/*
		The function will initialize this instance and set the data resource to the file specified by the given name.
		@ The function will throw std::runtime_error if the file could not be opened.
	*/
	FileAccess(string file_name);

	/*
		The function will return the byte at the index given.
	*/
	virtual char operator[](const int index);
	
	/*
		The function will return the string starting at the index given with the length given.
	*/
	virtual string substr(const int index, const int len = 1);
	
	/*
		The function will return the length (in bytes) of the data resource.
	*/
	virtual unsigned int length() const noexcept;

	/*
		The function will return the type of the data access
	*/
	virtual string getFilePath() const noexcept { return this->_file.getFullPath(); };

	/*
		The function will return the type of the data access
	*/
	virtual data_access_type getType() const noexcept { return data_access_type::file_access; }
};