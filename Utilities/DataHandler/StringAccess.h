#pragma once

#include "IDataAccess.h"

class StringAccess : public IDataAccess
{
private:
	string _data;

public:
	
	/*
		The function will initialize this instance and set the data resource to the string given.
	*/
	StringAccess(string data);

	/*
		The function will return the byte at the index given.
	*/
	virtual char operator[](const int index);
	
	/*
		The function will return the string starting at the index given with the length given.
	*/
	virtual string substr(const int index, const int len = 1);
	
	/*
		The function will return the length (in bytes) of the data resource.
	*/
	virtual unsigned int length() const noexcept;

	/*
		The function will return the type of the data access
	*/
	virtual string getFilePath() const noexcept { return RAW_DATA_MODULE_NAME; };

	/*
		The function will return the type of the data access
	*/
	virtual data_access_type getType() const noexcept { return data_access_type::string_access; }

};