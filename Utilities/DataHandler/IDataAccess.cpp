#include "IDataAccess.h"

void position_data::increment(const unsigned int& count) noexcept
{
	for (unsigned int i = total_pos; i < total_pos + count; i++)
	{
		x++;

		if (this->resource[i] == '\n')
		{
			y++;
			x = 0;
		}
	}

	this->x_end = this->x + 1;
	this->total_pos += count;
}

position_data::operator string() const noexcept
{
	stringstream s;
	s << "(x = " << this->x << " | end_x = " << this->x_end << ", y = " << this->y << ") -> total = " << this->total_pos;
	return s.str();
}

std::ostream& operator<<(std::ostream& stream, const position_data& pos) noexcept
{
	stream << "(x = " << pos.x << " | end_x = " << pos.x_end << ", y = " << pos.y << ") -> total = " << pos.total_pos;

	return stream;
}
