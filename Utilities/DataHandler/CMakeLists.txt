cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

set(data_handling_source_files
IDataAccess.h
IDataAccess.cpp
FileAccess.h
FileAccess.cpp
StringAccess.h
StringAccess.cpp
)

add_library(data_handling ${data_handling_source_files})

target_link_libraries(data_handling file_handler)
