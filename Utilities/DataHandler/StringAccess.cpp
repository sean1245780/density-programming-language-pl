#include "StringAccess.h"

StringAccess::StringAccess(string data)
{
	this->_data = data;
}

char StringAccess::operator[](const int index)
{
	if (index >= this->_data.size() || index < 0)
		return EOF;
	
	return this->_data[index];
}

string StringAccess::substr(const int index, const int len)
{
	return this->_data.substr(index, len);
}

unsigned int StringAccess::length() const noexcept
{
	return this->_data.size();
}