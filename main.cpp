#include "Utilities/Errors/reporter.hpp"
#include "Utilities/MainIncludes.h"
#include "CLI/Cli.h"

/*
	Function description.
	@ Comments and stuff.
	--> Special comments - Something very very important (Should end with "!").
*/

int main(int argc, const char* argv[])
{
	try
	{
		CliInfo cli_info(argc, argv);
		CLI cli(cli_info);
		cli.compile();
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << cendl;
	}

	return 0;
}