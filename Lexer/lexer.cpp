#include "lexer.h"

Token* Lexer::get_next_token(IDataAccess& data, position_data& position)
{
	if (position.total_pos < 0 || position.total_pos >= data.length())
		return nullptr;
	
	Token* res = nullptr;
	
	// skip whitespace
	lex_until(data, position, isspace, true);
	
	if (position.total_pos >= data.length()) 
		return nullptr;
	
	if (isalpha(data[position.total_pos]) || data[position.total_pos] == '_')
	{
		position_data temp_pos = position;
		
		std::string possible_identifier = lex_until(data, position, [](char c){ return isalnum(c) || c == '_'; }, true);
		temp_pos.x_end = position.x;
		
		if (Token::keywords.find(possible_identifier) != Token::keywords.end())
		{
			// lex keyword
			res = new KeywordToken(temp_pos);
			
			res->type = Token::keywords.find(possible_identifier)->second;
		}
		else if (Token::literal_keywords.find(possible_identifier) != Token::literal_keywords.end())
		{
			// lex literal keyword
			switch (Token::literal_keywords.find(possible_identifier)->second)
			{
				case token_type::literal_null:
					res = new LiteralCharacterToken(0, token_type::literal_null, temp_pos);
					break;
				case token_type::literal_bool:
					res = new LiteralBooleanToken(possible_identifier == "true", token_type::literal_bool, temp_pos);
					break;
				default:
					// Program execution should ideally not get to this error
					ErrorRegister::reportError(new LexerError("Encountered unrecognized keyword", position));
					break;
			}
		}
		else 
		{
			position_data temp_pos_1 = temp_pos;
			string op = lex_as_operator(data, temp_pos);
			if (!op.empty() && !isalnum(data[temp_pos.total_pos]) && data[temp_pos.total_pos] != '_')
			{
				temp_pos_1.x_end = temp_pos.x;
				//lex operator token
				res = new OperatorToken(temp_pos_1);
				
				static_cast<OperatorToken*>(res)->op = op;
				res->type = token_type::op;
			}
			else
			{
				// lex identifier
				res = new IdentifierToken(temp_pos_1);
				
				(static_cast<IdentifierToken*>(res))->name = possible_identifier;
				res->type = token_type::identifier;	
			}
		}
	}
	else if (data[position.total_pos] == '/' && position.total_pos < data.length() - 1 && 
			(data[position.total_pos + 1] == '/' || data[position.total_pos + 1] == '*'))
	{
		switch (data[position.total_pos + 1])
		{
			case '/': 
				// skip the '//'
				position.increment(2);
				
				lex_until(data, position, [](char c) {return c == '\n';}); 
				break;
			case '*': 
				// skip the '/*'
				position.increment(2);
				
				lex_until(data, position, [&](char c) {
					return c == '*' && position.total_pos < data.length() - 1 && data[position.total_pos + 1] == '/';
				});
				
				if (position.total_pos >= data.length())
				{
					ErrorRegister::reportError((new LexerError("Reached end of file without closing the comment", position))->withHelp("Add `*/` to close the comment"));
					break;
				}
				
				// skip the '*/'
				position.increment(2);
				break;
			default:
				break;
		}
	}
	else if (isdigit(data[position.total_pos]))
	{
		position_data temp_pos = position;
		
		std::string int_part = lex_until(data, position, [](char c){return isdigit(c);}, true);
		
		temp_pos.x_end = position.x;

		// lex int literal
		res = new LiteralIntToken(temp_pos);
		res->type = token_type::literal_int;

		try {
			((LiteralIntToken*)res)->value = std::stoull(int_part);
		} catch (std::exception& err) {
			ErrorRegister::reportError(new LexerError("Integer constant is too large", temp_pos,
				"Integer constant is too large"));
		}
	}
	else if (data[position.total_pos] == '"')
	{
		// lex string literal
		position_data temp_pos = position;

		// remove the starting '"'
		position.increment();

		std::string string_data = lex_string_literal(data, position);
		
		if (position.total_pos >= data.length())
		{
			ErrorRegister::reportError((new LexerError("Reached end of file without closing string literal", position))->withHelp("Add `\"` to close the string"));
		}
		else
		{
			// remove the closing '"'
			position.increment();

			res = new LiteralStringToken(temp_pos);
			((LiteralStringToken*)res)->value = string_data;
			res->type = token_type::literal_string;

			res->file_pos.x_end = position.x;
		}
	}
	else if (data[position.total_pos] == '\'')
	{
		// lex char literal
		position_data temp_pos = position;

		// remove the starting '
		position.increment();		
		
		if (position.total_pos >= data.length())
		{
			temp_pos.x_end = position.x;
	
			ErrorRegister::reportError((new LexerError("Reached end of file without closing char literal", temp_pos))->withHelp("Add `'` to close the char"));
		}
		else if (data[position.total_pos] == '\'')
		{
			position.increment();
			
			temp_pos.x_end = position.x;
			
			ErrorRegister::reportError((new LexerError("Encountered empty char literal", temp_pos))->withHelp(
					"Add `'` to close the char")->withNote("Chars must represent a single character value"));
		}
		else
		{
			char char_content = '\0';
			if (data[position.total_pos] == '\\')
			{
				// skip the backslash
				position.increment();

				// check if the string ends
				if (position.total_pos >= data.length())
					ErrorRegister::reportError((new LexerError("Reached end of file without closing string literal", position))->withHelp("Add `\"` to close the string"));
				else
					char_content = lex_escape_char(data, position);
			}
			else 
			{
				char_content = data[position.total_pos];
				position.increment();
			}

			// check if the string ends
			if (position.total_pos >= data.length())
			{
				ErrorRegister::reportError((new LexerError("Reached end of file without closing char literal", position))->withHelp("Add `\'` to close the string"));
			}

			if (data[position.total_pos] != '\'')
			{
				ErrorRegister::reportError((new LexerError("Too many characters in char literal", position))->withHelp(
					"Remove redundent data")->withNote("Chars must represent a single character value"));
				
				position.increment();
			}
			else
			{
				res = new LiteralCharacterToken(temp_pos);
				static_cast<LiteralCharacterToken*>(res)->value = char_content;
				res->type = token_type::literal_char;
		
				// remove the closing '
				position.increment();
				res->file_pos.x_end = position.x;
			}
		}

	}
	else if (Token::separators.find(data[position.total_pos]) != Token::separators.end())
	{
		//lex separator token
		res = new SeparatorToken(position);
		
		((SeparatorToken*)res)->separator = data[position.total_pos];
		position.increment();
		
		res->type = token_type::separator;

		res->file_pos.x_end = position.x;
	}
	else
	{
		position_data temp_pos = position;
		
		auto op = lex_as_operator(data, position);
		
		if (op != "")
		{
			temp_pos.x_end = position.x;
			//lex operator token
			res = new OperatorToken(temp_pos);
			
			(static_cast<OperatorToken*>(res))->op = op;
			res->type = token_type::op;
		}
		else
		{
			res = new UnknownToken(temp_pos);
			
			// skip the unknown token
			(static_cast<UnknownToken*>(res))->unknown_identifier = lex_until(data, position, isspace);
			res->type = token_type::unknown;
			res->file_pos.x_end = position.x;

			// Report an error: an unknown token has been encountered
			ErrorRegister::reportError(new LexerError("An unknown token has been encountered", res->file_pos, std::string("Token: ") + ((UnknownToken*)res)->unknown_identifier + std::string(" is not a valid token")));
		}
	}
	
	return res;
}

std::string Lexer::lex_until(IDataAccess& data, position_data& position, std::function<bool(char)> condition, bool negate)
{
	int len = 0;
	
	while (position.total_pos < data.length() && !(negate ^ condition(data[position.total_pos])))
	{
		position.increment();
		len++;
	}
	
	return data.substr(position.total_pos - len, len);
}

std::string Lexer::lex_as_operator(IDataAccess& data, position_data& position)
{
	Operator* tokn = nullptr;
	
	for(int i = Token::longest_operator; i > 0; i--)
	{
		if((tokn = OperatorsInfo::inst().getOperator(data.substr(position.total_pos, i))) != nullptr)
		{
			position.total_pos += tokn->op.size();
			position.x += tokn->op.size();
			return tokn->op;
		}
	}
	
	return "";
}

std::string Lexer::lex_string_literal(IDataAccess& data, position_data& position)
{
	string str;
	
	while (position.total_pos < data.length() && data[position.total_pos] != '"')
	{
		if (data[position.total_pos] == '\\')
		{
			// skip the backslash
			position.increment();

			// check if the string ends
			if (position.total_pos >= data.length())
			{
				ErrorRegister::reportError((new LexerError("Reached end of file without closing string literal", position))->withHelp("Add `\"` to close the string"));
				return "";
			}

			str += lex_escape_char(data, position);

		}
		else 
		{
			str += data[position.total_pos];
			position.increment();
		}
	}

	// check if the string ends
	if (position.total_pos >= data.length())
	{
		ErrorRegister::reportError((new LexerError("Reached end of file without closing string literal", position))->withHelp("Add `\"` to close the string"));
		return "";
	}

	return str;
}

unsigned long long Lexer::lex_as_base(IDataAccess& data, position_data& position, std::function<bool(char)> condition, int base)
{
	auto last_pos = position;
	string hex_str = lex_until(data, position, condition, true);

	try
	{
		return std::stoull(hex_str, 0, base);
	}
	catch(const std::exception& e)
	{
		ErrorRegister::reportError(new LexerError(
			"Base " + std::to_string(base) + " literal value is out of range",
			last_pos
		));
	}

	return 0;
}

char Lexer::lex_escape_char(IDataAccess& data, position_data& position)
{
	char ret_val = '\0';

	switch(data[position.total_pos])
	{
		case '\\': position.increment(); return '\\';
		case 'a': position.increment(); return '\a';
		case 'b': position.increment(); return '\b';
		case 'e': position.increment(); return '\e';
		case 'f': position.increment(); return '\f';
		case 'n': position.increment(); return '\n';
		case 'r': position.increment(); return '\r';
		case 't': position.increment(); return '\t';
		case 'v': position.increment(); return '\v';
		case '\'': position.increment(); return '\'';
		case '"': position.increment(); return '\"';
		case '?': position.increment(); return '\?';
		case 'x':
		{
			position.increment();
			ret_val = (unsigned char)lex_as_base(data, position, [](char c){
					return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') ||
							(c >= 'a' && c <= 'f');}, 16);

			// check if the string ends
			if (position.total_pos >= data.length())
			{
				ErrorRegister::reportError(new LexerError("Reached end of file without closing literal", position));
				return '\0';
			}

			break;
		}
		default:
		{
			// Unregistered escape charaters
			if(data[position.total_pos] > '7' || data[position.total_pos] < '0')
			{
				ret_val = data[position.total_pos];

				// Report the unregistered escape character
				ErrorRegister::reportWarning(new LexerWarning(
					"Unregistered escape character",
					position,
					string("The result of the sequence will be: ") + ret_val
				));

				position.increment();
			}
			else // Octal escape number
			{
				int count = 0;
				ret_val = (unsigned char)lex_as_base(data, position, [&count](char c){
						return (count++ < 3) && (c >= '0' && c <= '7'); }, 8);

				// check if the string ends
				if (position.total_pos >= data.length())
				{
					ErrorRegister::reportError((new LexerError("Reached end of file without closing string literal", position))->withHelp("Add `\"` to close the string"));
					return '\0';
				}
			}
		}
	}

	return ret_val;
}


std::vector<Token*> Lexer::lex(IDataAccess& data)
{
	std::vector<Token*> result;
	position_data position(data);

	while (position.total_pos < data.length())
	{
		Token* ptr = Lexer::get_next_token(data, position);
		
		if (ptr != nullptr)
			result.push_back(ptr);
	}
	
	return result;
}