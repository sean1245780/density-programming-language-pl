cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

set(lexer_source_files
lexer.h
lexer.cpp
)

add_library(lexer ${lexer_source_files})
target_link_libraries(lexer utils)
