#pragma once

#include "../Utilities/Errors/CompilerErrors.h"
#include "../Utilities/DataHandler/IDataAccess.h"
#include "../Utilities/DefinedObjs/Tokens.h"

#include <algorithm>
#include <functional>

using CompilerErrors::ErrorRegister;
using CompilerErrors::LexerError;
using CompilerErrors::LexerWarning;

class Lexer 
{
private:
	/*
		The function constructs an instance of this class.
		@ The default constructor is private to prevent instantiation of this class.
	*/
	Lexer();
	
	/*
		The function returns the token that starts at the position given.
		@ The IDataAccess& stores the untokenized data.
	*/
	static Token* get_next_token(IDataAccess& str, position_data& position);
	
	/*
		The function will return the string starting at the position given until the condition given returns true.
		@ The IDataAccess& stores the untokenized data.
		@ The position will be incremented accordingly.
		@ The negate flag negates the condition.
	*/
	static std::string lex_until(IDataAccess& data, position_data& position, std::function<bool(char)> condition, bool negate = false);
	
	/*
		The function will return the operator string starting at the location given.
		@ The IDataAccess& stores the untokenized data.
		@ The longest operator is matched first.
		@ If no operator was found, "" will be returned.
	*/
	static std::string lex_as_operator(IDataAccess& data, position_data& position);

	/*
		The function will return the lexed literal with escpae characters in mind.
		@ The IDataAccess& stores the untokenized data.
		@ The position will be incremented accordingly.
	*/
	static std::string lex_string_literal(IDataAccess& data, position_data& position);

	/*
		The function will return the lexed based number literal.
		@ The IDataAccess& stores the untokenized data.
		@ The position will be incremented accordingly.
	*/
	static unsigned long long lex_as_base(IDataAccess& data, position_data& position, std::function<bool(char)> condition, int base = 10);

	/*
		The function will return the lexed based number literal.
		@ The IDataAccess& stores the untokenized data.
		@ The position will be incremented accordingly.
	*/
	static char lex_escape_char(IDataAccess& data, position_data& position);

public:

	/*
		The function returns a vector that contains the tokens extracted from the untokenized data.
		@ The IDataAccess& stores the untokenized data.
	*/
	static std::vector<Token*> lex(IDataAccess& data);
	
};

