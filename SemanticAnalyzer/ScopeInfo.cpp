#include "ScopeInfo.h"

void ScopeInformation::addSymbol(const string& symbol, AbstractSyntaxTree::Node* definition)
{
    // insert the symbol only if the deepest scope that can contain the symbol
    if (!this->all_tables.empty())
    {
        const auto& found = this->all_tables.front().find(symbol);

        // check if that symbol is already defined
        if (found != this->all_tables.front().end())
            throw SymbolRedefinitionException(symbol, found->second, definition);

        // add the symbol
        this->all_tables.begin()->insert({symbol, definition});
    }
}

ScopeInformation& ScopeInformation::operator+=(const ScopeInformation& scope_info)
{
    if(this == &scope_info) return *this;

    // add the symbols of the other scope info
    if(!scope_info.all_tables.empty())
        this->operator+=(scope_info.all_tables.front());

    return *this;
}

ScopeInformation& ScopeInformation::operator+=(const SymbolTable& table)
{
    // insert all the symbols in the global scope of the given scope info into 
    // the deepest scope of this scope info
    for (const auto& command : table)
    {
        const auto& last_def = this->all_tables.front().find(command.first);

        // if the symbol is already defined, throw an exception
        if (last_def != this->all_tables.front().end())
            throw SymbolRedefinitionException(command.first, last_def->second, command.second);

        this->all_tables.front().insert(command);
    }

    return *this;
}

bool ScopeInformation::isDefined(const string& symbol) const noexcept
{
    for (const SymbolTable& last_table : all_tables)
        if (last_table.find(symbol) != last_table.end())
            return true;

    return false;
}

bool ScopeInformation::isDefinedCurrentScope(const string& symbol) const noexcept
{
    return !this->all_tables.empty() && this->all_tables.front().find(symbol) != 
                                        this->all_tables.front().end();
}

AbstractSyntaxTree::Node* ScopeInformation::getDefinitionCurrentScope(const std::string& symbol)
{
    if (this->all_tables.size() <= 0)
        return nullptr;

    auto found = this->all_tables.front().find(symbol);

    if (found != this->all_tables.front().end())
        return found->second;

    return nullptr;
}

void ScopeInformation::enterScope(const SymbolTable& table) noexcept
{
    this->all_tables.push_front(table);
}

void ScopeInformation::exitScope(bool create_alloc_block) noexcept
{
    this->all_tables.pop_front();
}

AbstractSyntaxTree::Node* ScopeInformation::getDefinition(const std::string& symbol)
{
    for (const auto& last_table : all_tables)
    {
        const auto& curr = last_table.find(symbol);

        if (curr != last_table.end())
            return curr->second;
    }

    return nullptr; 
}

AbstractSyntaxTree::Node* ScopeInformation::getDefinitionInModule(const std::string& symbol)
{
    if (this->curr_module == nullptr) return nullptr;

    const auto& curr = curr_module->getSymbolTable().find(symbol);

    if (curr != curr_module->getSymbolTable().end())
        return curr->second;

    return nullptr;
}

SymbolTable& ScopeInformation::deepestScopeTable() noexcept
{
    return this->all_tables.front();
}

void ScopeInformation::includeImportedSymbols()
{
    stack<Module*> modules;
    unordered_set<Module*> included;

    // add the current module as the initial imported module
    modules.push(curr_module);
    included.insert(curr_module);

    while (!modules.empty())
    {
        auto* curr_imported = modules.top();
        modules.pop();

        // add the symbols of the imported module
        if (curr_imported != this->curr_module)
            this->operator+=(curr_imported->getSymbolTable());

        // add the modules which were imported from the imported module
        for (auto* imported : curr_imported->getImportedModules())
        {
            if (included.find(imported) == included.end())
            {
                modules.push(imported);

                // mark the module as imported
                included.insert(imported);
            }
        }
    }
}

// ScopeInformation ScopeInformation::getScopeOf(const string& definition)
// {
//     list<AbstractSyntaxTree::SymbolTable> new_scope;
//     auto itr = all_tables.begin();
//     bool found = false;

//     while (itr != this->all_tables.end() && !found)
//         if ((*itr).find(definition) != (*itr).end())
//             found = true;
//         else itr++;

//     while (itr != this->all_tables.end() && !found)
//     {
//         new_scope.push_back(*itr);
//         itr++;
//     }

//     ScopeInformation scope_info;
//     scope_info.all_tables = new_scope;

//     return scope_info;
// }