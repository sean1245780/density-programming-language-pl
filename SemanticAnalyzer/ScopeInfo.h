#pragma once

#include "../Utilities/AbstractSyntaxTree/Nodes.h"
#include <vector>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <stack>
#include <string>
#include <unordered_set>

using AbstractSyntaxTree::SymbolTable;
using AbstractSyntaxTree::Module;
using AbstractSyntaxTree::Node;

using std::vector;
using std::unordered_map;
using std::unordered_set;
using std::stack;
using std::string;
using std::pair;
using std::tuple;
using std::list;
using std::unordered_set;

typedef struct SymbolRedefinitionException : public std::exception {
	string redefined_symbol;
	AbstractSyntaxTree::Node* previous_definition;
	AbstractSyntaxTree::Node* new_definition;

	SymbolRedefinitionException(string symbol, AbstractSyntaxTree::Node* last_def, 
				AbstractSyntaxTree::Node* new_def) noexcept : redefined_symbol(symbol), 
				previous_definition(last_def), new_definition(new_def) {}

	virtual const char* what() const noexcept { 
		return ("Symbol redefinition error: " + redefined_symbol + " defined in " + 
			(std::string)(previous_definition->getPosition()) + " is redefined in " + 
			(std::string)(new_definition->getPosition())).c_str();
	}

	virtual ~SymbolRedefinitionException() noexcept {}
} SymbolRedefinitionException;

template <class T>
class ScopeObject
{
private:
	T& _scope;
	bool _create_alloc_block = true;

public:
	ScopeObject(T& scope, const SymbolTable& table = {}) noexcept : _scope(scope)
    {
		this->_scope.enterScope(table);
	}

	ScopeObject(T& scope, bool create_alloc_block, const SymbolTable& table = {}) noexcept : _scope(scope)
	{
		this->_scope.enterScope(table);
		this->_create_alloc_block = create_alloc_block;

		if(!_create_alloc_block)
		{
			scope.functions_data.top().alloc_objs.pop_front();
			scope.functions_data.top().creation_objs.pop_front();
		}
	}

    ScopeObject(const ScopeObject&) noexcept = delete;
    ScopeObject(ScopeObject&&) noexcept = delete;
    ScopeObject& operator= (ScopeObject& obj) noexcept {return obj;}
    ScopeObject& operator= (ScopeObject&& obj) noexcept = delete;
    
	~ScopeObject()
    {
		this->_scope.exitScope(_create_alloc_block);
	}
};

typedef struct ScopeInformation {
	Module* curr_module = nullptr;

	// This list contains the symbol tables of the scopes in this scope info
	list<SymbolTable> all_tables;

	ScopeInformation(){}
    ScopeInformation(Module* mod, const SymbolTable& global_table) : curr_module(mod)
		{ this->all_tables.push_front(global_table); }

	ScopeInformation& operator+=(const ScopeInformation& scope_info);
	ScopeInformation& operator+=(const SymbolTable& table);

	bool isInGlobalScope() const noexcept { return this->all_tables.size() == 1; }
	bool isDefined(const string& symbol) const noexcept;
	bool isDefinedCurrentScope(const string& symbol) const noexcept;
	void addSymbol(const string& symbol, AbstractSyntaxTree::Node* definition);
	void enterScope(const SymbolTable& table) noexcept;
	void exitScope(bool create_alloc_block = true) noexcept;

	AbstractSyntaxTree::Node* getDefinition(const std::string& symbol);
	AbstractSyntaxTree::Node* getDefinitionInModule(const std::string& symbol);
	AbstractSyntaxTree::Node* getDefinitionCurrentScope(const std::string& symbol);
	SymbolTable& deepestScopeTable() noexcept;

	/*
        The function will iterate over the imported modules of the module given, 
		get the modules that should be included, and add their symbols to the scope 
		information struct given.
    */
	void includeImportedSymbols();

	// ScopeInformation getScopeOf(const string& definition);

} ScopeInformation;
