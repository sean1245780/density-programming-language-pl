#include "SemanticAnalyzer.h"

void SemanticAnalyzer::wrapRawModule()
{
    auto raw_module = this->_modules.find(RAW_DATA_MODULE_NAME);

    // return if there is no raw module
    if (raw_module == this->_modules.end()) return;

    // get the first statement - it will be considered as the start of the function when printing errors
    auto& commands = raw_module->second->getCommands();
    AbstractSyntaxTree::Statement* main_startpoint = nullptr;

    for (int i = 0; i < commands.size() && main_startpoint == nullptr; i++)
    {
        if (commands[i]->getNodeType() == AbstractSyntaxTree::Node::node_type::statement)
        {
            main_startpoint = static_cast<AbstractSyntaxTree::Statement*>(commands[i]);
        }
    }

    // return if there are no statements in the code
    if (main_startpoint == nullptr) return;

    // create the main function
    AbstractSyntaxTree::FunctionDefinition* main_func = new AbstractSyntaxTree::FunctionDefinition(main_startpoint->getPosition());
    AbstractSyntaxTree::StatementBlock* statements = new AbstractSyntaxTree::StatementBlock(main_startpoint->getPosition());

    main_func->setFunctionName(new AbstractSyntaxTree::IdentifierExpression(main_startpoint->getPosition(), "main"));
    main_func->setReturnType(new AbstractSyntaxTree::ComplexType(main_startpoint->getPosition()));
    main_func->setInputType(new AbstractSyntaxTree::ComplexType(main_startpoint->getPosition()));
    main_func->setCommands(statements);

    // add the statements to the main function
    for (int i = 0; i < commands.size(); i++)
    {
        if (commands[i]->getNodeType() == AbstractSyntaxTree::Node::node_type::statement)
        {
            statements->addStatement(static_cast<AbstractSyntaxTree::Statement*>(commands[i]));
            commands[i] = nullptr;
        }
    }

    // remove unused commands
    commands.erase(std::remove_if(commands.begin(), commands.end(), 
                [](AbstractSyntaxTree::Node* ptr){ 
                    return ptr == nullptr; 
                }), commands.end());

    // add the new main function instead of the deleted commands
    commands.push_back(main_func);
}

void SemanticAnalyzer::mergeImportedModules()
{    
    if(this->_modules.empty()) return;
    
    if(this->_modules.size() == 1)
    {
        this->_merged_modules.push_back({ this->_modules.begin()->first, this->_modules.begin()->second });
        return;
    }

    try
    {
        for (const auto& module : this->_modules)
            mergeImportedModules(module.first, module.second);
    } catch(ImportError& err) {
        ErrorRegister::reportError(err);
    }
}

void SemanticAnalyzer::mergeImportedModules(const string& module_name, Module* module)
{
    if (module == nullptr)  
        throw ImportError(module_name, ImportError::import_err_type::module_not_found);
    if (this->_modules_import_states[module_name] == import_state::importing)
        throw ImportError(module_name, ImportError::import_err_type::looping_import);
    if (this->_modules_import_states[module_name] == import_state::finished_importing)
        // Can't import it twice no matter where you are
        return;

    this->_modules_import_states[module_name] = import_state::importing;

    auto& commands = module->getCommands();
    for (unsigned int i = 0; i < commands.size(); i++)
    {
        // check if the node is an import statement
        if (commands[i] != nullptr && commands[i]->getNodeType() == AbstractSyntaxTree::Node::node_type::import_statement)
        {
            auto* import_statement = static_cast<AbstractSyntaxTree::ImportStatement*>(commands[i]);
            const auto& to_import = this->_modules.find(import_statement->getFullPath());

            // report an error - module was not found
            if (to_import == this->_modules.end())
                throw ImportError(import_statement->getFullPath(), ImportError::import_err_type::module_not_found);

            try {
                // merge the imported module
                mergeImportedModules(import_statement->getFullPath(), to_import->second);

                // change the import statement to a pointer to the imported module
                commands[i] = to_import->second;
            } catch(ImportError& err) {
                err.addIncludedFrom(import_statement->getPosition());
                throw err;
            }
        }
    }

    this->_modules_import_states[module_name] = import_state::finished_importing;
    this->_merged_modules.push_back({ module_name, module });
}

void SemanticAnalyzer::labelCheck()
{
    LabelChecker::labelCheck(this->_merged_modules, this->_modules);
}

void SemanticAnalyzer::flowCheck()
{
    FlowChecker::flowCheck(this->_modules);
}

void SemanticAnalyzer::typeCheck()
{
    TypeChecker::typeCheck(this->_merged_modules);
}

vector<pair<string, Module*>> SemanticAnalyzer::decorate(const unordered_map<IDataAccess*, vector<AbstractSyntaxTree::Node*>>& tree)
{
    SemanticAnalyzer analyzer(tree);

    if (ErrorRegister::hasErrors()) return {};
    analyzer.wrapRawModule();

    if (ErrorRegister::hasErrors()) return {};
    analyzer.mergeImportedModules();

    if (ErrorRegister::hasErrors()) return {};
    analyzer.labelCheck();

    if (ErrorRegister::hasErrors()) return {};
    analyzer.typeCheck();

    if (ErrorRegister::hasErrors()) return {};
    analyzer.flowCheck();

    return analyzer._merged_modules;
}

SemanticAnalyzer::SemanticAnalyzer(const unordered_map<IDataAccess*, vector<AbstractSyntaxTree::Node*>>& tree)
{
    Module* curr_module = nullptr;

    for (auto& file_data : tree)
    {
        curr_module = new Module(*(file_data.first), file_data.second);

        this->_modules.insert({file_data.first->getFilePath(), curr_module});
    }
}
