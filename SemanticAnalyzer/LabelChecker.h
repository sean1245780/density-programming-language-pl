#pragma once

#include "../Utilities/DefinedObjs/TypeSystemInfo.h"
#include "ScopeInfo.h"

using CompilerErrors::ErrorRegister;
using CompilerErrors::SemanticError;
using CompilerErrors::SemanticWarning;

class LabelChecker
{
private:
    AbstractSyntaxTree::SymbolTable _cross_module_symbols;
    const vector<pair<string, Module*>>& _modules;
    const unordered_map<string, Module*>& _modules_mapping;
    unordered_map<Module*, bool> _module_check_mode;

    LabelChecker(const vector<pair<string, Module*>>& modules, const unordered_map<string, Module*>& mapping) : _modules(modules), _modules_mapping(mapping) { }

    /*
        Initialize symbols is the first label checking pass, it initializes the symbol tables
        of the modules given. This prepass is needed to allow referencing other functions / types
        in a cyclic way without any forward declarations.
    */
    void initializeSymbols(Module* module);
    void initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::Node* node);

    void initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::FunctionDefinition* node);
    void initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::Statement* node);
    void initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::VariableDeclaration* node);
    void initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::NamespaceDefinition* node);
    void initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::TypeDefinition* node);
    void initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::Module* node);

    inline void reportRedefinition(const SymbolRedefinitionException& exception, const std::string& type);
    inline void reportRedefinition(const std::string& identifier, const std::string& type, AbstractSyntaxTree::Node* definition, AbstractSyntaxTree::Node* last_def);
    inline void reportUndefined(const std::string&identifier, AbstractSyntaxTree::Node* position);

    /*  
        This function returnes the definition of the symbol given after it tries to insert
        the new definition.
        if a different node is already defined with the same symbol, the function returns the
        previous defintion, else, the function returns the inserted node
        @ WARNING: is_cross_module
    */
    AbstractSyntaxTree::Node* getOrInsertDefinition(ScopeInformation& scope_info, const std::string& symbol, 
                                  AbstractSyntaxTree::Node* node);
    AbstractSyntaxTree::Node* getOrInsertDefinition(SymbolTable& symbol_table, const std::string& symbol, 
                                  AbstractSyntaxTree::Node* node);

    /*
        These functions return the definition of a given symbol using the resources provided.
    */
    AbstractSyntaxTree::Node* getDefinition(ScopeInformation& scope_info, const std::string& symbol);
    AbstractSyntaxTree::Node* getDefinition(SymbolTable& symbol_table, const std::string& symbol);

    /*
        labelCheck is the pass following global symbol initialization. In this pass,
        the label checker reports redefinition errors and undefined reference errors and initializes
        the symbol tables of any non global node that contains a symbol table.
    */
    void labelCheck();

    // label check of modules
    ScopeInformation labelCheck(AbstractSyntaxTree::Module* module);

    // label checks any type of node
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::Node* node);

    // Node superclass
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::Statement* node);
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::TypeExpression* node);
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::FunctionDefinition* node);
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::TypeDefinition* node);
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceDefinition* node);

    // Statement superclass
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::Expression* node); 
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::VariableDeclaration* node);
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::StatementBlock* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::ReturnStatement* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::ConditionalStatement* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::WhileStatement* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::ForStatement* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::DeleteFunctionStatement* node);

    // Expression superclass
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::CreateFunctionExpression* node);
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::NewFunctionExpression* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::UnaryOperatorExpression* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::IdentifierExpression* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::ExpressionList* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::CallExpression* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::CastExpression* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::VarArgsExpression* node);
	void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::TypeInstatiation* node);

    // TypeExpression superclass
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::SimpleType* node);
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::ComplexType* node);
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::NamelessComplexType* node);
    void labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::PointerType* node);

    /*
        The function sets the referenced nodes of the namespace access expression and report
        errors if any invalid reference exists. The function will return the referenced node the 
        namespace access expression given.
    */
    AbstractSyntaxTree::Node* setReferencedNodes(ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* ns_expr);

public:

    static void labelCheck(const vector<pair<string, Module*>>& modules, const unordered_map<string, Module*>& mapping);    
};