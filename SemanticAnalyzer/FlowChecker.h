#pragma once

#include "../Utilities/Errors/CompilerErrors.h"
#include "../Utilities/AbstractSyntaxTree/Nodes.h"
#include "../Utilities/DefinedObjs/Operators.h"
#include <unordered_map>
#include <vector>

using CompilerErrors::ErrorRegister;
using CompilerErrors::SemanticError;
using CompilerErrors::SemanticWarning;
using AbstractSyntaxTree::Module;
using std::unordered_map;
using std::vector;

class FlowChecker
{
private:
    AbstractSyntaxTree::FunctionDefinition* _main_entry_point = nullptr;

    FlowChecker();

    bool checkReturnValidity(AbstractSyntaxTree::Statement* node);
    bool checkReturnValidity(AbstractSyntaxTree::StatementBlock* node);
    bool checkReturnValidity(AbstractSyntaxTree::ConditionalStatement* node);
    bool checkReturnValidity(AbstractSyntaxTree::WhileStatement* node);
    bool checkReturnValidity(AbstractSyntaxTree::ForStatement* node);

    void checkNonModStatements(AbstractSyntaxTree::Statement* node);
    void checkInvalidModifiers(AbstractSyntaxTree::Statement* node);
    //bool checkInvalidPlacement(AbstractSyntaxTree::Statement* node, const AbstractSyntaxTree::Properties& properties);
    //bool checkInvalidPlacement(AbstractSyntaxTree::Expression* node, const AbstractSyntaxTree::Properties& properties);

    void flowCheckGlobal(const vector<AbstractSyntaxTree::Node*>& nodes);
    void flowCheckGlobal(AbstractSyntaxTree::Node* node);

    void flowCheckGlobal(AbstractSyntaxTree::Statement* node);

    void flowCheckGlobal(AbstractSyntaxTree::FunctionDefinition* node);
    void flowCheckGlobal(AbstractSyntaxTree::NamespaceDefinition* node);
    void flowCheckGlobal(AbstractSyntaxTree::TypeDefinition* node);
    void flowCheckGlobal(AbstractSyntaxTree::VariableDeclaration* node);
    void flowCheck(AbstractSyntaxTree::VariableDeclaration* node);
    void flowCheck(AbstractSyntaxTree::Statement* node);

public:
    static void flowCheck(const unordered_map<string, Module*>& modules);

};