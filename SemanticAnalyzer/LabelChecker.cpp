#include "LabelChecker.h"

void LabelChecker::labelCheck(const vector<pair<string, Module*>>& modules, const unordered_map<string, Module*>& mapping)
{
    LabelChecker checker(modules, mapping);

    checker.labelCheck();
}

void LabelChecker::initializeSymbols(Module* module)
{
    // set all module check modes to be false and set the imported modules unordered set to be empty
    _module_check_mode.insert({module, false});

    // initialize symbols for all symbols in global scope
    for (AbstractSyntaxTree::Node* command : module->getCommands())
		initializeSymbols(module, module->getSymbolTable(), command);
}

void LabelChecker::initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::Node* node)
{
    if (node == nullptr) return;

	switch(node->getNodeType())
	{
    case AbstractSyntaxTree::Node::node_type::function_definition:
        initializeSymbols(module, table, static_cast<AbstractSyntaxTree::FunctionDefinition*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::variable_declaration_component:
        initializeSymbols(module, table, static_cast<AbstractSyntaxTree::VariableDeclaration*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::statement:
        initializeSymbols(module, table, static_cast<AbstractSyntaxTree::Statement*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::namespace_definition:
        initializeSymbols(module, table, static_cast<AbstractSyntaxTree::NamespaceDefinition*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::type_definition:
        initializeSymbols(module, table, static_cast<AbstractSyntaxTree::TypeDefinition*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::module:
        initializeSymbols(module, table, static_cast<AbstractSyntaxTree::Module*>(node));
        break;
    default:
        break;
	}
}

void LabelChecker::initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::Module* node)
{
    if (node == nullptr) return;

    auto module_state = this->_module_check_mode.find(module);

    if (module_state == this->_module_check_mode.end())
            throw std::runtime_error("Module state wasn't found in label checking");

    // add this node as an imported module
    module->getImportedModules().insert(node);
}

void LabelChecker::initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::Statement* node)
{
    if (node == nullptr) return;

	switch(node->getStatementType())
	{
    case AbstractSyntaxTree::Statement::statement_type::variable_declaration:
        initializeSymbols(module, table, static_cast<AbstractSyntaxTree::VariableDeclaration*>(node));
        break;
    default:
        break;
	}
}

void LabelChecker::initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::VariableDeclaration* node)
{
    if (node == nullptr) return;

    // Inner variable declaration table setup
    SymbolTable& var_decl_table = node->getSymbolTable();

    // add all of the variable symbols to the table
    for (auto* decl : node->getVariables())
    {
        if(decl != nullptr)
        {
            const string& name = decl->getIdentifier()->getIdentifier();
            auto* def = this->getDefinition(table, name);
            auto* inserted = this->getOrInsertDefinition(table, name, decl);
            auto* def_var = this->getDefinition(var_decl_table, name);
            auto* inserted_var = this->getOrInsertDefinition(var_decl_table, name, decl);
            
            if (inserted == nullptr || inserted_var == nullptr)
                ErrorRegister::reportError(new SemanticError(
                    "Found null definition in the symbol table",
                    node->getPosition()
                ));
            else if ((def != nullptr && inserted != def) || (def_var != nullptr && inserted_var != def_var))
                reportRedefinition(name, "variable decleration", inserted, def);
        }
    }
}

void LabelChecker::initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::NamespaceDefinition* node)
{
    if (node == nullptr) return;
    if (node->getIdentifier() == nullptr) return;

    // add namespace symbols
    string curr_identifier = node->getIdentifier()->getIdentifier();

    // add the symbol
    auto* inserted_val = this->getOrInsertDefinition(table, curr_identifier, node);
    if (inserted_val == nullptr)
        ErrorRegister::reportError(new SemanticError("Found null definition in the symbol table"));
    else if (inserted_val != node)
        reportRedefinition(node->getIdentifier()->getIdentifier(), "Namespace", node, inserted_val);
    
    for(auto* command : node->getCommands())
        initializeSymbols(module, node->getSymbolTable(), command);
}

void LabelChecker::initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::TypeDefinition* node)
{
    if (node == nullptr) return;
    if (node->getTypeName() == nullptr) return;

    // add the type's name as a symbol
    string curr_identifier = node->getTypeName()->getIdentifier();

    // try adding the symbol
    auto* inserted_val = this->getOrInsertDefinition(table, curr_identifier, node);
    if (inserted_val == nullptr)
        ErrorRegister::reportError(new SemanticError("Found null definition in the symbol table"));
    else if (inserted_val != node)
        this->reportRedefinition(curr_identifier, "Type", node, inserted_val);

    // add "Self" to the symbol table as a reference to this type
    node->getDataSymbolTable().insert({SELF_TYPE_KEYWORD, node});
    node->getFunctionsSymbolTable().insert({SELF_TYPE_KEYWORD, node});

    // initialize the symbols for the data members
    for(auto* command : node->getTypeDataMembers())
        initializeSymbols(module, node->getDataSymbolTable(), command);

    // initialize the symbols for the type functions
    for(auto* command : node->getTypeFunctions())
    {
        static_cast<AbstractSyntaxTree::FunctionDefinition*>(command)->setParentType(node);

        initializeSymbols(module, node->getFunctionsSymbolTable(), command);
    }
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::ConditionalStatement* node)
{
    if (node == nullptr) return;

    labelCheck(scope_info, node->getCondition());
    
    { ScopeObject obj(scope_info); labelCheck(scope_info, node->getIfCommands());}
    { ScopeObject obj(scope_info); labelCheck(scope_info, node->getElseCommands());}
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::WhileStatement* node)
{
    if (node == nullptr) return;

    labelCheck(scope_info, node->getCondition());
    
    { ScopeObject obj(scope_info); labelCheck(scope_info, node->getCommands());}
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::ForStatement* node)
{
    if (node == nullptr) return;
    
    // create a new scope for the initialization
    ScopeObject obj(scope_info);
    
    labelCheck(scope_info, node->getInitialization());
    
    // save the symbol table inside the for statement node
    node->setSymbolTable(scope_info.deepestScopeTable());

    labelCheck(scope_info, node->getCondition());
    labelCheck(scope_info, node->getIncrementation());

    { ScopeObject obj(scope_info); labelCheck(scope_info, node->getCommands());}
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::CreateFunctionExpression* node)
{
    if (node == nullptr) return;
    
    labelCheck(scope_info, node->getCreationType());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::NewFunctionExpression* node)
{
    if (node == nullptr) return;
    
    labelCheck(scope_info, node->getCreationType());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::DeleteFunctionStatement* node)
{
    if (node == nullptr) return;
    
    labelCheck(scope_info, node->getDeletingObj());
}

void LabelChecker::initializeSymbols(Module* module, SymbolTable& table, AbstractSyntaxTree::FunctionDefinition* node)
{
    if (node == nullptr) return;
    if (node->getFunctionName() == nullptr) return;

    // add function symbols
    string curr_identifier = node->getFunctionName()->getIdentifier();

    // try adding the symbol
    auto* inserted_val = this->getOrInsertDefinition(table, curr_identifier, node);
    if (inserted_val == nullptr)
        ErrorRegister::reportError(new SemanticError("Found null definition in the symbol table"));
    else if (inserted_val != node)
        this->reportRedefinition(curr_identifier, "Function", node, inserted_val);
}

inline void LabelChecker::reportRedefinition(const SymbolRedefinitionException& exception, const std::string& type)
{
    reportRedefinition(exception.redefined_symbol, type, exception.new_definition, exception.previous_definition);
}

inline void LabelChecker::reportRedefinition(const std::string& identifier, const std::string& type, AbstractSyntaxTree::Node* definition, AbstractSyntaxTree::Node* last_def)
{
    ErrorRegister::reportError((new SemanticError(
			type + std::string(" redefinition"), definition->getPosition(),
			"Redefinition of '" + identifier + "'"
		))->withNote("'" + identifier + "' is already defined", last_def->getPosition()));
}

inline void LabelChecker::reportUndefined(const std::string& identifier, AbstractSyntaxTree::Node* position)
{
    ErrorRegister::reportError(new SemanticError(
			"Undefined identifier", position->getPosition(),
			identifier + " is used but not defined"
		));
}

AbstractSyntaxTree::Node* LabelChecker::getOrInsertDefinition(ScopeInformation& scope_info, const std::string& symbol, 
                                AbstractSyntaxTree::Node* node)
{
    if (node == nullptr) return nullptr;

    // get the definition from the scope info
    auto* scope_def = scope_info.getDefinition(symbol);
    
    if (scope_def != nullptr) 
        return scope_def;
    
    scope_info.addSymbol(symbol, node);

    return node;
}

AbstractSyntaxTree::Node* LabelChecker::getOrInsertDefinition(SymbolTable& symbol_table, const std::string& symbol, 
                                AbstractSyntaxTree::Node* node)
{
    if (node == nullptr) return nullptr;

    // check if the symbol exists in the given symbol table
    const auto& found = symbol_table.find(symbol);
    if (found != symbol_table.end()) 
        return found->second;

    symbol_table.insert({symbol, node});

    return node;
}

AbstractSyntaxTree::Node* LabelChecker::getDefinition(SymbolTable& symbol_table, const std::string& symbol)
{
    const auto& found = symbol_table.find(symbol);
    if (found != symbol_table.end()) 
        return found->second;

    return nullptr;
}

void LabelChecker::labelCheck()
{
    // initialize symbols for all modules
    for (const auto& module : this->_modules)
    {
        // initialize the symbol table of this module
        this->initializeSymbols(module.second);
    }

    // label check the contents of the module
    for (auto itr = this->_modules.begin(); itr != this->_modules.end(); itr++)
    {
        if(itr->second != nullptr)
        {
            labelCheck(itr->second);
        }
    }
}

ScopeInformation LabelChecker::labelCheck(AbstractSyntaxTree::Module* module)
{
    if(module == nullptr) return ScopeInformation();

    auto module_state = this->_module_check_mode.find(module);
    if (module_state == this->_module_check_mode.end())
        throw std::runtime_error("Module state wasn't found in label checking");

    // if the module was already label checked, return a scope info containing its symbols
    if (module_state->second)
        return ScopeInformation(module, module->getSymbolTable());

    ScopeInformation scope_info(module, module->getSymbolTable());

    // add the symbols of the imported modules to the scope info
    try {
        scope_info.includeImportedSymbols();
    } catch(SymbolRedefinitionException& err) {
        reportRedefinition(err, "Symbol");
    }

    // recursively check the labels of the nodes contained within this module
    for (auto* node : module->getCommands())
        labelCheck(scope_info, node);

    return std::move(scope_info);
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::Node* curr_node)
{
    if (curr_node == nullptr)
        return;

    switch (curr_node->getNodeType())
    {
    case AbstractSyntaxTree::Node::node_type::function_definition:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::FunctionDefinition*>(curr_node));
        break;
    case AbstractSyntaxTree::Node::node_type::statement:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::Statement*>(curr_node));
        break;
    case AbstractSyntaxTree::Node::node_type::type_expression:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::TypeExpression*>(curr_node));
        break;
    case AbstractSyntaxTree::Node::node_type::namespace_definition:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::NamespaceDefinition*>(curr_node));
        break;
    case AbstractSyntaxTree::Node::node_type::type_definition:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::TypeDefinition*>(curr_node));
        break;
    case AbstractSyntaxTree::Node::node_type::import_statement:
        // If it got here, it means an import nod wasn't replaced and it is in an inner scope
        ErrorRegister::reportError((new SemanticError(
            "Encountered a non global import statement",
            curr_node->getPosition()
        ))->withNote("Import statement must be in global scope"));
        break;
    case AbstractSyntaxTree::Node::node_type::module:
        // modules inside modules are import statements which are not label checked recursively
        break;
    default:
        // program execution would never get to here unless the semantic analyzer has been given invalid data
        ErrorRegister::reportError(new SemanticError("Encountered an invalid node type", curr_node->getPosition()));
        break;
    }
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::Statement* node)
{
    if (node == nullptr)
        return;

    switch (node->getStatementType())
    {
    case AbstractSyntaxTree::Statement::statement_type::expression:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::Expression*>(node));
        break;
    case AbstractSyntaxTree::Statement::statement_type::variable_declaration:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::VariableDeclaration*>(node));
        break;
    case AbstractSyntaxTree::Statement::statement_type::statement_block:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::StatementBlock*>(node));
        break;
    case AbstractSyntaxTree::Statement::statement_type::conditional_statement:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::ConditionalStatement*>(node));
        break;
    case AbstractSyntaxTree::Statement::statement_type::while_statement:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::WhileStatement*>(node));
        break;
    case AbstractSyntaxTree::Statement::statement_type::for_statement:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::ForStatement*>(node));
        break;
    case AbstractSyntaxTree::Statement::statement_type::return_statement:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::ReturnStatement*>(node));
        break;
    case AbstractSyntaxTree::Statement::statement_type::delete_function_statement:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::DeleteFunctionStatement*>(node));
        break;
    default:
        // program execution would never get to here unless the semantic analyzer has been given invalid data
        ErrorRegister::reportError(new SemanticError("Encountered an invalid node type", node->getPosition()));
        break;
    }
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::FunctionDefinition* node)
{
    if (node == nullptr) return;

    // label check the components - function name is known to be defined once (due to early symbol table initialization)
    labelCheck(scope_info, node->getReturnType());

    if (node->getInputType() != nullptr)
    {
        // add the input complex type to the table in a new scope
        ScopeObject obj(scope_info);

        // label check the components of the input type
        for (AbstractSyntaxTree::ComplexTypeComponent* component : node->getInputType()->getComponents())
        {
            labelCheck(scope_info, component->getType());

            if (component->getIdentifier() != nullptr)
            {
                // insert the identifier into the scope info
                try {
                    scope_info.addSymbol(component->getIdentifier()->getIdentifier(), component);
                } catch(SymbolRedefinitionException& err) {
                    reportRedefinition(err, "Complex type component");
                }
            }
        }

        node->getInputType()->setSymbolTable(scope_info.deepestScopeTable());

        // label check the body of the function
        labelCheck(scope_info, node->getCommands());
    }
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::TypeDefinition* node)
{
    if (node == nullptr) return;
    if (node->getTypeName() == nullptr) return;

    // insert the type into the scope info
    try {
        // add the symbol to the node only if the symbol table of the node is empty
        scope_info.addSymbol(node->getTypeName()->getIdentifier(), node);
    } catch(SymbolRedefinitionException& err) {
        // if the redefined symbol is the same node, do not report an error
        if (err.previous_definition != err.new_definition)
            reportRedefinition(err, "Type");
    }

    {
        // enter a new scope for the data members of the type
        ScopeObject obj(scope_info, node->getDataSymbolTable());

        // label check the type members
        for (auto* type_component : node->getTypeDataMembers())
            labelCheck(scope_info, type_component);
    }

    {
        // enter a new scope for the functions of the type
        ScopeObject obj(scope_info, node->getFunctionsSymbolTable());

        // label check the type functions
        for (auto* type_component : node->getTypeFunctions())
            labelCheck(scope_info, type_component);
    }
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceDefinition* node)
{
    if (node == nullptr) return;
    if (node->getIdentifier() == nullptr) return;

    auto* symbol_tab_def = scope_info.getDefinition(node->getIdentifier()->getIdentifier());

    // check that this namespace has been defined once 
    if (symbol_tab_def == nullptr)
        reportUndefined(node->getIdentifier()->getIdentifier(), node->getIdentifier());
    else if (symbol_tab_def != node)
        reportRedefinition(
            node->getIdentifier()->getIdentifier(),
            "Namespace", 
            node,
            symbol_tab_def
        );

    ScopeObject obj(scope_info, node->getSymbolTable()); 

    // Label check the inner commands of namespace
    for(auto* command : node->getCommands())
        labelCheck(scope_info, command);
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::TypeExpression* node)
{
    if (node == nullptr)
        return;
    
    switch (node->getTypeExpressionCategory())
    {
    case AbstractSyntaxTree::TypeExpression::type_expression_category::simple_type:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::SimpleType*>(node));
        break;
    case AbstractSyntaxTree::TypeExpression::type_expression_category::complex_type:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::ComplexType*>(node));
        break;
    case AbstractSyntaxTree::TypeExpression::type_expression_category::nameless_complex_type:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::NamelessComplexType*>(node));
        break;
    case AbstractSyntaxTree::TypeExpression::type_expression_category::pointer_type:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::PointerType*>(node));
        break;
    default:
        // program execution would never get to here unless the semantic analyzer has been given invalid data
        ErrorRegister::reportError(new SemanticError("Encountered an invalid node type", node->getPosition()));
        break;
    }
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::VariableDeclaration* node)
{
    if (node == nullptr)
        return;

    // label check the type of the declaration
    labelCheck(scope_info, node->getType());

    // Defining the inner variable declaration table
    SymbolTable new_symbol_table;
    
    for (AbstractSyntaxTree::VariableDeclarationComponent* var_decl : node->getVariables())
    {
        if (var_decl != nullptr && var_decl->getIdentifier() != nullptr)
        {
            // check the assignment expression
            labelCheck(scope_info, var_decl->getInitialization());
            
            // insert the variable into the scope info
            try {
                // add the symbol to the node only if the symbol table of the node is empty
                if (node->getSymbolTable().empty())
                    new_symbol_table.insert({var_decl->getIdentifier()->getIdentifier(), var_decl});
                
                scope_info.addSymbol(var_decl->getIdentifier()->getIdentifier(), var_decl);
            } catch(SymbolRedefinitionException& err) {
                // if the redefined symbol is the same node, do not report an error
                if (err.previous_definition != err.new_definition)
                    reportRedefinition(err, "Variable");
            }
        }
    }

    // For global scopes, there is no need to defined new symbol table for var declarations
    if (node->getSymbolTable().empty())
        node->setSymbolTable(new_symbol_table);
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::Expression* node)
{
    if (node == nullptr)
        return;

    switch (node->getExpressionType())
    {
    case AbstractSyntaxTree::Expression::expression_type::binary_operation:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::BinaryOperatorExpression*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::unary_operation:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::UnaryOperatorExpression*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::identifier_expression:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::IdentifierExpression*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::call_expression:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::CallExpression*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::cast_expression:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::CastExpression*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::member_access:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::MemberAccessExpression*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::namespace_access_expr:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::NamespaceAccessExpression*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::vargs_expression:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::VarArgsExpression*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::expression_list:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::ExpressionList*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::type_instantiation:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::TypeInstatiation*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::create_function_expression:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::CreateFunctionExpression*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::new_function_expression:
        labelCheck(scope_info, static_cast<AbstractSyntaxTree::NewFunctionExpression*>(node));
        break;
    case AbstractSyntaxTree::Expression::expression_type::literal_expression:
        // literal expressions are not depandent on the symbol table
        break;
    default:
        // program execution would never get to here unless the semantic analyzer has been given invalid data
        ErrorRegister::reportError(new SemanticError("Encountered an invalid node type", node->getPosition()));
        break;
    }
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::ExpressionList* node)
{
    if (node == nullptr)
        return;

    for(auto* obj : node->getExpressions())
    {
        labelCheck(scope_info, obj);
    }
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::CallExpression* node)
{
    if (node == nullptr)
        return;

    labelCheck(scope_info, node->getFunctionExpression());
    labelCheck(scope_info, node->getArguments());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::CastExpression* node)
{
    if (node == nullptr)
        return;

    labelCheck(scope_info, node->getInnerExpression());
    labelCheck(scope_info, node->getTargetType());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* node)
{
    if (node == nullptr) return;
    if (node->getLeftExpression() == nullptr) return;
    if (node->getRightExpression() == nullptr) return;

    // set the referenced nodes of this namespace access expression
    setReferencedNodes(scope_info, node);
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node)
{
    if (node == nullptr) return;
    if (node->getLeftExpression() == nullptr) return;
    if (node->getRightExpression() == nullptr) return;

    switch (node->getAccessType())
    {
    case AbstractSyntaxTree::MemberAccessExpression::AccessType::index_acs:
        labelCheck(scope_info, node->getLeftExpression());
        labelCheck(scope_info, node->getRightExpression());
        break;
    case AbstractSyntaxTree::MemberAccessExpression::AccessType::pointer_member_access:
    case AbstractSyntaxTree::MemberAccessExpression::AccessType::member_acs:
        labelCheck(scope_info, node->getLeftExpression());
        break;
    default:
        break;
    }
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::VarArgsExpression* node)
{
    if (node == nullptr) return;
    
    // Check the type
    labelCheck(scope_info, node->getTypeToGet());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::TypeInstatiation* node)
{
    if (node == nullptr) return;
    
    // Check the type and the parameters
    labelCheck(scope_info, node->getType());
    labelCheck(scope_info, node->getParameters());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::StatementBlock* node)
{
    if (node == nullptr)
        return;

    // enter a new scope
    ScopeObject obj(scope_info);
    
    for (AbstractSyntaxTree::Statement* statment : node->getCommands())
    {
        labelCheck(scope_info, statment);
    }

    node->setSymbolTable(scope_info.deepestScopeTable());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::ReturnStatement* node)
{
    if (node == nullptr)
        return;

    // check the components of the expression
    labelCheck(scope_info, node->getReturnValue());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node)
{
    if (node == nullptr)
        return;

    // check the components of the expression
    labelCheck(scope_info, node->getLeftExpression());
    labelCheck(scope_info, node->getRightExpression());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::UnaryOperatorExpression* node)
{
    if (node == nullptr)
        return;

    // check the components of the expression
    labelCheck(scope_info, node->getExpression());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::IdentifierExpression* node)
{
    if (node == nullptr)
        return;

    auto* definition = scope_info.getDefinition(node->getIdentifier());

    if (definition == nullptr)
        this->reportUndefined(node->getIdentifier(), node);
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::SimpleType* node)
{
    if (node == nullptr || node->getIdentifier().empty())
        return;

    // check if the type has either been defined or is a primitive
    if (node->getIdentifier() != TypeSystemInfo::generic_type && TypeSystemInfo::primitive_type_mapping.find(node->getIdentifier()) == 
                                                    TypeSystemInfo::primitive_type_mapping.end())
    {
        if (!scope_info.isDefined(node->getIdentifier()))
            this->reportUndefined(node->getIdentifier(), node);
    }
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::ComplexType* node)
{
    if (node == nullptr)
        return;

    // enter a new scope (the complex type components are only a part of the type itself)
    ScopeObject obj(scope_info);

    for (AbstractSyntaxTree::ComplexTypeComponent* component : node->getComponents())
    {
        labelCheck(scope_info, component->getType());
        
        // make sure that the inner parameter is not defined in the same scope already
        if (component->getIdentifier() != nullptr)
        {
            AbstractSyntaxTree::Node* definition = scope_info.getDefinitionCurrentScope(component->getIdentifier()->getIdentifier());

            if (definition == nullptr)
                scope_info.addSymbol(component->getIdentifier()->getIdentifier(), node);
            else
                this->reportRedefinition(component->getIdentifier()->getIdentifier(), "Complex Type component", component, definition);
        }
    }

    node->setSymbolTable(scope_info.deepestScopeTable());
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::NamelessComplexType* node)
{
    if (node == nullptr) return;

    for (AbstractSyntaxTree::TypeExpression* component : node->getComponents())
    {
        labelCheck(scope_info, component);
    }
}

AbstractSyntaxTree::Node* LabelChecker::setReferencedNodes(ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* ns_expr)
{
    if (ns_expr == nullptr) return nullptr;
    if (ns_expr->getRightExpression()->getExpressionType() != 
        AbstractSyntaxTree::MemberAccessExpression::expression_type::identifier_expression)
    {
        ErrorRegister::reportError((new SemanticError(
            "Expected an identifier",
            ns_expr->getRightExpression()->getPosition()
        ))->withNote("To access an element of a namespace / type, an identifier must be used"));

        return nullptr;
    }

    AbstractSyntaxTree::Node* definition = nullptr;

    // get the definition of the left expression
    if (ns_expr->getLeftExpression()->getExpressionType() == 
        AbstractSyntaxTree::MemberAccessExpression::expression_type::identifier_expression)
    {
        auto* identifier_expr = static_cast<AbstractSyntaxTree::IdentifierExpression*>(ns_expr->getLeftExpression());
        
        // get the definition of the left expression
        definition = scope_info.getDefinition(identifier_expr->getIdentifier());
        
        // check that the definition exists
        if (definition == nullptr)
        {
            reportUndefined(identifier_expr->getIdentifier(), identifier_expr);

            return nullptr;
        }
    }
    // if the left node is a member access expression, get the referenced node of the left expression
    else if (ns_expr->getLeftExpression()->getExpressionType() == AbstractSyntaxTree::Expression::expression_type::namespace_access_expr)
    {
        definition = setReferencedNodes(
            scope_info,
            static_cast<AbstractSyntaxTree::NamespaceAccessExpression*>(ns_expr->getLeftExpression())
        );

        if (definition == nullptr)
            return nullptr;
    }

    // evaluate the right identifier using the left expression
    switch (definition->getNodeType())
    {
    case AbstractSyntaxTree::Node::node_type::namespace_definition:
    {
        auto* ns = static_cast<AbstractSyntaxTree::NamespaceDefinition*>(definition);
        const auto& identifier = static_cast<AbstractSyntaxTree::IdentifierExpression*>(ns_expr->getRightExpression())->getIdentifier();
        auto* inner_def = getDefinition(ns->getSymbolTable(), identifier);

        // check that the right identifier is a member of the namespace
        if (inner_def == nullptr)
        {
            ErrorRegister::reportError(new SemanticError(
                "'" + identifier + "' is not a member of namespace " + ns->getIdentifier()->getIdentifier(), 
                ns_expr->getRightExpression()->getPosition()
            ));

            return nullptr;
        }

        ns_expr->setReferencedNode(inner_def);

        break;
    }
    case AbstractSyntaxTree::Node::node_type::type_definition:
    {
        auto* ty = static_cast<AbstractSyntaxTree::TypeDefinition*>(definition);
        const auto& identifier = static_cast<AbstractSyntaxTree::IdentifierExpression*>(ns_expr->getRightExpression())->getIdentifier();
        auto* inner_def = getDefinition(ty->getFunctionsSymbolTable(), identifier);

        // check that the right identifier is a member of the namespace
        if (inner_def == nullptr)
        {
            ErrorRegister::reportError(new SemanticError(
                "'" + identifier + "' is not a member of namespace " + ty->getTypeName()->getIdentifier(), 
                ns_expr->getRightExpression()->getPosition()
            ));

            return nullptr;
        }

        ns_expr->setReferencedNode(inner_def);

        break;
    }
    default:
        ErrorRegister::reportError(new SemanticError(
            "Only namespaces and types can be accessed statically (using '::')", 
            ns_expr->getLeftExpression()->getPosition()
        ));
        break;
    }
    
    return ns_expr->getReferencedNode();
}

void LabelChecker::labelCheck(ScopeInformation& scope_info, AbstractSyntaxTree::PointerType* node)
{
    if (node == nullptr) return;

    labelCheck(scope_info, node->getBaseType());
}