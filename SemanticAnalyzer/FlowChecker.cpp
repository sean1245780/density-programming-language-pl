#include "FlowChecker.h"

FlowChecker::FlowChecker()
{
    _main_entry_point = nullptr;

    if (TypeSystemInfo::getGlobalContainer() == nullptr)
        throw std::runtime_error("The type system information has not been initialized!");
}

bool FlowChecker::checkReturnValidity(AbstractSyntaxTree::Statement* node)
{
    if(node == nullptr) return false; // Check it out!
    
    switch (node->getStatementType())
    {
    case AbstractSyntaxTree::Statement::statement_type::statement_block:
        return checkReturnValidity(static_cast<AbstractSyntaxTree::StatementBlock*>(node));
    case AbstractSyntaxTree::Statement::statement_type::conditional_statement:
        return checkReturnValidity(static_cast<AbstractSyntaxTree::ConditionalStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::while_statement:
        return checkReturnValidity(static_cast<AbstractSyntaxTree::WhileStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::for_statement:
        return checkReturnValidity(static_cast<AbstractSyntaxTree::ForStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::return_statement:
        node->setHasReturn(true);
        return true;
    }

    return false;
}

bool FlowChecker::checkReturnValidity(AbstractSyntaxTree::StatementBlock* node)
{
    if(node == nullptr) return false; // Check it out!
    
    const auto& commands = node->getCommands();
    bool flag = false;

    for (long i = 0; i < commands.size(); i++)
    {
        if (flag)
        {
            if (commands[i] != nullptr)
                ErrorRegister::reportWarning(new SemanticWarning(
                    "Detected unreachable code due to an early return",
                    commands[i]->getPosition()
                ));
            else 
                ErrorRegister::reportWarning(new SemanticWarning(
                    "Detected unreachable code due to an early return in this scope",
                    node->getPosition()
                ));
        }
        else flag = checkReturnValidity(commands[i]);
    }

    node->setHasReturn(flag);
    return flag;
}

bool FlowChecker::checkReturnValidity(AbstractSyntaxTree::ConditionalStatement* node)
{
    if(node == nullptr) return false; // Check it out!
    if(node->getCondition() == nullptr) return false;
    if(node->getIfCommands() == nullptr) return false;

    checkNonModStatements(node->getIfCommands());

    if (node->getElseCommands() == nullptr) { checkReturnValidity(node->getIfCommands()); return false;}

    checkNonModStatements(node->getElseCommands());

    node->setHasReturn(checkReturnValidity(node->getIfCommands()) & checkReturnValidity(node->getElseCommands()));
    return node->getHasReturn();
}

bool FlowChecker::checkReturnValidity(AbstractSyntaxTree::WhileStatement* node)
{
    if(node == nullptr) return false; // Check it out!
    if (node->getCommands() == nullptr) return false;

    checkNonModStatements(node->getCommands());
    checkReturnValidity(node->getCommands());

    // while statements do not gurentee that a return statement would be called
    node->setHasReturn(false);
    return node->getHasReturn();
}

bool FlowChecker::checkReturnValidity(AbstractSyntaxTree::ForStatement* node)
{
    if(node == nullptr) return false; // Check it out!
    if (node->getCommands() == nullptr) return false;

    checkNonModStatements(node->getCommands());
    checkReturnValidity(node->getCommands());

    // for statements do not gurentee that a return statement would be called
    node->setHasReturn(false);
    return node->getHasReturn();
}

void FlowChecker::checkNonModStatements(AbstractSyntaxTree::Statement* node)
{
    if (node == nullptr) return;

    if (node->getStatementType() == AbstractSyntaxTree::Statement::statement_type::statement_block)
    {
        auto* statement_block = static_cast<AbstractSyntaxTree::StatementBlock*>(node);
        
        for (auto* statement : statement_block->getCommands())
        {
            checkNonModStatements(statement);
        }

        return;
    } else if (node->getStatementType() != AbstractSyntaxTree::Statement::statement_type::expression)
        return;

    auto* expression = static_cast<AbstractSyntaxTree::Expression*>(node);
    if (expression->getExpressionType() != AbstractSyntaxTree::Expression::expression_type::binary_operation)
        return;

    auto* bin_expression = static_cast<AbstractSyntaxTree::BinaryOperatorExpression*>(expression);
    if (OperatorsInfo::inst().getOperator(bin_expression->getOperator())->bin_mods.output !=
        operator_mod::reference)
    {
        ErrorRegister::reportError((new SemanticError(
            "Encountered a non-modfiying binary expression",
            node->getPosition()
        ))->withNote("Operators that do not modify a value cannot exist as regular commands"));
    }
}

void FlowChecker::flowCheckGlobal(const vector<AbstractSyntaxTree::Node*>& nodes)
{
    for (AbstractSyntaxTree::Node* node : nodes)
    {
        flowCheckGlobal(node);
    }
}

void FlowChecker::flowCheckGlobal(AbstractSyntaxTree::Node* node)
{
    if (node == nullptr) return;

    switch (node->getNodeType())
    {
    case AbstractSyntaxTree::Node::node_type::function_definition:
        flowCheckGlobal(static_cast<AbstractSyntaxTree::FunctionDefinition*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::namespace_definition:
        flowCheckGlobal(static_cast<AbstractSyntaxTree::NamespaceDefinition*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::type_definition:
        flowCheckGlobal(static_cast<AbstractSyntaxTree::TypeDefinition*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::statement:
        flowCheckGlobal(static_cast<AbstractSyntaxTree::Statement*>(node));
        break;
    }
}

void FlowChecker::flowCheckGlobal(AbstractSyntaxTree::Statement* node)
{
    if (node == nullptr) return;
    
    switch (node->getStatementType())
    {
    case AbstractSyntaxTree::Statement::statement_type::return_statement:
        ErrorRegister::reportError(new SemanticError("Return statement is outside of function", 
            node->getPosition()));
        break;
    case AbstractSyntaxTree::Statement::statement_type::conditional_statement:
        ErrorRegister::reportError(new SemanticError("Conditional statements are not allowed in global scope", 
            node->getPosition()));
        break;
    case AbstractSyntaxTree::Statement::statement_type::expression:
        ErrorRegister::reportError(new SemanticError("Expression statements are not allowed in global scope", 
            node->getPosition()));
        break;
    case AbstractSyntaxTree::Statement::statement_type::statement_block:
        ErrorRegister::reportError(new SemanticError("Statement blocks are not allowed in global scope", 
            node->getPosition()));
        break;
    case AbstractSyntaxTree::Statement::statement_type::for_statement:
        ErrorRegister::reportError(new SemanticError("For statements are not allowed in global scope", 
            node->getPosition()));
        break;
    case AbstractSyntaxTree::Statement::statement_type::while_statement:
        ErrorRegister::reportError(new SemanticError("While statements are not allowed in global scope", 
            node->getPosition()));
        break;
    case AbstractSyntaxTree::Statement::statement_type::variable_declaration:
        flowCheckGlobal(static_cast<AbstractSyntaxTree::VariableDeclaration*>(node));
        break;
    }
}

void FlowChecker::flowCheckGlobal(AbstractSyntaxTree::FunctionDefinition* node)
{
    if(node == nullptr) return; 
    if (node->getFunctionName() == nullptr) return;
    if (node->getReturnType() == nullptr) return;
    if (node->getReturnType()->getSynthesizedType() == nullptr) return;

    // check for main function entry point
    if (node->getFunctionName()->getIdentifier() == "main")
    {
        auto* void_type = TypeSystemInfo::getGlobalContainer()->getType("void");
        auto* int_type = TypeSystemInfo::getGlobalContainer()->getType("i32");
        bool valid = true;

        // check possible return types (void, int)
        if (!node->getReturnType()->getSynthesizedType()->compare(void_type) &&
            !node->getReturnType()->getSynthesizedType()->compare(int_type))
        {
            ErrorRegister::reportError(new SemanticError("Return type of main function must be @ or int", 
                    node->getPosition()));            
            valid = false;
        }
        
        // check possible input types (void)
        if (!node->getInputType()->getSynthesizedType()->compare(void_type))
        {
            ErrorRegister::reportError(new SemanticError("Input type of main function must be empty", 
                    node->getPosition()));
            valid = false;
        }
        
        if (valid)
        {
            if (_main_entry_point != nullptr)
            {
                ErrorRegister::reportError((new SemanticError("Found multiple main entry points", 
                        node->getPosition()))->withNote("Found multiple main entry points", _main_entry_point->getPosition()));
            }   
            else _main_entry_point = node;
        }
    }

    // check for ctors / dtors in global scope
    if (node->getParentType() == nullptr &&
        (node->getFunctionName()->getIdentifier() == CONSTRUCTOR_KEYWORD ||
        node->getFunctionName()->getIdentifier() == DESTRUCTOR_KEYWORD))
    {
        ErrorRegister::reportError(new SemanticError(
            "Constructors and destructors must be within a type",
            node->getPosition()
        ));
    }

    // check return validity
    if (node->getCommands() != nullptr && !checkReturnValidity(node->getCommands()))
    {
        if(!node->getReturnType()->getSynthesizedType()->compare(TypeSystemInfo::getGlobalContainer()->getType("void")))
        {
            ErrorRegister::reportError(new SemanticError("Not all paths return a value", 
                node->getPosition()));
        }
    }

    if (node->getProperties().is_extern && node->getCommands() != nullptr)
    {
        ErrorRegister::reportError(new SemanticError(
            "Function declared as extern can't have any commands", 
            node->getCommands()->getPosition()
        ));
    }
    else if (!node->getProperties().is_extern && node->getCommands() == nullptr)
    {
        ErrorRegister::reportError(new SemanticError(
            "Non external functions must have a body", 
            node->getPosition()
        ));        
    }

    checkNonModStatements(node->getCommands());

    flowCheck(node->getCommands());
}

void FlowChecker::flowCheckGlobal(AbstractSyntaxTree::NamespaceDefinition* node)
{
    if (node == nullptr) return;

    for(auto* cmd : node->getCommands())
    {
        flowCheckGlobal(cmd);
    }
}

void FlowChecker::flowCheckGlobal(AbstractSyntaxTree::TypeDefinition* node)
{
    if (node == nullptr) return;

    // flow check the functions of the type 
    for(auto* cmd : node->getTypeFunctions())
        flowCheckGlobal(cmd);

    // flow check the data members of the type
    for(auto* cmd : node->getTypeDataMembers())
        flowCheckGlobal(cmd);
}

void FlowChecker::flowCheck(AbstractSyntaxTree::Statement* node)
{
    if (node == nullptr) return;
    
    switch (node->getStatementType())
    {
    case AbstractSyntaxTree::Statement::statement_type::conditional_statement:
        flowCheck((static_cast<AbstractSyntaxTree::ConditionalStatement*>(node))->getIfCommands());
        flowCheck((static_cast<AbstractSyntaxTree::ConditionalStatement*>(node))->getElseCommands());
        break;
    case AbstractSyntaxTree::Statement::statement_type::statement_block:
    {
        auto* statement_block = static_cast<AbstractSyntaxTree::StatementBlock*>(node);

        // flow check the inner statements
        for (auto* statement : statement_block->getCommands())
            flowCheck(statement);

        break;
    }
    case AbstractSyntaxTree::Statement::statement_type::for_statement:
    {
        auto* for_statement = static_cast<AbstractSyntaxTree::ForStatement*>(node);
        
        // flow check the inner statements
        flowCheck(for_statement->getCommands());
        flowCheck(for_statement->getInitialization());

        break;
    }
    case AbstractSyntaxTree::Statement::statement_type::while_statement:
        flowCheck(static_cast<AbstractSyntaxTree::WhileStatement*>(node)->getCommands());
        break;
    case AbstractSyntaxTree::Statement::statement_type::variable_declaration:
        flowCheck(static_cast<AbstractSyntaxTree::VariableDeclaration*>(node));
        break;
    }
}

void FlowChecker::flowCheck(AbstractSyntaxTree::VariableDeclaration* node)
{
    if(node == nullptr) return;

    if (node->getProperties().is_extern)
    {
        ErrorRegister::reportError(new SemanticError(
            "Local variables can't be declared as extern", 
            node->getPosition()));
    }
}

void FlowChecker::flowCheckGlobal(AbstractSyntaxTree::VariableDeclaration* node)
{
    if(node == nullptr) return;

    if (node->getProperties().is_extern)
    {
        for(auto* var : node->getVariables())
        {
            if (var->getInitialization() != nullptr)
            {
                ErrorRegister::reportError(new SemanticError(
                    "Global variables declared as extern can't be initialized", 
                    var->getInitialization()->getPosition(),
                    "Can't be initialized"
                    ));
                return;
            }
        }
    }
}

void FlowChecker::flowCheck(const unordered_map<string, Module*>& modules)
{
    FlowChecker flowChecker;

    for (const auto& module : modules)
        flowChecker.flowCheckGlobal(module.second->getCommands());

    if (flowChecker._main_entry_point == nullptr)
        ErrorRegister::reportError((new SemanticError("Could not find main function"))->withHelp(
            "Add a main function - code execution will start from main()"
        ));
}
