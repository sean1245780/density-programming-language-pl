#pragma once

#include "llvm/IR/Value.h"
#include "llvm/IR/Function.h"

#include "../Utilities/Errors/CompilerErrors.h"
#include "../Utilities/DefinedObjs/TypeSystemInfo.h"
#include "../Utilities/DefinedObjs/Operators.h"
#include "ScopeInfo.h"

using CompilerErrors::ErrorRegister;
using CompilerErrors::SemanticError;
using CompilerErrors::CompilationError;

class TypeChecker
{
private:
    const vector<pair<string, Module*>>& _modules;
    llvm::LLVMContext& _context;

    TypeChecker(const vector<pair<string, Module*>>& modules) 
        : _modules(modules), _context(TypeSystemInfo::getGlobalLLVMContext()) {}

    // Top level checks
    void typeCheck();

    void typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::Node* node);
   
    void typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::Statement* node);
    void typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::VariableDeclaration* node);
    void typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::StatementBlock* node);

    void typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::FunctionDefinition* node);
    void typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceDefinition* node);
    void typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::TypeDefinition* node);

    /*
        Check return type functions recursively check that there is no 'ret' statement which 
        returns a type that does not match the return type of the function
    */
    void checkReturnType(ScopeInformation& scope_info, AbstractSyntaxTree::Statement* statement, SynthesizedType* function_ret_type);
    void checkReturnType(ScopeInformation& scope_info, AbstractSyntaxTree::StatementBlock* statement, SynthesizedType* function_ret_type);
    void checkReturnType(ScopeInformation& scope_info, AbstractSyntaxTree::ConditionalStatement* statement, SynthesizedType* function_ret_type);
    void checkReturnType(ScopeInformation& scope_info, AbstractSyntaxTree::WhileStatement* statement, SynthesizedType* function_ret_type);
    void checkReturnType(ScopeInformation& scope_info, AbstractSyntaxTree::ForStatement* statement, SynthesizedType* function_ret_type);

    /*
        The generate type info pass generates synthesized types to all nodes which require a
        synthesized type to be generated
    */
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::Node* node);

    void generateTypeInfo(const string& module_name, AbstractSyntaxTree::Module* module);

    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::Statement* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::StatementBlock* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ConditionalStatement* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::WhileStatement* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ForStatement* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::FunctionDefinition* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceDefinition* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::TypeDefinition* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::TypeExpression* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::Module* node);

    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::SimpleType* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ComplexType* node);
    SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NamelessComplexType* type_expr);

	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::VariableDeclaration* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::PointerType* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ReturnStatement* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::Expression* node); 
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::UnaryOperatorExpression* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::IdentifierExpression* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ExpressionList* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::CallExpression* node, CustomType* called_object = nullptr);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::CastExpression* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::VarArgsExpression* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::TypeInstatiation* node, bool must_custom = true);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::CreateFunctionExpression* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NewFunctionExpression* node);
	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::DeleteFunctionStatement* node);

	SynthesizedType* generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::LiteralExpression* node); 

    /*
        This function generates type info for the member access expression assuming that the
        right expression of the member access is an integer literal.
    */
    SynthesizedType* generateTypeInfoIndexAccess(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node, SynthesizedType* left_ty);

    /*
        This function generates type info for the member access expression assuming that the
        right expression of the member access is an identifier.
    */
    SynthesizedType* generateTypeInfoIdentifierAccess(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node, SynthesizedType* left_ty);

    /*
        This function generates type info for the member access expression assuming that the
        right expression of the member access is a call expression.
    */
    SynthesizedType* generateTypeInfoCallAccess(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node, SynthesizedType* left_ty);

    /*
        Check modifiable functions set the modifiable attribute of the expression given (and possibly
        expressions contained in the given expression)
    */
	AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::Expression* node); 
	AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node);
	AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::UnaryOperatorExpression* node);
	AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::IdentifierExpression* node);
	AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ExpressionList* node);
	AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::CallExpression* node);
	AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::LiteralExpression* node); 
    AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node);
    AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* node);
    AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::VarArgsExpression* node);
    AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::TypeInstatiation* node);
    AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NewFunctionExpression* node);
    AbstractSyntaxTree::Expression::modifiable_type checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::CreateFunctionExpression* node);

    // Auxiliary functions
    void reportTypeMismatch(SynthesizedType* first, SynthesizedType* second, const position_data& pos1, const position_data& pos2);
    void reportTypeMismatch(SynthesizedType* first, SynthesizedType* second, const position_data& pos1);

    void reportOperatorMismatch(SynthesizedType* left, SynthesizedType* right, const string& op, const position_data& pos);
    void reportOperatorMismatch(SynthesizedType* inner, const string& op, const position_data& pos);

    /*
        The function returns the synthesized type of the variable declaration by checking the
        type of the assignment expression if it exists
    */
    SynthesizedType* getDeclarationType(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::VariableDeclaration* node);

    /*
        The function returns the synthesized type of the function definition
    */
    FunctionType* getDeclarationType(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::FunctionDefinition* node);

    /*
        The function returns the synthesized type of the element of the complex type at the
        specified index while performing index bounds check
    */
    SynthesizedType* typeCheckComplexIndexAccess(SynthesizedType* complex_type, unsigned int index, const position_data& position);

    /*
        These functions return the synthesized type of a unary / binary operator expression using 
        operator synthesis metadata
    */
    SynthesizedType* getTypeByOperator(TypeContainer& type_container, SynthesizedType* inner, bool postfix, const array<TypeSystemInfo::type_synthesis_action, 3>& operator_synthesis);
    SynthesizedType* getTypeByOperator(TypeContainer& type_container, SynthesizedType* inner, const array<TypeSystemInfo::type_synthesis_action, 3>& operator_synthesis);

public:
    static void typeCheck(const vector<pair<string, Module*>>& modules);
};