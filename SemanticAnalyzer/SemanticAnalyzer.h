#pragma once

#include <thread>

#include "../Utilities/AbstractSyntaxTree/Nodes.h"

#include "ScopeInfo.h"
#include "FlowChecker.h"
#include "LabelChecker.h"
#include "TypeChecker.h"

using AbstractSyntaxTree::SymbolTable;
using AbstractSyntaxTree::Module;

class ImportError
{
public:
    enum class import_err_type { looping_import, module_not_found }; 
private:
    string import_err = "";
    vector<position_data> inclusion_paths;

public:
    ImportError(const string& file, const import_err_type& err_type) { 
        switch(err_type){
        case import_err_type::looping_import:
            import_err += "Detected looping import from file ";
            break;
        case import_err_type::module_not_found:
            import_err += "Could not find module ";
            break;
        default:
            import_err += "Could not generate type information for module ";
            break;
        }
        import_err += file; 
    }

    void addIncludedFrom(const position_data& pos) { 
        inclusion_paths.push_back(pos);
    }

    operator CompilationError*() const {
        CompilationError* err = new SemanticError(import_err);
        for (const position_data& pos : this->inclusion_paths)
            err = err->withNote(import_err + ": Imported from here", pos);
        return err;
    }
};

class SemanticAnalyzer
{
private:
    enum class import_state { not_imported, importing, finished_importing };

    std::unordered_map<string, Module*> _modules;
    unordered_map<string, import_state> _modules_import_states;
    vector<pair<string, Module*>> _merged_modules;

    void wrapRawModule();
    void mergeImportedModules();
    void mergeImportedModules(const string& module_name, Module* module);
    void flowCheck();
    void typeCheck();
    void labelCheck();

    SemanticAnalyzer(const unordered_map<IDataAccess*, vector<AbstractSyntaxTree::Node*>>& tree);

public:
    static std::vector<pair<string, Module*>> decorate(const unordered_map<IDataAccess*, vector<AbstractSyntaxTree::Node*>>& tree);

};