#include "TypeChecker.h"

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::Node* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    switch (node->getNodeType())
    {
    case AbstractSyntaxTree::Node::node_type::function_definition:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::FunctionDefinition*>(node));
    case AbstractSyntaxTree::Node::node_type::namespace_definition:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::NamespaceDefinition*>(node));
    case AbstractSyntaxTree::Node::node_type::statement:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::Statement*>(node));
    case AbstractSyntaxTree::Node::node_type::type_expression:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::TypeExpression*>(node));
    case AbstractSyntaxTree::Node::node_type::type_definition:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::TypeDefinition*>(node));
    case AbstractSyntaxTree::Node::node_type::module:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::Module*>(node));
    default:
        // program execution would never get to h unless the semantic analyzer has been given invalid data
        ErrorRegister::reportError(new SemanticError("Encountered an invalid node type", node->getPosition()));
    }

    return nullptr;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::Module* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    // generate type info for all nodes in this module
    for (auto* inner_node : node->getCommands())
        generateTypeInfo(type_container, scope_info, inner_node);
    
    node->setSynthesizedType(TypeSystemInfo::getGlobalContainer()->getType("module"));

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::LiteralExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();
        
    SynthesizedType* type = nullptr;
    
    switch (node->getLiteralType())
    {
    case AbstractSyntaxTree::LiteralExpression::literal_type::bool_type:
        type = TypeSystemInfo::getGlobalContainer()->getType("bool");
        break;
    case AbstractSyntaxTree::LiteralExpression::literal_type::char_type:
        type = TypeSystemInfo::getGlobalContainer()->getType("char");
        break;
    case AbstractSyntaxTree::LiteralExpression::literal_type::int_type:
        type = TypeSystemInfo::getGlobalContainer()->getType("i32");
        break;
    case AbstractSyntaxTree::LiteralExpression::literal_type::double_type:
        type = TypeSystemInfo::getGlobalContainer()->getType("f64");
        break;
    case AbstractSyntaxTree::LiteralExpression::literal_type::string_type:
        type = TypeSystemInfo::getGlobalContainer()->getOrCreatePointerType(
            TypeSystemInfo::getGlobalContainer()->getType("char"));
        break;
    case AbstractSyntaxTree::LiteralExpression::literal_type::null_type:
        type = TypeSystemInfo::getGlobalContainer()->getOrCreatePointerType(
            TypeSystemInfo::getGlobalContainer()->getType("i8"));
        break;
    default:
        // program execution would never get to here unless the semantic analyzer has been given invalid data
        ErrorRegister::reportError(new SemanticError("Encountered an invalid node type", node->getPosition()));
        break;
    }

    node->setSynthesizedType(type);
    
    return type;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::Statement* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();
    
    switch (node->getStatementType())
    {
    case AbstractSyntaxTree::Statement::statement_type::expression:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::Expression*>(node));        
    case AbstractSyntaxTree::Statement::statement_type::variable_declaration:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::VariableDeclaration*>(node));
    case AbstractSyntaxTree::Statement::statement_type::conditional_statement:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::ConditionalStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::while_statement:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::WhileStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::for_statement:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::ForStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::statement_block:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::StatementBlock*>(node));
    case AbstractSyntaxTree::Statement::statement_type::return_statement:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::ReturnStatement*>(node));
    case AbstractSyntaxTree::Statement::statement_type::delete_function_statement:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::DeleteFunctionStatement*>(node));
    default:
        // program execution would never get to here unless the semantic analyzer has been given invalid data
        ErrorRegister::reportError(new SemanticError("Encountered an invalid node type", node->getPosition()));
        break;
    }
    
    return nullptr;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::TypeExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    SynthesizedType* type = nullptr;

    switch (node->getTypeExpressionCategory())
    {
    case AbstractSyntaxTree::TypeExpression::type_expression_category::simple_type:
        type = generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::SimpleType*>(node));
        break;
    case AbstractSyntaxTree::TypeExpression::type_expression_category::complex_type:
        type = generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::ComplexType*>(node));
        break;
    case AbstractSyntaxTree::TypeExpression::type_expression_category::nameless_complex_type:
        type = generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::NamelessComplexType*>(node));
        break;
    case AbstractSyntaxTree::TypeExpression::type_expression_category::pointer_type:
        type = generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::PointerType*>(node));
        break;
    default:
        // program execution would never get to here unless the semantic analyzer has been given invalid data
        ErrorRegister::reportError(new SemanticError("Encountered an invalid node type", node->getPosition()));
        break;
    }

    node->setSynthesizedType(type);

    return type;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::FunctionDefinition* node)
{  
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();
    if (node->getFunctionName() == nullptr) return nullptr;
    
    auto* function_type = getDeclarationType(type_container, scope_info, node);

    if (function_type == nullptr) return nullptr;
    
    // set the types
    node->setSynthesizedType(function_type);
    node->getFunctionName()->setSynthesizedType(function_type);

    // create a new container for the body of the function
    type_container.enterDefinition(node->getFunctionName()->getIdentifier());
    ScopeObject obj(scope_info, node->getInputType()->getSymbolTable());

    generateTypeInfo(type_container, scope_info, node->getCommands());

    type_container.exitDefinition();

    return function_type;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceDefinition* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();
    if (node->getIdentifier() == nullptr) return nullptr;
    
    ScopeObject obj(scope_info, node->getSymbolTable());
    type_container.enterDefinition(node->getIdentifier()->getIdentifier());

    // generate type info for all of the inner commands
    for(auto* command : node->getCommands())
        generateTypeInfo(type_container, scope_info, command);
    
    // set the types
    node->setSynthesizedType(TypeSystemInfo::getGlobalContainer()->getType("namespace"));
    node->getIdentifier()->setSynthesizedType(node->getSynthesizedType());
    
    // exit out of the type container definition
    type_container.exitDefinition();

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::TypeDefinition* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();
    if (node->getTypeName() == nullptr) return nullptr;

    // create the type and set it
    auto* type = new CustomType(node->getTypeName()->getIdentifier());

    type->setDefinition(node);

    node->setSynthesizedType(type);
    node->getTypeName()->setSynthesizedType(type);

    // add the new type to the type container
    type_container.getOrCreateType(node->getTypeName()->getIdentifier(), [type](){ return type; });

    unordered_map<string, FunctionType*> functions;
    vector<pair<string, SynthesizedType*>> data_members;

    type_container.enterDefinition(node->getTypeName()->getIdentifier());

    {
        // enter functions scope
        ScopeObject obj(scope_info, node->getFunctionsSymbolTable());

        AbstractSyntaxTree::FunctionDefinition* constructor = nullptr;
        AbstractSyntaxTree::FunctionDefinition* destructor = nullptr;

        // create the functions types and assign them to the type
        for (AbstractSyntaxTree::FunctionDefinition* func : node->getTypeFunctions())
        {
            if (func->getFunctionName()->getIdentifier() == CONSTRUCTOR_KEYWORD)
                constructor = func;
            else if (func->getFunctionName()->getIdentifier() == DESTRUCTOR_KEYWORD)
                destructor = func;

            func->setParentType(node);

            functions.insert({
                func->getFunctionName()->getIdentifier(),
                getDeclarationType(type_container, scope_info, func)
            });
        }

        // create a default constructor if no constructor exists
        if (constructor == nullptr)
        {
            constructor = new AbstractSyntaxTree::FunctionDefinition(node->getPosition());

            // create the constructor
            constructor->setParentType(node);
            constructor->setReturnType(new AbstractSyntaxTree::ComplexType(node->getPosition()));

            constructor->setFunctionName(
                new AbstractSyntaxTree::IdentifierExpression(node->getPosition(), CONSTRUCTOR_KEYWORD)
            );
            constructor->setCommands(
                new AbstractSyntaxTree::StatementBlock(node->getPosition())
            );

            auto* input_type = new AbstractSyntaxTree::ComplexType(node->getPosition());

            input_type->addComponent(
                new AbstractSyntaxTree::PointerType(
                    node->getPosition(),
                    new AbstractSyntaxTree::SimpleType(node->getPosition(), SELF_TYPE_KEYWORD)
                ),
                new AbstractSyntaxTree::IdentifierExpression(node->getPosition(), SELF_TYPE_KEYWORD)
            );

            constructor->setInputType(input_type);

            functions.insert({ 
                CONSTRUCTOR_KEYWORD, 
                static_cast<FunctionType*>(constructor->getSynthesizedType())
            });

            node->getTypeFunctions().push_back(constructor);
            node->getFunctionsSymbolTable().insert({ CONSTRUCTOR_KEYWORD, constructor });
        }

        // create a default destructor if no destructor exists
        if (destructor == nullptr)
        {
            destructor = new AbstractSyntaxTree::FunctionDefinition(node->getPosition());

            // create the destructor
            destructor->setParentType(node);
            destructor->setReturnType(new AbstractSyntaxTree::ComplexType(node->getPosition()));

            destructor->setFunctionName(
                new AbstractSyntaxTree::IdentifierExpression(node->getPosition(), DESTRUCTOR_KEYWORD)
            );
            destructor->setCommands(
                new AbstractSyntaxTree::StatementBlock(node->getPosition())
            );

            auto* input_type = new AbstractSyntaxTree::ComplexType(node->getPosition());

            input_type->addComponent(
                new AbstractSyntaxTree::PointerType(
                    node->getPosition(),
                    new AbstractSyntaxTree::SimpleType(node->getPosition(), SELF_TYPE_KEYWORD)
                ),
                new AbstractSyntaxTree::IdentifierExpression(node->getPosition(), SELF_TYPE_KEYWORD)
            );

            destructor->setInputType(input_type);

            functions.insert({ 
                DESTRUCTOR_KEYWORD, 
                static_cast<FunctionType*>(destructor->getSynthesizedType())
            });

            node->getTypeFunctions().push_back(destructor);
            node->getFunctionsSymbolTable().insert({ DESTRUCTOR_KEYWORD, destructor });
        }

        type->setFunctions(functions);
    }

    {
        // enter data member scope
        ScopeObject obj(scope_info, node->getDataSymbolTable());

        // create the variables types and assign them to the type
        for (AbstractSyntaxTree::VariableDeclaration* var : node->getTypeDataMembers())
        {
            auto* var_type = getDeclarationType(type_container, scope_info, var);

            for (AbstractSyntaxTree::VariableDeclarationComponent* decl : var->getVariables())
                data_members.push_back({ 
                    decl->getIdentifier()->getIdentifier(),
                    var_type
                });
        }
    
        type->setDataMembers(data_members);

        // generate type info for the data of the type
        for (AbstractSyntaxTree::VariableDeclaration* var : node->getTypeDataMembers())
            generateTypeInfo(type_container, scope_info, var);
    }

    {
        // enter functions scope
        ScopeObject obj(scope_info, node->getFunctionsSymbolTable());

        // generate type info for the functions of the type
        for (AbstractSyntaxTree::FunctionDefinition* func : node->getTypeFunctions())
            if (generateTypeInfo(type_container, scope_info, func) != nullptr)
                static_cast<FunctionType*>(func->getSynthesizedType())->setParentType(type);
    }

    // // check if the default constructors can be called for all data members
    // for (const auto& member : type->getDataMembers())
    // {
    //     if (member.second->getType() == SynthesizedType::type_category::custom_type)
    //     {
    //         if ()
    //     }
    // }

    // exit out of the type container definition
    type_container.exitDefinition();

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::VariableDeclaration* node)
{
    if (node == nullptr) return nullptr;    
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();
    
    SynthesizedType* type = getDeclarationType(type_container, scope_info, node);
    
    if(type == nullptr)
    {
        ErrorRegister::reportError(new SemanticError(
            "Cannot deduce declaration type",
            node->getPosition()));

        return nullptr;
    }

    if (type->compare(TypeSystemInfo::getGlobalContainer()->getType("void")))
    {
        ErrorRegister::reportError(new SemanticError(
            "Cannot instantiate void type",
            node->getPosition()));

        return nullptr;
    }

    // Get the inner table of the node in order to init the symbol table
    ScopeObject inner_scope(scope_info, node->getSymbolTable());
    bool is_safe_type = type->getType() == SynthesizedType::type_category::strong_pointer_type || type->getType() == SynthesizedType::type_category::weak_pointer_type;

    // generate type info & check types for all variables 
    for (auto* variable : node->getVariables())
    {
        if (variable != nullptr)
        {
            if(variable->getIdentifier() == nullptr)
            {
                ErrorRegister::reportError(new SemanticError(
                    "Invalid Variable Identifier",
                    variable->getPosition()));
                return nullptr;
            }

            if (variable->getInitialization() != nullptr)
            {
                // generate type info for the initialization
                auto* initialization_type = generateTypeInfo(type_container, scope_info, variable->getInitialization());

                if (initialization_type == nullptr)
                    return nullptr;

                if (!type->isDefined("=", initialization_type))
                    reportOperatorMismatch(type, initialization_type, "=", variable->getInitialization()->getPosition());
            }

            variable->getIdentifier()->setSynthesizedType(type);
            variable->setSynthesizedType(type);
        }
        else
        {
            ErrorRegister::reportError(new SemanticError("Encountered an invalid variable declaration node", variable->getPosition()));
            return nullptr;
        }
    }

    node->getType()->setSynthesizedType(type);
    node->setSynthesizedType(TypeSystemInfo::getGlobalContainer()->getType("void"));

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::PointerType* node)
{
    if (node == nullptr) return nullptr;    
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    if(node->getBaseType() == nullptr)
    {
        ErrorRegister::reportError(new SemanticError(
            "Invalid pointer base type",
            node->getPosition()));

        return nullptr;
    }

    // generate type info for the base
    auto* base_type = generateTypeInfo(type_container, scope_info, node->getBaseType());
    
    if(base_type == nullptr) return nullptr;

    SynthesizedType* type = nullptr;
    
    if(node->getProperties().is_strong)
        type = type_container.getOrCreateStrongPointerType(base_type);
    else if(node->getProperties().is_weak)
        type = type_container.getOrCreateWeakPointerType(base_type);
    else
        type = type_container.getOrCreatePointerType(base_type);

    node->setSynthesizedType(type);

    return type;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ReturnStatement* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    // generate type info for the inner expression
    generateTypeInfo(type_container, scope_info, node->getReturnValue());

    node->setSynthesizedType(TypeSystemInfo::getGlobalContainer()->getType("void"));

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::Expression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    switch (node->getExpressionType())
    {
    case AbstractSyntaxTree::Expression::expression_type::binary_operation:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::BinaryOperatorExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::unary_operation:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::UnaryOperatorExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::identifier_expression:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::IdentifierExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::literal_expression:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::LiteralExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::expression_list:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::ExpressionList*>(node));
    case AbstractSyntaxTree::Expression::expression_type::call_expression:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::CallExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::cast_expression:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::CastExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::member_access:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::MemberAccessExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::namespace_access_expr:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::NamespaceAccessExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::vargs_expression:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::VarArgsExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::type_instantiation:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::TypeInstatiation*>(node));
    case AbstractSyntaxTree::Expression::expression_type::new_function_expression:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::NewFunctionExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::create_function_expression:
        return generateTypeInfo(type_container, scope_info, static_cast<AbstractSyntaxTree::CreateFunctionExpression*>(node));
    default:
        // program execution would never get to here unless the semantic analyzer has been given invalid data
        ErrorRegister::reportError(new SemanticError("Encountered an invalid node type", node->getPosition()));
        break;
    }

    return nullptr;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    // initialize type info for the inner expressions
    auto* left_type = generateTypeInfo(type_container, scope_info, node->getLeftExpression());
    auto* right_type = generateTypeInfo(type_container, scope_info, node->getRightExpression());

    if (left_type == nullptr || right_type == nullptr)
        return nullptr;

    // check if the operator is defined between the operands

    // An exception for operators: && and || because of their type -> bool
    if(node->getOperator() == "&&" || node->getOperator() == "||")
    {
        auto* bool_type = TypeSystemInfo::getGlobalContainer()->getType("bool");
        bool has_err = false;
        if (!bool_type->isDefined(node->getOperator(), left_type))
        {
            has_err = true;
            reportOperatorMismatch(bool_type, left_type,
                node->getOperator(), node->getLeftExpression()->getPosition());
        }

        if (!bool_type->isDefined(node->getOperator(), right_type))
        {
            has_err = true;
            reportOperatorMismatch(bool_type, right_type,
                node->getOperator(), node->getRightExpression()->getPosition());
        }

        if (has_err) return nullptr;
    }
    else if (!left_type->isDefined(node->getOperator(), right_type))
    {
        reportOperatorMismatch(node->getLeftExpression()->getSynthesizedType(), 
                node->getRightExpression()->getSynthesizedType(), node->getOperator(), node->getPosition());
        return nullptr;
    }

    const auto& operator_synthesis = TypeSystemInfo::operators_type_synthesis.find(node->getOperator());
    Operator* op = OperatorsInfo::inst().getOperator(node->getOperator());

    // Check if the operator is defined in the language
    if(op == nullptr ||
        operator_synthesis == TypeSystemInfo::operators_type_synthesis.end())
    {
        ErrorRegister::reportError(new SemanticError(
                "The operator: " + node->getOperator() + " isn't defined in the language",
                node->getPosition()
            ));
        return nullptr;
    }

    // check if the operator requires a reference
    if(op->bin_mods.left_input == operator_mod::reference)
    {
        auto modify_val = checkModifiable(type_container, scope_info, node->getLeftExpression());
        
        if(modify_val == AbstractSyntaxTree::Expression::modifiable_type::FALSE)
        {
            ErrorRegister::reportError(new SemanticError(
                "Expression must be a modifiable lvalue",
                node->getLeftExpression()->getPosition(),
                "The left expression is non-modifiable"
            ));
            return nullptr;
        }
        else if(modify_val == AbstractSyntaxTree::Expression::modifiable_type::NONE)
        {
            ErrorRegister::reportError(new SemanticError(
                "Invalid modifiablity, expression must be a modifiable lvalue",
                node->getLeftExpression()->getPosition()
            ));
            return nullptr;
        }
    }
    
    switch (operator_synthesis->second.at(1))
    {
    case TypeSystemInfo::type_synthesis_action::boolean:
        node->setSynthesizedType(TypeSystemInfo::getGlobalContainer()->getType("bool"));
        break;
    case TypeSystemInfo::type_synthesis_action::right:
        node->setSynthesizedType(right_type);
        break;
    case TypeSystemInfo::type_synthesis_action::left:
        node->setSynthesizedType(left_type);
        break;
    case TypeSystemInfo::type_synthesis_action::dereference:
    case TypeSystemInfo::type_synthesis_action::dref_access:
    case TypeSystemInfo::type_synthesis_action::safe_dref_access:
    default:
        ErrorRegister::reportError(new SemanticError(
                "The operator: " + node->getOperator() + " does not have a valid synthesis operation",
                node->getPosition()
            ));
        break;
    }

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::UnaryOperatorExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    // initialize type info for the inner expression
    auto* inner_type = generateTypeInfo(type_container, scope_info, node->getExpression());

    if(inner_type == nullptr) return nullptr;

    // check if the operator is defined between the operands
    if (!inner_type->isDefined(node->getOperator()))
    {
        reportOperatorMismatch(inner_type, node->getOperator(), node->getPosition());
        return nullptr;
    }

    const auto& operator_synthesis = TypeSystemInfo::operators_type_synthesis.find(node->getOperator());

    // check whether the operator has a defined synthesis action
    if(operator_synthesis == TypeSystemInfo::operators_type_synthesis.end())
    {
        ErrorRegister::reportError(new SemanticError(
                "The operator: " + node->getOperator() + " isn't defined in the language",
                node->getPosition()
            ));
        return nullptr;
    }

    Operator* op = OperatorsInfo::inst().getOperator(node->getOperator());
    
    auto input_req = (node->isPrefix() ? op->unary_mods.prefix_input : op->unary_mods.postfix_input);
    // check if the operator requires a reference
    if(input_req == operator_mod::reference)
    {
        auto modify_val = checkModifiable(type_container, scope_info, node->getExpression());
        
        if(modify_val == AbstractSyntaxTree::Expression::modifiable_type::FALSE)
        {
            ErrorRegister::reportError(new SemanticError(
                "Expression must be a modifiable lvalue",
                node->getExpression()->getPosition(),
                "The inner expression is non-modifiable"
            ));
            return nullptr;
        }
        else if(modify_val == AbstractSyntaxTree::Expression::modifiable_type::NONE)
        {
            ErrorRegister::reportError(new SemanticError(
                "Invalid modifiablity, expression must be a modifiable lvalue",
                node->getExpression()->getPosition()
            ));
            return nullptr;
        }
    } 
    else if (input_req == operator_mod::pointer)
        if (inner_type->getType() != SynthesizedType::type_category::pointer_type)
        {
            ErrorRegister::reportError(new SemanticError(
                    "Invalid modifiablity, expression must be a modifiable lvalue",
                    node->getExpression()->getPosition()
                ));
            return nullptr;
        }
    
    node->setSynthesizedType(getTypeByOperator(type_container, inner_type, !node->isPrefix(), operator_synthesis->second));
    
    if(node->getSynthesizedType() == nullptr)
    {
        ErrorRegister::reportError(new SemanticError(
                    "The operator: " + node->getOperator() + " does not have a valid synthesis operation",
                    node->getPosition()
                ));
        return nullptr;
    }

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::IdentifierExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    // get the definition of the identifier
    auto* res = scope_info.getDefinition(node->getIdentifier());

    if(res == nullptr) 
    {
        ErrorRegister::reportError(new SemanticError(
            "Could not find symbol " + node->getIdentifier(),
            node->getPosition()
        ));

        return nullptr;
    }

    node->setSynthesizedType(generateTypeInfo(type_container, scope_info, res));
    if(node->getSynthesizedType() == nullptr) return nullptr;
    
    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ExpressionList* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    // if there is no content, the type is void
    if(node->getExpressions().empty())
    {
        node->setSynthesizedType(TypeSystemInfo::getGlobalContainer()->getType("void"));

        return node->getSynthesizedType();        
    }

    // create the complex type
    auto* obj_cmplx = new UnnamedComplexType();
    for(auto* expr : node->getExpressions())
    {
        if(expr == nullptr)
        {
            ErrorRegister::reportError(new SemanticError(
                "The Expression List inner object is not defined",
                node->getPosition()
            ));
            
            delete obj_cmplx; return nullptr;
        }
        
        // generate type info for the expression
        auto* inner_type = generateTypeInfo(type_container, scope_info, expr);

        if(inner_type == nullptr) { delete obj_cmplx; return nullptr; }
        
        // check that the type is not void
        if(inner_type->compare(TypeSystemInfo::getGlobalContainer()->getType("void")))
        {
            ErrorRegister::reportError(new SemanticError(
                "Cannot instantiate a void type",
                expr->getPosition()
            ));

            delete obj_cmplx; return nullptr;
        }

        // set the type of the expression and add it the type
        expr->setSynthesizedType(inner_type);
        obj_cmplx->addComponent(inner_type);
    }

    // add the type
    obj_cmplx = type_container.getOrCreateUnnamedComplexType(obj_cmplx);
    node->setSynthesizedType(obj_cmplx);

    return obj_cmplx;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::CallExpression* node, CustomType* called_object)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    if(node->getArguments() == nullptr || node->getFunctionExpression() == nullptr)
        return nullptr;

    // generate type info for the function expression and arguments
    auto* func_type = generateTypeInfo(type_container, scope_info, node->getFunctionExpression());
    auto* args_type = generateTypeInfo(type_container, scope_info, node->getArguments());

    if (func_type == nullptr || args_type == nullptr)
        return nullptr;

    // check if the type is callable
    if (func_type->getType() != SynthesizedType::type_category::function_type)
    {
        ErrorRegister::reportError(new SemanticError(
                "Type " + func_type->getName() + " is not callable",
                node->getFunctionExpression()->getPosition()
            ));

        return nullptr;
    }

    // cast the type to a function
    auto* new_func_type = dynamic_cast<FunctionType*>(func_type);

    if (new_func_type == nullptr) return nullptr;
    if (new_func_type->getOutputType() == nullptr || new_func_type->getInputType() == nullptr) return nullptr;

    // compare the input type and the arguments
    if (!new_func_type->compareArgs(args_type, true, true, type_container.getOrCreatePointerType(called_object)))
    {
        if (new_func_type->getInputType()->compare(args_type))
            ErrorRegister::reportError(new SemanticError(
                "A static function cannot be used in a non static context",
                node->getPosition()
            ));
        else
            reportTypeMismatch(args_type, new_func_type->getInputType(), node->getPosition());
    }

    node->setSynthesizedType(new_func_type->getOutputType());

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::CastExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    // generate type information for the components
    if (generateTypeInfo(type_container, scope_info, node->getInnerExpression()) == nullptr) return nullptr;
    if (generateTypeInfo(type_container, scope_info, node->getTargetType()) == nullptr) return nullptr;
    
    auto* inner_type = node->getInnerExpression()->getSynthesizedType();
    auto* target_type = node->getTargetType()->getSynthesizedType();

    bool error = false;
    
    // if the types are the same, set the type appropriately and return it
    if (inner_type->compare(target_type))
    {
        node->setSynthesizedType(target_type);
        return target_type;
    }

    // check conversion to / from void type
    if (inner_type->getType() == SynthesizedType::type_category::void_type)
    {
        error = true;

        ErrorRegister::reportError((new SemanticError(
            "Cannot cast void type into another type",
            node->getInnerExpression()->getPosition()
        ))->withNote("Conversion from and to void type is not defined"));
    }
    
    if (target_type->getType() == SynthesizedType::type_category::void_type)
    {
        error = true;
        
        ErrorRegister::reportError((new SemanticError(
            "Cannot cast into void type",
            node->getTargetType()->getPosition()
        ))->withNote("Conversion from and to void type is not defined"));
    }
    
    if(error) return nullptr;

    switch (node->getCastingType())
    {
        case AbstractSyntaxTree::CastExpression::cast_type::cast_static:
            if(inner_type->isStaticCastCompatible(target_type))
                node->setSynthesizedType(target_type);
            else
            {
                error = true;
                ErrorRegister::reportError(new SemanticError(
                    "Static cast from " + node->getInnerExpression()->getSynthesizedType()->getName() + 
                    " to " + node->getTargetType()->getSynthesizedType()->getName() + 
                    " is not defined",
                    node->getPosition()
                ));
            }
            break;
        case AbstractSyntaxTree::CastExpression::cast_type::cast_bitcast:
            // check conversion between pointer and non pointer types
            if (!inner_type->isBitPtrCastCompatible(target_type))
            {
                error = true;

                ErrorRegister::reportError((new SemanticError(
                    "Cannot bitcast between non pointer types",
                    node->getInnerExpression()->getPosition()
                ))->withNote("Bitcasts may only cast between pointers"));
            }

            node->setSynthesizedType(target_type);
            break;
        default:
        {
            error = true;
            ErrorRegister::reportError(new SemanticError(
                "Invalid casting type",
                node->getPosition(),
                "The casting type isn't supported"
            ));
        }
    }

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    if (node->getLeftExpression() == nullptr) return nullptr;
    if (node->getRightExpression() == nullptr) return nullptr;

    // check that the referenced node is not nullptr
    if (node->getReferencedNode() == nullptr)
    {
        ErrorRegister::reportError(new SemanticError(
            "Could not find the definition of this expression - initialization of referenced node failed",
            node->getPosition()
        ));
    }

    // use the referenced node as the node to be generated type info for
    node->setSynthesizedType(generateTypeInfo(type_container, scope_info, node->getReferencedNode()));
    node->getRightExpression()->setSynthesizedType(node->getSynthesizedType());
    node->getLeftExpression()->setSynthesizedType(
        type_container.getType("namespace")
    );

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    if (node->getLeftExpression() == nullptr) return nullptr;
    if (node->getRightExpression() == nullptr) return nullptr;

    // get modifiability of the expression
	auto modify_val = checkModifiable(type_container, scope_info, node->getLeftExpression());
    SynthesizedType* left_type = nullptr;

    // generate type info for the left expression
    if(generateTypeInfo(type_container, scope_info, node->getLeftExpression()) == nullptr)
        return nullptr;

    left_type = node->getLeftExpression()->getSynthesizedType();

	switch (node->getAccessType())
    {
        case AbstractSyntaxTree::MemberAccessExpression::AccessType::pointer_member_access:
            if (left_type->getType() != SynthesizedType::type_category::pointer_type)
            {
                ErrorRegister::reportError(new SemanticError(
                    "Cannot derefernce a non pointer type using operator '->'",
                    node->getLeftExpression()->getPosition()
                ));

                return nullptr;
            }

            left_type = static_cast<PointerType*>(left_type)->getBaseType();
        case AbstractSyntaxTree::MemberAccessExpression::AccessType::member_acs:
        {
            // check if the right expression is an integer literal
            if (node->getRightExpression()->getExpressionType()  == AbstractSyntaxTree::Expression::expression_type::literal_expression)
                generateTypeInfoIndexAccess(type_container, scope_info, node, left_type);
            else if (node->getRightExpression()->getExpressionType() == AbstractSyntaxTree::Expression::expression_type::identifier_expression)
                generateTypeInfoIdentifierAccess(type_container, scope_info, node, left_type);
            else if (node->getRightExpression()->getExpressionType() == AbstractSyntaxTree::Expression::expression_type::call_expression)
                generateTypeInfoCallAccess(type_container, scope_info, node, left_type);
            else
                ErrorRegister::reportError((new SemanticError(
                    "Invalid expression type",
                    node->getRightExpression()->getPosition()
                ))->withNote("Accessing a member of a type must be done using an identifier expression or an integer literal (for unnamed complex types)"));

            break;
        }
        case AbstractSyntaxTree::MemberAccessExpression::AccessType::index_acs:
        {
            if (generateTypeInfo(type_container, scope_info, node->getRightExpression()) == nullptr)
                return nullptr;

            // the left hand side has to be a pointer in order to access an element
            if (node->getLeftExpression()->getSynthesizedType()->getType() != SynthesizedType::type_category::pointer_type)
            {
                ErrorRegister::reportError((new SemanticError(
                    "Expression must be a pointer",
                    node->getLeftExpression()->getPosition()
                ))->withNote("Accessing elements of an array must be done using a pointer to the beginning of the array"));

                return nullptr;
            }

			// Check if the right expression is an int type with casting checks
			string type_name = "u64";
            if (!node->getRightExpression()->getSynthesizedType()->compare(
				TypeSystemInfo::getGlobalContainer()->getType(type_name), true, true))
			{
				ErrorRegister::reportError((new SemanticError(
						"Index must be of type " + type_name,
						node->getLeftExpression()->getPosition()
                	))->withNote("Cannot cast " + node->getRightExpression()->getSynthesizedType()->getName()
						+ " to " + type_name));

				return nullptr;
			}

            // the type of the expression is the base type of the pointer
            node->setSynthesizedType(
                static_cast<PointerType*>(node->getLeftExpression()->getSynthesizedType())->getBaseType()
            );

			break;
        }
        default:
			ErrorRegister::reportError(new SemanticError(
				"Invalid member access type operator",
				node->getLeftExpression()->getPosition()
			));
			return nullptr;
            break;
    }

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfoIndexAccess(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node, SynthesizedType* left_ty)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();
    if (node->getLeftExpression() == nullptr) return nullptr;
    if (node->getRightExpression() == nullptr) return nullptr;
    if (node->getRightExpression()->getExpressionType() != AbstractSyntaxTree::Expression::expression_type::literal_expression) return nullptr;

    if (static_cast<AbstractSyntaxTree::LiteralExpression*>(node->getRightExpression())->getLiteralType() == 
        AbstractSyntaxTree::LiteralExpression::literal_type::int_type)
    {
        auto* index_literal = static_cast<AbstractSyntaxTree::IntegerLiteral*>(node->getRightExpression());

        // check that the literal expression is not nullptr
        if (index_literal == nullptr)
        {
            ErrorRegister::reportError(new SemanticError(
                "Accessing complex types with operator '.' requires a constant integer index",
                node->getPosition()
            ));

            return nullptr;
        }

        // generate type info for the literal index
        generateTypeInfo(type_container, scope_info, node->getRightExpression());

        node->setSynthesizedType(typeCheckComplexIndexAccess(
                left_ty,
                index_literal->getValue(),
                node->getPosition()
            ));
        node->setAccessIndex(index_literal->getValue());
    }
    else
    {
        ErrorRegister::reportError(new SemanticError(
            "Accessing complex types with operator '.' requires a constant integer index",
            node->getPosition()
        ));

        return nullptr;
    }

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfoIdentifierAccess(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node, SynthesizedType* left_ty)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();
    if (node->getLeftExpression() == nullptr) return nullptr;
    if (node->getRightExpression() == nullptr) return nullptr;
    if (node->getRightExpression()->getExpressionType() != AbstractSyntaxTree::Expression::expression_type::identifier_expression) return nullptr;

    string right_idnt = static_cast<AbstractSyntaxTree::IdentifierExpression*>(node->getRightExpression())->getIdentifier();

    // Accessing must be done thoguh complex/type nodes
    if(left_ty->getType() == SynthesizedType::type_category::named_complex_type)
    {
        auto* complex_type = static_cast<NamedComplexType*>(left_ty);
        
        const auto& components = complex_type->getComponents();

        for (unsigned int i = 0; i < components.size() && node->getSynthesizedType() == nullptr; i++)
            if(components[i].second == right_idnt)
            {
                node->setSynthesizedType(components[i].first);
                node->getRightExpression()->setSynthesizedType(components[i].first);
                node->setAccessIndex(i);
            }
                
        if(node->getSynthesizedType() == nullptr)
        {
            ErrorRegister::reportError(new SemanticError(
                "Type " + complex_type->getName() +
                " has no member \'" + right_idnt + "\'",
                node->getLeftExpression()->getPosition()
            ));
            return nullptr;
        }
    }
    else if(left_ty->getType() == SynthesizedType::type_category::custom_type)
    {
        auto* type = static_cast<CustomType*>(left_ty);
        
        const auto& components = type->getDataMembers();

        for (unsigned int i = 0; i < components.size() && node->getSynthesizedType() == nullptr; i++)
            if(components[i].first == right_idnt)
            {
                node->setSynthesizedType(components[i].second);
                node->getRightExpression()->setSynthesizedType(components[i].second);
                node->setAccessIndex(i);
            }
                
        if(node->getSynthesizedType() == nullptr)
        {
            ErrorRegister::reportError(new SemanticError(
                "Type " + type->getName() +
                " has no member \'" + right_idnt + "\'",
                node->getLeftExpression()->getPosition()
            ));
            return nullptr;
        }
    }
    else
    {
        ErrorRegister::reportError(new SemanticError(
            "Type " + left_ty->getName() +
            " has no member \'" + right_idnt + "\'",
            node->getLeftExpression()->getPosition()
        ));

        return nullptr;
    }

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfoCallAccess(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node, SynthesizedType* left_ty)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();
    if (node->getLeftExpression() == nullptr) return nullptr;
    if (node->getLeftExpression()->getSynthesizedType() == nullptr) return nullptr;
    if (left_ty == nullptr) return nullptr;
    if (node->getRightExpression() == nullptr) return nullptr;
    if (node->getRightExpression()->getExpressionType() != AbstractSyntaxTree::Expression::expression_type::call_expression) return nullptr;

    auto* call_expr = static_cast<AbstractSyntaxTree::CallExpression*>(node->getRightExpression());

    if (call_expr->getFunctionExpression() == nullptr) return nullptr;

    // the call expression must contain an identifier that identifies the function to call
    if (call_expr->getFunctionExpression()->getExpressionType() != AbstractSyntaxTree::Expression::expression_type::identifier_expression)
    {
        ErrorRegister::reportError(new SemanticError(
            "Accessing a function of a type must be done using an identifier",
            call_expr->getFunctionExpression()->getPosition()
        ));

        return nullptr;
    }

    string identifier = static_cast<AbstractSyntaxTree::IdentifierExpression*>(call_expr->getFunctionExpression())->getIdentifier();

    if (left_ty->getType() != SynthesizedType::type_category::custom_type)
    {
        ErrorRegister::reportError(new SemanticError(
            "The left expression is not a non-primitive type",
            call_expr->getFunctionExpression()->getPosition()
        ));

        return nullptr;
    }

    auto* type = static_cast<CustomType*>(left_ty);

    const auto& function = type->getFunctions().find(identifier);

    if (function == type->getFunctions().end())
    {
        ErrorRegister::reportError(new SemanticError(
            "'" + identifier + "' is not a function of " + type->getName(),
            call_expr->getFunctionExpression()->getPosition()
        ));

        return nullptr;
    }

    // set the function type of the call expression
    call_expr->getFunctionExpression()->setSynthesizedType(function->second);

    // set the synthesized type of the node to be the type of the call expression
    node->setSynthesizedType(
        generateTypeInfo(type_container, scope_info, call_expr, type)
    );

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::VarArgsExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    if(node->getTypeToGet() == nullptr)
    {
        ErrorRegister::reportError(new SemanticError(
            "Invalid inner type of vargs function",
            node->getPosition()
        ));

        return nullptr;
    }

    SynthesizedType* inner_type = nullptr;
    if((inner_type = generateTypeInfo(type_container, scope_info, node->getTypeToGet())) == nullptr) return nullptr;

    node->setSynthesizedType(inner_type);

    return inner_type;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::TypeInstatiation* node, bool must_custom)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    if(node->getType() == nullptr)
    {
        ErrorRegister::reportError(new SemanticError(
            "Invalid type of type instatiation",
            node->getPosition()
        ));

        return nullptr;
    }

    SynthesizedType* type = nullptr;
    SynthesizedType* params_type = nullptr;
    if((type = generateTypeInfo(type_container, scope_info, node->getType())) == nullptr) return nullptr;
    if((params_type = generateTypeInfo(type_container, scope_info, node->getParameters())) == nullptr) return nullptr;

    // If it is a basic type and it is allowed to be, then just make a type check
    if(!must_custom && type->getType() == SynthesizedType::type_category::basic_type)
    {   
        // Check that the given type is aligned with the created type
        if(node->getParameters() != nullptr && !node->getParameters()->getExpressions().empty())
        {
            if(node->getParameters()->getExpressions()[0]->getSynthesizedType() != type)
            {
                ErrorRegister::reportError(new SemanticError(
                    "Basic type instatiation: '" + type->getName() + "' doesn't match the created type: '"
                    + node->getParameters()->getExpressions()[0]->getSynthesizedType()->getName() + "'",
                    node->getParameters()->getPosition()
                ));
            }
        }

        node->setSynthesizedType(type);
        return type;
    }

    if (type->getType() != SynthesizedType::type_category::custom_type)
    {
        ErrorRegister::reportError((new SemanticError(
            "Cannot initialize a primitive type using a constructor",
            node->getPosition()
        ))->withHelp("Use literals to initialize primitive variables"));

        node->setSynthesizedType(type);
        return type;
    }

    CustomType* custom_type = static_cast<CustomType*>(type);
    auto func = custom_type->getFunctions().find(CONSTRUCTOR_KEYWORD);

    // if there is no defined constructor
    if (func == custom_type->getFunctions().end())
    {
        // if there are any parameters
        if (params_type->getType() != SynthesizedType::type_category::void_type)
        {
            ErrorRegister::reportError(new SemanticError(
                "No constructor is defined for the atguments " + params_type->getName(),
                node->getPosition()
            ));
        }
    }
    else if (!func->second->compareArgs(params_type, true, true, type_container.getOrCreatePointerType(custom_type)))
    {
        reportTypeMismatch(params_type, func->second->getInputType(), node->getPosition());
    }

    node->setSynthesizedType(type);

    return type;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::CreateFunctionExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    SynthesizedType* creation_type = nullptr;
    if((creation_type = generateTypeInfo(type_container, scope_info, node->getCreationType(), false)) == nullptr) return nullptr;

    // If a basic type then must have only one parameter and no more
    if(creation_type->getType() == SynthesizedType::type_category::basic_type &&
        node->getCreationType()->getParameters()->getExpressions().size() > 1)
    {
        ErrorRegister::reportError(new SemanticError(
            "Primitive types must be instantiated with maximum 1 parameters",
            node->getCreationType()->getPosition()
        ));
    }

    if(creation_type->getType() == SynthesizedType::type_category::void_type)
    {
        ErrorRegister::reportError(new SemanticError(
            "Can't create new instances with void type",
            node->getCreationType()->getPosition()
        ));
    }

    node->setSynthesizedType(type_container.getOrCreateStrongPointerType(creation_type));

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NewFunctionExpression* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    SynthesizedType* creation_type = nullptr;
    if((creation_type = generateTypeInfo(type_container, scope_info, node->getCreationType(), false)) == nullptr) return nullptr;

    // If a basic type then must have only one parameter and no more
    if(creation_type->getType() == SynthesizedType::type_category::basic_type &&
        node->getCreationType()->getParameters()->getExpressions().size() > 1)
    {
        ErrorRegister::reportError(new SemanticError(
            "Primitive types must be instantiated with maximum 1 parameters",
            node->getCreationType()->getPosition()
        ));
    }

    if(creation_type->getType() == SynthesizedType::type_category::void_type)
    {
        ErrorRegister::reportError(new SemanticError(
            "Can't create new instances with void type",
            node->getCreationType()->getPosition()
        ));
    }

    node->setSynthesizedType(type_container.getOrCreatePointerType(creation_type));

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::DeleteFunctionStatement* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    SynthesizedType* deleting_type = nullptr;
    if((deleting_type = generateTypeInfo(type_container, scope_info, node->getDeletingObj())) == nullptr) return nullptr;

    if(deleting_type->getType() != SynthesizedType::type_category::pointer_type)
    {
        ErrorRegister::reportError(new SemanticError(
            "Can't delete non-pointer types",
            node->getDeletingObj()->getPosition()
        ));
    }

    node->setSynthesizedType(deleting_type);

    return node->getSynthesizedType();
}

void TypeChecker::typeCheck()
{
    if (TypeSystemInfo::getGlobalContainer() == nullptr)
        throw std::runtime_error("The type system information has not been initialized!");

    auto& global_container = *(TypeSystemInfo::getGlobalContainer());

    for(const auto& module : _modules)
    {
        generateTypeInfo(module.first, module.second);
    }
}

void TypeChecker::typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::Node* node)
{
    if (node == nullptr)
        return;

    switch (node->getNodeType())
    {
    case AbstractSyntaxTree::Node::node_type::function_definition:
        typeCheck(scope_info, static_cast<AbstractSyntaxTree::FunctionDefinition*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::namespace_definition:
        typeCheck(scope_info, static_cast<AbstractSyntaxTree::NamespaceDefinition*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::type_definition:
        typeCheck(scope_info, static_cast<AbstractSyntaxTree::TypeDefinition*>(node));
        break;
    case AbstractSyntaxTree::Node::node_type::statement:
        typeCheck(scope_info, static_cast<AbstractSyntaxTree::Statement*>(node));
        break;
    default:
        break;
    }
}

void TypeChecker::typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::Statement* node)
{
    if (node == nullptr) return;
    if (node->getSynthesizedType() == nullptr) return;

    switch(node->getStatementType())
    {
    case AbstractSyntaxTree::Statement::statement_type::variable_declaration:
        typeCheck(scope_info, static_cast<AbstractSyntaxTree::VariableDeclaration*>(node));
        break;
    case AbstractSyntaxTree::Statement::statement_type::statement_block:
        typeCheck(scope_info, static_cast<AbstractSyntaxTree::StatementBlock*>(node));
        break;
    default:
        break;
    }
}

void TypeChecker::typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::StatementBlock* node)
{
    if (node == nullptr) return;
    if (node->getSynthesizedType() == nullptr) return;

    for (auto* command : node->getCommands())
        typeCheck(scope_info, command);        
}

void TypeChecker::typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::VariableDeclaration* node)
{
    if (node == nullptr) return;
    if (node->getSynthesizedType() == nullptr) return;

    // if a custom type is instantiated
    if (node->getType()->getSynthesizedType()->getType() == SynthesizedType::type_category::custom_type)
    {
        CustomType* type = static_cast<CustomType*>(node->getType()->getSynthesizedType());
        
        for (auto* decl : node->getVariables())
        {
            // if the declaration does not have an initialization
            if (decl->getInitialization() == nullptr)
            {
                const auto& ctor = type->getFunctions().find(CONSTRUCTOR_KEYWORD);
                
                // check that there is a default constructor
                if (ctor != type->getFunctions().end() && 
                    ctor->second != nullptr &&
                    ctor->second->getInputType()->getType() != SynthesizedType::type_category::void_type &&
                    static_cast<NamedComplexType*>(ctor->second->getInputType())->getComponents().size() != 1)
                {
                    ErrorRegister::reportError(new SemanticError(
                        "Cannot initialize object - no matching constructor exists",
                        decl->getPosition()
                    ));
                }
            }
        }
    }
}

void TypeChecker::typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::FunctionDefinition* node)
{
    if (node == nullptr) return;
    if (node->getSynthesizedType() == nullptr) return;

    auto* function_type = dynamic_cast<FunctionType*>(node->getSynthesizedType());

    if (function_type == nullptr || function_type->getOutputType() == nullptr ||
        function_type->getOutputType() == nullptr) return;

    // check that the first argument is the parent type if the function is a constructor
    if (node->getFunctionName()->getIdentifier() == CONSTRUCTOR_KEYWORD)
    {
        NamedComplexType* input_type = static_cast<NamedComplexType*>(function_type->getInputType());
        if (function_type->getInputType()->getType() != SynthesizedType::type_category::named_complex_type ||
            input_type->getComponents().size() == 0 ||
            input_type->getComponents()[0].first->getType() != SynthesizedType::type_category::pointer_type || 
            !((static_cast<PointerType*>(input_type->getComponents()[0].first)->getBaseType())->compare(function_type->getParentType())))
        {
            ErrorRegister::reportError(new SemanticError(
                "Constructor must receive a pointer to the instance to initialize as a first argument",
                node->getInputType()->getPosition()
            ));
        }
    }

    // check that the first argument is the parent type if the function is a constructor
    if (node->getFunctionName()->getIdentifier() == DESTRUCTOR_KEYWORD)
    {
        NamedComplexType* input_type = static_cast<NamedComplexType*>(function_type->getInputType());
        if (function_type->isVariadic() ||
            function_type->getInputType()->getType() != SynthesizedType::type_category::named_complex_type ||
            input_type->getComponents().size() != 1 || 
            input_type->getComponents()[0].first->getType() != SynthesizedType::type_category::pointer_type || 
            !((static_cast<PointerType*>(input_type->getComponents()[0].first)->getBaseType())->compare(function_type->getParentType())))
        {
            ErrorRegister::reportError(new SemanticError(
                "Destructors must receive only a single argument - a pointer to the instance to destruct",
                node->getInputType()->getPosition()
            ));
        }
    }

    //type check the content of the function
    typeCheck(scope_info, node->getCommands());

    // check the return statements
    checkReturnType(scope_info, node->getCommands(), function_type->getOutputType());
}

void TypeChecker::typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceDefinition* node)
{
    if (node == nullptr) return;
    if (node->getSynthesizedType() == nullptr) return;

    ScopeObject obj(scope_info, node->getSymbolTable());

    for (auto* inner_node : node->getCommands())
        typeCheck(scope_info, inner_node);
}

void TypeChecker::typeCheck(ScopeInformation& scope_info, AbstractSyntaxTree::TypeDefinition* node)
{
    if (node == nullptr) return;
    if (node->getSynthesizedType() == nullptr) return;

    ScopeObject funcs_obj(scope_info, node->getFunctionsSymbolTable());

    for (auto* func : node->getTypeFunctions())
        typeCheck(scope_info, func);
}

void TypeChecker::checkReturnType(ScopeInformation& scope_info, AbstractSyntaxTree::Statement* statement, SynthesizedType* function_ret_type)
{
    if (statement == nullptr) return;

    switch (statement->getStatementType())
    {
    case AbstractSyntaxTree::Statement::statement_type::return_statement:
    {
        SynthesizedType* ret_type = nullptr;
        auto* ret_statement = static_cast<AbstractSyntaxTree::ReturnStatement*>(statement);

        if (ret_statement == nullptr) return;

        if (ret_statement->getReturnValue() == nullptr) 
            ret_type = TypeSystemInfo::getGlobalContainer()->getType("void");
        else
            ret_type = ret_statement->getReturnValue()->getSynthesizedType();
        
        if(ret_type == nullptr) return;
        
        if (!ret_type->compare(function_ret_type, true, true))
            reportTypeMismatch(ret_type, function_ret_type, statement->getPosition());

        break;
    }
    case AbstractSyntaxTree::Statement::statement_type::statement_block:
        checkReturnType(scope_info, static_cast<AbstractSyntaxTree::StatementBlock*>(statement), function_ret_type);
        break;
    case AbstractSyntaxTree::Statement::statement_type::conditional_statement:
        checkReturnType(scope_info, static_cast<AbstractSyntaxTree::ConditionalStatement*>(statement), function_ret_type);
        break;
    case AbstractSyntaxTree::Statement::statement_type::while_statement:
        checkReturnType(scope_info, static_cast<AbstractSyntaxTree::WhileStatement*>(statement), function_ret_type);
        break;
    case AbstractSyntaxTree::Statement::statement_type::for_statement:
        checkReturnType(scope_info, static_cast<AbstractSyntaxTree::ForStatement*>(statement), function_ret_type);
        break;
    default:
        break;
    }
}

void TypeChecker::checkReturnType(ScopeInformation& scope_info, AbstractSyntaxTree::StatementBlock* statement, SynthesizedType* function_ret_type)
{
    if (statement == nullptr) return;    

    for (auto* curr_statement : statement->getCommands())
    {
        checkReturnType(scope_info, curr_statement, function_ret_type);
    }
}

void TypeChecker::checkReturnType(ScopeInformation& scope_info, AbstractSyntaxTree::ConditionalStatement* statement, SynthesizedType* function_ret_type)
{
    if (statement == nullptr) return;
    if (statement->getIfCommands() == nullptr) return;

    checkReturnType(scope_info, statement->getIfCommands(), function_ret_type);
    checkReturnType(scope_info, statement->getElseCommands(), function_ret_type);
}

void TypeChecker::checkReturnType(ScopeInformation& scope_info, AbstractSyntaxTree::WhileStatement* statement, SynthesizedType* function_ret_type)
{
    if (statement == nullptr) return;
    if (statement->getCommands() == nullptr) return;

    checkReturnType(scope_info, statement->getCommands(), function_ret_type);    
}

void TypeChecker::checkReturnType(ScopeInformation& scope_info, AbstractSyntaxTree::ForStatement* statement, SynthesizedType* function_ret_type)
{
    if (statement == nullptr) return;
    if (statement->getCommands() == nullptr) return;

    checkReturnType(scope_info, statement->getCommands(), function_ret_type);    
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::SimpleType* type_expr)
{
    if (type_expr == nullptr) return nullptr;
    if (type_expr->getSynthesizedType() != nullptr) return type_expr->getSynthesizedType();
    const auto& primitive_type = TypeSystemInfo::primitive_type_mapping.find(type_expr->getIdentifier());

    // check if the type has either been defined or is a primitive
    if (primitive_type == TypeSystemInfo::primitive_type_mapping.end())
    {
        // check if it is a var type
        if (type_expr->getIdentifier() == TypeSystemInfo::generic_type)
        {
            ErrorRegister::reportError((new SemanticError(
                string("Cannot use '") + TypeSystemInfo::generic_type + string("' in this context"), type_expr->getPosition()
            ))->withHelp("Replace with an explicit type", type_expr->getPosition()));

            return nullptr;
        }    

        // the type is not defined and is not a primitive
        if (!scope_info.isDefined(type_expr->getIdentifier()))
        {
            ErrorRegister::reportError(new SemanticError(
                "Undefined type", type_expr->getPosition(),
                type_expr->getIdentifier() + std::string(" is used but not defined")
            ));

            return nullptr;
        }
        
        // set the synthesized type to be the type referenced by the type expression
        type_expr->setSynthesizedType(generateTypeInfo(
            type_container, scope_info, scope_info.getDefinition(type_expr->getIdentifier())
        ));

        if (type_expr->getSynthesizedType() == nullptr)
        {
            // check if there is the definition is a type node (not type expression btw)
            ErrorRegister::reportError(new SemanticError(
                type_expr->getIdentifier() + " cannot be instantiated",
                type_expr->getPosition()
            ));

            return nullptr;
        }
    }
    else
    {
        // get the primitive type
        auto* type = TypeSystemInfo::getGlobalContainer()->getType(type_expr->getIdentifier());
        
        if (type == nullptr){
            ErrorRegister::reportError(new SemanticError(
                "The primitive type " + type_expr->getIdentifier() + " does not exist",
                type_expr->getPosition()
            ));
            
            return nullptr;
        }

        type_expr->setSynthesizedType(type);        
    }
    
    return type_expr->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ComplexType* type_expr)
{
    if (type_expr == nullptr) return nullptr;
    if (type_expr->getSynthesizedType() != nullptr) return type_expr->getSynthesizedType();

    SynthesizedType* new_type = nullptr;

    if(!type_expr->getComponents().empty())
    {
        auto* new_type_complx = new NamedComplexType();

        for(auto* val : type_expr->getComponents())
        {
            if (val == nullptr)
            { 
                ErrorRegister::reportError(new SemanticError(
                    "The declaration has invalid an invalid declared variable",
                    type_expr->getPosition()
                ));
                delete new_type_complx; return nullptr;
            }

            // Check if has definition
            if(val->getIdentifier() == nullptr) 
            {
                ErrorRegister::reportError(new SemanticError(
                    "The obj has an invalid identifier",
                    val->getPosition()));

                delete new_type_complx; return nullptr; 
            }

            // Checking for inner errors
            auto* res = generateTypeInfo(type_container, scope_info, val->getType());
            if(res == nullptr) { delete new_type_complx; return nullptr; }
            
            val->setSynthesizedType(res);
            val->getIdentifier()->setSynthesizedType(res);

            new_type_complx->addComponent({res, val->getIdentifier()->getIdentifier()});
        }

        // add the type
        new_type = type_container.getOrCreateNamedComplexType(new_type_complx);
    }
    else new_type = TypeSystemInfo::getGlobalContainer()->getType("void");

    type_expr->setSynthesizedType(new_type);
    
    return new_type;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NamelessComplexType* type_expr)
{
    if (type_expr == nullptr) return nullptr;
    if (type_expr->getSynthesizedType() != nullptr) return type_expr->getSynthesizedType();

    SynthesizedType* new_type = nullptr;

    if(!type_expr->getComponents().empty())
    {
        auto* new_type_complx = new UnnamedComplexType();
    
        for(auto* val : type_expr->getComponents())
        {
            if(val == nullptr)
            { 
                ErrorRegister::reportError(new SemanticError(
                    "The nameless-complex type has invalid inner objects",
                    type_expr->getPosition()));
                delete new_type_complx; return nullptr; 
            }
            
            auto* res = generateTypeInfo(type_container, scope_info, val);
                
            if(res == nullptr) { delete new_type_complx; return nullptr; }
            
            val->setSynthesizedType(res);

            new_type_complx->addComponent(res);
        }

        // add the type
        new_type = type_container.getOrCreateUnnamedComplexType(new_type_complx);
    }
    else new_type = TypeSystemInfo::getGlobalContainer()->getType("void");

    type_expr->setSynthesizedType(new_type);
    
    return new_type;
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::StatementBlock* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();
    
    ScopeObject obj(scope_info, node->getSymbolTable());

    for (auto* command : node->getCommands())
    {
        if (command == nullptr) return nullptr;
        if (generateTypeInfo(type_container, scope_info, command) == nullptr) return nullptr;
    }

    node->setSynthesizedType(TypeSystemInfo::getGlobalContainer()->getType("void"));

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ConditionalStatement* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    // check that the condition is not nullptr
    if (node->getCondition() == nullptr)
    {
        ErrorRegister::reportError(new SemanticError(
            "If statements must have a condition",
            node->getPosition()
        ));

        return nullptr;
    }

    // check that the commands of the if statement are not nullptr
    if (node->getIfCommands() == nullptr)
    {
        ErrorRegister::reportError(new SemanticError(
            "If statements must have a body",
            node->getPosition()
        ));

        return nullptr;
    }

    // generate type info for the condition
    generateTypeInfo(type_container, scope_info, node->getCondition());

    // generate type info for the if statements and else statements
    generateTypeInfo(type_container, scope_info, node->getIfCommands());
    if(node->getElseCommands() != nullptr) generateTypeInfo(type_container, scope_info, node->getElseCommands());

    auto* bool_type = TypeSystemInfo::getGlobalContainer()->getType("bool");

    // check that the type is not null
    if (node->getCondition()->getSynthesizedType() == nullptr) return nullptr;
    
    // check that the type is bool
    if (!node->getCondition()->getSynthesizedType()->compare(bool_type))
        reportTypeMismatch(node->getCondition()->getSynthesizedType(), bool_type,
            node->getCondition()->getPosition());
    
    // set the type to void and return it
    node->setSynthesizedType(TypeSystemInfo::getGlobalContainer()->getType("void"));

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::WhileStatement* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    // check that the condition and the commands are not nullptr
    if (node->getCondition() == nullptr) return nullptr;
    if (node->getCommands() == nullptr) return nullptr;

    // generate type info for the condition
    generateTypeInfo(type_container, scope_info, node->getCondition());

    // generate type info for the commands
    generateTypeInfo(type_container, scope_info, node->getCommands());

    auto* bool_type = TypeSystemInfo::getGlobalContainer()->getType("bool");

    // check that the type is not null
    if (node->getCondition()->getSynthesizedType() == nullptr) return nullptr;
    
    // check that the type is bool
    if (!node->getCondition()->getSynthesizedType()->compare(bool_type))
        reportTypeMismatch(node->getCondition()->getSynthesizedType(), bool_type,
            node->getCondition()->getPosition());
    
    // set the type to void and return it
    node->setSynthesizedType(TypeSystemInfo::getGlobalContainer()->getType("void"));

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::generateTypeInfo(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ForStatement* node)
{
    if (node == nullptr) return nullptr;
    if (node->getSynthesizedType() != nullptr) return node->getSynthesizedType();

    // check that the condition and the commands are not nullptr
    if (node->getCondition() == nullptr) return nullptr;
    if (node->getCommands() == nullptr) return nullptr;

    // enter into a new scope that contains the symbol table stored in the node
    ScopeObject obj(scope_info, node->getSymbolTable());

    // generate type info for the initialization, condition, incrementation and commands
    generateTypeInfo(type_container, scope_info, node->getInitialization());
    generateTypeInfo(type_container, scope_info, node->getCondition());
    generateTypeInfo(type_container, scope_info, node->getIncrementation());
    generateTypeInfo(type_container, scope_info, node->getCommands());

    auto* bool_type = TypeSystemInfo::getGlobalContainer()->getType("bool");

    // check that the type is not null
    if (node->getCondition()->getSynthesizedType() == nullptr) return nullptr;
    
    // check that the type is bool
    if (!node->getCondition()->getSynthesizedType()->compare(bool_type))
        reportTypeMismatch(node->getCondition()->getSynthesizedType(), bool_type,
            node->getCondition()->getPosition());
    
    // set the type to void and return it
    node->setSynthesizedType(TypeSystemInfo::getGlobalContainer()->getType("void"));

    return node->getSynthesizedType();
}

SynthesizedType* TypeChecker::getDeclarationType(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::VariableDeclaration* node)
{
    if (node == nullptr) return nullptr;
    if (node->getType() == nullptr) return nullptr;

    if (node->getType()->getTypeExpressionCategory() == AbstractSyntaxTree::TypeExpression::type_expression_category::simple_type)
    {
        auto* new_type = static_cast<AbstractSyntaxTree::SimpleType*>(node->getType());        

        if(new_type != nullptr)
        {
            if(new_type->getIdentifier() == TypeSystemInfo::generic_type)
            {
                for(auto* variable : node->getVariables())
                {
                    if(variable == nullptr) 
                    {
                        ErrorRegister::reportError(new SemanticError(
                            "The declaration has an invalid declared variable",
                            node->getPosition()
                        ));
                        
                        return nullptr;
                    }
                    
                    if(variable->getIdentifier() == nullptr)
                    {
                        ErrorRegister::reportError(new SemanticError(
                            "The obj has an invalid identifier",
                            variable->getPosition()));
                        return nullptr;
                    }
                    
                    if(variable->getInitialization() != nullptr)
                        return generateTypeInfo(type_container, scope_info, variable->getInitialization());
                }

                return nullptr;
            }
        }
        else return nullptr;
    }
    
    return generateTypeInfo(type_container, scope_info, node->getType());
}

FunctionType* TypeChecker::getDeclarationType(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::FunctionDefinition* node)
{
    if (node == nullptr) return nullptr;

    // generate type info for input/output
    auto* input_type = generateTypeInfo(type_container, scope_info, node->getInputType());
    auto* output_type = generateTypeInfo(type_container, scope_info, node->getReturnType());

    // check the types
    if(output_type == nullptr || input_type == nullptr)
        return nullptr;

    return type_container.getOrCreateFunctionType(
        input_type, 
        output_type, 
        node->getProperties().is_variadic, 
        node->getParentType() != nullptr ?
            static_cast<CustomType*>(node->getParentType()->getSynthesizedType())
            : nullptr
    );
}

void TypeChecker::reportTypeMismatch(SynthesizedType* first, SynthesizedType* second, const position_data& pos1, const position_data& pos2)
{
    string first_name = first->getName();
    string second_name = second->getName();
    
    ErrorRegister::reportError((new SemanticError(
        "Type Mismatch", 
        pos1,
        "Type " + first_name + " does not match " + second_name))->withNote(
        "Type " + first_name + " does not match " + second_name,
        pos2
    ));
}

void TypeChecker::reportTypeMismatch(SynthesizedType* first, SynthesizedType* second, const position_data& pos1)
{
    string first_name = first->getName();
    string second_name = second->getName();
    
    ErrorRegister::reportError((new SemanticError(
        "Type Mismatch", 
        pos1,
        "Type " + first_name + " does not match " + second_name)));
}

void TypeChecker::reportOperatorMismatch(SynthesizedType* left, SynthesizedType* right, const string& op, const position_data& pos)
{
    if(left == nullptr || right == nullptr)
        ErrorRegister::reportError(new SemanticError(
            "Operator Mismatch",
            pos, 
            "Operator '" + op + "' isn't defined for these operands"));
    else
        ErrorRegister::reportError(new SemanticError(
            "Operator Mismatch",
            pos, 
            "Operator '" + op + "' isn't defined between " + left->getName()
            + " and " + right->getName()));
}

void TypeChecker::reportOperatorMismatch(SynthesizedType* inner, const string& op, const position_data& pos)
{
    if(inner == nullptr)
        ErrorRegister::reportError(new SemanticError(
            "Operator Mismatch",
            pos, 
            "Operator '" + op + "' isn't defined for this type"));
    else
        ErrorRegister::reportError(new SemanticError(
            "Operator Mismatch",
            pos, 
            "Operator '" + op + "' isn't defined for " + inner->getName()));
}

void TypeChecker::typeCheck(const vector<pair<string, Module*>>& modules)
{
    TypeChecker typeChecker(modules);

    typeChecker.typeCheck();
}

void TypeChecker::generateTypeInfo(const string& module_name, AbstractSyntaxTree::Module* module)
{
    if (module == nullptr) return;

    auto& global_container = *(TypeSystemInfo::getGlobalContainer());

    ScopeInformation scope_info(module, module->getSymbolTable());

    // add the imported symbols of the module
    try {
        scope_info.includeImportedSymbols();
    } catch(SymbolRedefinitionException& err) {
        ErrorRegister::reportError((new SemanticError(
            "Redefined Symbol",
            err.previous_definition->getPosition()
        ))->withNote(err.redefined_symbol + " is already defined here", err.new_definition->getPosition()));
    }

    global_container.enterDefinition(std::to_string(module->getModuleId()));

    // recursively check the type of the nodes contained within this module
    for (AbstractSyntaxTree::Node* node : module->getCommands())
    {
        generateTypeInfo(global_container, scope_info, node);
        typeCheck(scope_info, node);
    }

    global_container.exitDefinition();
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::Expression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();

    switch (node->getExpressionType())
    {
    case AbstractSyntaxTree::Expression::expression_type::binary_operation:
        return checkModifiable(type_container, scope_info, static_cast<AbstractSyntaxTree::BinaryOperatorExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::unary_operation:
        return checkModifiable(type_container, scope_info, static_cast<AbstractSyntaxTree::UnaryOperatorExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::identifier_expression:
        return checkModifiable(type_container, scope_info, static_cast<AbstractSyntaxTree::IdentifierExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::literal_expression:
        return checkModifiable(type_container, scope_info, static_cast<AbstractSyntaxTree::LiteralExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::expression_list:
        return checkModifiable(type_container, scope_info, static_cast<AbstractSyntaxTree::ExpressionList*>(node));
    case AbstractSyntaxTree::Expression::expression_type::call_expression:
        return checkModifiable(type_container, scope_info, static_cast<AbstractSyntaxTree::CallExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::member_access:
        return checkModifiable(type_container, scope_info, static_cast<AbstractSyntaxTree::MemberAccessExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::namespace_access_expr:
        return checkModifiable(type_container, scope_info, static_cast<AbstractSyntaxTree::NamespaceAccessExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::vargs_expression:
        return checkModifiable(type_container, scope_info, static_cast<AbstractSyntaxTree::VarArgsExpression*>(node));
    case AbstractSyntaxTree::Expression::expression_type::type_instantiation:
        return checkModifiable(type_container, scope_info, static_cast<AbstractSyntaxTree::TypeInstatiation*>(node));
    default:
        // program execution would never get to here unless the semantic analyzer has been given invalid data
        return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    }
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::BinaryOperatorExpression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();

    bool outputs_reference = OperatorsInfo::inst().getOperator(node->getOperator())->bin_mods.output ==
        operator_mod::reference;

    // set the children nodes' modifiablility too
    checkModifiable(type_container, scope_info, node->getLeftExpression());

    // set this to be modifiable only if it outputs a reference
    node->setModifiable(outputs_reference ? AbstractSyntaxTree::Expression::modifiable_type::TRUE :
                                    AbstractSyntaxTree::Expression::modifiable_type::FALSE);
    return node->getModifiable();
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NamespaceAccessExpression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();
    if (node->getRightExpression() == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;

    // check if the node is an expression
    if (node->getRightExpression()->getNodeType() == AbstractSyntaxTree::Node::node_type::statement)
    {
        if (static_cast<AbstractSyntaxTree::Statement*>(node->getRightExpression())->getStatementType() == 
            AbstractSyntaxTree::Statement::statement_type::expression)
        {
            // set the modifiability of the node to be the modifiability of the referenced expression
            node->setModifiable(checkModifiable(
                type_container, 
                scope_info, 
                static_cast<AbstractSyntaxTree::Expression*>(node->getRightExpression())
            ));
        }
    }

    return node->getModifiable();
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::MemberAccessExpression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();

	switch (node->getAccessType())
	{
	case AbstractSyntaxTree::MemberAccessExpression::AccessType::member_acs:
		node->setModifiable(checkModifiable(type_container, scope_info, node->getLeftExpression()));
		break;
	case AbstractSyntaxTree::MemberAccessExpression::AccessType::pointer_member_access:
	case AbstractSyntaxTree::MemberAccessExpression::AccessType::index_acs:
        // index expressions and pointer accesses are dereferencing the pointer, so the type must be modifiable
        node->setModifiable(AbstractSyntaxTree::Expression::modifiable_type::TRUE);
        break;
	default:
		break;
	}
		

    return node->getModifiable();
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::UnaryOperatorExpression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();

    bool outputs_modifiable = node->isPrefix() ?
        OperatorsInfo::inst().getOperator(node->getOperator())->unary_mods.prefix_output == 
            operator_mod::reference : 
        OperatorsInfo::inst().getOperator(node->getOperator())->unary_mods.postfix_output == 
            operator_mod::reference;

    checkModifiable(type_container, scope_info, node->getExpression());

    node->setModifiable(outputs_modifiable ?
                        AbstractSyntaxTree::Expression::modifiable_type::TRUE:
                        AbstractSyntaxTree::Expression::modifiable_type::FALSE);
    return node->getModifiable();
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::IdentifierExpression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();

    auto* type = generateTypeInfo(type_container, scope_info, node);
    if(type == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;

    // if it is a function type, it is not modifiable (function identifier)
    if (type->getType() == SynthesizedType::type_category::function_type)
        node->setModifiable(AbstractSyntaxTree::Expression::modifiable_type::FALSE);
    else
        node->setModifiable(AbstractSyntaxTree::Expression::modifiable_type::TRUE);

    return node->getModifiable();
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::ExpressionList* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();

    node->setModifiable(AbstractSyntaxTree::Expression::modifiable_type::FALSE);
    return node->getModifiable();
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::CallExpression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();
    
    auto* func_type = dynamic_cast<FunctionType*>(generateTypeInfo(type_container, scope_info, node->getFunctionExpression()));

    if(func_type == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;

    if(func_type->getOutputType() == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    node->setModifiable(func_type->getOutputType()->modifiableByDefault() ? AbstractSyntaxTree::Expression::modifiable_type::TRUE : AbstractSyntaxTree::Expression::modifiable_type::FALSE);

    return node->getModifiable();
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::LiteralExpression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();
    
    node->setModifiable(AbstractSyntaxTree::Expression::modifiable_type::FALSE);
    return node->getModifiable();
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::VarArgsExpression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();

    return AbstractSyntaxTree::Expression::modifiable_type::FALSE;
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::TypeInstatiation* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();

    return AbstractSyntaxTree::Expression::modifiable_type::TRUE;
}

AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::NewFunctionExpression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();

    return AbstractSyntaxTree::Expression::modifiable_type::FALSE;
}
AbstractSyntaxTree::Expression::modifiable_type TypeChecker::checkModifiable(TypeContainer& type_container, ScopeInformation& scope_info, AbstractSyntaxTree::CreateFunctionExpression* node)
{
    if (node == nullptr) return AbstractSyntaxTree::Expression::modifiable_type::NONE;
    if (node->getModifiable() != AbstractSyntaxTree::Expression::modifiable_type::NONE) return node->getModifiable();

    return AbstractSyntaxTree::Expression::modifiable_type::FALSE;
}

SynthesizedType* TypeChecker::getTypeByOperator(TypeContainer& container, SynthesizedType* inner, bool postfix, const array<TypeSystemInfo::type_synthesis_action, 3>& operator_synthesis)
{
    if(inner == nullptr) return nullptr;
    
    switch (operator_synthesis.at(postfix ? 2 : 0))
    {
    case TypeSystemInfo::type_synthesis_action::boolean:
        return TypeSystemInfo::getGlobalContainer()->getType("bool");
    case TypeSystemInfo::type_synthesis_action::left:
    case TypeSystemInfo::type_synthesis_action::right:
        return inner;
    case TypeSystemInfo::type_synthesis_action::dereference:
        if (inner->getType() == SynthesizedType::type_category::pointer_type)
            return (static_cast<PointerType*>(inner))->getBaseType();
        else return nullptr;
    case TypeSystemInfo::type_synthesis_action::pointer_of:
        return TypeSystemInfo::getGlobalContainer()->getOrCreatePointerType(inner);
    case TypeSystemInfo::type_synthesis_action::safe_dref_access:
         if (inner->getType() == SynthesizedType::type_category::strong_pointer_type)
            return (static_cast<StrongPointerType*>(inner))->getBaseType();
        else if (inner->getType() == SynthesizedType::type_category::weak_pointer_type)
            return (static_cast<WeakPointerType*>(inner))->getBaseType();
        else return nullptr;
    case TypeSystemInfo::type_synthesis_action::safe_wrap:
        if (inner->getType() == SynthesizedType::type_category::pointer_type)
            return TypeSystemInfo::getGlobalContainer()->getOrCreateStrongPointerType((static_cast<PointerType*>(inner))->getBaseType());
        else return nullptr;
    default:
        return nullptr;
    }
}

SynthesizedType* TypeChecker::typeCheckComplexIndexAccess(SynthesizedType* complex_type, unsigned int index, const position_data& position)
{
    switch (complex_type->getType())
    {
        case SynthesizedType::type_category::unnamed_complex_type:
        {    
            const auto& components = static_cast<UnnamedComplexType*>(complex_type)->getComponents();

            if(index >= components.size())
            {
                ErrorRegister::reportError(new SemanticError(
                    "Accessing unnamed complex type out of bounds",
                    position
                ));

                return nullptr;
            }

            return components[index];
        }
        case SynthesizedType::type_category::named_complex_type:
        {
            const auto& components = static_cast<NamedComplexType*>(complex_type)->getComponents();

            if(index >= components.size())
            {
                ErrorRegister::reportError(new SemanticError(
                    "Accessing named complex type out of bounds",
                    position
                ));

                return nullptr;
            }

            return components[index].first;
        }
        default:
            ErrorRegister::reportError(new SemanticError(
                "Can't access member of type " + complex_type->getName() +
                " using operator '.'",
                position
            ));

            return nullptr;
    }

    return nullptr;
}