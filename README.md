# The Density Programming Language

### Description

The density programming language is a low level general purpose c-like programming language.

Density contains many features that are usually found in higher level languages, for example, complex variables which are equivalent to tuples, ARC grbage collection and more, while minimizing the language's runtime and therefore keeping execution time quick.

The compiler uses the LLVM library and therefore it supports compilation to various platforms and architectures.
This repository contains the source code for building the density compiler using cmake.
For more information about the language, you can see our ideas: [The link to development ideas!](https://drive.google.com/drive/folders/1PZQgC_95riIJtXUtuoJH8vTSvuT8W4Tj?usp=sharing)

### Building Density From Source Using Cmake

In order to build density from source, run:

```
cmake -G <Generator> -DCMAKE_BUILD_TYPE=<BuildType> .
```

and then:

```
cmake --build .
```

or

```
make
```

If LLVM is not found and an error is thrown, add the flag `-DLLVM_DIR=<PathToLLVM>`.

These commands will create an executable named `density` in the current working directory, which can be run to compile your code.

### Using Density To Compile Your Code

To compile a density file, use:

```
density <PathToFile> [flags]
```

This command will create a compiled executable in the current working directory.

To view the complete list of flags, use:

```
density --help
```

* Note - only files with the extensions `.dens`, `.dny`, `denh` and `dnh` are valid density files.

## Specification For Main Features

### Complex Types And Variables

Density supports creation of types which contain multiple inner types. For Example:

```
@(i32, i32) point = (1, 1);

// Or

@(i32 x, i32 y) point = (1, 1);
```

To simplify the code and improve readability, it is possible to use the `var` keyword instead of explicitly stating the type of the variable:

```
var point = (1,1);
```

Using the var keyword automatically defines the complex type to be unnamed.

Accessing elements of complex variables is done using the `.` operator:

```
point.0 = 4;
point.1 = 5;

// Or

point.x = 4;
point.y = 5;
```

### Conversions and casts

Density currently supports 2 types of conversions: `to` and `bitcast`.

`bitcast` does not change the value that is being casted and therefore it is used to cast pointers only. It converts the data to the specified destination type without changing the actual bits. For example, the expression `&a bitcast i32*`, would result to the same value, but the pointer can be used like an i32 pointer.

`to` changes the value that is being casted so that the value whould be mapped to the value that is associated with it. For example, the expression `1.0 to i32` would result to the integer value of 1. 

### Imports

Density allows importing density files and interfacing with them using the `import` statement.
Every density file is considered as a module that can be imported and used in other modules.

Importing files will be semantically identical to writing the content of the imported file directly into the importing file.

```
import "file.dens";
```

### Linking external functions

The `extern` keywords is used in order to link external functions when compiling executables.
For example:

```
extern i32 printf(char* format) vargs;
```

In order for the linkage to be successful, the implementation of the linked function must be provided at compile time.
By default, cstdlib functions are provided at compile time and can be linked without modifiying linkage procedure.
There are two ways to link external functions that are not provided by default:

1. Use the `-ir` parameter to emit llvm ir as a single module and then link it with external code using the llvm linker.

2. Use the `-obj` parameter to emit the code as an object file, which can be linked using any external linker.

### Variadic Functions

Variadic functions are functions that accept a variable number of arguments.
An example of a vararg function is:

```
@(f32, i32) func(i32 ty) vargs
{
	if (ty == 0)
		ret (0, vargs(i32));
	else
		ret (0, vargs(f32));
}
```

### Types

Density allows the creation of types (Also known as class).
Types can hold variables and functions which can be accessed through an instance of the type, or statically.
The syntax for creating function is identical to types, with an addition of the first argument being the pointer to the instance itself.
If the first argument isn't the pointer type to itself, then the function can be only accessed statically.
The keyword `Self` may be used as a replacement to the explicit name of the type. For example:

```
type Example
{
	i32 variable;
	i32 simple_func(Self* self, i64 param)
	{
		ret param + 1;
	}

	i32 static_func(i64 param)
	{
		ret param + 1;
	}
}
```

The function `simple_func` can be called using an instance, and `static_func` can only be called statically. For Example:

```
Example example;

example.simple_func(1);
Example::static_func(1);

example.variable = 0;
```

Types have default constractors and destructors.
In order to write your own constructor use the keyword `init`, and in order to write own destructor use the keyword `deinit`.
For Example:

```
type Example
{
	i32 variable;
	
	init(Self* self, i32 param)
	{
		self->variable = param;
	}
	
	deinit(Self* self)
	{
		self->variable = 0;
	}
}
```

The creation of types using non-default constructors can be done in either way:

```
Example example1:(1);

Example example2 = Example:(1);
```

### Memory Management: Manual

Density provideds manual dynamic memory managemnet.
Operators `new` and `delete` may be used to create and release dynamic memory on the heap.
Operator `new` is used to allocate dynamic memory on the heap and if the allocated type isn't primitive then the operator will call the constructor of the allocated type.
If the allocated type is a primitive type then by putting into the braces a value, it will be stored into the allocated pointer.
Operator `delete` is used to deallocate dynamic memory which was acquired by operator `new` and if the allocated type isn't primitive then the operator will call the destructor of the allocate type before releasing it.
For the next example we'll use the next type:

```
type Example
{
	i32 variable;
	init(Self* self, i32 p1, i32 p2)
	{
		self->variable = p1 + p2;
	}
	
	deinit(Self* self)
	{
		self->variable = 0;
	}
}
```

The example for using operators `new` and `delete`:

```
i32* a = new i32:(0);
Example* b = new Example:(1, 2);

delete a;
delete b;
```

### Memory Management: Automatic

Density provides a compile time garbage collection utility using ARC (Automatic Reference Counting), interfacing with programmers via safe pointers.

There are two types of safe pointers that can be used to ensure memory safety:

1. Strong pointers: 
Memory managed by ARCC will be automatically cleaned when no strong pointers point to it.
Strong pointers can be declared in the following syntax:

```
strong example* ptr;
```

* Note - The default value for all safe pointers is `null`.

2. Weak pointers: 
Memory pointed by weak pointers might be cleaned before the weak pointer goes out of scope, if no strong pointer is pointing at the same object.
Weak pointers exist to prevent cyclical data structures from posing a memory leak threat, for example:

```
type parent_type {
    strong child_type* t2;
}

type child_type {
    weak parent_type* t1;
}
```


In this example, the child instance will be deleted as soon as the parent instance is deleted, because no strong pointers are pointing to it when its external reference goes out of scope, and therefore memory will be cleaned automatically.

To create a new safe pointer, the `create` operator can be used:

```
strong i32* ptr1 = create i32:(5);
strong ty* ptr2 = create ty:();
```

The expression `create ty:()` allocates memory for the instance and calls the constructor using the paramenters given inside the braces.
* Note - safe pointers to primitive types can be initialized by giving the `create` operator a value.

In order to access the inner value of a safe pointer, the `$` operator can be used:

```
strong i32* ptr = create i32:(5);
i32 value = $ptr;
```

Density allows converting pointers that have been obtained externally into safe pointers using the `#` operator.
For example:

```
i8* ptr = #(new i8:(100));
```

* Note - The created pointer will be converted to a strong pointer.

